package com.truemoneywitami.controller;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Include;

import com.truemoneywitami.service.api.DaftarMemberService;
import com.truemoneywitami.service.impl.DaftarMemberServiceImpl;

public class DaftarMemberController extends SelectorComposer<Div> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static Logger log = Logger.getLogger(DaftarMemberController.class);
	
	DaftarMemberService daftarMemberService = new DaftarMemberServiceImpl();
	
	@Wire
	Include incl;
	
	@Listen("onClick=#btnYaHome")
	public void ya() {
		try {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("incl", incl);
			
			daftarMemberService.gotoNomorKartu(map);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("ctrl button ya home daftar member", e);
		}
	}
	
	@Listen("onOK=#listsearchkel")
	public void loadKelurahan() {
		try {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("incl", incl);
			
			daftarMemberService.loadKelurahan(map);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("ctrl loadKelurahan daftar member", e);
		}
	}
	
	@Listen("onClick=#btnLanjutForm")
	public void validasiForm() {
		try {
			Clients.alert("TEST LANJUT");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("ctrl validasiForm daftar member", e);
		}
	}
	
	@Listen("onClick=#btnBatalForm")
	public void backToNomorKartu() {
		try {
			Clients.alert("TEST BACK");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("ctrl validasiForm daftar member", e);
		}
	}
	
//	@Listen("onClick=#btnBackNomorKartu")
//	public void backToHome() {
//		try {
//			HashMap<String, Object> map = new HashMap<String, Object>();
//			map.put("incl", incl);
//			
//			daftarMemberService.gotoHome(map);
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.error("ctrl button backToHome daftar member", e);
//		}
//	}
	
//	@Listen("onClick=#btnSubmitNomorKartu")
//	public void submitNomorKartu() {
//		try {
//			HashMap<String, Object> map = new HashMap<String, Object>();
//			map.put("incl", incl);
//			
//			daftarMemberService.validasiKartu(map);
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.error("ctrl button submitNomorKartu daftar member", e);
//		}
//	}

}
