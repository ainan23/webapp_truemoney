package com.truemoneywitami;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.imageio.ImageIO;
import javax.sql.DataSource;

import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RequestListener;

import datasource.DataSourceConnectionPostgres;

import org.apache.commons.codec.binary.Base64;

public class Test extends Window  implements RequestListener {
	
	String test;
	@Override
	public void requestSucceeded(JSONObject obj) {
		test = obj.get("bit").toString();
		
	}

	@Override
	public void requestFailed(String message) {
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message);
		}
	}
	
	public void setData(Window wnd)  throws Exception {
		
		test();
		
	}
	
	public void test () throws Exception{
		JSONObject obj = new JSONObject();
		
		obj.put("Tipe", "testdownload");
		obj.put("noHp", "085711111116");
		
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		String image ="";
		String type = "";
		String splitan [] = test.split("kaizen");
		image = splitan[0];
		type = splitan[1];
        byte [] coy = Base64.decodeBase64(image);
	      
	      InputStream in = new ByteArrayInputStream(coy);
	      BufferedImage bfimg = ImageIO.read(in);
	      
//	      String type = "png";
	      
	      ImageIO.write(bfimg, type, new File("D:\\TestTitoRajLGBT."+type));
	}
	
	public static void main (String [] args) {
		try {
			new Test().insertAutoCommitFalse();
			new Test().insertAutoCommitTrue();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
		}
	}
	
	public void insertAutoCommitFalse() {
		Connection con = null;
		try {
			DataSourceConnectionPostgres.getInstance();
			DataSource ds = DataSourceConnectionPostgres.getDataSource();
			con = ds.getConnection();
			
			con.setAutoCommit(false);
			
			String sql = "insert into \"NoUrut\" values ('test_adet', 123)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.executeUpdate();
			
			con.commit();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!con.isClosed()) con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void insertAutoCommitTrue() {
		Connection con = null;
		try {
			DataSourceConnectionPostgres.getInstance();
			DataSource ds = DataSourceConnectionPostgres.getDataSource();
			con = ds.getConnection();
			
			String sql = "insert into \"NoUrut\" values ('test_adet_xxx', 123)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!con.isClosed()) con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
