package com.truemoneywitami.dto;

import org.zkoss.json.JSONArray;

public class ResponseApiDto {
	
	private String httpcode;
	private String data;
	private String ACK;
	private String pesan;
	private JSONArray listKelurahan;
	private JSONArray listKecamatan;
	private JSONArray listKotaProvinsi;
	private String namaKelurahan;
	public String getHttpcode() {
		return httpcode;
	}
	public void setHttpcode(String httpcode) {
		this.httpcode = httpcode;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getACK() {
		return ACK;
	}
	public void setACK(String aCK) {
		ACK = aCK;
	}
	public String getPesan() {
		return pesan;
	}
	public void setPesan(String pesan) {
		this.pesan = pesan;
	}
	public JSONArray getListKelurahan() {
		return listKelurahan;
	}
	public void setListKelurahan(JSONArray listKelurahan) {
		this.listKelurahan = listKelurahan;
	}
	public String getNamaKelurahan() {
		return namaKelurahan;
	}
	public void setNamaKelurahan(String namaKelurahan) {
		this.namaKelurahan = namaKelurahan;
	}
	public JSONArray getListKecamatan() {
		return listKecamatan;
	}
	public void setListKecamatan(JSONArray listKecamatan) {
		this.listKecamatan = listKecamatan;
	}
	public JSONArray getListKotaProvinsi() {
		return listKotaProvinsi;
	}
	public void setListKotaProvinsi(JSONArray listKotaProvinsi) {
		this.listKotaProvinsi = listKotaProvinsi;
	}

}
