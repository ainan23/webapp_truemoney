package com.truemoneywitami.dto;

public class ParamaterDto {

	private  String tipePesan = "";
	private  String pesan = "";
	private  String header="";
	
	private  String namaAccountTujuan = "namaAccountTujuan";
	private  String noRekeningTujuan = "noRekeningTujuan";
	private  String nominal = "nominal";
	private  String issue = "issue";
	private  String biaya = "biaya";
	private  String sender_name = "sender_name";
	private  String sender_cell = "sender_cell";
	private  String sender_add = "sender_add";
	private  String ben_cell = "ben_cell";
	private  String bank = "bank";
	private  String ben_add = "ben_add";
	private  String msgArr = "messageArray";
	private  String id_pelanggan = "";
	private  String namaPelanggan="";
	private  String tagihan ="";
	private  String total="";
	private  String id_account="";
	private  String nama ="";
	private  String email="";
	private  String tipeUser="";
	private  String handPhone="";
	private  String id_trx="";
	public String getTipePesan() {
		return tipePesan;
	}
	public void setTipePesan(String tipePesan) {
		this.tipePesan = tipePesan;
	}
	public String getPesan() {
		return pesan;
	}
	public void setPesan(String pesan) {
		this.pesan = pesan;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public String getNamaAccountTujuan() {
		return namaAccountTujuan;
	}
	public void setNamaAccountTujuan(String namaAccountTujuan) {
		this.namaAccountTujuan = namaAccountTujuan;
	}
	public String getNoRekeningTujuan() {
		return noRekeningTujuan;
	}
	public void setNoRekeningTujuan(String noRekeningTujuan) {
		this.noRekeningTujuan = noRekeningTujuan;
	}
	public String getNominal() {
		return nominal;
	}
	public void setNominal(String nominal) {
		this.nominal = nominal;
	}
	public String getIssue() {
		return issue;
	}
	public void setIssue(String issue) {
		this.issue = issue;
	}
	public String getBiaya() {
		return biaya;
	}
	public void setBiaya(String biaya) {
		this.biaya = biaya;
	}
	public String getSender_name() {
		return sender_name;
	}
	public void setSender_name(String sender_name) {
		this.sender_name = sender_name;
	}
	public String getSender_cell() {
		return sender_cell;
	}
	public void setSender_cell(String sender_cell) {
		this.sender_cell = sender_cell;
	}
	public String getSender_add() {
		return sender_add;
	}
	public void setSender_add(String sender_add) {
		this.sender_add = sender_add;
	}
	public String getBen_cell() {
		return ben_cell;
	}
	public void setBen_cell(String ben_cell) {
		this.ben_cell = ben_cell;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getBen_add() {
		return ben_add;
	}
	public void setBen_add(String ben_add) {
		this.ben_add = ben_add;
	}
	public String getMsgArr() {
		return msgArr;
	}
	public void setMsgArr(String msgArr) {
		this.msgArr = msgArr;
	}
	public String getId_pelanggan() {
		return id_pelanggan;
	}
	public void setId_pelanggan(String id_pelanggan) {
		this.id_pelanggan = id_pelanggan;
	}
	public String getNamaPelanggan() {
		return namaPelanggan;
	}
	public void setNamaPelanggan(String namaPelanggan) {
		this.namaPelanggan = namaPelanggan;
	}
	public String getTagihan() {
		return tagihan;
	}
	public void setTagihan(String tagihan) {
		this.tagihan = tagihan;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getId_account() {
		return id_account;
	}
	public void setId_account(String id_account) {
		this.id_account = id_account;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTipeUser() {
		return tipeUser;
	}
	public void setTipeUser(String tipeUser) {
		this.tipeUser = tipeUser;
	}
	public String getHandPhone() {
		return handPhone;
	}
	public void setHandPhone(String handPhone) {
		this.handPhone = handPhone;
	}
	public String getId_trx() {
		return id_trx;
	}
	public void setId_trx(String id_trx) {
		this.id_trx = id_trx;
	}
	
	
	
}
