package com.truemoneywitami.dto;

public class KomisiDto {

	String tanggal;
	String id_trx;
	String tipe;
	String nominal;
	String status;
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public String getId_trx() {
		return id_trx;
	}
	public void setId_trx(String id_trx) {
		this.id_trx = id_trx;
	}
	public String getTipe() {
		return tipe;
	}
	public void setTipe(String tipe) {
		this.tipe = tipe;
	}
	public String getNominal() {
		return nominal;
	}
	public void setNominal(String nominal) {
		this.nominal = nominal;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
