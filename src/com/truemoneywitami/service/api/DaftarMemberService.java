package com.truemoneywitami.service.api;

import java.util.HashMap;

import org.zkoss.json.JSONArray;
import org.zkoss.zul.Listbox;

public interface DaftarMemberService {
	
	public void gotoNomorKartu(HashMap<String, Object> map);
	public void gotoHome(HashMap<String, Object> map);
	public void validasiKartu(HashMap<String, Object> map);
	public void loadKelurahan(HashMap<String, Object> map);
	public void loadKecamatan(HashMap<String, Object> map);
	public void loadKotaProvinsi(HashMap<String, Object> map);
	public void setListboxValueKel(Listbox listbox, String idList, JSONArray data, int noUrut, boolean isSelect,
			boolean isClear, boolean firstRowIsID);
	public void setListboxValueKec(Listbox listbox, String idList, JSONArray data, int noUrut, boolean isSelect,
			boolean isClear, boolean firstRowIsID);

}
