package com.truemoneywitami.service.impl;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.json.parser.JSONParser;
import org.zkoss.zhtml.Button;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Include;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Textbox;

import com.google.gson.Gson;
import com.truemoneywitami.dto.ResponseApiDto;
import com.truemoneywitami.function.ApiConnection;
import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.service.api.DaftarMemberService;

public class DaftarMemberServiceImpl implements DaftarMemberService {
	
	private static Logger log = Logger.getLogger(DaftarMemberServiceImpl.class);
	
	CheckParameter chk = new CheckParameter();
	ZKFunction zk = new ZKFunction();
	Gson gson = new Gson();

	@Override
	public void gotoNomorKartu(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			Include incl = (Include) map.get("incl");
			incl.setSrc("com.truemoney.view/daftar/member/nomor_kartu.zul");
			
			Button btnSubmit = (Button) incl.getFellow("btnSubmitNomorKartu");
			btnSubmit.addEventListener("onClick", new EventListener<Event>() {
				@Override
				public void onEvent(Event event) throws Exception {
					validasiKartu(map);
				}
			});
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("service go to nomor kartu daftar member", e);
		}
	}

	@Override
	public void gotoHome(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			Include incl = (Include) map.get("incl");
			incl.setSrc("com.truemoney.view/daftar/member/home.zul");
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("service gotoHome daftar member", e);
		}
	}

	@Override
	public void validasiKartu(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			Include incl = (Include) map.get("incl");
			Textbox txtNomorKartu = (Textbox) incl.getFellow("txtNomorKartu");
			
			String nomorKartu = zk.getTextboxValue(txtNomorKartu);
			if (chk.isNil(nomorKartu)) {
				Clients.showNotification("Isi Nomor Kartu Anda.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else if (!chk.isNil(nomorKartu) && !chk.isNumber(nomorKartu)) {
				Clients.showNotification("Nomor Kartu harus angka.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else if (!chk.isNil(nomorKartu) && !chk.checkLength(nomorKartu, 12)) {
				Clients.showNotification("Nomor Kartu tidak valid.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else {
				String req = "{\"Tipe\":\"CEK_KARTU\", \"nomor_kartu\":\""+nomorKartu+"\"}";
				System.out.println(req);
				String response = ApiConnection.curl(req);
				System.out.println("res -> " + response);
				ResponseApiDto dtoRes = gson.fromJson(response, ResponseApiDto.class);
				if (dtoRes.getHttpcode().equals("200")) {
					System.out.println("masuk sini");
					ResponseApiDto dto = gson.fromJson(dtoRes.getData(), ResponseApiDto.class);
					
					if (dtoRes.getData().equals("")) {
						Clients.showNotification("Tidak ada response dari server.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
					} else {
						if (dto.getACK().equals("OK")) {
							incl.setSrc("com.truemoney.view/daftar/member/form.zul");
						} else {
							Clients.showNotification(dto.getPesan(), Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
						}
					}
					
					
				} else {
					dtoRes = gson.fromJson(dtoRes.getData(), ResponseApiDto.class);
					Clients.showNotification(dtoRes.getPesan(), Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("service validasiKartu daftar member", e);
		}
	}

	@Override
	public void loadKelurahan(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			
			Include incl = (Include) map.get("incl");
			Textbox txtSearch = (Textbox) incl.getFellow("listsearchkel");
			Bandbox bandkec = (Bandbox) incl.getFellow("bandkec");
			
			Listbox listbox = (Listbox) incl.getFellow("kel");
			listbox.addEventListener("onClick", new EventListener<Event>() {
				@Override
				public void onEvent(Event event) throws Exception {
					bandkec.setFocus(true);
					loadKecamatan(map);
				}
			});
			
			String search = zk.getTextboxValue(txtSearch);
			
			if (chk.isNil(search)) {
				Clients.showNotification("Isi Nama Kelurahan.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else if (search.trim().length() < 4){
//				Clients.showNotification("Minimal 4 Karakter");
			}
			else {
//				obj.put("Tipe", "inquiryKelurahan");
//				obj.put("namaKelurahan", namaKelurahan);
//				ProcessingListener processing = new ProcessingListener(this, obj.toString());
//				processing.processData();
//				setListboxValueKel(wnd, "kel", arrkel, 0, true, true, true);
				
//				countryObj.put("listKelurahan", kelurahanArray);
//				countryObj.put("ACK", "OK");
//				countryObj.put("ID", "kel");
				
				String req = "{\"Tipe\":\"inquiryKelurahan\", \"namaKelurahan\":\""+search+"\"}";
				String response = ApiConnection.curl(req);
				
				System.out.println("response -> " + response);
				
				ResponseApiDto dtoRes = gson.fromJson(response, ResponseApiDto.class);
				String responseKel = dtoRes.getData();
				
				System.out.println("data -> " + dtoRes.getData());
				if (dtoRes.getHttpcode().equals("200")) {
					System.out.println("masuk sini");
					ResponseApiDto dto = gson.fromJson(dtoRes.getData(), ResponseApiDto.class);
					
					if (dtoRes.getData().equals("")) {
						Clients.showNotification("Tidak ada response dari server.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
					} else {
						if (dto.getACK().equals("OK")) {
							
							JSONParser parser = new JSONParser();
							Object obj = parser.parse(responseKel);
							JSONObject responseObj = (JSONObject) obj;
							
							JSONArray arrkel = (JSONArray) responseObj.get("listKelurahan");

							setListboxValueKel(listbox, "kel", arrkel, 0, true, true, true);
						} else {
							Clients.showNotification(dto.getPesan(), Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
						}
					}
					
					
					
				} else {
					dtoRes = gson.fromJson(dtoRes.getData(), ResponseApiDto.class);
					Clients.showNotification(dtoRes.getPesan(), Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("service loadKelurahan daftar member", e);
		}
	}

	@Override
	public void setListboxValueKel(Listbox list, String idList,
			JSONArray data, int noUrut, boolean isSelect, boolean isClear,
			boolean firstRowIsID) {
		// TODO Auto-generated method stub
		try {
			list.getItems().clear();
			int iRow = data.size();
			for (int j = 0; j < iRow; j++) {
				Listitem li = new Listitem();
				JSONObject child = (JSONObject) data.get(j);
				li.setValue(child.get("namaKelurahan").toString());
				li.appendChild(new Listcell(child.get("namaKelurahan").toString()));
				list.appendChild(li);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("service setListboxValueKel daftar member", e);
		}
	}

	@Override
	public void loadKecamatan(HashMap<String, Object> map) {
		// TODO Auto-generated method stub
		try {
			
			Include incl = (Include) map.get("incl");
			Bandbox bandkec = (Bandbox) incl.getFellow("bandkec");
			Bandbox bandkel = (Bandbox) incl.getFellow("bandkel");
			Textbox kota = (Textbox) incl.getFellow("kota");
			Textbox prov = (Textbox) incl.getFellow("prov");
			Textbox pos = (Textbox) incl.getFellow("pos");
			Listbox list = (Listbox) incl.getFellow("kel");
			Listbox listKec = (Listbox) incl.getFellow("kec");
			listKec.addEventListener("onClick", new EventListener<Event>() {
				@Override
				public void onEvent(Event event) throws Exception {
					pos.setFocus(true);
					loadKotaProvinsi(map);
				}
			});
			
			bandkec.setDisabled(false);
			bandkec.setReadonly(true);
			bandkec.setText("");
			
			zk.setTextboxResetValue(kota);
			zk.setTextboxResetValue(prov);
			
			bandkel.setValue(zk.getListboxValue(list));
			
			
//				obj.put("Tipe", "inquiryKelurahan");
//				obj.put("namaKelurahan", namaKelurahan);
//				ProcessingListener processing = new ProcessingListener(this, obj.toString());
//				processing.processData();
//				setListboxValueKel(wnd, "kel", arrkel, 0, true, true, true);
				
//				countryObj.put("listKelurahan", kelurahanArray);
//				countryObj.put("ACK", "OK");
//				countryObj.put("ID", "kel");
				
				String req = "{\"Tipe\":\"inquiryKecamatan\", \"namaKelurahan\":\""+zk.getListboxValue(list)+"\"}";
				String response = ApiConnection.curl(req);
				
				System.out.println("response -> " + response);
				
				ResponseApiDto dtoRes = gson.fromJson(response, ResponseApiDto.class);
				String responseKel = dtoRes.getData();
				
				System.out.println("data -> " + dtoRes.getData());
				if (dtoRes.getHttpcode().equals("200")) {
					System.out.println("masuk sini");
					ResponseApiDto dto = gson.fromJson(dtoRes.getData(), ResponseApiDto.class);
					
					if (dtoRes.getData().equals("")) {
						Clients.showNotification("Tidak ada response dari server.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
					} else {
						if (dto.getACK().equals("OK")) {
							
							JSONParser parser = new JSONParser();
							Object obj = parser.parse(responseKel);
							JSONObject responseObj = (JSONObject) obj;
							
							JSONArray arrkec = (JSONArray) responseObj.get("listKecamatan");

							setListboxValueKec(listKec, "kec", arrkec, 0, true, true, true);
						} else {
							Clients.showNotification(dto.getPesan(), Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
						}
					}
					
					
					
				} else {
					dtoRes = gson.fromJson(dtoRes.getData(), ResponseApiDto.class);
					Clients.showNotification(dtoRes.getPesan(), Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("service loadKecamatan daftar member", e);
		}
	}

	@Override
	public void setListboxValueKec(Listbox listbox, String idList,
			JSONArray data, int noUrut, boolean isSelect, boolean isClear,
			boolean firstRowIsID) {
		// TODO Auto-generated method stub
		try {
			listbox.getItems().clear();
			int iRow = data.size();
			for (int j = 0; j < iRow; j++) {
				Listitem li = new Listitem();
				JSONObject child = (JSONObject) data.get(j);
				li.setValue(child.get("namaKecamatan").toString());
				li.appendChild(new Listcell(child.get("namaKecamatan").toString()));
				listbox.appendChild(li);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("service setListboxValueKec daftar member", e);
		}
	}

	@Override
	public void loadKotaProvinsi(HashMap<String, Object> map) {

		// TODO Auto-generated method stub
		try {
			
			Include incl = (Include) map.get("incl");
			
			Listbox listKec = (Listbox) incl.getFellow("kec");
			String kecamatan = zk.getListboxValue(listKec);
			
			Listbox listKel = (Listbox) incl.getFellow("kel");
			String kelurahan = zk.getListboxValue(listKel);
			
			Textbox kota = (Textbox) incl.getFellow("kota");
			Textbox prov = (Textbox) incl.getFellow("prov");
			
			Bandbox bandkec = (Bandbox) incl.getFellow("bandkec");
			bandkec.setValue(kecamatan);
				
				String req = "{\"Tipe\":\"inquiryKotaProvinsi\", \"namaKelurahan\":\""+kelurahan+"\", \"namaKecamatan\":\""+kecamatan+"\"}";
				String response = ApiConnection.curl(req);
				
				System.out.println("response -> " + response);
				
				ResponseApiDto dtoRes = gson.fromJson(response, ResponseApiDto.class);
				String responseKel = dtoRes.getData();
				
				System.out.println("data -> " + dtoRes.getData());
				if (dtoRes.getHttpcode().equals("200")) {
					System.out.println("masuk sini");
					ResponseApiDto dto = gson.fromJson(dtoRes.getData(), ResponseApiDto.class);
					
					if (dtoRes.getData().equals("")) {
						Clients.showNotification("Tidak ada response dari server.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
					} else {
						if (dto.getACK().equals("OK")) {
							
							JSONParser parser = new JSONParser();
							Object obj = parser.parse(responseKel);
							JSONObject responseObj = (JSONObject) obj;
							
							JSONArray arrkot = (JSONArray) responseObj.get("listKotaProvinsi");
							
							JSONObject child = (JSONObject) arrkot.get(0);
							
							String strkota = child.get("namaKota").toString();
							String strprov = child.get("namaProvinsi").toString();
							
							zk.setTextboxValue(kota, strkota);
							zk.setTextboxValue(prov, strprov);
						} else {
							Clients.showNotification(dto.getPesan(), Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
						}
					}
					
					
					
				} else {
					dtoRes = gson.fromJson(dtoRes.getData(), ResponseApiDto.class);
					Clients.showNotification(dtoRes.getPesan(), Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("service loadKotaProvinsi daftar member", e);
		}
	
	}

}
