/**
 * 
 */
package com.truemoneywitami.session.listener;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

/**
 * @author bolanger
 *
 */
public class SecurityFilter implements Filter {
	private static Logger log = Logger.getLogger(SecurityFilter.class);
	

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
//		log.info("EXECUTE SECURITY FILTER");
		// process request
		HttpServletRequest reqServ = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		
		String ctxPath = reqServ.getContextPath();
		HttpServletRequest req = (HttpServletRequest) request;
		
//		log.info("Context Path "+req.getContextPath());
		String reqUrl = req.getServletPath();

		log.info("request url "+reqUrl);

		if (reqUrl.equals("/login.zul")) {
			HttpSession session = req.getSession();
			
			if (session.getAttribute("username") != null) {
				resp.sendRedirect(ctxPath+"/index.zul");
			}
			
		}
		
		chain.doFilter(request,response);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig config) throws ServletException {}
	

	

}
