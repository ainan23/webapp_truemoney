package com.truemoneywitami.session.listener;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;

import com.truemoneywitami.function.Timeout;

public class SessionListener implements HttpSessionListener {
	
	private static Logger log = Logger.getLogger(SessionListener.class);
	


	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		// TODO Auto-generated method stub
		log.info("session created.");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		// TODO Auto-generated method stub
		log.info("session destroy");
		
		try {
			Timeout menu = new Timeout();
			
			HttpSession session = event.getSession();
			
			String username = "" + session.getAttribute("username");
			
			if(username.equals("null") || username.equals("")) {
				
			} else {
				log.info("invalidate this user : "+ username);
				
				menu.timeout(""+session.getAttribute("username"));
				
//				session.invalidate();
			}
			
		} catch (Exception e) {
			log.info("error -> " + e.getMessage());
			log.info(e);
		}
	}

}
