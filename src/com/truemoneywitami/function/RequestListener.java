package com.truemoneywitami.function;

import org.zkoss.json.JSONObject;

public interface RequestListener {
	void requestSucceeded(JSONObject data);
	void requestFailed(String message);
}
