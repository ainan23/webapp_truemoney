package com.truemoneywitami.function;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.log4j.Logger;


public class ImageFunction {
	
	private static Logger log = Logger.getLogger(ImageFunction.class);
	
//	public static void main(String [] args) {
//		try {
//			ImageFunction imageFunction = new ImageFunction();
//			imageFunction.compressImage("E:\\tes_compress.jpeg");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}	
//	}
	
	public void compressImage(String pathImage, InputStream input) {
		
		OutputStream os = null;
		ImageOutputStream ios = null;
		ImageWriter writer = null;
		
		try {
			
//			  File input = new File(pathImage);
		      BufferedImage image = ImageIO.read(input);

		      File compressedImageFile = new File(pathImage);
		      os = new FileOutputStream(compressedImageFile);

		      Iterator<ImageWriter>writers =  ImageIO.getImageWritersByFormatName("jpeg");
		      writer = (ImageWriter) writers.next();

		      ios = ImageIO.createImageOutputStream(os);
		      writer.setOutput(ios);

		      ImageWriteParam param = writer.getDefaultWriteParam();
		      
		      param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		      param.setCompressionQuality(0.5f);
		      writer.write(null, new IIOImage(image, null, null), param);
		      
		      log.info("compress image done");		      
		      
		} catch (Exception e) {	
			log.error("compress image failed..", e);
		} finally {
			try {
				os.close();
			} catch (Exception e) {
				log.error("", e);
			}
			
			try {
				ios.close();
			} catch (Exception e) {
				log.error("", e);
			}

			try {
				writer.dispose();
			} catch (Exception e) {
				log.error("", e);
			}
		}
	}

//	public static Bitmap resizeBitmap(String imagePath) {
//
//		long size_file = getFileSize(new File(imagePath));
//
//		size_file = (size_file) / 1000;// in Kb now
//		int ample_size = 1;
//
//		if (size_file <= 250) {
//
//			System.out.println("SSSSS1111= " + size_file);
//			ample_size = 1;
//
//		} else if (size_file > 251 && size_file < 1500) {
//
//			System.out.println("SSSSS2222= " + size_file);
//			ample_size = 2;
//
//		} else if (size_file >= 1500 && size_file < 3000) {
//
//			System.out.println("SSSSS3333= " + size_file);
//			ample_size = 4;
//
//		} else if (size_file >= 3000 && size_file <= 4500) {
//
//			System.out.println("SSSSS4444= " + size_file);
//			ample_size = 6;
//
//		} else if (size_file >= 4500) {
//
//			System.out.println("SSSSS4444= " + size_file);
//			ample_size = 8;
//		}
//
//		Bitmap bitmap = null;
//
//		BitmapFactory.Options bitoption = new BitmapFactory.Options();
//		bitoption.inSampleSize = ample_size;
//
//		Bitmap bitmapPhoto = BitmapFactory.decodeFile(imagePath, bitoption);
//
//		ExifInterface exif = null;
//		try {
//			exif = new ExifInterface(imagePath);
//		} catch (IOException e) {
//			// Auto-generated catch block
//			e.printStackTrace();
//		}
//		int orientation = exif
//				.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
//		Matrix matrix = new Matrix();
//
//		if ((orientation == 3)) {
//			matrix.postRotate(180);
//			bitmap = Bitmap.createBitmap(bitmapPhoto, 0, 0,
//					bitmapPhoto.getWidth(), bitmapPhoto.getHeight(), matrix,
//					true);
//
//		} else if (orientation == 6) {
//			matrix.postRotate(90);
//			bitmap = Bitmap.createBitmap(bitmapPhoto, 0, 0,
//					bitmapPhoto.getWidth(), bitmapPhoto.getHeight(), matrix,
//					true);
//
//		} else if (orientation == 8) {
//			matrix.postRotate(270);
//			bitmap = Bitmap.createBitmap(bitmapPhoto, 0, 0,
//					bitmapPhoto.getWidth(), bitmapPhoto.getHeight(), matrix,
//					true);
//
//		} else {
//			matrix.postRotate(0);
//			bitmap = Bitmap.createBitmap(bitmapPhoto, 0, 0,
//					bitmapPhoto.getWidth(), bitmapPhoto.getHeight(), matrix,
//					true);
//
//		}
//
//		return bitmap;
//	}

	public static long getFileSize(final File file) {
		if (file == null || !file.exists())
			return 0;
		if (!file.isDirectory())
			return file.length();
		final List<File> dirs = new LinkedList<File>();
		dirs.add(file);
		long result = 0;
		while (!dirs.isEmpty()) {
			final File dir = dirs.remove(0);
			if (!dir.exists())
				continue;
			final File[] listFiles = dir.listFiles();
			if (listFiles == null || listFiles.length == 0)
				continue;
			for (final File child : listFiles) {
				result += child.length();
				if (child.isDirectory())
					dirs.add(child);
			}
		}

		return result;
	}

}
