package com.truemoneywitami.function;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONObject;
import org.zkoss.json.parser.JSONParser;
import org.zkoss.zk.ui.util.Clients;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ResourceBundle;

public class ServerConnection {
	
//	private static String apiServerUrl = "http://localhost:8080/ApiWebAndroid/parsingJSON.jsp";
//	private static String apiServerUrl = "http://localhost:999/ApiWebAndroid/parsingJSON.jsp";
//	private static String apiServerUrl = "http://localhost:8081/web_api/parsingJSON.jsp";
//	private static String apiServerUrl = "http://localhost:8080/API_Website_Mobile/parsingJSON.jsp";
//	private static String apiServerUrl = "http://192.168.201.2:999/web_api/parsingJSON.jsp";

	
	private static Logger log = Logger.getLogger(ServerConnection.class);


    public static String curl_post(String data, boolean encode)throws Exception {
    	//System.out.println("REQ->" +data);
       ResourceBundle rb = ResourceBundle.getBundle("config.config");
       JSONObject responseObj = null;
        try {
            BufferedReader rd = null;
            String line = null;
            if (encode) {
                data = URLEncoder.encode(data.toString(), "UTF-8");
            }
            HttpURLConnection con = (HttpURLConnection) new URL(rb.getString("url")).openConnection();
            
//            HttpURLConnection con = (HttpURLConnection) new URL(apiServerUrl).openConnection();
            con.setConnectTimeout(Integer.parseInt(rb.getString("time_out")));
            con.setReadTimeout(Integer.parseInt(rb.getString("time_out")));
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

            con.setRequestProperty("Content-Length","" + Integer.toString(data.getBytes().length));
            con.setRequestProperty("Content-Language", "en-US");

            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);
            // Send request
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();

            String code = String.valueOf(con.getResponseCode());
            // Get Response
            InputStream is = con.getInputStream();
            rd = new BufferedReader(new InputStreamReader(is));

            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(response.toString());
            responseObj = (JSONObject) obj;

        } catch (SocketTimeoutException e) {
        	e.printStackTrace();
//        	responseObj.put("ACK", "NOK");
//        	responseObj.put("pesan", "Koneksi Gagal : Silahkan Cek status transaksi di Menu Mutasi Rekening.");
//        	return responseObj.toString();
        	String json = "{\"ACK\":\"NOK\", \"pesan\":\"TIMEOUT\"}";
        	JSONParser parser = new JSONParser();
            Object obj = parser.parse(json);
            responseObj = (JSONObject) obj;
            return (responseObj.toString());
            
        } catch (Exception e) {
//            e.printStackTrace();
        	log.info(e.getMessage() + " -> " + e);
        	e.printStackTrace();
//            Clients.showNotification(Constants.generalErrorMessage);
        }
        return (responseObj.toString());
    }
}
