package com.truemoneywitami.function;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.zkoss.json.JSONObject;
import org.zkoss.json.parser.JSONParser;
import org.zkoss.zul.Messagebox;

public class MOBFunction {
	
	private static Logger log = Logger.getLogger(MOBFunction.class);
	
	String SQL = null;
	DBConnection dbConnect = new DBConnection();

	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	public String getDate(String format) {
		// "yyyy-MM-dd H:i:s"
		Date now = new java.util.Date();
		SimpleDateFormat sdf;
		sdf = new java.text.SimpleDateFormat(format);
		String strDate = sdf.format(now);

		sdf = null;
		return strDate;
	}

	public String getNull(String lField) throws Exception {
		if (lField != null && lField != "") {
			return lField;
		} else {
			return " ";
		}
	}

	public String split(String string, String delimiter, int no) {
		String[] split = string.split(delimiter);
		return split[no - 1];
	}

	public String EncryptMd5(String input) {
		// input = "themobsterz..2011I10themobsterz201201121220050003";
		String result = "";
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(input.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			result = number.toString(16);
		} catch (NoSuchAlgorithmException e) {
			log.info(e.getMessage() + " -> " + e);
			throw new RuntimeException(e);
		}

		return result;

	}

	public String getJson(String[] dataObject, String[] ObjectValue) {
		JSONObject object = new JSONObject();

		for (int i = 0; i < dataObject.length; i++) {
			object.put(dataObject[i], ObjectValue[i]);
		}

		object.toJSONString();
		return object.toString();
	}

	public String JsonObject(String JsonFormat, String ObjectName) {
		JSONParser parser = new JSONParser();
		String Result = "";
		Object obj = parser.parse(JsonFormat);
		JSONObject obj2 = (JSONObject) obj;
		Result = (String) obj2.get(ObjectName);
		return Result;
	}

	public String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING
					.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}
	
	public String EncryptAESBase64(String plainText, String key)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException,
			BadPaddingException, UnsupportedEncodingException {
		SecretKeySpec keySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

		// Instantiate the cipher
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, keySpec);

		byte[] encryptedTextBytes = cipher.doFinal(plainText.getBytes("UTF-8"));

		return new Base64().encodeAsString(encryptedTextBytes);
	}

	public String DecryptAESBase64(String encryptedText, String key)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException,
			BadPaddingException, UnsupportedEncodingException {

		SecretKeySpec keySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

		// Instantiate the cipher
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, keySpec);

		byte[] encryptedTextBytes = new Base64().decodeBase64(encryptedText);
		byte[] decryptedTextBytes = cipher.doFinal(encryptedTextBytes);

		return new String(decryptedTextBytes);
	}

}
