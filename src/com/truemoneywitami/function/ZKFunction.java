package com.truemoneywitami.function;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.metainfo.WidgetAttribute;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Html;
import org.zkoss.zul.Image;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listgroup;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Longbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Row;
import org.zkoss.zul.Span;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

//import com.mob.session.mob_Session;

public class ZKFunction {
	
	private static Logger log = Logger.getLogger(ZKFunction.class);
	String SQL = null;
	DBConnection dbConnect = new DBConnection();
	MOBFunction fnBSK = new MOBFunction();
	
	private static ResourceBundle bundle = ResourceBundle.getBundle("config.config");

	public void setListboxValue(Window nmWindow, String lValue,
			String nmListbox, String lSQL, Connection Cn, String OneItemToAdd)
			throws Exception {
		// Connection Cn = dbConnect.getConnection();
		ResultSet rs = null;
		Statement st = null;

		Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
		list.getItems().clear();

		int lIndex = 0;

		try {
			st = Cn.createStatement();
			rs = st.executeQuery(lSQL);

			if (!OneItemToAdd.equals("")) {
				Listitem li1 = new Listitem();
				li1.setValue(OneItemToAdd);
				li1.appendChild(new Listcell(OneItemToAdd));
				li1.setSelected(true);
				list.appendChild(li1);
			}

			while (rs.next()) {
				Listitem li = new Listitem();

				// li.setId("item"+rs.getString(1));
				li.setValue(rs.getString(1));

				li.appendChild(new Listcell(rs.getString(2)));

				if (lValue.trim().equals("")) {
					if (OneItemToAdd.equals("")) {
						if (lIndex == 0) {
							li.setSelected(true);
						}
						lIndex++;
					}
				} else {
					//System.out.println("Data :"+rs.getString(2).trim()+" | value : "+lValue.trim());
					if (rs.getString(1).trim().equals(lValue.trim())) {
						li.setSelected(true);
					}
				}
				list.appendChild(li);
			}
		} catch (SQLException se) {
			Messagebox.show("Error: " + se.getMessage());
		} catch (Exception e) {
			Messagebox.show("Error Exception: " + e.getMessage());
		} finally {
			dbConnect.closeConnection(Cn, st, rs);
		}
	}
	

	public void setListboxResetValue(Window nmWindow,String nmListbox){
		Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
		list.getItems().clear();
	}
	
	
	public String moneyFormat3(String str) {
		  
		  String convertedString = str;

		  int length = 0;
		  for (int i = 0; i < str.length(); i++) {
		   if (str.charAt(i) == '.')
		    break;
		   length++;
		  }

		  if (length > 0) {
		   String temp = "";
		   int i = length;
		   while (i > 0) {
		    if (i > 2) {
		     if (temp.equalsIgnoreCase(""))
		      temp = str.substring(i - 3, i);
		     else
		      temp = str.substring(i - 3, i) + "." + temp;
		    } else {
		     if (temp.equalsIgnoreCase(""))
		      temp = str.substring(0, i);
		     else
		      temp = str.substring(0, i) + "." + temp;
		     i = 0;
		    }
		    i = i - 3;
		   }
		   String decimal = (str.length() == length ? "00" : str
		     .substring(length + 1));
		   decimal = (decimal.length() == 1 ? decimal + "0" : decimal);
		   convertedString = temp + "," + decimal;
		  }

		  return convertedString;
		 }
	
	public void setListboxValueNC(Window nmWindow, String lValue,
			String nmListbox, String lSQL, Connection Cn, String OneItemToAdd)
			throws Exception {
		// Connection Cn = dbConnect.getConnection();
		ResultSet rs = null;
		Statement st = null;

		Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
		list.getItems().clear();

		int lIndex = 0;

		try {
			st = Cn.createStatement();
			rs = st.executeQuery(lSQL);

			if (!OneItemToAdd.equals("")) {
				Listitem li1 = new Listitem();
				li1.setValue(OneItemToAdd);
				li1.appendChild(new Listcell(OneItemToAdd));
				li1.setSelected(true);
				list.appendChild(li1);
			}

			while (rs.next()) {
				Listitem li = new Listitem();

				// li.setId("item"+rs.getString(1));
				li.setValue(rs.getString(1));

				li.appendChild(new Listcell(rs.getString(2)));

				if (lValue.trim().equals("")) {
					if (OneItemToAdd.equals("")) {
						if (lIndex == 0) {
							li.setSelected(true);
						}
						lIndex++;
					}
				} else {
					//System.out.println("Data :"+rs.getString(2).trim()+" | value : "+lValue.trim());
					if (rs.getString(1).trim().equals(lValue.trim())) {
						li.setSelected(true);
					}
				}
				list.appendChild(li);
			}
			rs.close();
			st.close();
		} catch (SQLException se) {
			Messagebox.show("Error: " + se.getMessage());
		} catch (Exception e) {
			Messagebox.show("Error Exception: " + e.getMessage());
		} finally {
//			dbConnect.closeConnection(Cn, st, rs);
		}
	}
	
	public Date getSingleValueDate(String nmField, String SQL, Connection Cn)
			throws Exception {
		// String hasil = null;
		Date hasil = null;
		String SQL_in = SQL;

		// Connection Cn = dbConnect.getConnection();
		Statement st = Cn.createStatement();
		ResultSet rs = st.executeQuery(SQL_in);
		try {
			if (rs.next()) {

				// hasil = rs.getString(nmField);
				hasil = rs.getDate(nmField);
			}
		} catch (SQLException se) {
			Messagebox.show("SQLException Error :" + se.getMessage());
		} catch (Exception e) {
			Messagebox.show("Exception Error :" + e.getMessage());
		} finally {
			dbConnect.closeConnection(Cn, st, rs);
		}
		return hasil;
	}
	
	public Date getSingleValueDateNC(String nmField, String SQL, Connection Cn)
			throws Exception {
		// String hasil = null;
		Date hasil = null;
		String SQL_in = SQL;

		// Connection Cn = dbConnect.getConnection();
		Statement st = Cn.createStatement();
		ResultSet rs = st.executeQuery(SQL_in);
		try {
			if (rs.next()) {

				// hasil = rs.getString(nmField);
				hasil = rs.getDate(nmField);
			}
		} catch (SQLException se) {
			Messagebox.show("SQLException Error :" + se.getMessage());
		} catch (Exception e) {
			Messagebox.show("Exception Error :" + e.getMessage());
		} 
		return hasil;
	}


	
	public void setListboxValueNullStart(Window nmWindow, String lValue,
			String nmListbox, String lSQL, Connection Cn, String OneItemToAdd)
			throws Exception {
		// Connection Cn = dbConnect.getConnection();
		ResultSet rs = null;
		Statement st = null;

		Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
		list.getItems().clear();
		//liStart.app

		int lIndex = 0;

		try {
			Listitem liStart = new Listitem();
			liStart.setValue("");
			liStart.appendChild(new Listcell("-- Pilih --"));
			list.appendChild(liStart);
			
			st = Cn.createStatement();
			rs = st.executeQuery(lSQL);

			if (!OneItemToAdd.equals("")) {
				Listitem li1 = new Listitem();
				li1.setValue(OneItemToAdd);
				li1.appendChild(new Listcell(OneItemToAdd));
				//li1.setSelected(true);
				list.appendChild(li1);
				liStart.setSelected(true);
			}

			while (rs.next()) {
				Listitem li = new Listitem();

				// li.setId("item"+rs.getString(1));
				li.setValue(rs.getString(1));

				li.appendChild(new Listcell(rs.getString(2)));

				if (lValue.trim().equals("")) {
					if (OneItemToAdd.equals("")) {
						if (lIndex == 0) {
							//li.setSelected(true);
						}
						lIndex++;
					}
				} else {
					//System.out.println("Data :"+rs.getString(2).trim()+" | value : "+lValue.trim());
					if (rs.getString(1).trim().equals(lValue.trim())) {
						li.setSelected(true);
					}
				}
				list.appendChild(li);
			}
		} catch (SQLException se) {
			Messagebox.show("Error: " + se.getMessage());
		} catch (Exception e) {
			Messagebox.show("Error Exception: " + e.getMessage());
		} finally {
			dbConnect.closeConnection(Cn, st, rs);
		}
	}	

	public void setListboxValuebyName(Window nmWindow, String lValue,
			String nmListbox, String lSQL, Connection Cn, String OneItemToAdd)
			throws Exception {
		// Connection Cn = dbConnect.getConnection();
		ResultSet rs = null;
		Statement st = null;

		Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
		list.getItems().clear();

		int lIndex = 0;

		try {
			st = Cn.createStatement();
			rs = st.executeQuery(lSQL);

			if (!OneItemToAdd.equals("")) {
				Listitem li1 = new Listitem();
				li1.setValue(OneItemToAdd);
				li1.appendChild(new Listcell(OneItemToAdd));
				li1.setSelected(true);
				list.appendChild(li1);
			}

			while (rs.next()) {
				Listitem li = new Listitem();

				// li.setId("item"+rs.getString(1));
				li.setValue(rs.getString(1));

				li.appendChild(new Listcell(rs.getString(2)));

				if (lValue.trim().equals("")) {
					if (OneItemToAdd.equals("")) {
						if (lIndex == 0) {
							li.setSelected(true);
						}
						lIndex++;
					}
				} else {
					//System.out.println("Data :"+rs.getString(2).trim()+" | value : "+lValue.trim());
					if (rs.getString(2).trim().equals(lValue.trim())) {
						li.setSelected(true);
					}
				}
				list.appendChild(li);
			}
		} catch (SQLException se) {
			Messagebox.show("Error: " + se.getMessage());
		} catch (Exception e) {
			Messagebox.show("Error Exception: " + e.getMessage());
		} finally {
			dbConnect.closeConnection(Cn, st, rs);
		}
	}

	
	
	
	public void setListboxId(Window nmWindow, String lValue, String nmListbox,
			String lSQL, Connection Cn, String OneItemToAdd) throws Exception {
		// Connection Cn = dbConnect.getConnection();
		ResultSet rs = null;
		Statement st = null;

		Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
		list.getItems().clear();

		int lIndex = 0;

		try {
			st = Cn.createStatement();
			rs = st.executeQuery(lSQL);

			if (!OneItemToAdd.equals("")) {
				Listitem li1 = new Listitem();
				li1.setValue(OneItemToAdd);
				li1.appendChild(new Listcell(OneItemToAdd));
				li1.setSelected(true);
				list.appendChild(li1);
			}

			while (rs.next()) {
				Listitem li = new Listitem();

				li.setId("item" + rs.getString(1));
				li.setValue(rs.getString(1));

				li.appendChild(new Listcell(rs.getString(2)));

				if (lValue.trim().equals("")) {
					if (OneItemToAdd.equals("")) {
						if (lIndex == 0) {
							li.setSelected(true);
						}
						lIndex++;
					}
				} else {
					if (rs.getString(1).trim().equals(lValue.trim())) {
						li.setSelected(true);
					}
				}
				list.appendChild(li);
			}
		} catch (SQLException se) {
			Messagebox.show("Error: " + se.getMessage());
		} catch (Exception e) {
			Messagebox.show("Error Exception: " + e.getMessage());
		} finally {
			dbConnect.closeConnection(Cn, st, rs);
		}
	}

	// Function untuk memberi variable inputan jenis LISTBOX
	public void setMultipleListboxValue(Window nmWindow, String nmListbox,
			String SQL, Connection Cn, boolean clearList) throws Exception {
		DecimalFormat mDecimal = new DecimalFormat("#,##0.##");
		DateFormat mDate = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		int lIndex = 0;

		// Connection Cn = dbConnect.getConnection();
		ResultSet rs = null;
		Statement st = null;

		st = Cn.createStatement();
		rs = st.executeQuery(SQL);

		Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
		if (clearList) {
			list.getItems().clear();
		}

		int lColumn = rs.getMetaData().getColumnCount();
		int type[] = new int[lColumn + 1];
		for (int i = 1; i <= lColumn; i++) {
			type[i] = rs.getMetaData().getColumnType(i);
//			 System.out.println(i+" : "+type[i]);
		}
		while (rs.next()) {
			Listitem li = new Listitem();

			for (int i = 1; i <= lColumn; i++) {
				if (rs.getString(i) == null) {
					li.appendChild(new Listcell(""));
				} else {
					if (type[i] == 91 || type[i] == 93) {// 1
						String tgl = mDate.format(rs.getDate(i));
						java.sql.Date ltgl = new java.sql.Date(mDate.parse(tgl)
								.getTime());
						if (i == 1) {
							li.setValue(String.valueOf(ltgl));
						}
						li.appendChild(new Listcell(String.valueOf(ltgl)));
					} else if (type[i] == 4 || type[i] == -5 || type[i] == 2) {// 2
						if (i == 1) {
							li.setValue(rs.getDouble(i));
						}
						li.appendChild(new Listcell(mDecimal.format(rs
								.getDouble(i))));
					} else if (type[i] == 12 || type[i] == 3 || type[i] == 1 || type[i] == -1) {
						if (i == 1) {
							li.setValue(fnBSK.getNull(rs.getString(i)));
						}
						li.appendChild(new Listcell(fnBSK.getNull(rs
								.getString(i))));
					}
				}
			}

			if (lIndex == 0 && clearList) {
				li.setSelected(true);
				lIndex++;
			}

			list.appendChild(li);
		}

	}

	// untuk mengakumulasikan semua item jika terdapat perulangan pemanggilan
	// Fungsi ini + ditambah dengan nourut <Sujono>
	// Tambahan: jika rowcount <=0 , maka function tidak dieksekusi
	public void setMultipleListboxValueAkumulasi(Window nmWindow,
			String nmListbox, String SQL, int noUrut, Connection Cn,
			boolean isSelect, boolean isClear, boolean firstRowIsID)
			throws Exception {
		try {
			DecimalFormat mDecimal = new DecimalFormat("#,##0.##");
			DateFormat mDate = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			int lIndex = 0;

				ResultSet rs = null;
				Statement st = null;

				st = Cn.createStatement();
				rs = st.executeQuery(SQL);
				
				Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
				if (isClear) {
					list.getItems().clear();
				}
				
			if(!rs.next()){
//				System.out.println("Jumlah record : 0");
				}
			else{
				rs.beforeFirst();

				int lColumn = rs.getMetaData().getColumnCount();
				int type[] = new int[lColumn + 1];
				for (int i = 1; i <= lColumn; i++) {
					type[i] = rs.getMetaData().getColumnType(i);
//				System.out.println(String.valueOf(type[i]));
				}
				
				while (rs.next()) {
					Listitem li = new Listitem();
					noUrut = noUrut + 1;

					int n = 1;
					String id = String.valueOf(noUrut);
					if (firstRowIsID) {
						n = 2;
						id = rs.getString(1);
					}
					
					li.setValue(String.valueOf(id));
					li.appendChild(new Listcell(String.valueOf(noUrut)));
					
					
					
					for (int i = n; i <= lColumn; i++) {
						if (rs.getString(i) == null) {
							li.appendChild(new Listcell(""));
						} else {
							if (type[i] == 91 || type[i] == 93  ) {// 1
								String tgl = mDate.format(rs.getDate(i));
								java.sql.Date ltgl = new java.sql.Date(mDate.parse(
										tgl).getTime());
								if (i == 1) {
									li.setValue(String.valueOf(ltgl));
								}
								li.appendChild(new Listcell(String.valueOf(ltgl)));
							} else if (type[i] == 4 || type[i] == -5
									|| type[i] == 2 || type[i] == 3 || type[i] == -6 ) {// 2
								if (i == 1) {
									li.setValue(rs.getDouble(i));
								}
								li.appendChild(new Listcell(mDecimal.format(rs
										.getDouble(i))));
							} else if (type[i] == 12 || type[i] == 93
									|| type[i] == 1 || type[i] == 8 || type[i] == -1) {
								if (i == 1) {
									li.setValue(fnBSK.getNull(rs.getString(i)));
								}
								li.appendChild(new Listcell(fnBSK.getNull(rs
										.getString(i))));
							}
						}
					}

					if (lIndex == 0 && isSelect) {
						li.setSelected(true);
						lIndex++;
					}

					list.appendChild(li);
				}
			}
		}catch(Exception e){
//			e.printStackTrace();
			log.info(e.getMessage() + " -> " + e);
		}
		finally {
			Cn.close();
			
		}
	}
	

	public void setMultipleListGroupValue(Window nmWindow, String nmListbox,
			String nmGroup, String SQL, Connection Cn) throws Exception {
		DecimalFormat mDecimal = new DecimalFormat("#,##0.##");
		DateFormat mDate = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		int lIndex = 0;

		// Connection Cn = dbConnect.getConnection();
		ResultSet rs = null;
		Statement st = null;

		st = Cn.createStatement();
		rs = st.executeQuery(SQL);

		Listbox list = (Listbox) nmWindow.getFellow("lAcc");
		Listgroup lg = new Listgroup(nmGroup);
		// ==> Setting Tambahan <<Sujono>>
		lg.setOpen(false);
		list.appendChild(lg);

		int lColumn = rs.getMetaData().getColumnCount();

		int type[] = new int[lColumn + 1];
		for (int i = 1; i <= lColumn; i++) {
			type[i] = rs.getMetaData().getColumnType(i);
			// Messagebox.show(String.valueOf(type[i]));
		}
		while (rs.next()) {
			Listitem li = new Listitem();

			for (int i = 1; i <= lColumn; i++) {
				if (rs.getString(i) == null) {
					li.appendChild(new Listcell(""));
				} else {
					if (type[i] == 91) {
						String tgl = mDate.format(rs.getDate(i));
						java.sql.Date ltgl = new java.sql.Date(mDate.parse(tgl)
								.getTime());
						if (i == 1) {
							li.setValue(String.valueOf(ltgl));
						}
						li.appendChild(new Listcell(String.valueOf(ltgl)));
					} else if (type[i] == 2) {
						if (i == 1) {
							li.setValue(rs.getDouble(i));
						}
						li.appendChild(new Listcell(mDecimal.format(rs
								.getDouble(i))));
					} else if (type[i] == 12) {
						if (i == 1) {
							li.setValue(fnBSK.getNull(rs.getString(i)));
						}
						li.appendChild(new Listcell(fnBSK.getNull(rs
								.getString(i))));
					}
				}
			}

			if (lIndex == 0) {
				li.setSelected(true);
				lIndex++;
			}

			list.appendChild(li);
		}
		// list.setRows(20);
	}
	
	public boolean isUserAvail(String temp)
	{
		boolean hasil = false;
		try
		{
			String sql = "SELECT user_name FROM vision.`tb_user`";
			open_cn();
			Statement st = Cn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next())
			{
				if(rs.getString(1).equalsIgnoreCase(temp))
				{
					hasil = true;
					break;
				}
			}
		}
		catch(Exception e)
		{
//			e.printStackTrace();
			log.info(e.getMessage() + " -> " + e);
		}
		finally
		{
			try {
				close_cn();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				log.info(e.getMessage() + " -> " + e);
			}
		}
		return hasil;
	}
	
	public boolean isKoorAvail(String temp)
	{
		boolean hasil = false;
		try
		{
			String sql = "SELECT virtualacc FROM witami.`tb_partner`";
			open_cn();
			Statement st = Cn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next())
			{
				if(rs.getString(1).equals(temp))
				{
					hasil = true;
					break;
				}
			}
		}
		catch(Exception e)
		{
//			e.printStackTrace();
			log.info(e.getMessage() + " -> " + e);
		}
		finally
		{
			try {
				if(!Cn.isClosed()) close_cn();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				log.info(e.getMessage() + " -> " + e);
			}
		}
		return hasil;
	}

	
	
//	public boolean isOperatorAvail(String temp)
//	{
//		boolean hasil = false;
//		try
//		{
//			String sql = "SELECT operator FROM witami.`tb_operator_copy`";
//			open_cn();
//			Statement st = Cn.createStatement();
//			ResultSet rs = st.executeQuery(sql);
//			while(rs.next())
//			{
//				if(rs.getString(1).equals(temp))
//				{
//					hasil = true;
//					break;
//				}
//			}
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
//		finally
//		{
//			try {
//				if(!Cn.isClosed()) close_cn();
//				
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		return hasil;
//	}
	
//	public boolean isDenomAvail(String temp)
//	{
//		boolean hasil = false;
//		try
//		{
//			String sql = "SELECT productcode FROM witami.`tb_denom_copy`";
//			open_cn();
//			Statement st = Cn.createStatement();
//			ResultSet rs = st.executeQuery(sql);
//			while(rs.next())
//			{
//				if(rs.getString(1).equals(temp))
//				{
//					hasil = true;
//					break;
//				}
//			}
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
//		finally
//		{
//			try {
//				if(!Cn.isClosed()) close_cn();
//				
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		return hasil;
//	}
	
//	public boolean isDenomBuyAvail(String temp)
//	{
//		boolean hasil = false;
//		try
//		{
//			String sql = "SELECT productcodesup FROM witami.`tb_product_buy_copy`";
//			open_cn();
//			Statement st = Cn.createStatement();
//			ResultSet rs = st.executeQuery(sql);
//			while(rs.next())
//			{
//				if(rs.getString(1).equals(temp))
//				{
//					hasil = true;
//					break;
//				}
//			}
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
//		finally
//		{
//			try {
//				if(!Cn.isClosed()) close_cn();
//				
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		return hasil;
//	}
	
	public boolean isAvail(String SQL, String temp)
	{
		boolean hasil = false;
		try
		{
			String sql = SQL;
			open_cn();
			Statement st = Cn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next())
			{
				if(rs.getString(1).equals(temp))
				{
					hasil = true;
					break;
				}
			}
		}
		catch(Exception e)
		{
//			e.printStackTrace();
			log.info(e.getMessage() + " -> " + e);
		}
		finally
		{
			try {
				if(!Cn.isClosed()) close_cn();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				log.info(e.getMessage() + " -> " + e);
			}
		}
		return hasil;
	}

	// Function untuk mengambil nilai dari sebuah TEXTBOX
	public String getTextboxValue(Window wnd, String namaTextbox) {
		Textbox txt = (Textbox) wnd.getFellow(namaTextbox);
		return txt.getValue();
	}
	
	public void setTextboxFocus(Window wnd, String namaTextbox) {
		Textbox txt = (Textbox) wnd.getFellow(namaTextbox);
		txt.setFocus(true);
	}
	
	public void setLongboxFocus(Window wnd, String namaTextbox) {
		Longbox txt = (Longbox) wnd.getFellow(namaTextbox);
		txt.setFocus(true);
	}
	
	public void setDivVisible(Window wnd, String namaTextbox) {
		Div txt = (Div) wnd.getFellow(namaTextbox);
		txt.setVisible(true);
	}
	
	public void setDivVisible(Window wnd, String namaTextbox, boolean values) {
		Div txt = (Div) wnd.getFellow(namaTextbox);
		txt.setVisible(values);
	}
	
	public void setDivWidth(Window wnd, String namaTextbox, String width) {
		Div dv = (Div) wnd.getFellow(namaTextbox);
		dv.setWidth(width);
	}
	
	public void setSpanVisible(Window wnd, String namaTextbox, boolean values) {
		Span txt = (Span) wnd.getFellow(namaTextbox);
		txt.setVisible(values);
	}
	
	public String getSrcImg(Window wnd, String img)
	{
		String hasil = "";
		Image Img = (Image) wnd.getFellow(img);
		hasil = Img.getSrc();
		return hasil;
	}
	
	public void setSrcImg(Window wnd, String img, String src)
	{
		Image Img = (Image) wnd.getFellow(img);
		Img.setSrc(src);
	}
	
	public void setWindowVisible(Window wnd, boolean value) {
		wnd.setVisible(value);
	}
	
	public void setDateboxFocus(Window wnd, String namaTextbox) {
		Datebox txt = (Datebox) wnd.getFellow(namaTextbox);
		txt.setFocus(true);
	}

	// Function untuk mengambil variable inputan jenis INTBOX
	public int getIntboxValue(Window nmWindow, String nmIntbox) {
		Intbox box = (Intbox) nmWindow.getFellow(nmIntbox);
		return box.getValue();
	}
	
	public long getLongboxValue(Window nmWindow, String nmIntbox) {
		Longbox box = (Longbox) nmWindow.getFellow(nmIntbox);
		return box.getValue();
	}

	// Function untuk mengambil variable inputan jenis DECIMALBOX
	public BigDecimal getDecimalboxValue(Window nmWindow, String nmDecimalbox) {
		Decimalbox box = (Decimalbox) nmWindow.getFellow(nmDecimalbox);
		return box.getValue();
	}

	// Function untuk mengambil variable inputan jenis DECIMALBOX
	public String getDecimalboxText(Window nmWindow, String nmDecimalbox) {
		Decimalbox box = (Decimalbox) nmWindow.getFellow(nmDecimalbox);
		String formatdata = box.getFormat();
		box.setFormat("##########.##");
		String nilaidecimal = box.getText();
		box.setFormat(formatdata);
		return nilaidecimal;
	}

	// Function untuk mengambil variable inputan jenis DATEBOX
	public Date getDateboxValue(Window nmWindow, String nmDatebox) {
		Datebox box = (Datebox) nmWindow.getFellow(nmDatebox);
		return box.getValue();
	}

	// Function untuk mengambil variable inputan jenis DATEBOX
//	public String getDateboxText(Window nmWindow, String nmDatebox) {
//		Datebox box = (Datebox) nmWindow.getFellow(nmDatebox);
//		return box.getText();
//	}
	
    // Function untuk mengambil variable inputan jenis DATEBOX
    public String getDateboxText(Window nmWindow, String nmDatebox) {
           String nilai ="";
           try {
        	   Datebox box = (Datebox) nmWindow.getFellow(nmDatebox);
               String formatdatebox = box.getFormat();
               box.setFormat("yyyy-MM-dd");
               nilai = box.getText();
               box.setFormat(formatdatebox);
           } catch (Exception e) {
        	   
           }
        return nilai;
    }	

	// Function untuk hapus data di listbox
	public void listboxClear(Window wnd, String namaListbox) throws Exception {
		Listbox lb = (Listbox) wnd.getFellow(namaListbox);
		Listitem li = lb.getSelectedItem();
		li.detach();
	}
	
	public void listboxHapus(Window wnd, String namaListbox) {
		try {
			Listbox list = (Listbox) wnd.getFellow(namaListbox);
			list.getItems().clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Function untuk mengambil variable value inputan jenis LISTBOX (DROPDOWN)
	public String getListboxValue(Window nmWindow, String nmListbox)
			throws Exception {
		Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
		Listitem li = list.getSelectedItem();
		return li.getValue().toString().trim();
	}

	// Function untuk mengambil variable label inputan jenis LISTBOX (DROPDOWN)
	public String getListboxLabel(Window nmWindow, String nmListbox)
			throws Exception {
		Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
		Listitem li = list.getSelectedItem();
		return li.getLabel().toString().trim();
	}
	
	// Function untuk mengambil variable inputan jenis LISTBOX
	public String getListboxValue(Window nmWindow, String nmListbox, int nRow,
			int nColumn) throws Exception {
		Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
		Listitem li = list.getItemAtIndex(nRow);
		Listcell lc = (Listcell) (li).getChildren().get(nColumn);
		return lc.getLabel().trim();
	}
	
	// Function untuk mengambil variable inputan jenis LISTBOX selected kolom ke n
	public String getListboxValueBySelected(Window nmWindow, String nmListbox, int nColumn) throws Exception {
		int nRow = 0;
		Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
		nRow = list.getSelectedIndex();
		Listitem li = list.getItemAtIndex(nRow);
		Listcell lc = (Listcell) (li).getChildren().get(nColumn);
		return lc.getLabel().trim();
	}
	
	public int getListboxSelectedCount(Window wnd, String comp)
	{
		int hasil = 0;
		try {
			Listbox list = (Listbox) wnd.getFellow(comp);
			hasil = list.getSelectedCount();
		} catch (Exception e) {
//			e.printStackTrace();
			log.info(e.getMessage() + " -> " + e);
		}
		return hasil;
	}
	
	public void setImage(Window wnd, String nama, String path)
	{
		Image img = (Image) wnd.getFellow(nama);
		img.setSrc(path);
	}
	
	// Function untuk mengeset variable inputan jenis LISTBOX selected kolom ke n
	public void setListboxValueBySelected(Window nmWindow, String nmListbox, int nColumn,String itmToReplace) throws Exception {
		int nRow = 0;
		Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
		nRow = list.getSelectedIndex();
		Listitem li = list.getItemAtIndex(nRow);
		Listcell lc = (Listcell) (li).getChildren().get(nColumn);
		lc.setLabel(itmToReplace);
	}		
	

	// Function untuk menghitung jumlah data LISTBOX
	public int getListboxCountData(Window nmWindow, String nmListbox)
			throws Exception {
		int vReturn = 0;
		try{
			Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
			vReturn = list.getItemCount();
		}catch(Exception e){
//			e.printStackTrace();
			log.info(e.getMessage() + " -> " + e);
		}
		return vReturn;
	}

	// Cari data listbox
	public boolean getListboxFindData(Window nmWindow, String nmListbox,
			int nColumn, String lvalue) throws Exception {
		Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
		boolean cek = false;
		for (int nRow = 0; nRow < list.getItemCount(); nRow++) {
			Listitem li = list.getItemAtIndex(nRow);
			Listcell lc = (Listcell) (li).getChildren().get(nColumn);
			if (lc.getLabel().trim().equals(lvalue.trim())) {
				cek = true;
				break;
			}
		}
		return cek;
	}

	// Function untuk mengambil variable inputan jenis CHECKBOX
	public boolean getCheckboxValue(Window nmWindow, String nmCheckBox) {
		Checkbox chk = (Checkbox) nmWindow.getFellow(nmCheckBox);
		if (chk.isChecked())
			return true;
		else
			return false;
	}
	
	Connection Cn= null;
	DBConnection dbconnection = new DBConnection();
	
	public void open_cn() throws Exception{
		Cn = dbconnection.getConnection();
	}
	
	public void close_cn() throws Exception{
		Cn.close();
	}
	
	public void update(String sql)
	{
		try
		{
			open_cn();
			Statement st = Cn.createStatement();
			st.executeUpdate(sql);
			Cn.commit();
			st.close();
		}
		catch(Exception e)
		{
			
		}
		finally
		{
			try {
				close_cn();
			} catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				log.info(e.getMessage() + " -> " + e);
			}
		}
	}
	
	public void setVisibleImage(Window wnd, String nama, boolean value)
	{
		Image img = (Image) wnd.getFellow(nama);
		img.setVisible(value);
	}
	
	public void setHtmlContent(Window wnd, String component, String isi)
	{
		Html html = (Html) wnd.getFellow(component);
		html.setContent(isi);
	}
	
	public String getSingleValueAdet(String sql)
	{
		String hasil = "";
		try
		{
			open_cn();
			Statement st = Cn.createStatement();
	    	ResultSet rs = st.executeQuery(sql);
	    	if(rs.next()) hasil = rs.getString(1);
	    	rs.close();
	    	st.close();
		}
		catch(Exception e)
		{
			log.info(e.getMessage() + " -> " + e);
		}
		finally
		{
	    	try {
				close_cn();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.info(e.getMessage() + " -> " + e);
			}
		}
		return hasil;
	}
	
	public int getSingleValueIntNC(String sql,Connection Cn)
	{
		int hasil = 0;
		try
		{
			
			Statement st = Cn.createStatement();
	    	ResultSet rs = st.executeQuery(sql);
	    	if(rs.next()) hasil = rs.getInt(1);
	    	rs.close();
	    	st.close();
		}
		catch(Exception e)
		{
			log.info(e.getMessage() + " -> " + e);
		}
		return hasil;
	}
	
	public double getSingleValueDoubleNC(String sql,Connection Cn)
	{
		double hasil = 0;
		try
		{
			
			Statement st = Cn.createStatement();
	    	ResultSet rs = st.executeQuery(sql);
	    	if(rs.next()) hasil = rs.getDouble(1);
	    	rs.close();
	    	st.close();
		}
		catch(Exception e)
		{
			log.info(e.getMessage() + " -> " + e);
		}
		return hasil;
	}
	
	public int getSingleValueInt(String sql)
	{
		int hasil = 0;
		try
		{
			open_cn();
			Statement st = Cn.createStatement();
	    	ResultSet rs = st.executeQuery(sql);
	    	if(rs.next()) hasil = rs.getInt(1);
	    	rs.close();
	    	st.close();
		}
		catch(Exception e)
		{
			log.info(e.getMessage() + " -> " + e);
		}
		finally
		{
	    	try {
				close_cn();
			} catch (Exception e) {}
		}
		return hasil;
	}
	
	public int getLastSingleValueInt(String sql)
	{
		int hasil = 0;
		try
		{
			open_cn();
			Statement st = Cn.createStatement();
	    	ResultSet rs = st.executeQuery(sql);
	    	while(rs.next()) hasil = rs.getInt(1);
	    	rs.close();
	    	st.close();
		}
		catch(Exception e)
		{
			log.info(e.getMessage() + " -> " + e);
		}
		finally
		{
	    	try {
				close_cn();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.info(e.getMessage() + " -> " + e);
			}
		}
		return hasil;
	}
	
	public String getLastSingleValueString(String sql)
	{
		String hasil = "";
		try
		{
			open_cn();
			Statement st = Cn.createStatement();
	    	ResultSet rs = st.executeQuery(sql);
	    	while(rs.next()) hasil = rs.getString(1);
	    	rs.close();
	    	st.close();
		}
		catch(Exception e)
		{
			log.info(e.getMessage() + " -> " + e);
		}
		finally
		{
	    	try {
				close_cn();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.info(e.getMessage() + " -> " + e);
			}
		}
		return hasil;
	}
	
	public void clearTable(String nama)
	{
		try
		{
			open_cn();
			Cn.setAutoCommit(false);
			String sql = "delete from " + nama;
			Statement st = Cn.createStatement();
			
			st.executeUpdate(sql);
			st.close();
			Cn.commit();
		}
		catch(Exception e)
		{
			log.info(e.getMessage() + " -> " + e);
		}
		finally
		{
			try {
				Cn.setAutoCommit(true);
				close_cn();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.info(e.getMessage() + " -> " + e);
			}
		}
	}

	// Function untuk mengambil variable inputan jenis RADIO
	public boolean getRadioValue(Window nmWindow, String nmRadio) {
		Radio rd = (Radio) nmWindow.getFellow(nmRadio);
		if (rd.isChecked())
			return true;
		else
			return false;
	}

	// Function untuk mengambil variable inputan jenis LABEL
	public String getLabelValue(Window nmWindow, String nmLabel) {
		Label lbl = (Label) nmWindow.getFellow(nmLabel);
		return lbl.getValue();
	}

	// Function untuk memberi variable inputan jenis TEXTBOX
	public void setTextboxValue(Window nmWindow, String nmTextbox, String lvalue)
			throws Exception {
		Textbox box = (Textbox) nmWindow.getFellow(nmTextbox);
		box.setValue(lvalue.trim());
	}

	// Function untuk memberi variable inputan jenis TEXTBOX
	public void setTextboxResetValue(Window nmWindow, String nmTextbox)
			throws Exception {
		Textbox box = (Textbox) nmWindow.getFellow(nmTextbox);
		box.setRawValue(null);
	}

	public void setIntboxResetValue(Window nmWindow, String nmIntbox)throws Exception {
		Intbox box = (Intbox) nmWindow.getFellow(nmIntbox);
		box.setRawValue(null);
	}

	
	// Function untuk memberi variable inputan jenis INTBOX
	public void setIntboxValue(Window nmWindow, String nmIntbox, int lvalue)
			throws Exception {
		Intbox box = (Intbox) nmWindow.getFellow(nmIntbox);
		box.setValue(lvalue);
	}
	
	public void setIntboxFocus(Window nmWindow, String nmIntbox,boolean statFocus)
			throws Exception {
		Intbox box = (Intbox) nmWindow.getFellow(nmIntbox);
		box.setFocus(statFocus);
	}
	
	public String getIntboxText(Window nmWindow, String nmIntbox) {
		Intbox box = (Intbox) nmWindow.getFellow(nmIntbox);
		return box.getText();
	}
	
	public void setLongboxValue(Window nmWindow, String nmIntbox, long lvalue)
			throws Exception {
		Longbox box = (Longbox) nmWindow.getFellow(nmIntbox);
		box.setValue(lvalue);
	}

	// Function untuk memberi variable inputan jenis DECIMALBOX
	public void setDecimalboxValue(Window nmWindow, String nmDecimalbox,
			BigDecimal lvalue) throws Exception {
		Decimalbox box = (Decimalbox) nmWindow.getFellow(nmDecimalbox);
		box.setValue(lvalue);
	}

	public void setDecimalResetValue(Window nmWindow, String nmDecimalbox) throws Exception {
		Decimalbox box = (Decimalbox) nmWindow.getFellow(nmDecimalbox);
		box.setRawValue(null);
		
	}
	
	
	
	// Function untuk memberi variable inputan jenis DECIMALBOX(SETTEXT)
	public void setDecimalboxText(Window nmWindow, String nmDecimalbox,
			String lvalue) throws Exception {
		Decimalbox box = (Decimalbox) nmWindow.getFellow(nmDecimalbox);
		box.setText(lvalue);
	}

	// Function untuk memberi variable inputan jenis DATEBOX
	public void setDateboxValue(Window nmWindow, String nmDatebox, Date lvalue)
			throws Exception {
		Datebox box = (Datebox) nmWindow.getFellow(nmDatebox);
		box.setValue(lvalue);
	}
	
	// Function untuk membuat inputan jenis DATEBOX visible
	public void setDateboxVisible(Window nmWindow, String nmDatebox, boolean lvalue)
			throws Exception {
		Datebox box = (Datebox) nmWindow.getFellow(nmDatebox);
		box.setVisible(lvalue);
	}

	// Function untuk memberi variable inputan jenis DATEBOX(SETTEXT)
	public void setDateboxText(Window nmWindow, String nmDatebox, String lvalue)
			throws Exception {
		Datebox box = (Datebox) nmWindow.getFellow(nmDatebox);
		box.setText(lvalue);
	}

	// Function untuk memberi variable inputan jenis DATEBOX(SETTEXT)
	public void setDateboxConstraint(Window nmWindow, String nmDatebox,
			String constraint) throws Exception {
		Datebox box = (Datebox) nmWindow.getFellow(nmDatebox);
		box.setConstraint(constraint);
	}
	
	// Function untuk mereset variable inputan jenis DATEBOX
	public void setDateboxResetValue(Window nmWindow, String nmDatebox)
			throws Exception {
		Datebox box = (Datebox) nmWindow.getFellow(nmDatebox);
		box.setRawValue(null);
	}

	// Function untuk memberi variable inputan jenis CHECKBOX
	public void setCheckboxValue(Window nmWindow, String nmCheckbox, boolean sts)
			throws Exception {
		Checkbox chk = (Checkbox) nmWindow.getFellow(nmCheckbox);
		chk.setChecked(sts);
//		chk.setValue(sts);
	}

	// Function untuk memberi variable inputan jenis RADIO
	public void setRadioValue(Window nmWindow, String nmRadio, boolean sts)
			throws Exception {
		Radio rd = (Radio) nmWindow.getFellow(nmRadio);
		rd.setChecked(sts);
	}
	
	public void setRadioLabel(Window nmWindow, String nmRadio, String value)
			throws Exception {
		Radio rd = (Radio) nmWindow.getFellow(nmRadio);
		rd.setLabel(value);
	}

	// Function untuk memberi variable inputan jenis LABEL
	public void setLabelValue(Window nmWindow, String nmLabel, String lvalue)
			throws Exception {
//		System.out.println(lvalue);
		Label lbl = (Label) nmWindow.getFellow(nmLabel);
		lbl.setValue(lvalue.trim());
	}
	
	public void setButtonVisible(Window nmWindow, String nmButton, boolean lvalue) throws Exception {
		Button btn = (Button) nmWindow.getFellow(nmButton);
		btn.setVisible(lvalue);
	}
	
	public void setLabelVisible(Window nmWindow, String nmLabel, boolean lvalue) throws Exception {
		Label lbl = (Label) nmWindow.getFellow(nmLabel);
		lbl.setVisible(lvalue);
	}

	// Function untuk memberi Label jenis BUTTON
	public void setButtonLabel(Window nmWindow, String nmButton, String lvalue)
			throws Exception {
		Button btn = (Button) nmWindow.getFellow(nmButton);
		btn.setLabel(lvalue);
	}

	// Function untuk mengubah property disable jenis TEXTBOX
	public void setTextboxDisabled(Window nmWindow, String nmTextbox,
			boolean lvalue) throws Exception {
		Textbox box = (Textbox) nmWindow.getFellow(nmTextbox);
		box.setDisabled(lvalue);
	}
	
	public void setTextboxVisible(Window nmWindow, String nmTextbox, boolean lvalue) throws Exception {
		Textbox box = (Textbox) nmWindow.getFellow(nmTextbox);
		box.setVisible(lvalue);
	}

	// Function untuk mengubah property disable jenis INTBOX
	public void setIntboxDisabled(Window nmWindow, String nmIntbox,
			boolean lvalue) throws Exception {
		Intbox box = (Intbox) nmWindow.getFellow(nmIntbox);
		box.setDisabled(lvalue);
	}

	// Function untuk mengubah property disable jenis DECIMALBOX
	public void setDecimalboxDisabled(Window nmWindow, String nmDecimalbox,
			boolean lvalue) throws Exception {
		Decimalbox box = (Decimalbox) nmWindow.getFellow(nmDecimalbox);
		box.setDisabled(lvalue);
	}

	// Function mengubah property disable jenis DATEBOX
	public void setDateboxDisabled(Window nmWindow, String namaDatebox,
			boolean lvalue) throws Exception {
		Datebox box = (Datebox) nmWindow.getFellow(namaDatebox);
		box.setDisabled(lvalue);
	}

	// Function mengubah property disable jenis LISTBOX
	public void setListboxDisabled(Window nmWindow, String nmListbox,
			boolean lvalue) throws Exception {
		Listbox box = (Listbox) nmWindow.getFellow(nmListbox);
		box.setDisabled(lvalue);
	}

	// Function mengubah property disable jenis CHECKBOX
	public void setCheckboxDisabled(Window nmWindow, String nmCheckbox,
			boolean lvalue) throws Exception {
		Checkbox chk = (Checkbox) nmWindow.getFellow(nmCheckbox);
		chk.setDisabled(lvalue);
	}

	// Function mengubah property disable jenis RADIO
	public void setRadioDisabled(Window nmWindow, String nmRadio, boolean lvalue)
			throws Exception {
		Radio rd = (Radio) nmWindow.getFellow(nmRadio);
		rd.setDisabled(lvalue);
	}

	// Function mengubah property disable jenis BUTTON
	public void setButtonDisabled(Window nmWindow, String nmButton,
			boolean lvalue) throws Exception {
		Button btn = (Button) nmWindow.getFellow(nmButton);
		btn.setDisabled(lvalue);
	}

	// Function untuk mengubah property readonly jenis TEXTBOX
	public void setTextboxReadonly(Window nmWindow, String nmTextbox,
			boolean lvalue) throws Exception {
		Textbox box = (Textbox) nmWindow.getFellow(nmTextbox);
		box.setReadonly(lvalue);
	}

	// Function untuk mengubah property readonly jenis INTBOX
	public void setIntboxReadonly(Window nmWindow, String nmIntbox,
			boolean lvalue) throws Exception {
		Intbox box = (Intbox) nmWindow.getFellow(nmIntbox);
		box.setReadonly(lvalue);
	}

	// Function untuk mengubah property readonly jenis DECIMALBOX
	public void setDecimalboxReadonly(Window nmWindow, String nmDecimalbox,
			boolean lvalue) throws Exception {
		Decimalbox box = (Decimalbox) nmWindow.getFellow(nmDecimalbox);
		box.setReadonly(lvalue);
	}

	// Function mengubah property readonly jenis DATEBOX
	public void setDateboxReadonly(Window nmWindow, String namaDatebox,
			boolean lvalue) throws Exception {
		Datebox box = (Datebox) nmWindow.getFellow(namaDatebox);
		box.setReadonly(lvalue);
		if (lvalue)
			box.setButtonVisible(false);
		else
			box.setButtonVisible(true);
	}

	// Function mengubah property disable jenis LISTBOX <<YONAN>>
	public void setListboxSelected(Window nmWindow, String nmListbox,
			String nmItem) throws Exception {
		Listbox box = (Listbox) nmWindow.getFellow(nmListbox);
		Listitem item = (Listitem) nmWindow.getFellow(nmItem);
		box.setSelectedItem(item);
	}
	
	public String getStringComma(int number)
	{
		String hasil = "";
		try
		{
			DecimalFormat formatter = new DecimalFormat("#,###");
			hasil = formatter.format(number);
		}
		catch(Exception e)
		{
			log.info(e.getMessage() + " -> " + e);
		}
		return hasil;
	}
	
	public String getStringComma(long number)
	{
		String hasil = "";
		try
		{
			DecimalFormat formatter = new DecimalFormat("#,###");
			hasil = formatter.format(number);
		}
		catch(Exception e)
		{
			log.info(e.getMessage() + " -> " + e);
		}
		return hasil;
	}
	
	public String getNominal(long number)
	{
		String hasil = "";
		try
		{
			DecimalFormat formatter = new DecimalFormat("#,###");
			hasil = formatter.format(number);
		}
		catch(Exception e)
		{
			log.info(e.getMessage() + " -> " + e);
		}
		return "Rp. "+hasil;
	}
	
	public String getRekFormat(int number)
	{
		String hasil = "";
		try
		{
			DecimalFormat formatter = new DecimalFormat("###-###-####");
			hasil = formatter.format(number);
		}
		catch(Exception e)
		{
			log.info(e.getMessage() + " -> " + e);
		}
		return hasil;
	}

	public String getSingleValue(String nmField, String SQL, Connection Cn)
			throws Exception {
		String hasil = null;
		String SQL_in = SQL;

		// Connection Cn = dbConnect.getConnection();
		Statement st = Cn.createStatement();
		ResultSet rs = st.executeQuery(SQL_in);
		try {
			if (rs.next()) {
				hasil = rs.getString(nmField);
			} else {
				// hasil = SQL_in;
				hasil = "-";
			}
		} catch (SQLException se) {
			Messagebox.show("SQLException Error :" + se.getMessage());
		} catch (Exception e) {
			Messagebox.show("Exception Error :" + e.getMessage());
		} finally {
			dbConnect.closeConnection(Cn, st, rs);
		}
		return hasil;
	}
	
	public String getSingleValueNC(String nmField, String SQL, Connection Cn)
			throws Exception {
		String hasil = null;
		String SQL_in = SQL;

		// Connection Cn = dbConnect.getConnection();
		Statement st = Cn.createStatement();
		ResultSet rs = st.executeQuery(SQL_in);
		try {
			if (rs.next()) {
				hasil = rs.getString(nmField);
			} else {
				// hasil = SQL_in;
				hasil = "-";
			}
			rs.close();
			st.close();
		} catch (SQLException se) {
			Messagebox.show("SQLException Error :" + se.getMessage());
		} catch (Exception e) {
			Messagebox.show("Exception Error :" + e.getMessage());
		} finally {
//			dbConnect.closeConnection(Cn, st, rs);
		}
		return hasil;
	}

	public int getjumlahdata(String nmField, String SQL, Connection Cn)
			throws Exception {
		int hasil = 0;
		String SQL_in = SQL;

		// Connection Cn = dbConnect.getConnection();
		Statement st = Cn.createStatement();
		ResultSet rs = st.executeQuery(SQL_in);
		try {
			if (rs.next()) {
				// hasil = rs.getString(nmField);
				hasil = rs.getInt(nmField);
			} else {
				// hasil = SQL_in;
				hasil = 0;
			}
		} catch (SQLException se) {
			Messagebox.show("SQLException Error :" + se.getMessage());
		} catch (Exception e) {
			Messagebox.show("Exception Error :" + e.getMessage());
		} finally {
			dbConnect.closeConnection(Cn, st, rs);
		}
		return hasil;
	}

	public int insertdata(String SQL, Connection Cn) throws Exception {
		int hasil = 0;
		String SQL_in = SQL;
		ResultSet rs = null;
		// Connection Cn = dbConnect.getConnection();
		Statement st = Cn.createStatement();
		hasil = st.executeUpdate(SQL_in);
		Cn.commit();
		// ResultSet rs = st.executeQuery(SQL_in);
		try {
			/*
			 * if(rs.next()){ //hasil = rs.getString(nmField); hasil =
			 * rs.getInt(nmField); }else { //hasil = SQL_in; hasil=0; }
			 */
		} catch (Exception e) {
			Messagebox.show("Exception Error :" + e.getMessage());
		} finally {
			dbConnect.closeConnection(Cn, st, rs);
		}
		return hasil;
	}
	
	public void setRowVisible(Window wnd, String nama, boolean value)
	{
		try {
			Row row = (Row) wnd.getFellow(nama);
			row.setVisible(value);
		} catch (Exception e) {
			log.info(e.getMessage() + " -> " + e);
		}
	}
	
	public String formatRp(int angka) {
        
		DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
	    DecimalFormatSymbols dfs = new DecimalFormatSymbols();
	    dfs.setCurrencySymbol("");
	    dfs.setMonetaryDecimalSeparator(',');
	    dfs.setGroupingSeparator('.');
	    df.setDecimalFormatSymbols(dfs);
	    return "Rp " + df.format(angka);  

	}
	
	public String formatRp(String angka) {
        
		int number = Integer.parseInt(angka);
		DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
	    DecimalFormatSymbols dfs = new DecimalFormatSymbols();
	    dfs.setCurrencySymbol("");
	    dfs.setMonetaryDecimalSeparator(',');
	    dfs.setGroupingSeparator('.');
	    df.setDecimalFormatSymbols(dfs);
	    return "Rp " + df.format(number);  

	}
	
	public String formatRp(long angka) {
        
		DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
	    DecimalFormatSymbols dfs = new DecimalFormatSymbols();
	    dfs.setCurrencySymbol("");
	    dfs.setMonetaryDecimalSeparator(',');
	    dfs.setGroupingSeparator('.');
	    df.setDecimalFormatSymbols(dfs);
	    return "Rp " + df.format(angka);
       
       
 }
	
	public String formatDecimal(double angka) {
	    
		Locale locale  = new Locale("id", "IN");
			DecimalFormat myFormatter = (DecimalFormat) NumberFormat.getNumberInstance(locale);
			myFormatter.applyPattern("###,###.00");
			String output = "";
			if (angka == 0) {
				output = "0,00";
			} else {
				output = myFormatter.format(angka);
			}
			
		    return output;
	       
	       
	 }
	
	public String formatRp(double angka) {
        
		Locale locale  = new Locale("id", "IN");
			DecimalFormat myFormatter = (DecimalFormat) NumberFormat.getNumberInstance(locale);
			myFormatter.applyPattern("###,###.00");
			String output = "";
			if (angka == 0) {
				output = "0,00";
			} else {
				output = myFormatter.format(angka);
			}
			
		    return "Rp " + output;
	       
	       
	 }
	
	public String getDateFormatNow(String bentuk)
	{
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat(bentuk);
		String hasil = df.format(date);
		return hasil;
	}
	
	public String getDateFormat(String bentuk, Date input)
	{
		SimpleDateFormat df = new SimpleDateFormat(bentuk);
		String hasil = df.format(input);
		return hasil;
	}
	
	public Date getPrevNextDate(int n)
	{
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.add(Calendar.MONTH, n);
		aCalendar.set(Calendar.DATE, 1);
		Date firstDateOfPreviousMonth = aCalendar.getTime();
		return firstDateOfPreviousMonth;
	}
	
	public Date getPrevNextDay(int n)
	{
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.add(Calendar.DAY_OF_YEAR, n);
		//aCalendar.set(Calendar.DATE, 1);
		Date firstDateOfPreviousMonth = aCalendar.getTime();
		return firstDateOfPreviousMonth;
	}
	
	public String formatNumber(int number)
	{
		DecimalFormat nf4 = new DecimalFormat("#0000");
		return nf4.format(number);
	}
	
	public String formatNumber(int number, String pattern)
	{
		DecimalFormat nf4 = new DecimalFormat(pattern);
		return nf4.format(number);
	}
	
	public String formatNumber(long number, String pattern)
	{
		DecimalFormat nf4 = new DecimalFormat(pattern);
		return nf4.format(number);
	}

	
/*		
	public void konfirmasi(String Judul, String Message,String SQlOk, String Messagereject) throws Exception {
	
		
		Messagebox.show(Message, Judul, Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION, new org.zkoss.zk.ui.event.EventListener() {

					public void onEvent(Event e) throws Exception {
						// TODO Auto-generated method stub
						try {
							boolean statustrx;						
							if ("onOK".equals(e.getName())) {
								
								//statustrx = true;
		//						Messagebox.show(SQlOk);
							} else {
								// ("onCancel".equals(e.getName()))
			//					statustrx = false;
								/*
								 * Messagebox.show(
								 * "Anda telah menolak konfirmasi, data belum kami terima, terima kasih "
								 * );
								 */
	//						}
	//					 return statustrx;	
		//				} catch (Exception ex) {
							// TODO: handle exception
			//				System.out.println("Exception " + ex.getMessage());
				//		}
			//		}
		//		});
	//}

	public int showMessageQuestion(String Judul, String Message)
			throws Exception {
		int status = 0;
		status = Messagebox.show(Message, Judul,
				Messagebox.YES | Messagebox.NO, Messagebox.QUESTION);
		return status;
	}
	
	public String checkNull(String data) throws Exception {
		String Result ="";
		
		if((data != null) && (data != "")){
			Result = data;
		}else{
			Result="";
		}
			
			
		return Result;
	}


	public boolean checkfile_exits(String pathlengkap) throws Exception{
		 boolean status = false;
		
		 try {
			
		 File f = new File(pathlengkap);
		 
		  if(f.exists()){
			  status = true;
		  }else{
			  status = false;
		  }
		 
			} catch (Exception e) {
				// TODO: handle exception
				status = false;
			}

		  return status;
	}
	
	
	public int hitungumur(String tgllahir)throws Exception{
		
		Connection Cn;
		
		Cn = this.dbConnect.getConnection();
		
		int umur =0;
		String SQL ="SELECT DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT('"+tgllahir+"', '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT('"+tgllahir+"', '00-%m-%d')) AS umur ";
		
		
		umur = Integer.parseInt(getSingleValue("umur", SQL, Cn));
		
		return umur;
	}
	
	//added by ferry, cek keberadaan data(return boolean)
	public boolean checkDataExistNC(Connection Cn,String nmTable,String nmField,String nmItem) throws Exception{
		String sqlCek = "select count(*) COUNT from "+nmTable+" where "+nmField+" = '"+nmItem+"'";
		
		String strCount = getSingleValueNC("COUNT", sqlCek, Cn);
		int numCount = Integer.parseInt(strCount);
		
		if (numCount > 0) {
			return true;
		}else{
			return false;
		}
	}
	
	public boolean checkDataExist(String nmTable,String nmField,String nmItem) throws Exception{
		String sqlCek = "select count(*) COUNT from "+nmTable+" where "+nmField+" = '"+nmItem+"'";
		Connection Cn;
		Cn = this.dbConnect.getConnection();
		String strCount = getSingleValue("COUNT", sqlCek, Cn);
		int numCount = Integer.parseInt(strCount);
		
		if (numCount > 0) {
			return true;
		}else{
			return false;
		}
	}
	
    public String dateFormatter(String dtFormatOld,String dtFormatNew,String dtINput) throws ParseException{
    	SimpleDateFormat parse = new SimpleDateFormat(dtFormatOld, Locale.ENGLISH);
    	SimpleDateFormat formatter = new SimpleDateFormat(dtFormatNew);
    	
		java.util.Date parsed =parse.parse(dtINput);

		String formatted = formatter.format(parsed);	
		return formatted;
    }	
    
	//function untuk menghapus data dari selected row pada listbox
	public void delSelectedListboxData(Window nmWindow,String nmListbox) throws Exception{
		int nRow = 0;
		Listbox list = (Listbox) nmWindow.getFellow(nmListbox);
		nRow = list.getSelectedIndex();
		Listitem li = list.getItemAtIndex(nRow);
		li.detach();
	}
	
	public String generateTerbilang(double number) {
		// siapkan map dari angka ke teks, sebagai kamus
		HashMap<Integer, String> singleNumberToText = new HashMap();
		HashMap<Integer, String> doubleNumberToText = new HashMap();

		singleNumberToText.put(1, "Satu");
		singleNumberToText.put(2, "Dua");
		singleNumberToText.put(3, "Tiga");
		singleNumberToText.put(4, "Empat");
		singleNumberToText.put(5, "Lima");
		singleNumberToText.put(6, "Enam");
		singleNumberToText.put(7, "Tujuh");
		singleNumberToText.put(8, "Delapan");
		singleNumberToText.put(9, "Sembilan");

		doubleNumberToText.put(10, "Sepuluh");
		doubleNumberToText.put(11, "Sebelas");
		doubleNumberToText.put(12, "Dua Belas");
		doubleNumberToText.put(12, "Tiga Belas");
		doubleNumberToText.put(14, "Empat Belas");
		doubleNumberToText.put(15, "Lima Belas");
		doubleNumberToText.put(16, "Enam Belas");
		doubleNumberToText.put(17, "Tujuh Belas");
		doubleNumberToText.put(18, "Delapan Belas");
		doubleNumberToText.put(19, "Sembilan Belas");

		// format angka yang diberikan sehingga ada separator ribuan
		// untuk membagi angka ke dalam grup-grup angka
		DecimalFormat df = (DecimalFormat) NumberFormat.getInstance();
		df.setDecimalSeparatorAlwaysShown(false);
		
//		System.out.println("number : "+number);
		
		String numberText = df.format(number);
		
//		System.out.println("Locale : "+Locale.getDefault());
//		Locale locale = Locale.getDefault();
//		System.out.println("language : "+locale.getDisplayLanguage()) ;
//		System.out.println("country : "+locale.getDisplayCountry());
		
		
		// jika angkanya = 1250000, maka splittedNumber[] = {1,250,000}
		String[] splittedNumber = numberText.split(",");
//		String[] splittedNumber = numberText.split("\\.");
		
//		if(splittedNumber.length == 0){
//			splittedNumber = numberText.split(",");
//		}
		
		String result = "";

		// process grup-grup angka
		for (int x = 0; x < splittedNumber.length; x++) {
			String temp = splittedNumber[x];
			if (temp.length() == 1){
				result += singleNumberToText.get(Integer.parseInt(temp));
				
			}
			else if (temp.length() == 2) {
				int val = Integer.parseInt(temp);
				if (val < 20) {
					result += doubleNumberToText.get(val);
				} else {

					result += singleNumberToText.get(Integer.parseInt(temp
							.substring(0, 1)));
					result += "Puluh";

					if (!temp.substring(1, 2).equals("0")) {
						result += " ";
						result += singleNumberToText.get(Integer.parseInt(temp
								.substring(1, 2)));
					}
				}
			} else if (temp.length() == 3) {
				if (!temp.substring(0, 1).equals("0")) {
					if (temp.substring(0, 1).equals("1")) {
						result += "Seratus ";
					} else {
						result += singleNumberToText.get(Integer.parseInt(temp
								.substring(0, 1)));
						result += " Ratus";
					}
				}

				int lastTwo = Integer.parseInt(temp.substring(1));
				if (lastTwo < 20 && lastTwo != 0) {
					result += doubleNumberToText.get(lastTwo);
				} else {
					if (!temp.substring(1, 2).equals("0")) {
						result += " ";
						result += singleNumberToText.get(Integer.parseInt(temp
								.substring(1, 2)));
						result += " Puluh";
					}

					if (!temp.substring(2).equals("0")) {
						result += " ";
						result += singleNumberToText.get(Integer.parseInt(temp
								.substring(2)));
					}
				}

			}
			if (splittedNumber.length - x == 3)
				result += " Juta ";
			if (splittedNumber.length - x == 2 && Integer.parseInt(temp) != 0)
				result += " Ribu ";
		}
		result += " Rupiah";
		
		return result;
	}
	
	public void clearListbox(Window wnd, String nmListbox)throws Exception{
		try{
			Listbox list = (Listbox) wnd.getFellow(nmListbox);
			list.getItems().clear();
			
		}catch(Exception e){
			log.info(e.getMessage() + " -> " + e);
		}
	}

	public void setMultipleListboxClear(Window wnd, String nama)
	{
		try {
			Listbox l = (Listbox) wnd.getFellow(nama);
			l.getItems().clear();
		} catch (Exception e) {
			log.info(e.getMessage() + " -> " + e);
		}
	}

	public boolean isIdAvail(String sql)
	{
		boolean hasil = false;
		try {
			int ada = getSingleValueInt(sql);
			if(ada == 0) hasil = false;
			else hasil = true;
		} catch (Exception e) {
			log.info(e.getMessage() + " -> " + e);
		}
		return hasil;
	}

	public void setDecimalboxFocus(Window wnd, String namaTextbox) {
		Decimalbox txt = (Decimalbox) wnd.getFellow(namaTextbox);
		txt.setFocus(true);
	}

	public void setIntboxText(Window nmWindow, String nmTextbox, String lvalue)
			throws Exception {
		Intbox box = (Intbox) nmWindow.getFellow(nmTextbox);
		box.setText(lvalue.trim());
	}
	
	//menambahkan days
	public Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }
	
	public int getRowCount(ResultSet resultSet)throws Exception{
		int size = 0;
		try {
		    resultSet.last();
		    size = resultSet.getRow();
		    resultSet.beforeFirst();
		}
		catch(Exception ex) {
			return 0;
		}
		return size;
	}

	/**
	 * commented by adet
	 * 11/18/2016 11:09 AM
	 * @return
	 */
//	public String getIDTRX() {
//		
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
//		Date date = new Date();
//		String id = "2" + sdf.format(date);
//		
//		Random rd = new Random();
//		int y = rd.nextInt(9999);
//		String w = String.valueOf(y);
//		String random;
//		if (w.length() == 1){
//			random = "000" +w;
//		}
//		else if (w.length() == 2){
//			random = "00" +w;
//		}
//		else if (w.length() ==3) {
//			random = "000"  +w;
//		}
//		else {
//			random =w;
//		}
//		id= id+w;
//		//System.out.println(id);
//		return id;
//	}
	
	public String getIDTRX() {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = new Date();
		String id = "2" + sdf.format(date);
		
		Random rd = new Random();
		int y = rd.nextInt(9999);
		
		DecimalFormat decimalFormat = new DecimalFormat("0000");
		
		id+=decimalFormat.format(y);
		
		return id;
	}
	
	public void cetakLog (String mess1,String mess2){
		try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(bundle.getString("log_out"), true)))) {
		    out.println(mess1);
//		    out.print("Waktu Yang dibutuhkan untuk cek Saldo=");
		    //more code
		    out.println(mess2);
		    //more code
		}catch (Exception e) {
		    //exception handling left as an exercise for the reader
			e.printStackTrace();
		}
	}
	
	public String getTextboxValue(Textbox textbox) {
		return textbox.getValue().toString();
	}
	
	public void setTextboxResetValue(Textbox textbox) {
		textbox.setRawValue(null);
	}
	
	public String getListboxValue(Listbox list) {
		Listitem li = list.getSelectedItem();
		return li.getValue().toString().trim();
	}
	
	public void setTextboxValue(Textbox nmTextbox, String lvalue) {
		nmTextbox.setValue(lvalue.trim());
	}
}
