package com.truemoneywitami.function.session;

import java.io.File;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import org.zkoss.io.Files;
import org.zkoss.util.media.Media;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.DBConnection;
import com.truemoneywitami.function.MOBFunction;
import com.truemoneywitami.function.ZKFunction;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;

//import com.mob.function.ZKFunction;

/**
 * Created by Eclipse User: Sujono Date: Oktober 14, 2011 Time: 11:33:13 AM To
 * change this template use File | Settings | File Templates.
 */

public class mac_Login extends Window {
	/**
	 * 
	 */
	mob_Session sesi = new mob_Session();
	MOBFunction fnBSK = new MOBFunction();
	String SQL = null;
	DBConnection dbConnect = new DBConnection();
	ZKFunction zk = new ZKFunction();
	Connection Cn = null;
	Session sess = Sessions.getCurrent();

	int count;
	String temp = "";

	public void open_cn() throws Exception {
		Cn = dbConnect.getConnection();
		Cn.setAutoCommit(false);
	}

	public void close_cn() throws Exception {
		Cn.close();
	}

	public void doLogout() throws Exception {
		Sessions.getCurrent().invalidate();
		Executions.sendRedirect("/login.zul");
	}

	public void onLogin(Window wnd, String userstr, String passstr, String tipe) throws Exception {
		Connection Cn = null;

		ResultSet rs = null;
		Statement st = null;
		Statement stStor = null;
		String SQLstor = "";
		ResultSet rsStor = null;
		try {

			Cn = dbConnect.getConnection();
			st = Cn.createStatement();
			stStor = Cn.createStatement();

			Textbox txtUser = (Textbox) this.getFellow("txtUser");
			Textbox txtPass = (Textbox) this.getFellow("txtPass");

			String Sqlcheckadmin = "SELECT a.id, " + "a.user_name, " + "a.name, " + "a.email, " + "a.password, "
					+ "b.nama type_user, " + "a.id_storage, " + "c.storage_type, " + "a.valid_until, "
					+ "b.idgroupakses " + "FROM vision.tb_user a, " + "vision.tb_groupakses b, "
					+ "vision.tb_mst_storage c " + "WHERE a.idgroupakses = b.idgroupakses " + "AND a.id_storage = c.id "
					+ "AND a.flagactive = 'Y' " + "AND b.flagactive = 'Y' " + "AND c.flagactive = 'Y' "
					+ "AND upper(a.user_name) = upper('" + txtUser.getText().toString().trim() + "') "
					+ "AND upper(a.password) = upper('" + txtPass.getText().toString().trim() + "')";

			/*
			 * String Sqlcheckadmin =
			 * " SELECT namapartner,username,`password`,tipe FROM witami.tb_partner  "
			 * + " WHERE TRIM(UPPER(username)) = TRIM(UPPER('"+
			 * txtUser.getText().toString().trim()+"')) " +
			 * " AND TRIM(UPPER(`password`)) = TRIM(UPPER('"
			 * +txtPass.getText().toString().trim()+"'))";
			 */
			/*
			 * String Sqlcheckadmin =
			 * "SELECT tb_partner.idpartner, tb_partner.namapartner,tb_partner.username,tb_partner.password,tb_group.tipe FROM tb_partner,  tb_group "
			 * + " WHERE tb_partner.idgroup = tb_group.id "+
			 * " AND TRIM(UPPER(tb_partner.username)) = TRIM(UPPER('"
			 * +txtUser.getText().toString().trim()+"')) "+
			 * " AND TRIM(UPPER(tb_partner.password)) = TRIM(UPPER('"
			 * +txtPass.getText().toString().trim()+
			 * "')) and `status` = 'active'";
			 */
			// System.out.println("SQL :"+Sqlcheckadmin);

			rs = st.executeQuery(Sqlcheckadmin);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

			if (rs.next()) {
				Date dtValid = rs.getDate("valid_until");

				// System.out.println(dtValid);
				try {
					if ((sdf.format(dtValid).equals(sdf.format(new Date()))) || dtValid.after(new Date())) {

						(Sessions.getCurrent()).setAttribute("name", rs.getString("name"));
						(Sessions.getCurrent()).setAttribute("username", rs.getString("user_name"));
						(Sessions.getCurrent()).setAttribute("tipe", rs.getString("type_user"));
						(Sessions.getCurrent()).setAttribute("id_user", rs.getString("id"));
						(Sessions.getCurrent()).setAttribute("idgroupakses", rs.getString("idgroupakses"));

						(Sessions.getCurrent()).setAttribute("id_storage", rs.getString("id_storage"));
						(Sessions.getCurrent()).setAttribute("tipe_storage", rs.getString("storage_type"));
						Clients.evalJavaScript("explode()");
						Executions.sendRedirect("/index.zul");

					} else {
						Clients.showNotification("Masa berlaku akun sudah lewat !", Clients.NOTIFICATION_TYPE_INFO,
								null, "middle_center", 2000);
					}
				} catch (NullPointerException ne) {
					Clients.showNotification("Masa berlaku akun belum disetting. Hubungi admin !",
							Clients.NOTIFICATION_TYPE_INFO, null, "middle_center", 2000);
				}

				// System.out.println("tipe :"+rs.getString("idgroupakses"));
				// zk.update("UPDATE witami.`tb_partner` SET counterblok = 0
				// WHERE username = '"+rs.getString("username")+"'");
				// setHistory(wnd, "tb_history_login", sesi.getUsername());
			}

		} catch (SQLException se) {
			Messagebox.show("error SQL : " + se.getMessage(), "ERROR", Messagebox.OK, Messagebox.ERROR);
			se.printStackTrace();
		} catch (Exception e) {
			Messagebox.show("error Exception : " + e.getMessage(), "ERROR", Messagebox.OK, Messagebox.ERROR);
			e.printStackTrace();
		} finally {
			try {
				rsStor.close();
				rs.close();
				st.close();
				Cn.close();
			} catch (Exception e) {
			}
		}
	}

	public void blockUser(Window wnd) {
		try {
			String vAcc = zk.getSingleValueAdet("SELECT virtualacc FROM witami.`tb_partner` where username = '"
					+ zk.getTextboxValue(wnd, "txtUser") + "'");
			zk.setLabelValue(wnd, "txtDisable", " " + zk.getTextboxValue(wnd, "txtUser") + " (" + vAcc + ")"
					+ " telah kami BLOKIR. Silahkan hubungi ####### / Datang ke Outlet WTM untuk mengaktifkan akun Anda kembali.");
			zk.setDivVisible(wnd, "error");
			// Clients.showNotification("Your Account is disable now.",
			// Clients.NOTIFICATION_TYPE_WARNING, wnd.getFellow("txtUser"),
			// "end_center", 5000, true);
			String username = zk.getTextboxValue(wnd, "txtUser");
			zk.update("UPDATE witami.`tb_partner` SET `status` = 'Blocked' WHERE username = '" + username + "'");
			zk.setTextboxValue(wnd, "txtUser", "");
			zk.setTextboxValue(wnd, "txtPass", "");
			zk.setTextboxFocus(wnd, "txtUser");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (Cn != null) {
				try {
					close_cn();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void clearReg(Window popup) {
		try {
			String sql = "SELECT 'L' AS ID, 'Laki-laki' AS JENIS UNION SELECT 'P' AS ID, 'Perempuan' AS JENIS";
			open_cn();
			zk.setListboxValue(popup, "L", "lstJK", sql, Cn, "");
			zk.setCheckboxValue(popup, "chkTerm", false);
			zk.setButtonDisabled(popup, "divbtn", true);
			zk.setTextboxValue(popup, "txtNamaLengkap", "");
			zk.setTextboxValue(popup, "txtVacc", "");
			zk.setTextboxValue(popup, "txtKoor", "");
			zk.setTextboxValue(popup, "txtTempatLahir", "");
			zk.setTextboxValue(popup, "txtAlamat", "");
			zk.setTextboxValue(popup, "txtNomorKTP", "");
			zk.setTextboxValue(popup, "txtWN", "");
			// zk.setTextboxValue(popup, "txtSumberDana", "");
			zk.setDateboxText(popup, "dtTglLahir", "");
			zk.setLabelValue(popup, "path", "");
			zk.setLabelValue(popup, "status", " file image (min. 500Kb)");
			zk.setTextboxFocus(popup, "txtNamaLengkap");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (!Cn.isClosed())
					close_cn();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	

	public void checkForm(Window wnd, Window popup, Window ver) {
		try {
			// int hasilkonfirmasi = 0;

			if (zk.getTextboxValue(popup, "txtNamaLengkap").trim().equals("".trim())) {
				zk.setTextboxFocus(popup, "txtNamaLengkap");
			} else if (zk.getTextboxValue(popup, "txtVacc").trim().equals("".trim())) {
				zk.setTextboxFocus(popup, "txtVacc");
			} else if (zk.getTextboxValue(popup, "txtKoor").trim().equals("".trim())) {
				zk.setTextboxFocus(popup, "txtKoor");
			} else if (zk.getTextboxValue(popup, "txtTempatLahir").trim().equals("".trim())) {
				zk.setTextboxFocus(popup, "txtTempatLahir");
			} else if (zk.getDateboxText(popup, "dtTglLahir").trim().equals("".trim())) {
				zk.setDateboxFocus(popup, "dtTglLahir");
			} else if (zk.getTextboxValue(popup, "txtAlamat").trim().equals("".trim())) {
				zk.setTextboxFocus(popup, "txtAlamat");
			} else if (zk.getTextboxValue(popup, "txtNomorKTP").trim().equals("".trim())) {
				zk.setTextboxFocus(popup, "txtNomorKTP");
			} else if (zk.getTextboxValue(popup, "txtWN").trim().equals("".trim())) {
				zk.setTextboxFocus(popup, "txtWN");
			}
			// else if(zk.getTextboxValue(popup,
			// "txtSumberDana").trim().equals("".trim()))
			// {
			// zk.setTextboxFocus(popup, "txtSumberDana");
			// }
			else if (zk.getLabelValue(popup, "path").equals("")) {
				Clients.showNotification("Please, upload an image.", Clients.NOTIFICATION_TYPE_WARNING,
						popup.getFellow("btnUpload"), "end_center", 5000, true);
			} else if (!zk.getTextboxValue(popup, "txtVacc").substring(0, 1).equals("0")
					&& !zk.getTextboxValue(popup, "txtVacc").substring(0, 2).equals("62")
					&& !zk.getTextboxValue(popup, "txtVacc").substring(0, 3).equals("+62")) {
				Clients.showNotification("No.HP yang Anda daftarkan salah.", Clients.NOTIFICATION_TYPE_ERROR,
						popup.getFellow("txtVacc"), "end_center", 5000, true);
				// Messagebox.show("No.HP yang Anda daftarkan salah.", "ERROR",
				// Messagebox.OK, Messagebox.ERROR);
				zk.setTextboxValue(popup, "txtVacc", "");
				zk.setTextboxFocus(popup, "txtVacc");
			} else if (zk.getCheckboxValue(popup, "chkTerm") == false) {
				Clients.showNotification("Anda belum menyetujui Syarat dan Ketentuan Layanan Witami Tunai Mandiri.",
						Clients.NOTIFICATION_TYPE_WARNING, popup.getFellow("chkTerm"), "start_center", 5000, true);
				// Messagebox.show("Anda belum menyetujui Syarat dan Ketentuan
				// Layanan Witami Tunai Mandiri.", "WARNING", Messagebox.OK,
				// Messagebox.EXCLAMATION);
			} else {
				if (checkSave(wnd, popup)) {
					if (checkKoor(wnd, popup)) {
						ver.doHighlighted();
					}
				}
				// hasilkonfirmasi = zk.showMessageQuestion("Konfirmasi", "Data
				// Sudah Benar?");
				// if(hasilkonfirmasi == 16)
				// {
				// //save(wnd, popup);
				// if(checkSave(wnd, popup))
				// {
				// ver.doHighlighted();
				// }
				// }
				// else
				// {
				// zk.setTextboxFocus(popup, "txtNamaLengkap");
				// }
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void sendSms(Window popup) {
		try {
			String msisdn = zk.getTextboxValue(popup, "txtVacc");

			if (msisdn.substring(0, 2).equals("62")) {
				msisdn = "0" + msisdn.substring(2);
			} else if (msisdn.substring(0, 3).equals("+62")) {
				msisdn = "0" + msisdn.substring(3);
			}

			String user = zk
					.getSingleValueAdet("SELECT username FROM witami.`tb_partner` WHERE virtualacc = '" + msisdn + "'");
			String password = zk.getSingleValueAdet(
					"SELECT `password` FROM witami.`tb_partner` WHERE virtualacc = '" + msisdn + "'");
			String pin = zk
					.getSingleValueAdet("SELECT pin FROM witami.`tb_partner` WHERE virtualacc = '" + msisdn + "'");
			String messagetouser = "SELAMAT BERGABUNG DENGAN WITAMI TUNAI MANDIRI. USERNAME: " + user + ". PASSWORD: "
					+ password + ". PIN: " + pin + ".";
			String TrxID = "ACT" + zk.getDateFormatNow("yyyy-MM-dd").replace("-", "") + zk.formatNumber(
					zk.getSingleValueInt("select `no` from witami.`no_urut` where `type` = 'active'"), "#000000");
			zk.update("UPDATE witami.`no_urut` SET `no` = `no`+1 WHERE `type` = 'active'");
			String sql = "INSERT INTO tb_mt(trxid,msisdn,message,id_account,displayweb) VALUES('" + TrxID + "','"
					+ msisdn + "','" + messagetouser + "','"
					+ zk.getSingleValueAdet(
							"SELECT idpartner FROM witami.`tb_partner` WHERE virtualacc = '" + msisdn + "'")
					+ "','NOK')";
			open_cn();
			zk.insertdata(sql, Cn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!Cn.isClosed())
					close_cn();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public boolean checkKoor(Window wnd, Window popup) {
		boolean status = true;
		try {
			String koor = zk.getTextboxValue(popup, "txtKoor");

			if (koor.length() >= 5) {
				if (zk.isKoorAvail(koor) == true) {
					String statusKoor = zk.getSingleValueAdet(
							"SELECT `status` FROM witami.`tb_partner` WHERE virtualacc = '" + koor + "'");
					String idgrup = zk.getSingleValueAdet(
							"SELECT idgroup FROM witami.`tb_partner` WHERE virtualacc = '" + koor + "'");
					if (idgrup.equals("3") || idgrup.equals("5") || idgrup.equals("6")) {
						if (statusKoor.equalsIgnoreCase("inactive") || statusKoor.equalsIgnoreCase("archived")) {
							Clients.showNotification("Koordinator/Dealer tidak aktif.",
									Clients.NOTIFICATION_TYPE_WARNING, popup.getFellow("txtKoor"), "end_center", 5000,
									true);
							// Messagebox.show("Koordinator/Dealer belum
							// diaktifkan.", "ERROR", Messagebox.OK,
							// Messagebox.ERROR);
							zk.setTextboxFocus(popup, "txtKoor");
							status = false;
						} else if (statusKoor.equalsIgnoreCase("blocked")) {
							Clients.showNotification("Koordinator/Dealer sedang diblokir.",
									Clients.NOTIFICATION_TYPE_WARNING, popup.getFellow("txtKoor"), "end_center", 5000,
									true);
							// Messagebox.show("Koordinator/Dealer sedang
							// disable.", "ERROR", Messagebox.OK,
							// Messagebox.ERROR);
							zk.setTextboxFocus(popup, "txtKoor");
							status = false;
						} else {
							status = true;
						}
					} else {
						Clients.showNotification("Koordinator/Dealer tidak terdaftar.", Clients.NOTIFICATION_TYPE_ERROR,
								popup.getFellow("txtKoor"), "end_center", 5000, true);
						// Messagebox.show("Koordinator/Dealer tidak
						// terdaftar.", "ERROR", Messagebox.OK,
						// Messagebox.ERROR);
						zk.setTextboxFocus(popup, "txtKoor");
						status = false;
					}

				}

				else if (zk.isKoorAvail(koor) == false) {
					Clients.showNotification("Koordinator/Dealer tidak terdaftar.", Clients.NOTIFICATION_TYPE_ERROR,
							popup.getFellow("txtKoor"), "end_center", 5000, true);
					// Messagebox.show("Koordinator/Dealer tidak terdaftar.",
					// "ERROR", 0, Messagebox.ERROR);
					zk.setTextboxFocus(popup, "txtKoor");
					status = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (!Cn.isClosed())
					close_cn();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return status;
	}

	public boolean checkSave(Window wnd, Window popup) {
		boolean status = true;
		try {

			boolean status2 = true;

			String nama = zk.getTextboxValue(popup, "txtNamaLengkap");
			String vacc = zk.getTextboxValue(popup, "txtVacc");
			String koor = zk.getTextboxValue(popup, "txtKoor");
			String sql = "SELECT virtualacc FROM witami.`tb_partner`";
			open_cn();
			Statement st = Cn.createStatement();
			ResultSet rs = st.executeQuery(sql);

			if (nama.length() < 4) {
				Clients.showNotification("Minimal 4 karakter.", Clients.NOTIFICATION_TYPE_ERROR,
						popup.getFellow("txtNamaLengkap"), "end_center", 5000, true);
				zk.setTextboxFocus(popup, "txtNamaLengkap");
				status2 = false;
			}

			else if (vacc.length() < 5) {
				Clients.showNotification("Nomor invalid.", Clients.NOTIFICATION_TYPE_ERROR, popup.getFellow("txtVacc"),
						"end_center", 5000, true);
				zk.setTextboxFocus(popup, "txtVacc");
				status2 = false;
			}

			else if (koor.length() < 5) {
				Clients.showNotification("Nomor invalid.", Clients.NOTIFICATION_TYPE_ERROR, popup.getFellow("txtKoor"),
						"end_center", 5000, true);
				zk.setTextboxFocus(popup, "txtKoor");
				status2 = false;
			}

			else {
				while (rs.next()) {
					if (vacc.equals(rs.getString(1))) {
						Clients.showNotification("Nomor sudah terdaftar.", Clients.NOTIFICATION_TYPE_ERROR,
								popup.getFellow("txtVacc"), "end_center", 5000, true);
						zk.setTextboxFocus(popup, "txtVacc");
						status2 = false;
						break;
					} else
						status2 = true;
				}

			}

			status = status && status2;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				close_cn();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return status;
	}

	public void prosesUpload(Window wnd, Window popup, Media media) {
		try {
			String last = fnBSK.randomAlphaNumeric(5);
			String name = last + "." + media.getFormat();
			String path = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/");
			// System.out.println("Path :"+path);
			String Realpath = path + "temp/";

			Files.copy(new File(Realpath + name), media.getStreamData());

			// File nFile = new File(Realpath + media.getName());

			zk.setLabelValue(popup, "path", "/temp/" + name);
			zk.setLabelValue(popup, "status", " " + media.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setHistory(Window wnd, String table, String username) {
		try {
			// URL whatismyip = new URL("http://checkip.amazonaws.com/");
			// BufferedReader in = new BufferedReader(new
			// InputStreamReader(whatismyip.openStream()));
			// String ip = in.readLine();

			String ip = sess.getRemoteAddr();
			String agent = Executions.getCurrent().getHeader("user-agent");

			// String username = sesi.getUsername();

			String login = zk.getDateFormatNow("yyyy-MM-dd HH:mm:ss");

			String sql = "INSERT INTO witami.`" + table + "` (ip, username, time_login, user_agent) VALUES ('" + ip
					+ "', '" + username + "', '" + login + "', '" + agent + "')";
			open_cn();
			zk.insertdata(sql, Cn);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (Cn != null) {
				try {
					close_cn();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void setKonten(Window wnd) {
		zk.setHtmlContent(wnd, "htmlTerm", zk.getSingleValueAdet("SELECT isi FROM witami.`tb_tnc`"));
	}

	public void setCek(Window register, Window term) {
		try {
			Checkbox chk = (Checkbox) register.getFellow("chkTerm");
			// System.out.println("chk value:"+chk.isChecked());
			chk.setChecked(false);
			chk.setChecked(true);
			System.out.println("chk value:" + chk.isChecked());
			// zk.setCheckboxValue(register, "chkTerm", true);
			term.setVisible(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
