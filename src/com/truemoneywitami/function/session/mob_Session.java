package com.truemoneywitami.function.session;
import java.sql.Connection;

import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.DBConnection;
import com.truemoneywitami.function.ZKFunction;

public class mob_Session {
	protected String username="";
	protected String password="";
	protected String pin="";
	protected String admin="";
	protected String currency="";
	protected String tipe="";
	protected String email =""; 
	protected String idoperator = "";
	
	
	
	protected boolean IsSession = false;
	DBConnection dbconnect = new DBConnection();
	ZKFunction fn = new ZKFunction();
	
	public boolean getSession(String atorisasi) throws Exception{
		if(
				(Sessions.getCurrent()).getAttribute("username")!=null &&
				(Sessions.getCurrent()).getAttribute("password")!=null &&
				(Sessions.getCurrent()).getAttribute("type")!=null){
			username =  (Sessions.getCurrent()).getAttribute("username").toString();
			password =  (Sessions.getCurrent()).getAttribute("password").toString();
			tipe = (Sessions.getCurrent()).getAttribute("type").toString();
//			String sql = "select count(*) as jum from thk_db.tab_user WHERE username='"+username+"' AND PASSWORD='"+password+"' AND tipe='"+atorisasi+"' ";
//		if(tipe.equals("Admin"))
//			System.out.println("tipe :"+tipe);
		if(tipe.equals("admin"))
		{
			String sql = "SELECT COUNT(*) AS jum FROM vision.tb_user WHERE UPPER(user_name) = UPPER('"+ username +"') AND UPPER(PASSWORD) = UPPER('"+ password+"') ";
//			System.out.println("sqlSession :"+sql);
			Connection Cn = dbconnect.getConnection();
			if(fn.getSingleValue("jum",sql, Cn).equals("0")){
				IsSession = false;
			}else{
				IsSession = true;
			}
		}else{
			//tipe hotel
			
		}
		
		
		}else{
			IsSession = false;
		}
		return IsSession;
	}
	
	public boolean getAmsSession() throws Exception{
		if((Sessions.getCurrent()).getAttribute("username")!=null ){
			IsSession = true;
		}else{
			IsSession = false;
		}
		return IsSession;
	}
	
	public boolean getAdminSession() throws Exception{
		if(
			(Sessions.getCurrent()).getAttribute("admin")!=null &&	
			(Sessions.getCurrent()).getAttribute("tipe")!=null
		){		
			IsSession = true;			
		}else{
			IsSession = false;
		}
		return IsSession;
	}
	
	public String getsessionparameter(String sessionname){
		String result = "";
		
		result = (String) (Sessions.getCurrent()).getAttribute(sessionname);
		return result;
	}
	
	 public boolean getSession() throws Exception{
		 // Messagebox.show("Session username" + (Sessions.getCurrent()).getAttribute("username").toString());
		  if(
		   (Sessions.getCurrent()).getAttribute("username")!=null &&
		   (Sessions.getCurrent()).getAttribute("tipe")!=null 



		  ){  
		   IsSession = true;   
		  }else{
		   IsSession = false;
		   //seharusnya false
		  }
		  return IsSession;
		 }
	 
	public String getTipe() {
		  tipe = (String) (Sessions.getCurrent()).getAttribute("type");
		  return tipe;
		 }
		 
		 public String getUsername() {
		  username = (String) (Sessions.getCurrent()).getAttribute("username");
		  return username;
		 }
		 
		 public String getEmail() {
		  email = (String) (Sessions.getCurrent()).getAttribute("email");
		  return email;
		 }
		 
//		 public String getIdOperator() {
//		  idoperator = (String) (Sessions.getCurrent()).getAttribute("idoperator");
//		  return idoperator;
//		 }
	
//		 public String getSendername() {
//			 String sender_name = (String) (Sessions.getCurrent()).getAttribute("sender_name");
//			  return username;
//		 }
	
//		 public String[] gettransferPosData(){
//			 String[] data={};
//			  data[0] = (String) (Sessions.getCurrent()).getAttribute("sender_name");
//			  data[1] = (String) (Sessions.getCurrent()).getAttribute("sender_HP");
//			  data[2] = (String) (Sessions.getCurrent()).getAttribute("sender_add");
//			  data[3] = (String) (Sessions.getCurrent()).getAttribute("ben_name");
//			  data[4] = (String) (Sessions.getCurrent()).getAttribute("ben_HP");
//			  data[5] = (String) (Sessions.getCurrent()).getAttribute("ben_add");
//			  data[6] = (String) (Sessions.getCurrent()).getAttribute("amount");
//			  data[7] = (String) (Sessions.getCurrent()).getAttribute("ket");
//			  data[8] = (String) (Sessions.getCurrent()).getAttribute("admin");
//			  
//			  return data;
//		 }
		 
		 
//		 public String getIdTujuanTrue(){
//			  String id_tujuan = (String) (Sessions.getCurrent()).getAttribute("noRekeningTujuan");
//			  return id_tujuan;
//		 }
//		 
//		 public String getnamaTujuanTrue(){
//			  String nama_tujuan = (String) (Sessions.getCurrent()).getAttribute("namaAccountTujuan");
//			  return nama_tujuan;
//		 }
//		 
//		 public String getnominal(){
//			  String nominal = (String) (Sessions.getCurrent()).getAttribute("nominal");
//			  return nominal;
//		 }
//		 
//		 public String getIssue(){
//			  String issue = (String) (Sessions.getCurrent()).getAttribute("issue");
//			  return issue;
//		 }
		 
//		 public String getBiaya(){
//			  String biaya = (String) (Sessions.getCurrent()).getAttribute("biaya");
//			  return biaya;
//		 }
//		 
//		 public String getSenderName(){
//			  String biaya = (String) (Sessions.getCurrent()).getAttribute("sender_name");
//			  return biaya;
//		 }
//		 
//		 public String getSenderCell(){
//			  String biaya = (String) (Sessions.getCurrent()).getAttribute("sender_cell");
//			  return biaya;
//		 }
//		 
//		 public String getSenderadd(){
//			  String biaya = (String) (Sessions.getCurrent()).getAttribute("sender_add");
//			  return biaya;
//		 }
//		 
//		 public String getBenCell(){
//			  String biaya = (String) (Sessions.getCurrent()).getAttribute("ben_cell");
//			  return biaya;
//		 }
//		 
//		 public String getBank(){
//			  String biaya = (String) (Sessions.getCurrent()).getAttribute("bank");
//			  return biaya;
//		 }
	
	public String getPassword() {
		password = (String) (Sessions.getCurrent()).getAttribute("password");
		return password;
	}
	
	public String getPin() {
		pin = (String) (Sessions.getCurrent()).getAttribute("pin");
		return pin;
	}

	public boolean IsSession() {
		return IsSession;
	}

	public void setSession(String userSesion){
		(Sessions.getCurrent()).setAttribute("username",userSesion);  
	}
	
	public String getIdgrup()
	{
		String hasil = "";
		try {
			hasil = fn.getSingleValueAdet("SELECT idgroup FROM vision.`tb_user` WHERE user_name = '"+getUsername()+"'");
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return hasil;
	}

	public void setSessionAttribute(String attributeName, String sessionValue){
		  (Sessions.getCurrent()).setAttribute(attributeName,sessionValue);  
	}
	
	public void setWindowAttribute (String name, Window wnd){
		(Sessions.getCurrent()).setAttribute(name,wnd);
	}
		
	public Window getWindowAttribute(String name) throws Exception{
		
		Window wnd = (Window) Sessions.getCurrent().getAttribute(name);
		return wnd;
	}
	
		 public String getSessionAttribute(String attribute) throws Exception{
		  String value = (String) Sessions.getCurrent().getAttribute(attribute);
		  return value;
		 }
}
