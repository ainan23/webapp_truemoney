package com.truemoneywitami.function;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Executions;

public class Cookies {
	
	private static Logger log = Logger.getLogger(Cookies.class);
	
	public static void setCookie(String username, String value) {
		HttpServletResponse servletResponse = 	((HttpServletResponse) Executions.getCurrent().getNativeResponse());
		servletResponse.addCookie(new Cookie(
				username, value));
		log.info("setCookie>>"+username+"-->"+value);
	}

	public static String getCookie(String username) {
		Cookie[] cookies = ((HttpServletRequest) Executions.getCurrent().getNativeRequest())
				.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(username)) {
					log.info("getCookie>>"+username +"-->"+cookie.getValue());
					return cookie.getValue();
				}
			}
		}
		return null;
	}
	
}
