package com.truemoneywitami.function;

import org.apache.log4j.Logger;


/**
 * Created by adetiamarhadi on 8/10/2016.
 */
public class NullValidator {

    private static Logger log = Logger.getLogger(NullValidator.class);

    public String valid(String value) {
        String hasil = "";
        try {
            if (value.trim().length() == 0 || value == null) {
                hasil = "";
            } else {
                hasil = value;
            }
        } catch (Exception e) {
            log.error("", e);
            e.printStackTrace();
            hasil = "";
        }
        return hasil;
    }
}
