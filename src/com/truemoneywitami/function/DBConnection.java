package com.truemoneywitami.function;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class DBConnection {
	
	private static Logger log = Logger.getLogger(DBConnection.class);
	
	Connection Cn = null;
	Statement st = null;
	ResultSet rs = null;
	
	public Connection getConnection() throws Exception{
		java.sql.Connection Conn = null;
		
		try {
			
//			String userName = "postgres";
//		    String password = "postgres";
//		    String url = "jdbc:postgresql://localhost:5432/postgres";
			
			String userName = "postgres";
		    String password = "";
		    String url = "jdbc:postgresql://192.168.201.2:5432/TMN";
		
		    Class.forName("org.postgresql.Driver");
		    Conn = DriverManager.getConnection (url, userName, password);
		    Conn.setAutoCommit(false);
		    
		}catch (Exception e) {
			log.info(e.getMessage() + " -> " + e);
			// e.printStackTrace();
			log.info("Error Koneksi database : " + e.getMessage());
		}
		System.out.println("Database has been connected");
		return Conn;		
	}
	
	
	public Connection getConnectionLocal() throws Exception{
		java.sql.Connection Conn = null;
		
		try {
			String userName = "postgres";
		    String password = "postgres";
		    String url = "jdbc:postgresql://localhost:5432/postgres";
			
		    Class.forName("org.postgresql.Driver");
		    Conn = DriverManager.getConnection (url, userName, password);

		}catch (Exception e) {
			log.info(e.getMessage() + " -> " + e);
			log.info("Error Koneksi : " + e.getMessage());
		}
		return Conn;
		
	}
	
	public void closeConnection(Connection Cn, Statement st, ResultSet rs) throws Exception{
		if(Cn != null){Cn.close();}
		if(st != null){st.close();}
		if(rs != null){rs.close();}
	}
	
	public static void main(String[] args) throws Exception {
		DBConnection dbCon = new DBConnection();
		dbCon.getConnection();
	}
    
}
