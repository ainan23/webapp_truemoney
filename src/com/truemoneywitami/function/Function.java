package com.truemoneywitami.function;

import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONObject;

public class Function {
	
	private static Logger log = Logger.getLogger(Function.class);

	public String sha1(String input) {
		String hasil = "";
		try {
			MessageDigest mDigest = MessageDigest.getInstance("SHA1");
	        byte[] result = mDigest.digest(input.getBytes());
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < result.length; i++) {
	            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
	        }
	        hasil = sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
        
         
        return hasil;
    }
	
	public String getFormatRp(String value) {
		String hasil = "";
		try {
			Double nominal = Double.parseDouble(value);
			NumberFormat kurensiEropa = NumberFormat.getCurrencyInstance(Locale.US);
	        hasil = String.format("%s %n", kurensiEropa.format(nominal).replace("$","Rp"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hasil;
	}
	
	public String getFormatTitik(String value) {
		String hasil = "";
		try {
			String pattern = ",###";
			DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
			symbols.setGroupingSeparator('.');
			
			DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
			
			String number = decimalFormat.format(Double.parseDouble(value));
			
			hasil = number;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hasil;
	}
	
	public String normalisasiJsonResponse(JSONObject json, String keyJson, String valDefault) {
		String hasil = "";
		try {
			hasil = json.get(keyJson).toString();
		} catch (Exception e) {
			log.error("key json "+keyJson+" is null." );
			hasil = valDefault;
		}
		return hasil;
	}
	
	public static void main (String [] args) {
		Function func = new Function();
		System.out.println("hasil -> " + func.getFormatTitik(""+350000));
	}

}
