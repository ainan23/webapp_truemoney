package com.truemoneywitami.function;

import org.zkoss.zul.Window;

public class Constants {
	public static final String MSG_TIMEOUT_ERROR = "Transaksi Timeout.";
	
	public static final String ref = "ref";
	
	public static final String accountType = "";
	public static final String body = "body";
	//set path file to include in menu body
	public static final String memberHeaderPath = "com.truemoney.view/header.zul";
	public static final String adminHeaderPath = "com.truemoney.view/adminheader.zul";
	
	public static final String setorTunaiPath = "com.truemoney.view/setordantariktunai/cashin.zul";
	public static final String konfirmasiSetorTunaiPath = "com.truemoney.view/setordantariktunai/konfirmasisetortunai.zul";
	public static final String tarikTunaiPath = "com.truemoney.view/setordantariktunai/tariktunai.zul";
	public static final String konfirmasiTarikTunaiPath = "com.truemoney.view/setordantariktunai/konfirmasitariktunai.zul";
	
	public static final String tambahdeposit = "com.truemoney.view/deposit/tambahdeposit.zul";
	public static final String Konfirmasitambahdeposit = "com.truemoney.view/deposit/tambahdepositkonfirmasi.zul";
	public static final String tambahdeposittiket = "com.truemoney.view/deposit/tambahdeposittiket.zul";
	public static final String transfertruehome = "com.truemoney.view/transfer/transferTrue.zul"; 
	public static final String transferrekening = "com.truemoney.view/transfer/transferTrueRekening.zul";
	public static final String transfertruekonfirm = "com.truemoney.view/transfer/transferTrueKonfirmasi.zul";
	
	public static final String transfersuccess = "com.truemoney.view/transfer/TransferTrueSukses.zul";
	public static final String transferfail = "com.truemoney.view/transaksiGagal.zul";
	
	public static final String transferbank = "com.truemoney.view/transferBank/transferTrueBank.zul";
	public static final String transferbankkonfirm = "com.truemoney.view/transferBank/transferBankKonfirmasi.zul";
	
	public static final String cairkansaldo = "com.truemoney.view/transferBank/transferTrueBankAgent.zul";
	public static final String transferbankkonfirmagent = "com.truemoney.view/transferBank/transferBankKonfirmasiAgent.zul";
	
	public static final String transferposhome = "com.truemoney.view/transferpos/transferPos.zul";
	public static final String transferposkonfirm = "com.truemoney.view/transferpos/transferPosKonf.zul";
	
	public static final String inforekeningagen = "com.truemoney.view/inforekening/inforekeningagen.zul";
	public static final String inforekeningmember = "com.truemoney.view/inforekening/inforekeningmember.zul";
	public static final String komisi = "com.truemoney.view/infokomisi/infokomisi.zul";
	
	public static final String adminprofile = "com.truemoney.view/profile/adminprofileagen.zul";
	public static final String memberprofile = "com.truemoney.view/profile/viewmemberprofile.zul";
	public static final String agenprofileedit = "com.truemoney.view/profile/adminprofileagenedit.zul";
	public static final String memberprofileedit ="com.truemoney.view/profile/viewmemberprofileedit.zul";
	
	public static final String pembelianpulsa = "com.truemoney.view/pembelian/pulsa/pembelianpulsa.zul";
	public static final String pembeliangames = "com.truemoney.view/pembelian/games/produk.zul";
	public static final String pembeliantv = "com.truemoney.view/pembelian/tv/pembelian.zul";
	public static final String Konfirmasipembelianpulsa ="com.truemoney.view/pembelian/pulsa/pembelianpulsakonfirmasi.zul";
	public static final String pembelianpln="com.truemoney.view/pembelian/pln/pembelianpln.zul";
	public static final String Konfirmasipembelianpln = "com.truemoney.view/pembelian/pln/pembelianplnkonfirmasi.zul";
	public static final String Konfirmasipembeliantv = "com.truemoney.view/pembelian/tv/konfirmasi.zul";
	
	public static final String pembayaranpln = "com.truemoney.view/pembayaran/pln/pembayaranpln.zul";
	public static final String Konfirmasipembayaranpln="com.truemoney.view/pembayaran/pln/konfirmasipembayaranpln.zul";
	public static final String pembayaranpdam="com.truemoney.view/pembayaran/pdam/pembayaranpdam.zul";
	public static final String Konfirmasipembayaranpdam="com.truemoney.view/pembayaran/pdam/konfirmasipembayaranpdam.zul";
	public static final String pembayarantelepon="com.truemoney.view/pembayaran/telepon/pembayarantelepon.zul";
	public static final String Konfirmasipembayarantelepon="com.truemoney.view/pembayaran/telepon/konfirmasipembayarantelepon.zul";
	public static final String pembayaraninternet="com.truemoney.view/pembayaran/internet/pembayaraninternet.zul";
	public static final String Konfirmasipembayaraninternet="com.truemoney.view/pembayaran/internet/konfirmasipembayaraninternet.zul";
	public static final String pembayaranbpjs="com.truemoney.view/pembayaran/bpjs/pembayaranbpjs.zul";
	public static final String pembayaranmultifinance="com.truemoney.view/pembayaran/multifinance/pembayaran_multifinance.zul";
	public static final String Konfirmasipembayaranbpjs="com.truemoney.view/pembayaran/bpjs/konfirmasipembayaranbpjs.zul";
	public static final String Konfirmasipembayaranmf="com.truemoney.view/pembayaran/multifinance/konfirmasi_multifinance.zul";
	public static final String pembayarantv="com.truemoney.view/pembayaran/tvkabel/pembayarantv.zul";
	public static final String Konfirmasipembayarantv="com.truemoney.view/pembayaran/tvkabel/konfirmasipembayarantv.zul";
	
	public static final String home = "com.truemoney.view/inforekening/inforekeningagen.zul";
	
	public static final String contact = "com.truemoney.view/admin_contact.zul";
	
	public static final String pembayaranpulsa="com.truemoney.view/pembayaran/pulsa/pembayaranpulsa.zul";
	public static final String Konfirmasipembayaranpulsa="com.truemoney.view/pembayaran/pulsa/konfirmasipembayaranpulsa.zul";
	public static final String pembayaranpulsapasca = "com.truemoney.view/pembayaran/pulsa/pembayaranpulsa.zul";
	
	public static final String signup = "signup_successMember.zul";
	public static final String signupagen = "signup_successAgen.zul";
	public static final String signin = "com.truemoney.view/login.zul";
	public static final String uploadFoto = "upload.zul";
	public static final String allmessages ="com.truemoney.view/message/message.zul";
	public static final String allnotif = "com.truemoney.view/message/notifikasi.zul";
	public static final String changepassword ="com.truemoney.view/administrator/adminpasswordmember.zul";
	public static final String changepin = "com.truemoney.view/administrator/changepin.zul";
	//General Success
	public static final String SuccessPaymentpath = "com.truemoney.view/pembayaransukses.zul";
	public static final String bpjsSuccessPaymentpath = "com.truemoney.view/pembayaran/bpjs/pembayaransukses.zul";
	public static final String failedgeneralpath = "com.truemoney.view/transaksiGagal.zul";
	public static final String Successbuypath = "com.truemoney.view/pembeliansukses.zul";
	public static final String cash ="com.truemoney.view/CashSukses.zul";
	public static final String banksukses ="com.truemoney.view/transferBank/TransferBankSukses.zul";
	public static final String possukses ="com.truemoney.view/transferpos/TransferPosSukses.zul";
	// fix number
	public static final long minamount = 20000;
	public static final long maxamount = 5000000;
	public static final long MAX_AMOUNT_BANK_MEMBER = 4992500;
	
//	public static final long minAmountCashout = 10000;
//	public static final long maxAmountCashout = 5000000;
	
	//Error message for several errorr
	public static final String generalErrorMessage = "Server sedang bermasalah";
	public static final String agentAccountBlocked = "Akun terblokir. Silakan hubungi Customer Care 0804 100 100.";
	public static final String memberAccountBlocked = "Akun terblokir. Silakan hubungi Customer Care 0804 100 100.";
	public static final String saldoTidakCukup = "Saldo tidak mencukupi. Silakan pilih nominal yang lebih kecil.";
	public static final String pinSalah = "Kode PIN salah. Silakan ulangi kembali.";
	public static final String rekeningTidakTerdaftar = "Nomor rekening tidak terdaftar";
	public static final String failedRequest = "Permintaan gagal. Silakan ulangi kembali.";
	public static final String beneficiariNewFailed = "Akun Penerima tidak dapat melakukan layanan ini.";
	public static final String senderNewFailed = "Akun Anda tidak dapat melakukan layanan ini.";
	
	public static final String tipePesan = "tipePesan";
	public static final String pesan = "pesan";
	public static final String header="header";
	public static final String tipe="tipe";
	//set window for include menu
	public static  Window windowBody = null;
	//create object to transfer value between zul file
	public static final String namaAccountTujuan = "namaAccountTujuan";
	public static final String noRekeningTujuan = "noRekeningTujuan";
	public static final String nominal = "nominal";
//	public static String issue = "issue";
	public static final String biaya = "biaya";
	public static final String sender_name = "sender_name";
	public static final String sender_cell = "sender_cell";
	public static final String sender_add = "sender_add";
	public static final String ben_cell = "ben_cell";
	public static final String bank = "bank";
	public static final String ben_add = "ben_add";
//	public static String msgArr = "messageArray";
	public static final String id_pelanggan = "id_pelanggan";
	public static final String namaPelanggan="namaPelanggan";
	public static final String tagihan ="tagihan";
	public static final String total="total";
	public static final String id_account="id_account";
	public static final String email="email";
	public static final String tipeUser="tipeUser";
	public static final String handPhone="handPhone";
	public static final String id_trx="id_trx";
	public static final String daya= "daya";
	public static final String lwbp="lwbp";
	public static final String periode ="periode";
	public static final String nama="nama";
	public static final String id_agent ="id_agent";
	public static final String id_member ="id_member";
	public static final String status= "status";
	public static final String waktu = "waktu";
	public static final String issue = "issue";
	public static final String ktp = "ktp";
	public static final String inquiry = "inquiry";
	public static final String namaPengirim = "namaPengirim";
	public static final String noRekeningPengirim = "noRekeningPengirim";
	public static final String keterangan = "keterangan";
	// FIX Operator id
	public static final String plnpasca = "37";
	public static final String alamatPengirim = "alamatPengirim";
	public static final String alamatPenerima = "alamatPenerima";
	public static final String noHpPenerima = "noHpPenerima";
	public static final String id_transaksi = "id_transaksi";
	public static final String idTransaksi = "idTransaksi";
	
	public static final String TERBILANG = "terbilang";
	public static final String PERIODE_ARR = "periode_list";
	public static final String TANGGAL = "tanggal";
	public static final String JUMLAH_ADMIN = "jml_admin";
	public static final String STAND_METER = "stand_meter";
	public static final String PDAM_NAME = "pdam_name";
	public static final String NO_RESI = "no_resi";
	public static final String NON_TAG_AIR = "non_tag_air";
	public static final String ALAMAT = "alamat";
	public static final String PEMAKAIAN = "pemakaian";
	public static final String JUMLAH_BAYAR = "jml_bayar";
	public static final String JUMLAH_TAGIHAN = "jml_tagihan";
	public static final String NAMA_PELANGGAN = "nama_pelanggan";
	public static final String BEBAN = "beban";
	public static final String DENDA = "denda";
	public static final String NOMINAL_STRUK = "nominal_struk";
	
	public static final int TIMER_NOTIF = 4000;
	public static final String POSITION_NOTIF = "middle_center";
}



