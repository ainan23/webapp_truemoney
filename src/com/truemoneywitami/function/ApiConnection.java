package com.truemoneywitami.function;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.truemoneywitami.dto.ResponseApiDto;

public class ApiConnection {
	
//	private static String apiServerUrl = "http://localhost:8080/ApiWebAndroid/parsingJSON.jsp";
//	private static String apiServerUrl = "http://localhost:999/ApiWebAndroid/parsingJSON.jsp";
//	private static String apiServerUrl = "http://localhost:8081/web_api/parsingJSON.jsp";
//	private static String apiServerUrl = "http://localhost:8080/API_Website_Mobile/parsingJSON.jsp";
//	private static String apiServerUrl = "http://192.168.201.2:999/web_api/parsingJSON.jsp";

	
//	private static Logger log = Logger.getLogger(ApiConnection.class);
	
	private static Logger log = Logger.getLogger(ApiConnection.class);
	
	
	
	public static String curl(String data) {
		
		String hasil = "";

		  try {
			  
			  Gson gson = new Gson();
			  
			ResourceBundle rb = ResourceBundle.getBundle("config.config");

			URL url = new URL(rb.getString("url"));
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setConnectTimeout(Integer.parseInt(""+30000));
			conn.setReadTimeout(Integer.parseInt(""+30000));

			String input = data;

			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();

//			if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
//				throw new RuntimeException("Failed : HTTP error code : "
//					+ conn.getResponseCode());
//			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String output;
			System.out.println("Output from Server ....");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				hasil = output;
			}
			
			String code = String.valueOf(conn.getResponseCode());
			
			ResponseApiDto dto = new ResponseApiDto();
			dto.setHttpcode(code);
			dto.setData(hasil);
			
			hasil = gson.toJson(dto);

			conn.disconnect();

		  } catch (Exception e) {
			  
			  Gson gson = new Gson();
			  
			  ResponseApiDto pesan = new ResponseApiDto();
			  pesan.setPesan("Tidak ada response dari Server.");
			  
			  ResponseApiDto dto = new ResponseApiDto();
			  dto.setHttpcode("500");
			  dto.setData(gson.toJson(pesan));
				
			  hasil = gson.toJson(dto);

			e.printStackTrace();
			log.error("api connection", e);

		 }
		  
		  return hasil;

		}



    public static void curl_post(String data, boolean encode)throws Exception {
       log.warn("REQ -> " +data);
       ResourceBundle rb = ResourceBundle.getBundle("config.config");
//       JSONObject responseObj = null;
        try {
            BufferedReader rd = null;
            String line = null;
            if (encode) {
                data = URLEncoder.encode(data.toString(), "UTF-8");
            }
            HttpURLConnection con = (HttpURLConnection) new URL(rb.getString("url")).openConnection();
            
//            HttpURLConnection con = (HttpURLConnection) new URL(apiServerUrl).openConnection();
            con.setConnectTimeout(Integer.parseInt(rb.getString("time_out")));
            con.setReadTimeout(Integer.parseInt(rb.getString("time_out")));
            con.setRequestMethod("POST");
//            con.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
            con.setRequestProperty("Content-Type","application/json");
//
//            con.setRequestProperty("Content-Length","" + Integer.toString(data.getBytes().length));
//            con.setRequestProperty("Content-Language", "en-US");

            con.setUseCaches(false);
            con.setDoInput(true);
            con.setDoOutput(true);
            // Send request
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();

            String code = String.valueOf(con.getResponseCode());
            log.warn("RES HTTPCODE -> " +code);
            // Get Response
            InputStream is = con.getInputStream();
            rd = new BufferedReader(new InputStreamReader(is));

            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
//            JSONParser parser = new JSONParser();
//            Object obj = parser.parse(response.toString());
//            responseObj = (JSONObject) obj;

        } catch (SocketTimeoutException e) {
        	e.printStackTrace();
//        	responseObj.put("ACK", "NOK");
//        	responseObj.put("pesan", "Koneksi Gagal : Silahkan Cek status transaksi di Menu Mutasi Rekening.");
//        	return responseObj.toString();
//        	String json = "{\"ACK\":\"NOK\", \"pesan\":\"Koneksi Gagal : Silahkan Cek status transaksi di Menu Informasi Rekening.\"}";
//        	JSONParser parser = new JSONParser();
//            Object obj = parser.parse(json);
//            responseObj = (JSONObject) obj;
//            return (responseObj.toString());
        } catch (Exception e) {
        	e.printStackTrace();
//            e.printStackTrace();
//        	log.info(e.getMessage() + " -> " + e);
//            Clients.showNotification(Constants.generalErrorMessage);
        }
//        return (responseObj.toString());
    }
}
