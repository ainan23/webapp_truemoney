package com.truemoneywitami.function;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONObject;
import org.zkoss.json.parser.JSONParser;
import org.zkoss.zk.ui.util.Clients;

public class ProcessingListenerPrabayar {
	private static Logger log = Logger.getLogger(ProcessingListenerPrabayar.class);
	
	private ServerConnectionPrabayar serverConnection;
	private RequestListener rListener;
	private String data;

	public ProcessingListenerPrabayar(RequestListener rListener, String data) {
		// TODO Auto-generated constructor stub
		serverConnection = new ServerConnectionPrabayar();
		this.rListener = rListener;
		this.data = data;
	}

	public void processData() throws Exception {
		try  {

//			for (int i = 0; i < 2; i++) {
//				if (i == 0) {
					Clients.showBusy("On Progress...");
//				} else {
					String responseString = serverConnection.curl_post(data,false);
//					System.out.println("RES -----> "+responseString);
					if(!data.contains("CountUnread")) {
						log.info("REQ -----> "+data);
					} 
					
					if (!responseString.contains("countNotif")) {
						log.info("RES -----> "+responseString);
					}
					
					if (!responseString.isEmpty()) {
						JSONParser parser = new JSONParser();
						Object obj = parser.parse(responseString);
						JSONObject responseObj = (JSONObject) obj;
						String ack = responseObj.get("ACK").toString();
						if (ack.equalsIgnoreCase("OK")) {
//							Clients.clearBusy();
							rListener.requestSucceeded(responseObj);
						} else {
//							Clients.clearBusy();
							String message = responseObj.get("pesan").toString();
							rListener.requestFailed(message);
						}
					} else {
//						Clients.clearBusy();
//						rListener.requestFailed(Constants.generalErrorMessage);
						rListener.requestFailed(Constants.failedRequest);
					}
//				}
//			}
		} catch (Exception e) {
			e.printStackTrace();
			rListener.requestFailed(Constants.failedRequest);
			log.info(e.getMessage() + " -> " + e);
			// TODO Auto-generated catch block
//			Clients.clearBusy();
//			rListener.requestFailed(Constants.generalErrorMessage);
			
		} finally {
			Clients.clearBusy();
		}
		
	}
}
