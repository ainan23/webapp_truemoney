package com.truemoneywitami.function;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Path;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Include;
import org.zkoss.zul.Window;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.json.parser.JSONParser;
import org.zkoss.zhtml.Br;
import org.zkoss.zhtml.Li;
import org.zkoss.zul.Span;
//import org.zkoss.zhtml.Span;
import org.zkoss.zul.Label;
import org.zkoss.zul.Div;

import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.session.mob_Session;

public class RedirectMenu extends Window implements RequestListener{
	
	private static Logger log = Logger.getLogger(RedirectMenu.class);

	Constants constants = new Constants();
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	String getData;
	
	Window window; 
	//Window window = Constants.windowBody;
	
	public void setMenuLink( String menuName,Window wnd) {
//		setMenuWindow(header);
//		window = getMenuWindow();
		
		Include inclBody = (Include) wnd.getFellow("inclBody");
		//System.out.println(menuName);
		if (menuName.equalsIgnoreCase("setorTunai")) {
			inclBody.setSrc(Constants.setorTunaiPath);
		} else if (menuName.equalsIgnoreCase("tarikTunai")) {
			inclBody.setSrc(constants.tarikTunaiPath);
		} else if (menuName.equalsIgnoreCase("konfirmasiSetorTunai")) {
			inclBody.setSrc(constants.konfirmasiSetorTunaiPath);
		} else if (menuName.equalsIgnoreCase("tarikTunai")) {
			inclBody.setSrc(constants.tarikTunaiPath );
		} else if (menuName.equalsIgnoreCase("konfirmasiTarikTunai")) {
			inclBody.setSrc(constants.konfirmasiTarikTunaiPath);
		} else if (menuName.equalsIgnoreCase("truehome")) {
			inclBody.setSrc(constants.transfertruehome);
		} else if (menuName.equalsIgnoreCase("truerek")) {
			inclBody.setSrc(constants.transferrekening);
		} else if (menuName.equalsIgnoreCase("truekonfirm")) {
			inclBody.setSrc(constants.transfertruekonfirm);
		} else if (menuName.equalsIgnoreCase("truebank")) {
			inclBody.setSrc(constants.transferbank);
		} else if (menuName.equalsIgnoreCase("truebankkonfirm")) {
			inclBody.setSrc(constants.transferbankkonfirm);
		} else if (menuName.equalsIgnoreCase("truesukses")) {
			inclBody.setSrc(constants.transfersuccess);
		} else if (menuName.equalsIgnoreCase("transfergagal")){
			inclBody.setSrc(constants.transferfail);
		}else if (menuName.equalsIgnoreCase("truepos")) {
			inclBody.setSrc(constants.transferposhome);
		} else if (menuName.equalsIgnoreCase("transferposkonfirm")) {
			inclBody.setSrc(constants.transferposkonfirm);
		} else if (menuName.equalsIgnoreCase("inforekeningagen")) {
			inclBody.setSrc(constants.inforekeningagen);
		} else if (menuName.equalsIgnoreCase("inforekeningmember")) {
			inclBody.setSrc(constants.inforekeningmember);
		} else if (menuName.equalsIgnoreCase("profileagen")) {
			inclBody.setSrc(constants.adminprofile);
		} else if (menuName.equalsIgnoreCase("profilemember")) {
			inclBody.setSrc(constants.memberprofile);
		} else if (menuName.equalsIgnoreCase("profilememberedit")) {
			inclBody.setSrc(constants.memberprofileedit);
		} else if (menuName.equalsIgnoreCase("profileagenedit")) {
			inclBody.setSrc(constants.agenprofileedit);
		} else if (menuName.equalsIgnoreCase("pembelianpulsa")){
			inclBody.setSrc(constants.pembelianpulsa);
		} else if (menuName.equalsIgnoreCase("pembeliangames")){
			inclBody.setSrc(constants.pembeliangames);
		} else if (menuName.equalsIgnoreCase("pembeliantv")){
			inclBody.setSrc(constants.pembeliantv);
		} else if (menuName.equalsIgnoreCase("konfirmasipembelianpulsa")){
			inclBody.setSrc(constants.Konfirmasipembelianpulsa);
		} else if (menuName.equalsIgnoreCase("pembelianpln")){
			inclBody.setSrc(constants.pembelianpln);
		} else if (menuName.equalsIgnoreCase("konfirmasipembelianpln")){
			inclBody.setSrc(constants.Konfirmasipembelianpln);
		} else if (menuName.equalsIgnoreCase("konfirmasipembeliantv")){
			inclBody.setSrc(constants.Konfirmasipembeliantv);
		} else if (menuName.equalsIgnoreCase("pembayaranpln")){
			inclBody.setSrc(constants.pembayaranpln);
		} else if (menuName.equalsIgnoreCase("konfirmasipembayaranpln")){
			inclBody.setSrc(constants.Konfirmasipembayaranpln);
		} else if (menuName.equalsIgnoreCase("pembayaranpdam")){
			inclBody.setSrc(constants.pembayaranpdam);			
		} else if (menuName.equalsIgnoreCase("konfirmasipembayaranpdam")){
			inclBody.setSrc(constants.Konfirmasipembayaranpdam);
		} else if (menuName.equalsIgnoreCase("pembayarantelepon")){
			inclBody.setSrc(constants.pembayarantelepon);
		} else if (menuName.equalsIgnoreCase("konfirmasipembayarantelepon")){
			inclBody.setSrc(constants.Konfirmasipembayarantelepon);
		} else if (menuName.equalsIgnoreCase("pembayaraninternet")){
			inclBody.setSrc(constants.pembayaraninternet);
		} else if (menuName.equalsIgnoreCase("pembayaranbpjs")){
			inclBody.setSrc(constants.pembayaranbpjs);
		} else if (menuName.equalsIgnoreCase("pembayaranmultifinance")){
			inclBody.setSrc(constants.pembayaranmultifinance);
		} else if (menuName.equalsIgnoreCase("konfirmasipembayaraninternet")){
			inclBody.setSrc(constants.Konfirmasipembayaraninternet);
		} else if (menuName.equalsIgnoreCase("konfirmasipembayaranbpjs")){
			inclBody.setSrc(constants.Konfirmasipembayaranbpjs);
		} else if (menuName.equalsIgnoreCase("konfirmasipembayaranmf")){
			inclBody.setSrc(constants.Konfirmasipembayaranmf);
		} else if (menuName.equalsIgnoreCase("pembayarantv")){
			inclBody.setSrc(constants.pembayarantv);
		} else if (menuName.equalsIgnoreCase("konfirmasipembayarantv")){
			inclBody.setSrc(constants.Konfirmasipembayarantv);
		} else if (menuName.equalsIgnoreCase("deposit")){
			inclBody.setSrc(constants.tambahdeposit);
		} else if (menuName.equalsIgnoreCase("konfirmasideposit")){
			inclBody.setSrc(constants.Konfirmasitambahdeposit);
		} else if (menuName.equalsIgnoreCase("deposittiket")){
		    inclBody.setSrc(constants.tambahdeposittiket);
		} else if (menuName.equalsIgnoreCase(null) || menuName.equalsIgnoreCase("")) {
			if (username.startsWith("0")){
				inclBody.setSrc(constants.inforekeningmember);
			}
			else {
				inclBody.setSrc(constants.inforekeningagen);
			}
		} else if (menuName.equalsIgnoreCase("paymentSuccess")) {
			inclBody.setSrc(constants.SuccessPaymentpath);
		} else if (menuName.equalsIgnoreCase("paymentBpjsSuccess")) {
			inclBody.setSrc(constants.bpjsSuccessPaymentpath);
		} else if (menuName.equalsIgnoreCase("buySuccess")) {
			inclBody.setSrc(constants.Successbuypath);
		}
		else if (menuName.equalsIgnoreCase("login")){
			inclBody.setSrc(constants.signin);
		}
		else if (menuName.equalsIgnoreCase("allmessages")){
			inclBody.setSrc(constants.allmessages);
		}
		else if (menuName.equalsIgnoreCase("changepassword")){
			inclBody.setSrc(constants.changepassword);
		}
		else if (menuName.equalsIgnoreCase("changepin")){
			inclBody.setSrc(constants.changepin);
		}
		else if (menuName.equalsIgnoreCase("transaksiGagal")){
			inclBody.setSrc(constants.failedgeneralpath);
		}
		else if (menuName.equalsIgnoreCase("allnotification")){
			inclBody.setSrc(Constants.allnotif);
		}
		else if (menuName.equalsIgnoreCase("lihatkomisi")){
			inclBody.setSrc(Constants.komisi);
		}
		else if (menuName.equalsIgnoreCase("signupmember")){
			inclBody.setSrc(Constants.signup );
		}
		else if (menuName.equalsIgnoreCase("signupagen")) {
			inclBody.setSrc(Constants.signupagen );
		}
		else if (menuName.equalsIgnoreCase("admin-contact")) {
			inclBody.setSrc(Constants.contact );
		}
		else if (menuName.equalsIgnoreCase("cashsukses")){
			inclBody.setSrc(Constants.cash );
		}
		else if (menuName.equalsIgnoreCase("banksukses")){
			inclBody.setSrc(Constants.banksukses );
		}
		else if (menuName.equalsIgnoreCase("possukses")){
			inclBody.setSrc(Constants.possukses);
		}
		else if (menuName.equalsIgnoreCase("pembayaranpulsa")){
			inclBody.setSrc(constants.pembayaranpulsa);
		}
		else if (menuName.equalsIgnoreCase("konfirmasipembayaranpulsa")){
			inclBody.setSrc(constants.Konfirmasipembayaranpulsa);
		} 
		else if (menuName.equalsIgnoreCase("pembayaranpulsapasca")){
			inclBody.setSrc(Constants.pembayaranpulsapasca);
		}
		else if (menuName.equalsIgnoreCase("cairkansaldo")){
			inclBody.setSrc(Constants.cairkansaldo);
		} else if (menuName.equalsIgnoreCase("truebankkonfirmagent")) {
			inclBody.setSrc(constants.transferbankkonfirmagent);
		} 
	}

	public void setMenuWindow(Window window) {
		//Constants.windowBody = window;
		//System.out.println("setWindow" +window);
		this.window = window;
	}
	public Window getMenuWindow(){
		return this.window;
	}

	public void setQuickViewMessage(Window wnd) {
		try {
			JSONParser parser = new JSONParser();
			String tipeUser = sesi.getSessionAttribute("tipe");
			JSONObject object = new JSONObject();
			object.put("Tipe", "ViewQuickMessage");
			object.put("username", username);
			object.put("tipeUser", tipeUser);
			object.put("id_TipePesan", "2");
			ProcessingListener processing = new ProcessingListener(this, object.toString());
			processing.processData();
			Object object2 = new JSONObject();
			// parser.parse(getData);
			object2 = parser.parse(getData);
			JSONObject jsonObject = (JSONObject) object2;
			JSONArray pesanArr = (JSONArray) jsonObject.get("pesan");
			JSONArray idTransaksiArr = (JSONArray) jsonObject.get("id_Transaksi");
			JSONArray timeStampArr = (JSONArray) jsonObject.get("timestamp");
			JSONArray statusreadArr = (JSONArray) jsonObject.get("statusread");
			int count = pesanArr.size();	
			Div divParent = new Div();
			divParent = (Div) wnd.getFellow("rowBodyNotif");
			Components.removeAllChildren(divParent);
			for (int i = 0; i < count; i++) {
				 String pesan = pesanArr.get(i) + "";
				 String idTransaksi = idTransaksiArr.get(i) + "";
					 Li quickLi = new Li();
					 quickLi.setId(i+"");
					 quickLi.setParent(divParent);
					 if (statusreadArr.get(i).toString().equalsIgnoreCase("f")) {
						 quickLi.setSclass("notif-msg unread");			
						 quickLi.addEventListener("onClick", new EventListener() {
							 @Override
							 public void onEvent(Event arg0) throws Exception {
							 quickLi.setSclass("notif-msg");		
							 read(idTransaksi, wnd);
							 RedirectMenu rd = new RedirectMenu();
							 rd.setViewData(window);
							 }
						 });
					 } else {
						 quickLi.setSclass("notif-msg");
					 }
					 Br br = new Br();
					 Span isiPesanSpan = new Span();
					 //isiPesanSpan.setClass("notif-unread");
					 isiPesanSpan.setClass("notif-date");
					 isiPesanSpan.setParent(quickLi);
					 Label quickIsiPesan = new Label();
					 quickIsiPesan.setParent(isiPesanSpan);
					 quickIsiPesan.setValue(pesanArr.get(i) + "" );
					 br.setParent(quickLi);
					 
					 
					 
					 Span dateSpan = new Span();
					 //dateSpan.setClass("notif-date");
					 dateSpan.setClass("notif-unread");
					 dateSpan.setParent(quickLi);
					 Label quickDateLabel = new Label();
					 quickDateLabel.setParent(dateSpan);
					 quickDateLabel.setValue(timeStampArr.get(i) + "");
//				}
			}
		} catch (Exception e)

		{
			// TODO Auto-generated catch block
//			System.out.println("ERORRRRR:setQuickView:MESSAGE::"+e.getMessage());
			log.info(e.getMessage() + " -> " + e);
		}

	}
	
	public void setQuickViewNotif(Window wnd){
		try {
			JSONParser parser = new JSONParser();
			String tipeUser = sesi.getSessionAttribute("tipe");
			JSONObject object = new JSONObject();
			object.put("Tipe", "ViewQuickNotifikasi");
			object.put("username", username);
			object.put("tipeUser", tipeUser);
			ProcessingListener processing = new ProcessingListener(this, object.toString());
			processing.processData();

//			RedirectMenu rd = new RedirectMenu();
//			rd.setViewData(Constants.headWindow);

			Object object2 = new JSONObject();
			object2 = parser.parse(getData);
			JSONObject jsonObject = (JSONObject) object2;
			JSONArray detailnotifikasiArr = (JSONArray) jsonObject.get("detailnotifikasi");
			JSONArray timeStampArr = (JSONArray) jsonObject.get("timestamp");
			JSONArray statusreadArr = (JSONArray) jsonObject.get("statusread");
			int count = detailnotifikasiArr.size();
			Div divParent = (Div) wnd.getFellow("rowBodyMessage");
			Components.removeAllChildren(divParent);
			
			for (int i = 0; i < count; i++) {
				 String pesan = detailnotifikasiArr.get(i) + "";
					 Li quickLi = new Li();
					 quickLi.setId(i+"");
					 quickLi.setParent(divParent);
					 if (statusreadArr.get(i).toString().equalsIgnoreCase("f")) {
						 quickLi.setSclass("notif-li unread");			
//						 quickLi.addEventListener("onClick", new EventListener() {
//							 @Override
//							 public void onEvent(Event arg0) throws Exception {
//							 quickLi.setSclass("notif-li");		
//							 RedirectMenu rd = new RedirectMenu();
//							 rd.setViewData(Constants.windowBody);
//							 }
//						 });
					 } else {
						 quickLi.setSclass("notif-msg");
					 }
					 Br br = new Br();
					 Span isiPesanSpan = new Span();
					 //isiPesanSpan.setClass("notif-unread");
					 isiPesanSpan.setClass("notif-date");
					 isiPesanSpan.setParent(quickLi);
					 Label quickIsiPesan = new Label();
					 quickIsiPesan.setParent(isiPesanSpan);
					 quickIsiPesan.setValue(detailnotifikasiArr.get(i) + "\n\n");
					 br.setParent(quickLi);
					 
					 Span dateSpan = new Span();
					 //dateSpan.setClass("notif-date");
					 dateSpan.setClass("notif-unread");
					 dateSpan.setParent(quickLi);
					 Label quickDateLabel = new Label();
					 quickDateLabel.setParent(dateSpan);
					 quickDateLabel.setValue("\n"+timeStampArr.get(i) + "");
//				}
			}
		} catch (Exception e)

		{
			// TODO Auto-generated catch block
//			System.out.println("ERORRRRR:setQuickView:NOTIF::"+e.getMessage());
			log.info(e.getMessage() + " -> " + e);
		}

	
	}
	
	public void setViewData(Window wnd) throws Exception {
		try {
			Span spanPesan = (Span) wnd.getFellow("spanPesan");
			Span spanNotif = (Span) wnd.getFellow("spanNotif");
			JSONParser parser = new JSONParser();
			String tipeUser = sesi.getSessionAttribute("tipe");
			JSONObject object = new JSONObject();
			object.put("Tipe", "CountUnread");
			object.put("username", username);
			object.put("tipeUser", tipeUser);
			ProcessingListener processing = new ProcessingListener(this, object.toString());
			processing.processData();
			
			Object object2 = new JSONObject();
			object2 = parser.parse(getData);
			JSONObject jsonObject = (JSONObject) object2;
			int jPesan = Integer.parseInt(jsonObject.get("countMess").toString());
			int jnotif = Integer.parseInt(jsonObject.get("countNotif").toString());
//			System.out.println("jumlahpesan>>>>>>>>>>>>>"+jPesan+"  Jumlah notif>>>"+jnotif);
			if (jPesan != 0) {
				spanPesan.setVisible(true);
				Label labelPesan = (Label) wnd.getFellow("labelPesan");
				labelPesan.setValue(jPesan+"");
			} else {
				spanPesan.setVisible(false);
			}
			
			if (jnotif != 0) {
				spanNotif.setVisible(true);
				Label labelNotif = (Label) wnd.getFellow("labelNotif");
				labelNotif.setValue(jnotif+"");
			} else {
				spanNotif.setVisible(false);
			}
			
		} catch (Exception e){	
			// TODO Auto-generated catch block
//			System.out.println("errorrrrrrrrrrr>>"+e.getMessage());
//			System.out.println(e + "failed");
			log.info(e.getMessage() + " -> " + e);
		}

	}
	
	public void read(String idTransaksi, Window wnd) {
		try {
			JSONObject obj = new JSONObject();
			obj.put("Tipe", "UbahStatus");
			obj.put("id_transaksi", idTransaksi);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			setQuickViewMessage(wnd);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getMessage() + " -> " + e);
		}
	}

	@Override
	public void requestSucceeded(JSONObject data) {
		// TODO Auto-generated method stub
		getData = data.toString(); 

	}

	@Override
	public void requestFailed(String message) {
		// TODO Auto-generated method stub
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		}
	}
	
	public void logout() {
		try {
			JSONObject obj = new JSONObject();
			obj.put("Tipe", "logout");
			obj.put("username", (Sessions.getCurrent()).getAttribute("username"));
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
		} catch (Exception e) {
			log.info(e.getMessage() + " -> " + e);
		}
	}
	
	
}
