package com.truemoneywitami.function;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;

public class Timeout {
	
	private static Logger log = Logger.getLogger(Timeout.class);
	
	public void timeout(String username) {
		try {
			JSONObject obj = new JSONObject();
			obj.put("Tipe", "logout");
			obj.put("username", username);
			
			log.info("timeout -> " + obj.toString());
			
//			ProcessingListener processing = new ProcessingListener(this, obj.toString());
//			processing.processData();
			
			log.info("response timeout -> "+ServerConnectionPrabayar.curl_post(obj.toString(),false));
		} catch (Exception e) {
//			e.printStackTrace();
			log.info(e.getMessage() + " -> " + e);
		}
	}

}
