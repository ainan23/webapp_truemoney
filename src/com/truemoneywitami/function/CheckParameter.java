package com.truemoneywitami.function;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class CheckParameter {

	private static Logger log = Logger.getLogger(CheckParameter.class);
	Pattern pattern;
	Matcher matcher;

	public boolean isKosong(String param) {
		boolean hasil = false;
		try {
			int lenght = param.trim().length();
			if (lenght == 0) {
				hasil = true;
			}
		} catch (Exception e) {
			log.info(e.getMessage() + " -> " + e);
		}
		return hasil;
	}
	
		public boolean cekValidasiEmail(String value) {
		final String EMAIL_PATTERN = 
				"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(value);
		return matcher.matches();
	}

	public boolean isNumber(String param) {
		boolean hasil = false;
		try {
			Long.parseLong(param);
			hasil = true;
		} catch (Exception e) {
			hasil = false;
			log.info(e.getMessage() + " -> " + e);
//			System.out.println("Ini dia sayang eksepsinya muah   " +e);
		}
		return hasil;
	}

	public boolean pinIsValid(String value) {
		final String EMAIL_PATTERN = "^[0-9]{1,6}$";
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(value);

		if ((matcher.matches()) && value.length() == 6) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean checkAlpha(String inputan) {
		Boolean hasSpecialChar = false;
		Pattern p = Pattern.compile("[a-zA-Z]");
		hasSpecialChar = p.matcher(inputan).find();
		return hasSpecialChar;
	}
	
	public boolean checkNumber(String inputan) {
		Boolean hasSpecialChar = false;
		Pattern p = Pattern.compile("[0-9]");
		hasSpecialChar = p.matcher(inputan).find();
		return hasSpecialChar;
	}

	public boolean checkNumeric(String inputan) {
		Boolean hasSpecialChar = false;
		Pattern p = Pattern.compile("[0123456789]");
		hasSpecialChar = p.matcher(inputan).find();
		log.info("check num : " + String.valueOf(hasSpecialChar));
		return hasSpecialChar;
	}

	// pattern Check Alphabet big
	public boolean checkBigAlphabet(String inputan) {
		Boolean hasSpecialChar = false;
		Pattern p = Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZ]");
		hasSpecialChar = p.matcher(inputan).find();
		log.info("big alpha : " + String.valueOf(hasSpecialChar));
		return hasSpecialChar;
	}

	// pattern Check Alphabet big
	public boolean checkSmallAlphabet(String inputan) {
		Boolean hasSpecialChar = false;
		Pattern p = Pattern.compile("[abcdefghijklmnopqrstuvwxyz]");
		hasSpecialChar = p.matcher(inputan).find();
		log.info("small alpha char : " + String.valueOf(hasSpecialChar));
		return hasSpecialChar;
	}
	
	
	public boolean isNull(String value) {
		boolean hasil = false;
		try {
			if (value == null || value.trim().length() == 0) {
				hasil = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hasil;
	}
	
	public boolean isAngka(String value) {
		boolean hasil = false;
		try {
			Pattern p = Pattern.compile("[0123456789]");
			hasil = p.matcher(value).find();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hasil;
	}
	
	public boolean checkLength(String value, int fix) {
		boolean hasil = false;
		try {
			if (value.length() == fix) {
				hasil = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hasil;
	}
	
	public boolean checkLength(String value, int first, int last) {
		boolean hasil = false;
		try {
			if (value.length() > first && value.length() < last) {
				hasil = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hasil;
	}
	
	public boolean isNil(String value) {
		boolean hasil = false;
		try {
			if (value == null) {
				hasil = true;
			} else if (value.trim().length() == 0) {
				hasil = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hasil;
	}
	
	public boolean numberValidator(String value) {
		boolean hasil = false;
		try {
			pattern = Pattern.compile("^[0-9]*$");
			matcher = pattern.matcher(value);
			hasil = matcher.matches();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hasil;
	}

}
