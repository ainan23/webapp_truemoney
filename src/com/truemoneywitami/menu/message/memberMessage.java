package com.truemoneywitami.menu.message;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;


//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;
import org.zkoss.json.parser.JSONParser;
import org.zkoss.json.parser.ParseException;

public class memberMessage extends Window implements RequestListener {

	public String nmAccount = "";
	public String noRek = "";
	public String amount = "";
	mob_Session sesi = new mob_Session();
	ZKFunction func = new ZKFunction();
	JSONParser parser = new JSONParser();
	String username = sesi.getUsername();
	String getData;
	Window window = new Window();
	public void setViewData(Window wnd) throws Exception {
		try {
			JSONParser parser = new JSONParser();
			String tipeUser = sesi.getSessionAttribute("tipe");
			JSONObject object = new JSONObject();
			object.put("Tipe", "ViewMessage");
			object.put("username", username);
			object.put("tipeUser", tipeUser);
			object.put("id_TipePesan", "2");
			ProcessingListener processing = new ProcessingListener(this, object.toString());
			processing.processData();
			Object object2 = new JSONObject();
			// parser.parse(getData);
			object2 = parser.parse(getData);
			JSONObject jsonObject = (JSONObject) object2;
			JSONArray idTransaksiArr = (JSONArray) jsonObject.get("id_Transaksi");
			JSONArray pesanArr = (JSONArray) jsonObject.get("pesan");
			JSONArray timeStampArr = (JSONArray) jsonObject.get("timestamp");
			JSONArray statusreadArr = (JSONArray) jsonObject.get("statusread");

			for (int i = 0; i < idTransaksiArr.size(); i++) {
				 Div divParent = (Div) wnd.getFellow("rowBody");
				 String idTransaksi = idTransaksiArr.get(i) + "";
				 try{
					 Div pesanHeaderDiv = (Div) wnd.getFellow(idTransaksi);
					 if (statusreadArr.get(i).toString().equalsIgnoreCase("f")) {
						 pesanHeaderDiv.setClass("notification-box new text-left");				
						 pesanHeaderDiv.addEventListener("onClick", new EventListener() {
							 @Override
							 public void onEvent(Event arg0) throws Exception {
							 // TODO Auto-generated method stub
							 pesanHeaderDiv.setClass("notification-box text-left");
							 read(idTransaksi);
							 RedirectMenu rd = new RedirectMenu();
							 rd.setViewData(window);
							 }
						 });
					 } else {
						 pesanHeaderDiv.setClass("notification-box text-left");
					 }
				 } 
				 catch (Exception e) 
				 {
					 Div pesanHeaderDiv = new Div();
					 pesanHeaderDiv.setId(idTransaksi);
					 pesanHeaderDiv.setParent(divParent);
					 if (statusreadArr.get(i).toString().equalsIgnoreCase("f")) {
						 pesanHeaderDiv.setClass("notification-box new text-left");				
						 pesanHeaderDiv.addEventListener("onClick", new EventListener() {
							 @Override
							 public void onEvent(Event arg0) throws Exception {
							 // TODO Auto-generated method stub
							 pesanHeaderDiv.setClass("notification-box text-left");
							 read(idTransaksi);
							 RedirectMenu rd = new RedirectMenu();
							 rd.setViewData(window);
							 //Messagebox.show("Message Urutan ke "+pesanHeaderDiv.getId(), "Information", Messagebox.OK, Messagebox.INFORMATION);
							 }
						 });
					 } else {
						 pesanHeaderDiv.setClass("notification-box text-left");
					 }
					 Label labelJudul = new Label();
					 Div judulPesanDiv = new Div();
					 judulPesanDiv.setParent(pesanHeaderDiv);
					 labelJudul.setValue(idTransaksiArr.get(i) + "");
					 labelJudul.setParent(judulPesanDiv);
					 labelJudul.setStyle("font-size:10pt;font-weight:bold;");
					
					 Label labelKonten = new Label();
					 Div isiPesanDiv = new Div();
					 isiPesanDiv.setParent(pesanHeaderDiv);
					 labelKonten.setValue(pesanArr.get(i).toString());
					 labelKonten.setParent(isiPesanDiv);
					 labelKonten.setStyle("font-size:10pt");
					
					 Label labelWaktu = new Label();
					 Div waktu = new Div();
					 waktu.setParent(pesanHeaderDiv);
					 labelWaktu.setValue(timeStampArr.get(i) + "");
					 labelWaktu.setParent(waktu);
					 labelWaktu.setClass("notif-date");
				}
			}
		} catch (Exception e)

		{
			// TODO Auto-generated catch block
			System.out.println("ERORRRRR::::"+e.getMessage());
		}

	}

//	public void setUnread(Window w) {
//		try {
//			JSONParser parser = new JSONParser();
//			String tipeUser = sesi.getSessionAttribute("tipe");
//			JSONObject object = new JSONObject();
//			object.put("Tipe", "ViewMessage");
//			object.put("username", username);
//			object.put("tipeUser", tipeUser);
//			object.put("id_TipePesan", "2");
//			ProcessingListener processing = new ProcessingListener(this, object.toString());
//			processing.processData();
//			Object object2 = new JSONObject();
//			// parser.parse(getData);
//			object2 = parser.parse(getData);
//			JSONObject jsonObject = (JSONObject) object2;
//			JSONArray idTransaksiArr = (JSONArray) jsonObject.get("id_Transaksi");
//
//			Div pesanHeaderDiv = (Div) w.getFellow("pesanHeaderDiv");
//			pesanHeaderDiv.setClass("notification-box new text-left");
//			// pesanHeaderDiv.setId(i+"");
//
//		} catch (Exception e) {
//			System.out.println(e);
//		}
//	}

	public void read(String idTransaksi) {
		try {
			JSONObject obj = new JSONObject();
			obj.put("Tipe", "UbahStatus");
			obj.put("id_transaksi", idTransaksi);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void requestFailed(String message) {
		// TODO Auto-generated method stub
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
//			Clients.showNotification(message);
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}

	@Override
	public void requestSucceeded(JSONObject data) {
		// TODO Auto-generated method stub
		getData = data.toString();

	}

}
