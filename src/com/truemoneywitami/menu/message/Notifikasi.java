package com.truemoneywitami.menu.message;

import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.json.parser.JSONParser;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class Notifikasi extends Window implements RequestListener {

	public String nmAccount = "";
	public String noRek = "";
	public String amount = "";
	mob_Session sesi = new mob_Session();
	ZKFunction func = new ZKFunction();
	JSONParser parser = new JSONParser();
	String username = sesi.getUsername();
	String getData;

	public void setViewData(Window wnd) {
		try {
			JSONParser parser = new JSONParser();
			String tipeUser = sesi.getSessionAttribute("tipe");
			JSONObject object = new JSONObject();
			object.put("Tipe", "ViewNotifikasi");
			object.put("username", username);
			object.put("tipeUser", tipeUser);
			ProcessingListener processing = new ProcessingListener(this, object.toString());
			processing.processData();

			Object object2 = new JSONObject();
			object2 = parser.parse(getData);
			JSONObject jsonObject = (JSONObject) object2;
			JSONArray detailnotifikasiArr = (JSONArray) jsonObject.get("detailnotifikasi");
			JSONArray timeStampArr = (JSONArray) jsonObject.get("timestamp");
			JSONArray statusreadArr = (JSONArray) jsonObject.get("statusread");
			for (int i = 0; i < detailnotifikasiArr.size(); i++) {
				Div divParent = (Div) wnd.getFellow("rowBody");
				String detailNotifikasi = detailnotifikasiArr.get(i) + "";
				try {
					Div pesanHeaderDiv = (Div) wnd.getFellow(detailNotifikasi);
					if (statusreadArr.get(i).toString().equalsIgnoreCase("f")) {
						pesanHeaderDiv.setClass("notification-box new text-left");
						divParent.addEventListener("onClick", new EventListener() {
							@Override
							public void onEvent(Event arg0) throws Exception {
								pesanHeaderDiv.setClass("notification-box text-left");
							}
						});
					} else {
						pesanHeaderDiv.setClass("notification-box text-left");
					}
				} catch (Exception e) {
					Div pesanHeaderDiv = new Div();
					pesanHeaderDiv.setId(detailNotifikasi);
					divParent.addEventListener("onClick", new EventListener() {
						@Override
						public void onEvent(Event arg0) throws Exception {
							pesanHeaderDiv.setClass("notification-box text-left");
						}
					});

					if (statusreadArr.get(i).toString().equalsIgnoreCase("f")) {
						pesanHeaderDiv.setClass("notification-box new text-left");
						pesanHeaderDiv.setId(i + "");
					} else {
						pesanHeaderDiv.setClass("notification-box text-left");
					}
					pesanHeaderDiv.setParent(divParent);
					Label labelJudul = new Label();
					Div judulPesanDiv = new Div();
					judulPesanDiv.setParent(pesanHeaderDiv);
					labelJudul.setValue(detailnotifikasiArr.get(i) + "");
					labelJudul.setParent(judulPesanDiv);
					labelJudul.setStyle("font-size:10pt;font-weight:bold;");

					Label labelWaktu = new Label();
					Div waktu = new Div();
					waktu.setParent(pesanHeaderDiv);
					labelWaktu.setValue(timeStampArr.get(i) + "");
					labelWaktu.setParent(waktu);
					labelWaktu.setClass("notif-date");

				}
			}

		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			System.out.println(e + "failed");
		}

	}

	@Override
	public void requestFailed(String message) {
		// TODO Auto-generated method stub
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
//			Clients.showNotification(message);
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}

	@Override
	public void requestSucceeded(JSONObject data) {
		// TODO Auto-generated method stub
		getData = data.toString();
	}

}
