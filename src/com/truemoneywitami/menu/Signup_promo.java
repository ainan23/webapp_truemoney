package com.truemoneywitami.menu;

import java.io.File;
import java.io.FileInputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Fileupload;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.Cookies;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class Signup_promo extends Window implements RequestListener {
	
	private static Logger log = Logger.getLogger(Signup_promo.class);

	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	CheckParameter check = new CheckParameter();
	JSONObject obj = new JSONObject();
	Cookies cookie = new Cookies();
	JSONArray arrkel, arrkec, arrkot;
	boolean isValid = false;
	Window win;
	JSONArray arrbank;
//	CheckParameter cek = new CheckParameter();
	
	String kelSelected, kecSelected,noKartu="";
	private String ack = "",
			tipeUser = "",
			givemail = "",
			givname = "",
			givaccount = "";
	String nama="",
			ktp="",
			jk="",
			telp="",
			Hp="",
			tgl="",
			tempat="",
			alamat="", RT="", RW="", kel1="", kec="", kota="", prov="", pos="", work="", agama="", status="", edu="",
			bank="-", norek="", nopass="", tgl2="", npwp="", ibu="", waris="", warisadd="", waristelp="", warisHP="", email="", pass="", repass = "";
	private String proses="NOK";
	private String prosesBaru = "NOK";
	
	mob_Session sesi = new mob_Session();
//	File fiswajah,fisktp,fisttd;
	Pattern pattern;
	Matcher matcher;
//	FileInputStream fotowajah;
//	FileInputStream fotoktp;
//	FileInputStream fotottd;
	
	@Override
	public void requestSucceeded(JSONObject obj) {
		
		log.info("masuk ini nggak!");
		log.info("-------------------- " + obj.toString());
		// TODO Auto-generated method stub
		if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("kel")) {
			arrkel = (JSONArray) obj.get("listKelurahan");
		} else if (obj.get("ACK").toString().equalsIgnoreCase("OK")
				&& obj.get("ID").toString().equalsIgnoreCase("kec")) {
			arrkec = (JSONArray) obj.get("listKecamatan");
		} else if (obj.get("ACK").toString().equalsIgnoreCase("OK")
				&& obj.get("ID").toString().equalsIgnoreCase("prov")) {
			arrkot = (JSONArray) obj.get("listKotaProvinsi");
		} else if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("Bank") ){
			arrbank = (JSONArray) obj.get("ListBank");
		}
		else if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("cekMember")){
			proses = "OK";
		}
		else if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("cekKartu")){
			proses = "OK";
		}
		else if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("cekHpEmail")){
			proses = "OK";
		}
		else if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("cekHpEmailExist")){
			System.out.println("masuk sini! validasi ok");
			prosesBaru = "OK";
		}
		else if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("reg")){
			ack = "OK";
		}
		
		
		
		
		
		
//		if (obj.get("ID").toString().equalsIgnoreCase("cekMember")){
//			proses = obj.get("ACK").toString();
//		} else if (obj.get("ID").toString().equalsIgnoreCase("cekKartu")){
//			proses = obj.get("ACK").toString();
//		} else if (obj.get("ID").toString().equalsIgnoreCase("cekHpEmail")){
//			proses = obj.get("ACK").toString();
//		} else if (obj.get("ID").toString().equalsIgnoreCase("cekHpEmailExist")){
//			System.out.println("masuk sini! validasi ok");
//			log.info("adetttt hasil cek hp email exist L "+obj.get("ACK").toString());
//			prosesBaru = obj.get("ACK").toString();
//		} else if (obj.get("ID").toString().equalsIgnoreCase("reg")){
//			ack = obj.get("ACK").toString();
//		}
	}

	@Override
	public void requestFailed(String message) {
		// TODO Auto-generated method stub
		
		proses = "NOK";
		prosesBaru = "NOK";
		ack = "NOK";
		
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		}  else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		
		
		
	}
	
	public void setBank(String i_bank) throws Exception {
		try{
			System.out.println("Bank Selected>>"+ bank);
			bank = i_bank;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void cekImage(Window wnd){
		
		try
		{
			org.zkoss.util.media.Media media = Fileupload.get();
			
			if(media instanceof org.zkoss.image.Image)
			{
				int size = media.getByteData().length / 1024;
				
				if(size <= 1000)
				{
//					uploadFile1.setText(media.getName());
					func.setTextboxValue(wnd, "upload1", media.getName());
//					java.io.FileInputStream test = media.getStreamData();
//					wnd.setMedia(wnd,test,1);
					System.out.println("Masuk Validasiiiiiiiiiiiuuuuuuuuu");
					
				}
				else
				{
					System.out.println("Masuk Validasiiiiiiiiiiiiiiiiiiiiiiiiiiiiii22222222222");
				}
			}
			else
			{
//				Messagebox.show("Not an image: "+media, "Error", Messagebox.OK, Messagebox.ERROR);
				Clients.showNotification("Not an image" +media, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
		}
		catch(NullPointerException e)
		{
		  	e.printStackTrace();
		}
		catch(Exception e)
		{
		  	e.printStackTrace();
		}
	}
	
	public boolean cekMember(Window wnd) {
		boolean registered = false;
		try {
			noKartu = func.getTextboxValue(wnd,"nokar");
			String VerHP= func.getTextboxValue(wnd, "noHP");
			JSONObject obj2 = new JSONObject();
			obj2.put("Tipe","CekMember");
			obj2.put("noHP",VerHP);
			obj2.put("noKartu",noKartu);
			ProcessingListener processing = new ProcessingListener(this, obj2.toString());
			processing.processData(); 
			log.info("validasi cek member -> " + proses);
			if(proses.equals("OK")){
				registered = true;
			}
		
		}
		catch (Exception e) {
			Clients.showNotification("Terjadi Kesalahan ", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			e.printStackTrace();
		}
		
		return registered;
	}
	
	public void setData(Window wnd) throws Exception {
		try {
				JSONObject obj2 = new JSONObject();
				obj2.put("Tipe","BankData");
				ProcessingListener processing = new ProcessingListener(this, obj2.toString());
				processing.processData();
				setListboxValue(wnd,"bank", arrbank, 0, true, true, true);
				System.out.println("Mulai ngeSet data"); 
		} catch (Exception e) {
			e.getMessage();
		}
	}
	
//	public void setMedia(Window wnd, FileInputStream media, int param) {
//		
//		if (param == 1){
//			fotowajah = media;
//		} else if (param==2) {
//			fotoktp = media;
//		}
//		else {
//			fotottd = media;
//		}
//	}
	
	
	public void setListboxValue(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID) throws Exception {
		try{
			Listbox list = (Listbox) wnd.getFellow(idList);
			list.getItems().clear();
			int a = data.size();
			for (int i =0;i<a;i++){
				Listitem li = new Listitem();
				JSONObject child  = (JSONObject) data.get(i);
				li.setValue(child.get("kode").toString());
				String nama = child.get("nama").toString();
				li.appendChild(new Listcell(nama));
				list.appendChild(li);
				
				if (i == 0) {
					li.setSelected(true);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void loadKelurahan(Window wnd){
		String namaKelurahan = func.getTextboxValue(wnd, "listsearchkel");
		try {
			if (namaKelurahan.trim().length() < 4){
//				Clients.showNotification("Minimal 4 Karakter");
			}
			else {
				obj.put("Tipe", "inquiryKelurahan");
				obj.put("namaKelurahan", namaKelurahan);
				ProcessingListener processing = new ProcessingListener(this, obj.toString());
				processing.processData();
				setListboxValueKel(wnd, "kel", arrkel, 0, true, true, true);
			}
			
		} catch (Exception e){
		
		}
	}

	public void loadKecamatan(Window wnd, Bandbox bd) throws Exception {
	
		try {
			Bandbox bkec = (Bandbox) wnd.getFellow("bandkec");
			bkec.setDisabled(false);
			
			bkec.setReadonly(true);
			bkec.setText("");
			
			//clear kota
			func.listboxHapus(wnd, "lstKota");
			
			func.setTextboxResetValue(wnd, "prov");
			
			kelSelected = func.getListboxValue(wnd, "kel");
			kel1 = kelSelected;
			bd.setValue(kelSelected);
			obj.put("Tipe","inquiryKecamatan");
			obj.put("namaKelurahan", kelSelected);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			setListboxValueKec(wnd, "kec", arrkec, 0, true, true, true);
		} catch (Exception e) {
			
		}
	}

	public void setListboxValueKel(Window wnd, String idList, JSONArray data, int noUrut, boolean isSelect,
			boolean isClear, boolean firstRowIsID) {

		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int iRow = data.size();
		for (int j = 0; j < iRow; j++) {
			Listitem li = new Listitem();
			JSONObject child = (JSONObject) data.get(j);
			li.setValue(child.get("namaKelurahan").toString());
			li.appendChild(new Listcell(child.get("namaKelurahan").toString()));
			list.appendChild(li);
		}

	}

	public void setListboxValueKec(Window wnd, String idList, JSONArray data, int noUrut, boolean isSelect,
			boolean isClear, boolean firstRowIsID) {
		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int iRow = data.size();
		for (int j = 0; j < iRow; j++) {
			Listitem li = new Listitem();
			JSONObject child = (JSONObject) data.get(j);
			li.setValue(child.get("namaKecamatan").toString());
			li.appendChild(new Listcell(child.get("namaKecamatan").toString()));
			list.appendChild(li);
		}
	}

	public void setKotaProv(Window wnd, Bandbox bd) throws Exception {
		kecSelected = func.getListboxValue(wnd, "kec");
		kec = kecSelected;
		bd.setValue(kecSelected);
		obj.put("Tipe", "inquiryKotaProvinsi");
		obj.put("namaKelurahan", kelSelected);
		obj.put("namaKecamatan", kecSelected);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		
//		JSONObject child = (JSONObject) arrkot.get(0);
//		kota = child.get("namaKota").toString();
//		prov = child.get("namaProvinsi").toString();
//		func.setTextboxValue(wnd, "kota", kota);
		func.setTextboxValue(wnd, "prov", "");
		
		setListboxValueKota(wnd, "lstKota", arrkot);
	}
	
	public void daftar(Window wnd) throws Exception {
		try {
			setWindow(wnd);
			nama = func.getTextboxValue(wnd, "nama");
			ktp = func.getTextboxValue(wnd, "ktp");
			jk = func.getTextboxValue(wnd, "jk");
			tempat = func.getTextboxValue(wnd, "tempat");
			tgl = func.getDateboxText(wnd, "tgl");
			telp = func.getTextboxValue(wnd, "telp");
			Hp = func.getTextboxValue(wnd, "noHP");
			alamat = func.getTextboxValue(wnd, "alamat");
			RT = func.getTextboxValue(wnd, "RT");
			RW = func.getTextboxValue(wnd, "RW");
//			kota = func.getTextboxValue(wnd, "kota");
			kota = func.getListboxLabel(wnd, "lstKota");
			prov = func.getTextboxValue(wnd, "prov");
			pos = func.getTextboxValue(wnd, "pos");
			work = func.getTextboxValue(wnd, "work");
			agama = func.getTextboxValue(wnd, "agama");
			status = func.getTextboxValue(wnd, "status");
			edu = func.getTextboxValue(wnd, "eduselect");
			norek = func.getTextboxValue(wnd, "norek");
			nopass = func.getTextboxValue(wnd, "nopass");
			tgl2 = func.getDateboxText(wnd, "tgl2");
			npwp = func.getTextboxValue(wnd, "npwp");
			ibu = func.getTextboxValue(wnd, "ibu");
			waris = func.getTextboxValue(wnd, "waris");
			warisadd = func.getTextboxValue(wnd, "warisadd");
			waristelp = func.getTextboxValue(wnd, "waristel");
			warisHP = func.getTextboxValue(wnd, "warishp");
			email = func.getTextboxValue(wnd, "email");
			noKartu = func.getTextboxValue(wnd,"nokar");
//			pass = func.getTextboxValue(wnd, "pass");
//			repass = func.getTextboxValue(wnd, "repass");
			if(func.getRadioValue(wnd, "rw4")) {
				if(check.isKosong(work)) {
					func.setTextboxFocus(wnd, "work");
					Messagebox.show("Pekerjan Lainnya tidak boleh kosong", "Truemoney", Messagebox.OK, Messagebox.ERROR);
					return;
				}
			}
			
//			if(func.getRadioValue(wnd, "re8")) {
//				if(check.isKosong(edu)) {
//					func.setTextboxFocus(wnd, "edu");
//					Messagebox.show("Pendidikan Terakhir Lainnya tidak boleh kosong", "Truemoney", Messagebox.OK, Messagebox.ERROR);
//					return;
//				}
//			}

			Cookies.setCookie("Tipe", "RegisterPromo");
			Cookies.setCookie("noHp", Hp);
			Cookies.setCookie("password", pass);
			Cookies.setCookie("namaLengkap", nama);
			Cookies.setCookie("noKTP", ktp);
			Cookies.setCookie("email", email);
			Cookies.setCookie("jenisKelamin", jk);
			Cookies.setCookie("tempatLahir", tempat);
			Cookies.setCookie("tglLahir", tgl);
			Cookies.setCookie("noTelepon", telp);
			Cookies.setCookie("alamat", alamat);
			Cookies.setCookie("rt", RT);
			Cookies.setCookie("rw", RW);
			Cookies.setCookie("kelurahan", kel1);
			Cookies.setCookie("kecamatan", kec);
			Cookies.setCookie("kota", kota);
			Cookies.setCookie("provinsi", prov);
			Cookies.setCookie("kodePos", pos);
			Cookies.setCookie("agama", agama);
			Cookies.setCookie("pekerjaan", work);
			Cookies.setCookie("statusPernikahan", status);
			Cookies.setCookie("pendidikanTerakhir", edu);
			Cookies.setCookie("ibuKandung", ibu);
			Cookies.setCookie("ahliWarisNama", waris);
			Cookies.setCookie("ahliWarisNoTelp", waristelp);
			Cookies.setCookie("ahliWarisNoHP", warisHP);
			Cookies.setCookie("ahliWarisAlamat", warisadd);
			Cookies.setCookie("namaBank", bank);
			Cookies.setCookie("noPaspor", nopass);
			Cookies.setCookie("tanggalBerlakuPaspor", tgl2);
			Cookies.setCookie("noRekening", norek);
			Cookies.setCookie("noNPWP", npwp);
			
			System.out.println("!!!!masuk Validasi Pass OK!!!!!!!");
			String Ibank = "False", Inpwp = "False", Ipassport = "False";
			if (func.getRadioValue(wnd, "rb1")) {
				Ibank = "True";
			}
			if (func.getRadioValue(wnd, "rnp1")) {
				Inpwp = "True";
			}
			if (func.getRadioValue(wnd, "rp1")) {
				Ipassport = "True";
			}

			JSONObject obj = new JSONObject();
			obj.put("Tipe", "RegisterPromo");
			obj.put("noHp", Hp);
			obj.put("password", pass);
			obj.put("namaLengkap", nama);
			obj.put("noKTP", ktp);
			obj.put("email", email);
			obj.put("jenisKelamin", jk);
			obj.put("tempatLahir", tempat);
			obj.put("tglLahir", tgl);
			obj.put("noTelepon", telp);
			obj.put("alamat", alamat);
			obj.put("rt", RT);
			obj.put("rw", RW);
			obj.put("kelurahan", kel1);
			obj.put("kecamatan", kec);
			obj.put("kota", kota);
			obj.put("provinsi", prov);
			obj.put("kodePos", pos);
			obj.put("agama", agama);
			obj.put("pekerjaan", work);
			obj.put("statusPernikahan", status);
			obj.put("pendidikanTerakhir", edu);
			obj.put("ibuKandung", ibu);
			obj.put("ahliWarisNama", waris);
			obj.put("ahliWarisNoTelp", waristelp);
			obj.put("ahliWarisNoHP", warisHP);
			obj.put("ahliWarisAlamat", warisadd);
			obj.put("namaBank", bank);
			obj.put("noPaspor", nopass);
			obj.put("tanggalBerlakuPaspor", tgl2);
			obj.put("IsiBank", Ibank);
			obj.put("noRekening", norek);
			obj.put("IsiPaspor", Ipassport);
			obj.put("IsiNPWP", Inpwp);
			obj.put("noNPWP", npwp);
			obj.put("noKartu", noKartu);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			if (ack.equalsIgnoreCase("OK")) {
//				Executions.sendRedirect(Constants.signup);
				sesi.setSessionAttribute(Constants.handPhone,Hp);
				Executions.sendRedirect(Constants.uploadFoto);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
	}
	
	public void daftarIqbal(Window wnd) throws Exception {
		try {
			setWindow(wnd);
			nama = func.getTextboxValue(wnd, "nama");
			ktp = func.getTextboxValue(wnd, "ktp");
			jk = func.getTextboxValue(wnd, "jk");
			tempat = func.getTextboxValue(wnd, "tempat");
			tgl = func.getDateboxText(wnd, "tgl");
			telp = func.getTextboxValue(wnd, "telp");
			Hp = func.getTextboxValue(wnd, "noHP");
			alamat = func.getTextboxValue(wnd, "alamat");
			RT = func.getTextboxValue(wnd, "RT");
			RW = func.getTextboxValue(wnd, "RW");
			kota = func.getListboxLabel(wnd, "lstKota");//func.getTextboxValue(wnd, "kota");
			prov = func.getTextboxValue(wnd, "prov");
			pos = func.getTextboxValue(wnd, "pos");
			work = func.getTextboxValue(wnd, "work");
			agama = func.getTextboxValue(wnd, "agama");
			status = func.getTextboxValue(wnd, "status");
			edu = func.getTextboxValue(wnd, "eduselect");
			norek = func.getTextboxValue(wnd, "norek");
			nopass = func.getTextboxValue(wnd, "nopass");
			tgl2 = func.getDateboxText(wnd, "tgl2");
			npwp = func.getTextboxValue(wnd, "npwp");
			ibu = func.getTextboxValue(wnd, "ibu");
			waris = func.getTextboxValue(wnd, "waris");
			warisadd = func.getTextboxValue(wnd, "warisadd");
			waristelp = func.getTextboxValue(wnd, "waristel");
			warisHP = func.getTextboxValue(wnd, "warishp");
			email = func.getTextboxValue(wnd, "email");
			noKartu = func.getTextboxValue(wnd,"nokar");
//			pass = func.getTextboxValue(wnd, "pass");
//			repass = func.getTextboxValue(wnd, "repass");
			if(func.getRadioValue(wnd, "rw4")) {
				if(check.isKosong(work)) {
					func.setTextboxFocus(wnd, "work");
					Messagebox.show("Pekerjan Lainnya tidak boleh kosong", "Truemoney", Messagebox.OK, Messagebox.ERROR);
					return;
				}
			}
			
//			if(func.getRadioValue(wnd, "re8")) {
//				if(check.isKosong(edu)) {
//					func.setTextboxFocus(wnd, "edu");
//					Messagebox.show("Pendidikan Terakhir Lainnya tidak boleh kosong", "Truemoney", Messagebox.OK, Messagebox.ERROR);
//					return;
//				}
//			}

			Cookies.setCookie("Tipe", "RegisterPromo");
			Cookies.setCookie("noHp", Hp);
			Cookies.setCookie("password", pass);
			Cookies.setCookie("namaLengkap", nama);
			Cookies.setCookie("noKTP", ktp);
			Cookies.setCookie("email", email);
			Cookies.setCookie("jenisKelamin", jk);
			Cookies.setCookie("tempatLahir", tempat);
			Cookies.setCookie("tglLahir", tgl);
			Cookies.setCookie("noTelepon", telp);
			Cookies.setCookie("alamat", alamat);
			Cookies.setCookie("rt", RT);
			Cookies.setCookie("rw", RW);
			Cookies.setCookie("kelurahan", kel1);
			Cookies.setCookie("kecamatan", kec);
			Cookies.setCookie("kota", kota);
			Cookies.setCookie("provinsi", prov);
			Cookies.setCookie("kodePos", pos);
			Cookies.setCookie("agama", agama);
			Cookies.setCookie("pekerjaan", work);
			Cookies.setCookie("statusPernikahan", status);
			Cookies.setCookie("pendidikanTerakhir", edu);
			Cookies.setCookie("ibuKandung", ibu);
			Cookies.setCookie("ahliWarisNama", waris);
			Cookies.setCookie("ahliWarisNoTelp", waristelp);
			Cookies.setCookie("ahliWarisNoHP", warisHP);
			Cookies.setCookie("ahliWarisAlamat", warisadd);
			Cookies.setCookie("namaBank", bank);
			Cookies.setCookie("noPaspor", nopass);
			Cookies.setCookie("tanggalBerlakuPaspor", tgl2);
			Cookies.setCookie("noRekening", norek);
			Cookies.setCookie("noNPWP", npwp);
			
			System.out.println("!!!!masuk Validasi Pass OK!!!!!!!");
			String Ibank = "False", Inpwp = "False", Ipassport = "False";
			if (func.getRadioValue(wnd, "rb1")) {
				Ibank = "True";
			}
			if (func.getRadioValue(wnd, "rnp1")) {
				Inpwp = "True";
			}
			if (func.getRadioValue(wnd, "rp1")) {
				Ipassport = "True";
			}

			JSONObject obj = new JSONObject();
			obj.put("Tipe", "RegisterPromo");
			obj.put("noHp", Hp);
			obj.put("password", pass);
			obj.put("namaLengkap", nama);
			obj.put("noKTP", ktp);
			obj.put("email", email);
			obj.put("jenisKelamin", jk);
			obj.put("tempatLahir", tempat);
			obj.put("tglLahir", tgl);
			obj.put("noTelepon", telp);
			obj.put("alamat", alamat);
			obj.put("rt", RT);
			obj.put("rw", RW);
			obj.put("kelurahan", kel1);
			obj.put("kecamatan", kec);
			obj.put("kota", kota);
			obj.put("provinsi", prov);
			obj.put("kodePos", pos);
			obj.put("agama", agama);
			obj.put("pekerjaan", work);
			obj.put("statusPernikahan", status);
			obj.put("pendidikanTerakhir", edu);
			obj.put("ibuKandung", ibu);
			obj.put("ahliWarisNama", waris);
			obj.put("ahliWarisNoTelp", waristelp);
			obj.put("ahliWarisNoHP", warisHP);
			obj.put("ahliWarisAlamat", warisadd);
			obj.put("namaBank", bank);
			obj.put("noPaspor", nopass);
			obj.put("tanggalBerlakuPaspor", tgl2);
			obj.put("IsiBank", Ibank);
			obj.put("noRekening", norek);
			obj.put("IsiPaspor", Ipassport);
			obj.put("IsiNPWP", Inpwp);
			obj.put("noNPWP", npwp);
			obj.put("noKartu", noKartu);
//			ProcessingListener processing = new ProcessingListener(this, obj.toString());
//			processing.processData();
			
			/**
			 * edit by adet, karena lebih simple langsung set session data json nya
			 */
//			sesi.setSessionAttribute("Tipe", "RegisterPromo");
//			sesi.setSessionAttribute("noHp", Hp);
//			sesi.setSessionAttribute("password", pass);
//			sesi.setSessionAttribute("namaLengkap", nama);
//			sesi.setSessionAttribute("noKTP", ktp);
//			sesi.setSessionAttribute("email", email);
//			sesi.setSessionAttribute("jenisKelamin", jk);
//			sesi.setSessionAttribute("tempatLahir", tempat);
//			sesi.setSessionAttribute("tglLahir", tgl);
//			sesi.setSessionAttribute("noTelepon", telp);
//			sesi.setSessionAttribute("alamat", alamat);
//			sesi.setSessionAttribute("rt", RT);
//			sesi.setSessionAttribute("rw", RW);
//			sesi.setSessionAttribute("kelurahan", kel1);
//			sesi.setSessionAttribute("kecamatan", kec);
//			sesi.setSessionAttribute("kota", kota);
//			sesi.setSessionAttribute("provinsi", prov);
//			sesi.setSessionAttribute("kodePos", pos);
//			sesi.setSessionAttribute("agama", agama);
//			sesi.setSessionAttribute("pekerjaan", work);
//			sesi.setSessionAttribute("statusPernikahan", status);
//			sesi.setSessionAttribute("pendidikanTerakhir", edu);
//			sesi.setSessionAttribute("ibuKandung", ibu);
//			sesi.setSessionAttribute("ahliWarisNama", waris);
//			sesi.setSessionAttribute("ahliWarisNoTelp", waristelp);
//			sesi.setSessionAttribute("ahliWarisNoHP", warisHP);
//			sesi.setSessionAttribute("ahliWarisAlamat", warisadd);
//			sesi.setSessionAttribute("namaBank", bank);
//			sesi.setSessionAttribute("noPaspor", nopass);
//			sesi.setSessionAttribute("tanggalBerlakuPaspor", tgl2);
//			sesi.setSessionAttribute("IsiBank", Ibank);
//			sesi.setSessionAttribute("noRekening", norek);
//			sesi.setSessionAttribute("IsiPaspor", Ipassport);
//			sesi.setSessionAttribute("IsiNPWP", Inpwp);
//			sesi.setSessionAttribute("noNPWP", npwp);
//			sesi.setSessionAttribute("noKartu", noKartu);
			sesi.setSessionAttribute("json_register", obj.toString());
			
//			if (ack.equalsIgnoreCase("OK")) {
//				Executions.sendRedirect(Constants.signup);
				sesi.setSessionAttribute(Constants.handPhone,Hp);
				sesi.setSessionAttribute("email", email);
				Executions.sendRedirect(Constants.uploadFoto);
//			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
	}

	
	public void daftarInsert(Window wnd) throws Exception {
		try {
			setWindow(wnd);
			nama = func.getTextboxValue(wnd, "nama");
			ktp = func.getTextboxValue(wnd, "ktp");
			jk = func.getTextboxValue(wnd, "jk");
			tempat = func.getTextboxValue(wnd, "tempat");
			tgl = func.getDateboxText(wnd, "tgl");
			telp = func.getTextboxValue(wnd, "telp");
			Hp = func.getTextboxValue(wnd, "noHP");
			alamat = func.getTextboxValue(wnd, "alamat");
			RT = func.getTextboxValue(wnd, "RT");
			RW = func.getTextboxValue(wnd, "RW");
			kota = func.getListboxLabel(wnd, "lstKota");//func.getTextboxValue(wnd, "kota");
			prov = func.getTextboxValue(wnd, "prov");
			pos = func.getTextboxValue(wnd, "pos");
			work = func.getTextboxValue(wnd, "work");
			agama = func.getTextboxValue(wnd, "agama");
			status = func.getTextboxValue(wnd, "status");
			edu = func.getTextboxValue(wnd, "eduselect");
			norek = func.getTextboxValue(wnd, "norek");
			nopass = func.getTextboxValue(wnd, "nopass");
			tgl2 = func.getDateboxText(wnd, "tgl2");
			npwp = func.getTextboxValue(wnd, "npwp");
			ibu = func.getTextboxValue(wnd, "ibu");
			waris = func.getTextboxValue(wnd, "waris");
			warisadd = func.getTextboxValue(wnd, "warisadd");
			waristelp = func.getTextboxValue(wnd, "waristel");
			warisHP = func.getTextboxValue(wnd, "warishp");
			email = func.getTextboxValue(wnd, "email");
			noKartu = func.getTextboxValue(wnd,"nokar");
//			pass = func.getTextboxValue(wnd, "pass");
//			repass = func.getTextboxValue(wnd, "repass");
			if(func.getRadioValue(wnd, "rw4")) {
				if(check.isKosong(work)) {
					func.setTextboxFocus(wnd, "work");
					Messagebox.show("Pekerjan Lainnya tidak boleh kosong", "Truemoney", Messagebox.OK, Messagebox.ERROR);
					return;
				}
			}
			
//			if(func.getRadioValue(wnd, "re8")) {
//				if(check.isKosong(edu)) {
//					func.setTextboxFocus(wnd, "edu");
//					Messagebox.show("Pendidikan Terakhir Lainnya tidak boleh kosong", "Truemoney", Messagebox.OK, Messagebox.ERROR);
//					return;
//				}
//			}

			Cookies.setCookie("Tipe", "RegisterMemberNew");
			Cookies.setCookie("noHp", Hp);
			Cookies.setCookie("password", pass);
			Cookies.setCookie("namaLengkap", nama);
			Cookies.setCookie("noKTP", ktp);
			Cookies.setCookie("email", email);
			Cookies.setCookie("jenisKelamin", jk);
			Cookies.setCookie("tempatLahir", tempat);
			Cookies.setCookie("tglLahir", tgl);
			Cookies.setCookie("noTelepon", telp);
			Cookies.setCookie("alamat", alamat);
			Cookies.setCookie("rt", RT);
			Cookies.setCookie("rw", RW);
			Cookies.setCookie("kelurahan", kel1);
			Cookies.setCookie("kecamatan", kec);
			Cookies.setCookie("kota", kota);
			Cookies.setCookie("provinsi", prov);
			Cookies.setCookie("kodePos", pos);
			Cookies.setCookie("agama", agama);
			Cookies.setCookie("pekerjaan", work);
			Cookies.setCookie("statusPernikahan", status);
			Cookies.setCookie("pendidikanTerakhir", edu);
			Cookies.setCookie("ibuKandung", ibu);
			Cookies.setCookie("ahliWarisNama", waris);
			Cookies.setCookie("ahliWarisNoTelp", waristelp);
			Cookies.setCookie("ahliWarisNoHP", warisHP);
			Cookies.setCookie("ahliWarisAlamat", warisadd);
			Cookies.setCookie("namaBank", bank);
			Cookies.setCookie("noPaspor", nopass);
			Cookies.setCookie("tanggalBerlakuPaspor", tgl2);
			Cookies.setCookie("noRekening", norek);
			Cookies.setCookie("noNPWP", npwp);
			
			System.out.println("!!!!masuk Validasi Pass OK!!!!!!!");
			String Ibank = "False", Inpwp = "False", Ipassport = "False";
			if (func.getRadioValue(wnd, "rb1")) {
				Ibank = "True";
			}
			if (func.getRadioValue(wnd, "rnp1")) {
				Inpwp = "True";
			}
			if (func.getRadioValue(wnd, "rp1")) {
				Ipassport = "True";
			}

			JSONObject obj = new JSONObject();
			obj.put("Tipe", "RegisterMemberNew");
			obj.put("noHp", Hp);
			obj.put("password", pass);
			obj.put("namaLengkap", nama);
			obj.put("noKTP", ktp);
			obj.put("email", email);
			obj.put("jenisKelamin", jk);
			obj.put("tempatLahir", tempat);
			obj.put("tglLahir", tgl);
			obj.put("noTelepon", telp);
			obj.put("alamat", alamat);
			obj.put("rt", RT);
			obj.put("rw", RW);
			obj.put("kelurahan", kel1);
			obj.put("kecamatan", kec);
			obj.put("kota", kota);
			obj.put("provinsi", prov);
			obj.put("kodePos", pos);
			obj.put("agama", agama);
			obj.put("pekerjaan", work);
			obj.put("statusPernikahan", status);
			obj.put("pendidikanTerakhir", edu);
			obj.put("ibuKandung", ibu);
			obj.put("ahliWarisNama", waris);
			obj.put("ahliWarisNoTelp", waristelp);
			obj.put("ahliWarisNoHP", warisHP);
			obj.put("ahliWarisAlamat", warisadd);
			obj.put("namaBank", bank);
			obj.put("noPaspor", nopass);
			obj.put("tanggalBerlakuPaspor", tgl2);
			obj.put("IsiBank", Ibank);
			obj.put("noRekening", norek);
			obj.put("IsiPaspor", Ipassport);
			obj.put("IsiNPWP", Inpwp);
			obj.put("noNPWP", npwp);
			obj.put("noKartu", noKartu);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			
			if (ack.equalsIgnoreCase("OK")) {
//				Executions.sendRedirect(Constants.signup);
				sesi.setSessionAttribute(Constants.handPhone,Hp);
				Executions.sendRedirect(Constants.uploadFoto);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
	}
	
	public void daftarInsertIqbal(Window wnd) throws Exception {
		try {
			setWindow(wnd);
			nama = func.getTextboxValue(wnd, "nama");
			ktp = func.getTextboxValue(wnd, "ktp");
			jk = func.getTextboxValue(wnd, "jk");
			tempat = func.getTextboxValue(wnd, "tempat");
			tgl = func.getDateboxText(wnd, "tgl");
			telp = func.getTextboxValue(wnd, "telp");
			Hp = func.getTextboxValue(wnd, "noHP");
			alamat = func.getTextboxValue(wnd, "alamat");
			RT = func.getTextboxValue(wnd, "RT");
			RW = func.getTextboxValue(wnd, "RW");
			kota = func.getListboxLabel(wnd, "lstKota");//func.getTextboxValue(wnd, "kota");
			prov = func.getTextboxValue(wnd, "prov");
			pos = func.getTextboxValue(wnd, "pos");
			work = func.getTextboxValue(wnd, "work");
			agama = func.getTextboxValue(wnd, "agama");
			status = func.getTextboxValue(wnd, "status");
			edu = func.getTextboxValue(wnd, "eduselect");
			norek = func.getTextboxValue(wnd, "norek");
			nopass = func.getTextboxValue(wnd, "nopass");
			tgl2 = func.getDateboxText(wnd, "tgl2");
			npwp = func.getTextboxValue(wnd, "npwp");
			ibu = func.getTextboxValue(wnd, "ibu");
			waris = func.getTextboxValue(wnd, "waris");
			warisadd = func.getTextboxValue(wnd, "warisadd");
			waristelp = func.getTextboxValue(wnd, "waristel");
			warisHP = func.getTextboxValue(wnd, "warishp");
			email = func.getTextboxValue(wnd, "email");
			noKartu = func.getTextboxValue(wnd,"nokar");
//			pass = func.getTextboxValue(wnd, "pass");
//			repass = func.getTextboxValue(wnd, "repass");
			if(func.getRadioValue(wnd, "rw4")) {
				if(check.isKosong(work)) {
					func.setTextboxFocus(wnd, "work");
					Messagebox.show("Pekerjan Lainnya tidak boleh kosong", "Truemoney", Messagebox.OK, Messagebox.ERROR);
					return;
				}
			}
			
//			if(func.getRadioValue(wnd, "re8")) {
//				if(check.isKosong(edu)) {
//					func.setTextboxFocus(wnd, "edu");
//					Messagebox.show("Pendidikan Terakhir Lainnya tidak boleh kosong", "Truemoney", Messagebox.OK, Messagebox.ERROR);
//					return;
//				}
//			}

			Cookies.setCookie("Tipe", "RegisterMemberNew");
			Cookies.setCookie("noHp", Hp);
			Cookies.setCookie("password", pass);
			Cookies.setCookie("namaLengkap", nama);
			Cookies.setCookie("noKTP", ktp);
			Cookies.setCookie("email", email);
			Cookies.setCookie("jenisKelamin", jk);
			Cookies.setCookie("tempatLahir", tempat);
			Cookies.setCookie("tglLahir", tgl);
			Cookies.setCookie("noTelepon", telp);
			Cookies.setCookie("alamat", alamat);
			Cookies.setCookie("rt", RT);
			Cookies.setCookie("rw", RW);
			Cookies.setCookie("kelurahan", kel1);
			Cookies.setCookie("kecamatan", kec);
			Cookies.setCookie("kota", kota);
			Cookies.setCookie("provinsi", prov);
			Cookies.setCookie("kodePos", pos);
			Cookies.setCookie("agama", agama);
			Cookies.setCookie("pekerjaan", work);
			Cookies.setCookie("statusPernikahan", status);
			Cookies.setCookie("pendidikanTerakhir", edu);
			Cookies.setCookie("ibuKandung", ibu);
			Cookies.setCookie("ahliWarisNama", waris);
			Cookies.setCookie("ahliWarisNoTelp", waristelp);
			Cookies.setCookie("ahliWarisNoHP", warisHP);
			Cookies.setCookie("ahliWarisAlamat", warisadd);
			Cookies.setCookie("namaBank", bank);
			Cookies.setCookie("noPaspor", nopass);
			Cookies.setCookie("tanggalBerlakuPaspor", tgl2);
			Cookies.setCookie("noRekening", norek);
			Cookies.setCookie("noNPWP", npwp);
			
			System.out.println("!!!!masuk Validasi Pass OK!!!!!!!");
			String Ibank = "False", Inpwp = "False", Ipassport = "False";
			if (func.getRadioValue(wnd, "rb1")) {
				Ibank = "True";
			}
			if (func.getRadioValue(wnd, "rnp1")) {
				Inpwp = "True";
			}
			if (func.getRadioValue(wnd, "rp1")) {
				Ipassport = "True";
			}

			JSONObject obj = new JSONObject();
			obj.put("Tipe", "RegisterMemberNew");
			obj.put("noHp", Hp);
			obj.put("password", pass);
			obj.put("namaLengkap", nama);
			obj.put("noKTP", ktp);
			obj.put("email", email);
			obj.put("jenisKelamin", jk);
			obj.put("tempatLahir", tempat);
			obj.put("tglLahir", tgl);
			obj.put("noTelepon", telp);
			obj.put("alamat", alamat);
			obj.put("rt", RT);
			obj.put("rw", RW);
			obj.put("kelurahan", kel1);
			obj.put("kecamatan", kec);
			obj.put("kota", kota);
			obj.put("provinsi", prov);
			obj.put("kodePos", pos);
			obj.put("agama", agama);
			obj.put("pekerjaan", work);
			obj.put("statusPernikahan", status);
			obj.put("pendidikanTerakhir", edu);
			obj.put("ibuKandung", ibu);
			obj.put("ahliWarisNama", waris);
			obj.put("ahliWarisNoTelp", waristelp);
			obj.put("ahliWarisNoHP", warisHP);
			obj.put("ahliWarisAlamat", warisadd);
			obj.put("namaBank", bank);
			obj.put("noPaspor", nopass);
			obj.put("tanggalBerlakuPaspor", tgl2);
			obj.put("IsiBank", Ibank);
			obj.put("noRekening", norek);
			obj.put("IsiPaspor", Ipassport);
			obj.put("IsiNPWP", Inpwp);
			obj.put("noNPWP", npwp);
			obj.put("noKartu", noKartu);
//			ProcessingListener processing = new ProcessingListener(this, obj.toString());
//			processing.processData();
			
			/**
			 * edit by adet, karena lebih simple langsung set session data json nya
			 */
//			sesi.setSessionAttribute("Tipe", "RegisterMemberNew");
//			sesi.setSessionAttribute("noHp", Hp);
//			sesi.setSessionAttribute("password", pass);
//			sesi.setSessionAttribute("namaLengkap", nama);
//			sesi.setSessionAttribute("noKTP", ktp);
//			sesi.setSessionAttribute("email", email);
//			sesi.setSessionAttribute("jenisKelamin", jk);
//			sesi.setSessionAttribute("tempatLahir", tempat);
//			sesi.setSessionAttribute("tglLahir", tgl);
//			sesi.setSessionAttribute("noTelepon", telp);
//			sesi.setSessionAttribute("alamat", alamat);
//			sesi.setSessionAttribute("rt", RT);
//			sesi.setSessionAttribute("rw", RW);
//			sesi.setSessionAttribute("kelurahan", kel1);
//			sesi.setSessionAttribute("kecamatan", kec);
//			sesi.setSessionAttribute("kota", kota);
//			sesi.setSessionAttribute("provinsi", prov);
//			sesi.setSessionAttribute("kodePos", pos);
//			sesi.setSessionAttribute("agama", agama);
//			sesi.setSessionAttribute("pekerjaan", work);
//			sesi.setSessionAttribute("statusPernikahan", status);
//			sesi.setSessionAttribute("pendidikanTerakhir", edu);
//			sesi.setSessionAttribute("ibuKandung", ibu);
//			sesi.setSessionAttribute("ahliWarisNama", waris);
//			sesi.setSessionAttribute("ahliWarisNoTelp", waristelp);
//			sesi.setSessionAttribute("ahliWarisNoHP", warisHP);
//			sesi.setSessionAttribute("ahliWarisAlamat", warisadd);
//			sesi.setSessionAttribute("namaBank", bank);
//			sesi.setSessionAttribute("noPaspor", nopass);
//			sesi.setSessionAttribute("tanggalBerlakuPaspor", tgl2);
//			sesi.setSessionAttribute("IsiBank", Ibank);
//			sesi.setSessionAttribute("noRekening", norek);
//			sesi.setSessionAttribute("IsiPaspor", Ipassport);
//			sesi.setSessionAttribute("IsiNPWP", Inpwp);
//			sesi.setSessionAttribute("noNPWP", npwp);
//			sesi.setSessionAttribute("noKartu", noKartu);
			sesi.setSessionAttribute("json_register", obj.toString());
			sesi.setSessionAttribute(Constants.handPhone,Hp);
			sesi.setSessionAttribute("email", email);
			Executions.sendRedirect(Constants.uploadFoto);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
	}
	
	public void setWindow (Window wnd) {
		this.win = wnd;
	}
	
	public Window getWindow() {
		return this.win;
	}
	
	public void cekAkun(Window wnd, Textbox nokar, Textbox noHP) {
//		Textbox nokar = step0.getFellow("nokar");
//		Textbox noHP  = step0.getFellow("noHP");
		
		Div step0 = (Div) wnd.getFellow("step0");
		Div step1 = (Div) wnd.getFellow("step1");
		boolean isValid = false;
		
//		if (nokar.getValue().trim().isEmpty()){
//			Clients.showNotification("No Kartu tidak boleh kosong");
//			isValid = false;
//			func.setTextboxFocus(wnd,"nokar");
//		}
//		else if (!nokar.getValue().trim().isEmpty() && !cek.isAngka(nokar.getValue().trim().toString()) && !cek.checkLength(nokar.getValue().trim().toString(), 12)) {
//			System.out.println(nokar.toString());
//			Clients.showNotification("No Kartu Tidak Valid. Silahkan Coba Kembali");
//			isValid = false;
//			func.setTextboxFocus(wnd,"nokar");
//		}
//		
//		else if (noHP.getValue().trim().isEmpty()){
//			Clients.showNotification("No Handphone tidak boleh kosong");
//			isValid = false;
//			func.setTextboxFocus(wnd,"noHP");
//		}
//		else if (!noHP.getValue().trim().isEmpty() && !cek.isAngka(noHP.getValue().trim().toString()) && !cek.checkLength(noHP.getValue().trim().toString(), 8, 15)) {
//			Clients.showNotification("No Handphone Tidak Valid. Silahkan Coba Kembali");
//			isValid = false;
//			func.setTextboxFocus(wnd,"noHP");
//		}
//		else {
//			isValid = true;
//		}
		
		if (check.isNull(nokar.getValue().trim())) {
		    Clients.showNotification("Isi Nomor Kartu...", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		   } else if (!check.isNull(nokar.getValue().trim()) && !check.isNumber(nokar.getValue().trim())) {
		    Clients.showNotification("Isi Nomor Kartu dengan Angka...", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		   } else if (!check.isNull(nokar.getValue().trim()) && !check.checkLength(nokar.getValue().trim(), 12)) {
		    Clients.showNotification("Nomor Kartu tidak valid...", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		   } else if (check.isNull(noHP.getValue().trim())) {
		    Clients.showNotification("Isi Nomor Handphone...", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		   } else if (!check.isNull(noHP.getValue().trim()) && !check.isNumber(noHP.getValue().trim())) {
		    Clients.showNotification("Isi Nomor Handphone dengan Angka...", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		   } else if (!check.isNull(noHP.getValue().trim()) && !check.checkLength(noHP.getValue().trim(), 8, 15)) {
		    Clients.showNotification("Nomor Handphone tidak valid...", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		   }
		   else {
			   isValid = true;
		   }
		
		if (isValid){
			boolean reg = cekMember(wnd);
			System.out.println(reg + "123");
			if (reg){
				step0.setVisible(false);
				step1.setVisible(true);
			}else {
//				Clients.showNotification("Member Belum terdaftar");
			}
		}
		else {
//			Clients.showNotification("Silahkan Periksa Kembali data Anda");
		}
		
		
	}
	
	public void cekSelfData(Window wnd,Textbox nama,Textbox ktp,Textbox email, Textbox jk, Textbox tempat, Datebox tgl,Textbox telp,Textbox alamat,Textbox RT, Textbox RW,Bandbox bandkel, Bandbox bandkec,Textbox pos){
		
		try {
			boolean isValid = false;
			Div step1 = (Div) wnd.getFellow("step1");
			Div step2 = (Div) wnd.getFellow("step2");
			
			Textbox hp = (Textbox) wnd.getFellow("noHP");
			Textbox nokartu = (Textbox) wnd.getFellow("nokar");
			
			Listbox lstKota = (Listbox) wnd.getFellow("lstKota");
			String kota = func.getListboxValue(lstKota);
			
			if (nama.getValue().trim().isEmpty()) {
				Clients.showNotification("Nama Lengkap tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				nama.setFocus(true);
				isValid = false;
			} else if (ktp.getValue().isEmpty()) { // KTP
				Clients.showNotification("KTP tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				ktp.setFocus(true);
				isValid = false;
			} 
			else if (!ktp.getValue().isEmpty() && (ktp.getValue().length() > 16 || ktp.getValue().length() <12)) { // KTP Max and Min Value
				Clients.showNotification("KTP tidak Valid", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				ktp.setFocus(true);
				isValid = false;
			}
			else if (!ktp.getValue().isEmpty() && !check.isNumber(ktp.getValue().trim())) {
				Clients.showNotification("KTP harus angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				func.setTextboxFocus(wnd, "ktp");
				isValid = false;
			} 
			else if (email.getValue().isEmpty()) {
				Clients.showNotification("Email tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				func.setTextboxFocus(wnd, "email");
				isValid = false;
			} else if (!email.getValue().isEmpty() && !check.cekValidasiEmail(email.getValue())) {
				Clients.showNotification("Format Email tidak benar", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				func.setTextboxFocus(wnd, "email");
				isValid = false;
			}
			else if (jk.getValue().isEmpty()) {
				Clients.showNotification("Pilih jenis Kelamin", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				jk.setFocus(true);
				isValid = false;
			} else if (tempat.getValue().trim().isEmpty()) {
				Clients.showNotification("Tempat Lahir tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				tempat.setFocus(true);
				isValid = false;
			} else if (tgl.getText().isEmpty()) {
				Clients.showNotification("Tanggal Lahir tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				tgl.setFocus(true);
				isValid = false;
			} 
//			else if (telp.getValue().trim().isEmpty()) {
//				Clients.showNotification("Telepon tidak Valid");
//				func.setTextboxFocus(wnd, "telp");
//				isValid = false;
//			} 
			else if (!telp.getValue().trim().isEmpty() && !check.isNumber(telp.getValue().trim())) {
				Clients.showNotification("No. Telepon harus diisi angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				func.setTextboxFocus(wnd, "telp");
				isValid = false;
			} else if (hp.getValue().trim().isEmpty()) {
				Clients.showNotification("No. Handphone tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				alamat.setFocus(true);
				isValid = false;
			} else if (!hp.getValue().trim().isEmpty() && !check.isNumber(hp.getValue().trim())) {
				Clients.showNotification("No. Handphone harus diisi angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				func.setTextboxFocus(wnd, "telp");
				isValid = false;
			} else if (alamat.getValue().trim().isEmpty()) {
				Clients.showNotification("Alamat tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				alamat.setFocus(true);
				isValid = false;
			} else if (RT.getValue().isEmpty()) {
				Clients.showNotification("RT tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				RT.setFocus(true);
				isValid = false;
			} else if (!RT.getValue().isEmpty() && !check.isNumber(RT.getValue().trim())) {
				Clients.showNotification("RT harus angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				func.setTextboxFocus(wnd, "RT");
				isValid = false;
			} else if (RW.getValue().isEmpty()) {
				Clients.showNotification("RW tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				RW.setFocus(true);
				isValid = false;
			} else if (!RW.getValue().isEmpty() && !check.isNumber(RW.getValue().trim())) {
				Clients.showNotification("RW harus angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				func.setTextboxFocus(wnd, "RW");
				isValid = false;
			} else if (bandkel.getValue().isEmpty()) {
				Clients.showNotification("Silahkan Pilih Kelurahan", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				func.setTextboxFocus(wnd, "bandkel");
				isValid = false;
			} else if (bandkec.getValue().isEmpty()) {
				Clients.showNotification("Silahkan Pilih Kecamatan", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				func.setTextboxFocus(wnd, "bandkec");
				isValid = false;
			} else if (kota.trim().equalsIgnoreCase("PILIHKOTA")) {
				Clients.showNotification("Silahkan Pilih Kota", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				isValid = false;
			} else if (pos.getValue().isEmpty()) {
				Clients.showNotification("Kode Pos tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				pos.setFocus(true);
				isValid = false;
			} else if (!pos.getValue().isEmpty() && !check.isNumber(pos.getValue().trim())) {
				Clients.showNotification("Kode Pos harus diisi angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				func.setTextboxFocus(wnd, "pos");
				isValid = false;
			}
			 
			else {
				isValid = true;
			}
			
			if (isValid) {
				
				String adaKartu = func.getLabelValue(wnd, "punyaKartu");
				
				if (adaKartu.equals("ya")) {
					
					JSONObject obj2 = new JSONObject();
					obj2.put("Tipe","CEK_HP_EMAIL_EXIST");
					obj2.put("handphone", hp.getValue().trim());
					obj2.put("email", email.getValue().trim());
					obj2.put("nokartu", nokartu.getValue().trim());
					ProcessingListener processing = new ProcessingListener(this, obj2.toString());
					processing.processData(); 
					System.out.println("nilai proses setelah meminta validasi -> " + proses);
					log.info("CEK_HP_EMAIL_EXIST -> " + prosesBaru);
					if(prosesBaru.equals("OK")){
						
						step1.setVisible(false);
						step2.setVisible(true);
					}
					
				} else if (adaKartu.equals("tidak")) {
					JSONObject obj2 = new JSONObject();
					obj2.put("Tipe","CEK_HP_EMAIL");
					obj2.put("handphone", hp.getValue().trim());
					obj2.put("email", email.getValue().trim());
					ProcessingListener processing = new ProcessingListener(this, obj2.toString());
					processing.processData(); 
					log.info("CEK_HP_EMAIL -> " + proses);
					if(proses.equals("OK")){
						
						step1.setVisible(false);
						step2.setVisible(true);
					}
				}
				
				
				
				
			} else { 
//				Clients.showNotification("Silahkan Periksa Kembali Data Anda");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void cekAddData(Window wnd,Radio rb1,Textbox norek,Radio rp1,Textbox nopass,Radio rnp1,Textbox npwp){
		boolean isValid = false;
		Div step2 = (Div) wnd.getFellow("step2");
		Div step3 = (Div) wnd.getFellow("step3");
		
		Listbox list = (Listbox) wnd.getFellow("bank");
		String s_tgl = func.getDateboxText(wnd, "tgl2");
		
		if (rb1.isChecked() && list.getSelectedItem().toString().isEmpty()) {
			Clients.showNotification("Nama Bank harus diisi", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "bank");
			isValid = false;
		}
		else if (rb1.isChecked() && norek.getValue().isEmpty()) {
			Clients.showNotification("Nomor Rekening harus diisi", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "norek");
			isValid = false;
		}
		else if (rb1.isChecked() && !check.isNumber(norek.getValue())) {
			Clients.showNotification("Nomor Rekening harus diisi angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "norek");
			isValid = false;
		} 
		
		else if (rp1.isChecked() && nopass.getValue().trim().isEmpty()) {
			Clients.showNotification("Nomor Paspor harus tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "nopass");
			isValid = false;
		} 
		else if (rp1.isChecked() && s_tgl.isEmpty()) {
			Clients.showNotification("Tanggal Berlaku Paspor Belum Dipilih", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setDateboxFocus(wnd,"tgl2");
			isValid = false;
		} //-------------------------------------
		else if (rnp1.isChecked() && npwp.getValue().trim().isEmpty()) {
			Clients.showNotification("Nomor NPWP harus tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "npwp");
			isValid = false;
		} else if (rnp1.isChecked() && !check.isNumber(npwp.getValue())) {
			Clients.showNotification("Nomor NPWP harus diisi angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "npwp");
			isValid = false;
		}
		else {
			isValid = true;
		}
		
		if (isValid) {
			step2.setVisible(false);
			step3.setVisible(true);
		} else { 
//			Clients.showNotification("Silahkan Periksa Kembali Data Anda");
		}
	}
	
	public void cekWarisData(Window wnd,Textbox ibu,Textbox waris,Textbox warisadd,Textbox waristel, Textbox warishp) throws Exception {
		boolean isValid = false;
		
//		Div step3 = (Div) wnd.getFellow("step3");
//		Div step4 = (Div) wnd.getFellow("step4");
		
		if (ibu.getValue().trim().isEmpty()) {
			Clients.showNotification("Nama Ibu Kandung tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "ibu");
			isValid = false;
		} else if (waris.getValue().trim().isEmpty()) {
			Clients.showNotification("Nama Ahli Waris tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "waris");
			isValid = false;
		} else if (warisadd.getValue().trim().isEmpty()) {
			Clients.showNotification("Alamat Ahli Waris tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "warisadd");
			isValid = false;
		} else if (waristel.getValue().isEmpty()) {
			Clients.showNotification("Telepon Ahli Waris tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "waristel");
			isValid = false;
		} else if (!waristel.getValue().isEmpty() && !check.isNumber(waristel.getValue())) {
			Clients.showNotification("Teleopn Ahli Waris harus angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "waristel");
			isValid = false;
		} else if (warishp.getValue().isEmpty()) {
			Clients.showNotification("No. Handphone Ahli Waris tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "warishp");
			isValid = false;
		} else if (!warishp.getValue().isEmpty() && !check.isNumber(warishp.getValue())) {
			Clients.showNotification("No. Handphone Ahliwaris harus angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "warishp");
			isValid = false;
		} else {
			isValid = true;
		}
		
		if (isValid) {
			String punyaKartu = func.getLabelValue(wnd, "punyaKartu");
			System.out.println("punya kartu -> " + punyaKartu);
			if (punyaKartu.equals("ya")) {
				// update
				System.out.println("ya");
				daftarIqbal(wnd);
			} else if (punyaKartu.equals("tidak")) {
				// insert
				System.out.println("tidak");
				daftarInsertIqbal(wnd);
			}
			
		} else { 
//			Clients.showNotification("Silahkan Periksa Kembali Data Anda");
		}
		
	}
	
	 public void recheck(Window wnd){
		boolean isValid = false;
		Listbox list = (Listbox) wnd.getFellow("bank");
		String s_tgl = func.getDateboxText(wnd, "tgl2");
		   Button button = (Button) wnd.getFellow("button");
		   Checkbox read = (Checkbox) wnd.getFellow("read");
		   Textbox nama = (Textbox) wnd.getFellow("nama");
		   Textbox ktp = (Textbox) wnd.getFellow("ktp");
		   Textbox jk = (Textbox) wnd.getFellow("jk");
		   Textbox tempat = (Textbox) wnd.getFellow("tempat");
		   Datebox tgl = (Datebox) wnd.getFellow("tgl");
		   Textbox HP = (Textbox) wnd.getFellow("noHP");
		   Textbox telp = (Textbox) wnd.getFellow("telp");
		   Textbox alamat = (Textbox) wnd.getFellow("alamat");
		   Textbox RT = (Textbox) wnd.getFellow("RT");
		   Textbox RW = (Textbox) wnd.getFellow("RW");
		   Textbox pos = (Textbox) wnd.getFellow("pos");
		   Textbox email = (Textbox) wnd.getFellow("email");
		   Radio rb1 = (Radio) wnd.getFellow("rb1");
		   Radio rp1 = (Radio) wnd.getFellow("rp1");
		   Bandbox bandkel = (Bandbox) wnd.getFellow("bandkel");
		   Bandbox bandkec = (Bandbox) wnd.getFellow("bandkec");
		   Textbox norek = (Textbox) wnd.getFellow("norek");
		   Radio rnp1 = (Radio) wnd.getFellow("rnp1");
		   Textbox npwp = (Textbox) wnd.getFellow("npwp");
		   Textbox ibu = (Textbox) wnd.getFellow("ibu");
		   Textbox waris = (Textbox) wnd.getFellow("waris");
		   Textbox warisadd = (Textbox) wnd.getFellow("warisadd");
		   Textbox waristel = (Textbox) wnd.getFellow("waristel");
		   Textbox warishp = (Textbox) wnd.getFellow("warishp");
		   Textbox noHP = (Textbox) wnd.getFellow("noHP");
		   Textbox nopass = (Textbox) wnd.getFellow("nopass");
		
		if (nama.getValue().isEmpty()) {
			Clients.showNotification("Nama Lengkap tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			nama.setFocus(true);
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (ktp.getValue().isEmpty()) { // KTP
			Clients.showNotification("KTP tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			ktp.setFocus(true);
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (!ktp.getValue().isEmpty() && !check.isNumber(ktp.getValue())) {
			Clients.showNotification("KTP harus angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "ktp");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (jk.getValue().isEmpty()) {
			Clients.showNotification("Pilih jenis Kelamin", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			jk.setFocus(true);
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (tempat.getValue().isEmpty()) {
			Clients.showNotification("Tempat Lahir tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			tempat.setFocus(true);
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (tgl.getText().isEmpty()) {
			Clients.showNotification("Tanggal Lahir tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			tgl.setFocus(true);
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} 
//		else if (HP.getValue().isEmpty()) {
//			Clients.showNotification("No Handphone tidak boleh kososng");
//			HP.setFocus(true);
//			isValid = false;
//			button.setDisabled(true);
//			read.setChecked(false);
//		} 
//		else if (!HP.getValue().isEmpty() && !cekValidasiAngka(HP.getValue())) {
//			Clients.showNotification("No. Handphone harus angka");
//			func.setTextboxFocus(wnd, "HP");
//			isValid = false;
//			button.setDisabled(true);
//			read.setChecked(false);
//		} 
		else if (!telp.getValue().isEmpty() && !check.isNumber(telp.getValue())) {
			Clients.showNotification("No. Telepon harus diisi angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "telp");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (alamat.getValue().isEmpty()) {
			Clients.showNotification("Alamat tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			alamat.setFocus(true);
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (RT.getValue().isEmpty()) {
			Clients.showNotification("RT tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			RT.setFocus(true);
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (!RT.getValue().isEmpty() && !check.isNumber(RT.getValue())) {
			Clients.showNotification("RT harus angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "RT");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (RW.getValue().isEmpty()) {
			Clients.showNotification("RW tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			RW.setFocus(true);
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (!RW.getValue().isEmpty() && !check.isNumber(RW.getValue())) {
			Clients.showNotification("RW harus angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "RW");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (bandkel.getValue().isEmpty()) {
			Clients.showNotification("Silahkan Pilih Kelurahan", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "bandkel");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (bandkec.getValue().isEmpty()) {
			Clients.showNotification("Silahkan Pilih Kecamatan", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "bandkec");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (pos.getValue().isEmpty()) {
			Clients.showNotification("Kode Pos tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			pos.setFocus(true);
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (!pos.getValue().isEmpty() && !check.isNumber(pos.getValue())) {
			Clients.showNotification("Kode Pos harus diisi angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "pos");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (rb1.isChecked() && list.getSelectedItem().toString().isEmpty()) {
			Clients.showNotification("Nama Bank harus diisi", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "bank");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (rb1.isChecked() && norek.getValue().isEmpty()) {
			Clients.showNotification("Nomor Rekening harus tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "norek");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (rb1.isChecked() && !norek.getValue().isEmpty() && !check.isNumber(norek.getValue())) {
			Clients.showNotification("Nomor Rekening harus diisi angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "norek");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (rp1.isChecked() && nopass.getValue().isEmpty()) {
			Clients.showNotification("Nomor Paspor harus tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "nopass");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		}  
		else if (rp1.isChecked() && s_tgl.isEmpty()) {
			Clients.showNotification("Tanggal Berlaku Paspor Belum Dipilih", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setDateboxFocus(wnd,"tgl2");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} //-------------------------------------
		else if (rnp1.isChecked() && npwp.getValue().isEmpty()) {
			Clients.showNotification("Nomor NPWP harus tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "npwp");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (rnp1.isChecked() && !npwp.getValue().isEmpty() && !check.isNumber(npwp.getValue())) {
			Clients.showNotification("Nomor NPWP harus diisi angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "npwp");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} //-------------------------------
		else if (ibu.getValue().trim().isEmpty()) {
			Clients.showNotification("Nama Ibu Kandung tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "ibu");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (waris.getValue().trim().isEmpty()) {
			Clients.showNotification("Nama Ahli Waris tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "waris");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (warisadd.getValue().trim().isEmpty()) {
			Clients.showNotification("Alamat Ahli Waris tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "warisadd");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (waristel.getValue().isEmpty()) {
			Clients.showNotification("Telepon Ahli Waris tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "waristel");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (!waristel.getValue().isEmpty() && !check.isNumber(waristel.getValue())) {
			Clients.showNotification("Teleopn Ahli Waris harus angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "waristel");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (warishp.getValue().isEmpty()) {
			Clients.showNotification("No. Handphone Ahli Waris tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "warishp");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (!warishp.getValue().isEmpty() && !check.isNumber(warishp.getValue())) {
			Clients.showNotification("No. Handphone Ahliwaris harus angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "warishp");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (email.getValue().isEmpty()) {
			Clients.showNotification("Email tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "email");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);
		} else if (!email.getValue().isEmpty() && !check.cekValidasiEmail(email.getValue())) {
			Clients.showNotification("Format Email tidak benar", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "email");
			isValid = false;
			button.setDisabled(true);
			read.setChecked(false);

		} else {
			isValid = true;
			button.setDisabled(false);
		}
	}
	 
	 public boolean validasiKartu(Window wnd) {
			boolean registered = false;
			try {
				
				String nomorKartu = func.getTextboxValue(wnd, "nokar");
				
				if (check.isNil(nomorKartu)) {
					Clients.showNotification("Isi Nomor Kartu Anda.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				} else if (!check.isNil(nomorKartu) && !check.isNumber(nomorKartu)) {
					Clients.showNotification("Nomor Kartu harus angka.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				} else if (!check.isNil(nomorKartu) && !check.checkLength(nomorKartu, 12)) {
					Clients.showNotification("Nomor Kartu tidak valid.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				} else {
					JSONObject obj2 = new JSONObject();
					obj2.put("Tipe","CEK_KARTU");
					obj2.put("nomor_kartu",nomorKartu);
					ProcessingListener processing = new ProcessingListener(this, obj2.toString());
					processing.processData(); 
					System.out.println("ack punya kartu : " + proses);
					log.info("CEK_KARTU : " + proses);
					if(proses.equals("OK")){
						registered = true;
						func.setLabelValue(wnd, "punyaKartu", "ya");
						func.setDivVisible(wnd, "ifYa", false);
						func.setDivVisible(wnd, "step1", true);
					}
				}
				
			
			}
			catch (Exception e) {
				Clients.showNotification("Terjadi Kesalahan ", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				e.printStackTrace();
			}
			
			return registered;
		}
	 
	 public void setListboxValueKota(Window wnd,String idList,JSONArray data) throws Exception {
			
			try {
			Listbox list = (Listbox) wnd.getFellow(idList);
			list.getItems().clear();
			int a = data.size();
			
			//int lIndex = 0;
			
			if (a > 1) {
				
				Listitem li = new Listitem();
				li.setValue("PILIHKOTA");
				li.appendChild(new Listcell("--- PILIH ---"));
				li.setSelected(true);
				list.appendChild(li);
				
				for (int i =0;i<a;i++){
					li = new Listitem();
					JSONObject child  = (JSONObject) data.get(i);
					li.setValue(child.get("namaProvinsi").toString());
					String nama = child.get("namaKota").toString();
					li.appendChild(new Listcell(nama));
					list.appendChild(li);
				}
			} else if (a == 1) {
				JSONObject child  = (JSONObject) data.get(0);
				Listitem li = new Listitem();
				li.setValue(child.get("namaProvinsi").toString());
				li.appendChild(new Listcell(child.get("namaKota").toString()));
				li.setSelected(true);
				list.appendChild(li);
				
				func.setTextboxValue(wnd, "prov", child.get("namaProvinsi").toString());
			}
			
			
			} catch (Exception e){
				log.info("Error di PLN ." + e.getMessage());
			}
		}
	 
	 public void setProv(Window wnd) throws Exception {
		 Listbox lstKota = (Listbox) wnd.getFellow("lstKota");
		 String value = func.getListboxValue(lstKota);
		 if (value.equalsIgnoreCase("PILIHKOTA")) {
			 func.setTextboxValue(wnd, "prov", "");
		 } else {
			 func.setTextboxValue(wnd, "prov", value);
		 }
	 }

}
