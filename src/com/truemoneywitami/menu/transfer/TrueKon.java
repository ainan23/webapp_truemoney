package com.truemoneywitami.menu.transfer;

import java.util.Date;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import  com.truemoneywitami.function.ZKFunction;
import  com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.ProcessingListenerPrabayar;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;

public class TrueKon extends Window implements RequestListener {
	
	Logger log = Logger.getLogger(TrueKon.class);
	String id="",
	nama="",
	nominal="",
	keterangan="",
	biaya="",
	pesan="",
	saldo="",
	id_trx="";
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	CheckParameter check = new CheckParameter();
	JSONObject obj;
	String ack="NOK";
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	//String username = "0852235455";
	int total;String block="";
	JSONArray arr = new JSONArray();
	Window win;int count =0;
	String send_trx = "";
	Window winheader;
	long transfersend,transferget,transfertime;
	@Override
	public void requestSucceeded(JSONObject obj){	 
		System.out.println("tuts");
		if(obj.get("ACK").toString().equals("OK") && obj.get("ID").equals("biaya")){
			arr = (JSONArray) obj.get("biayaAdmin");
		}
		else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").equals("Transfer")){
			transferget = new Date().getTime();
			transfertime = transferget - transfersend;
			String m1 = "Waktu saat Penerimaan  Transfer True " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk Transfer adalah" +transfertime + " ms";
			func.cetakLog(m1, m2);
			pesan = obj.get("pesan").toString();
			id_trx = obj.get("id_Trx").toString();
			saldo = obj.get("lastBalance").toString();
			ack = obj.get("ACK").toString();
			
			//Executions.sendRedirect("transferTruesukses.zul");	
		}
		else if (obj.get("ACK").equals("OK") && obj.get("ID").equals("Block")){
			block = obj.get("ACK").toString();
		}
		
	}
	
	@Override
	public void requestFailed(String message){
		//System.out.println("tutos");
		try {
		Window wnd = getWindow();
		
		if (message.contains("PIN")) {
			Clients.showNotification("PIN Anda Salah. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			count++;
			if (count >3){
				JSONObject obj = new JSONObject();
				obj.put("Tipe", "BlockAccount");
				obj.put("username", username);
				ProcessingListener processing = new ProcessingListener(this, obj.toString());
				processing.processData();
				if (block.equals("OK")){
					Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
					func.setTextboxDisabled(wnd, "pin", true);
				}
				else {
					System.out.println("Akun Gagal di Blokir");
				}
			}
		}
		else if (message.contains("blokir") && count ==3){
			count++;
			Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxDisabled(wnd, "pin", true);
		}
		else if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		}
		else if(message.equals("TIMEOUT")) {
			ack = "NOK";
			Clients.showNotification(Constants.MSG_TIMEOUT_ERROR, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else {
			//System.out.println("tets");
			sesi.setSessionAttribute(Constants.tipePesan,"TransferTrue");
			sesi.setSessionAttribute(Constants.pesan,message);
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("transaksiGagal",this.winheader);
		}
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	public void setWindow (Window wnd) {
		this.win = wnd;
	}
	
	public Window getWindow() {
		return this.win;
	}
	
	public void setData(Window wnd,Window header)  {
		try {
		setWindow(wnd);
		setMenuWindow(header);
		id = sesi.getSessionAttribute(Constants.noRekeningTujuan);
		nama = sesi.getSessionAttribute(Constants.namaAccountTujuan);
		nominal =sesi.getSessionAttribute(Constants.nominal);
		keterangan =sesi.getSessionAttribute(Constants.issue);
		String ktp = sesi.getSessionAttribute(Constants.ktp);
		JSONObject obj = new JSONObject();
		obj.put("Tipe","requestBiayaAdmin");
		obj.put("nominal",nominal);
		obj.put("id_TipeTransaksi", 5);
		obj.put("id_TipeAplikasi", 2);
		obj.put("username", username);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		int fee = (int) arr.get(0);
		biaya = String.valueOf(fee);
		sesi.setSessionAttribute(Constants.biaya, biaya);
		func.setLabelValue(wnd, "id_tuju",": " + id);
		func.setLabelValue(wnd, "nama", ": " + nama);
		func.setLabelValue(wnd, "ktp", ": " + ktp);
		func.setLabelValue(wnd, "issue",": " + keterangan);
		func.setLabelValue(wnd, "nominal",": " +func.formatRp(nominal));
		func.setLabelValue(wnd, "biaya",": " +func.formatRp(biaya));
		int jumlah = Integer.parseInt(nominal);
		int charge = Integer.parseInt(biaya);
		total = charge + jumlah;
		sesi.setSessionAttribute(Constants.total, String.valueOf(total));
		func.setLabelValue(wnd, "total",": " +func.formatRp(String.valueOf(total)));
		} catch (Exception e){
			log.info("Error di Transfer True." + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void doTransfer(Window wnd,Window header) {
		try {
		JSONObject obj = new JSONObject();
		String pin = func.getTextboxValue(wnd, "pin");
		String ktp = sesi.getSessionAttribute(Constants.ktp);
		
		if(check.isKosong(pin)) {
			Clients.showNotification("PIN tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else if (!(check.pinIsValid(pin))) {
			Clients.showNotification("PIN tidak valid, Silahkan coba kembali.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else {
			String checked = sesi.getSessionAttribute("check");
//			send_trx = func.getIDTRX();
			send_trx = sesi.getSessionAttribute("id_trx");
			if (username.startsWith("0")){
				obj.put("Tipe","TransferTrueWM");
			}
			else {
				obj.put("Tipe","TransferTrueWMA");
			}
			obj.put("username",username);
			obj.put("nominal",nominal);
			obj.put("idAccountTujuan",id);
			obj.put("biayaTransferTrue",biaya);
			obj.put("verifikasi","PIN");
			obj.put("hargaCetak", String.valueOf(total));
			obj.put("PIN",pin);
			obj.put("issue",keterangan);
			obj.put("id_TipeAplikasi",2);
			obj.put("id_trx", send_trx);
			obj.put("ktp", ktp);
			System.out.println(nominal);
			String m1 = "Waktu saat Pengiriman Transfer True" + new Date().toString();
			String m2 = "";
			func.cetakLog(m1, m2);
			transfersend = new Date().getTime();
			ProcessingListenerPrabayar processing = new ProcessingListenerPrabayar(this, obj.toString());
			processing.processData();
			if (ack.equalsIgnoreCase("OK")){
				sesi.setSessionAttribute(Constants.tipePesan,"TransferTrue");
				sesi.setSessionAttribute(Constants.header, "Transfer True Ke True");
				sesi.setSessionAttribute(Constants.pesan,"Transfer True ke True Sukses");
				sesi.setSessionAttribute(Constants.id_trx,id_trx);
				sesi.setSessionAttribute(Constants.status, "Transfer Berhasil");
				RedirectMenu menu = new RedirectMenu();
				menu.setMenuLink("truesukses",header);
			}
			else {
				
			}
		}
		
		
		} catch (Exception e){
			log.info("Error di Transfer True." + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void setBack(Window wnd)  {
		try {
		RedirectMenu menu = new RedirectMenu();
		menu.setMenuLink("truehome",wnd);
		} catch (Exception e){
			log.info("Error di Transfer True." + e.getMessage());
		}
	}
	
	public void setMenuWindow(Window header){
		this.winheader= header;
	}
}
