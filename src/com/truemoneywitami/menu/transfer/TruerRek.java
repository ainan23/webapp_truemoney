package com.truemoneywitami.menu.transfer;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;

public class TruerRek extends Window implements RequestListener {

	Logger log = Logger.getLogger(TruerRek.class);
	String id="";
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	JSONObject obj;
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername(); 
	
	private CheckParameter check = new CheckParameter();
	
	String nama="",
	status="",
	pesan="";
	
	@Override
	public void requestSucceeded(JSONObject obj) {

		if (obj.get("ACK").toString().equals("OK")) {
			nama = obj.get("namaTujuan").toString();
			status = obj.get("namaStatus").toString();
		} else {
			pesan = obj.get("pesan").toString();
			requestFailed(pesan);
		}
	}

	@Override
	public void requestFailed(String message) {
		//Clients.showNotification(message);
		status = "failed";
		
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}

	public void setData(Window wnd,Window header)  {
			try {
			id = sesi.getSessionAttribute(Constants.noRekeningTujuan);
			nama = sesi.getSessionAttribute(Constants.namaAccountTujuan);
//			String checked = sesi.getSessionAttribute("check");
//			JSONObject obj = new JSONObject();
//			obj.put("Tipe", "RequestTrueWM");
//			obj.put("idAccountTujuan", id);
//			obj.put("username", username);
//			obj.put("simpan", checked);
//			ProcessingListener processing = new ProcessingListener(this, obj.toString());
//			processing.processData();
//			if (status.equalsIgnoreCase("failed")) {
//				func.setLabelValue(wnd, "id_member", ":" + id);
//				func.setLabelValue(wnd, "HP_member", ":" + "ID Member tidak ditemukan");
//				func.setButtonDisabled(wnd, "next", true);
//				
//			}
//			else {
				func.setLabelValue(wnd, "id_member", ":" + id);
				func.setLabelValue(wnd, "HP_member", ":" + nama);
//			}
			
			} catch (Exception e){
				log.info("Error di Transfer True." + e.getMessage());
				e.printStackTrace();
			}
	}

	public void nextStep(Window wnd,Window header)  {
		try {
		String nominal="", ket="";
		nominal = func.getTextboxValue(wnd, "nominal");
		ket = func.getTextboxValue(wnd, "ket");
		String ktp = func.getTextboxValue(wnd, "ktp").trim();
		if (ktp.length() == 0) {
			Clients.showNotification("Silahkan Isi Nomor KTP", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (!check.numberValidator(ktp)) {
			Clients.showNotification("Nomor KTP tidak valid", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (!check.checkLength(ktp, 16)) {
			Clients.showNotification("Nomor KTP harus 16 digit", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (nominal.equals("")){
			Clients.showNotification("Silahkan Isi Jumlah Nominal", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (ket.equals("")) {
			Clients.showNotification("Silahkan Isi Keterangan", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else {
			try {
			long test = Long.parseLong(nominal);
			if (test < Constants.minamount){
				Clients.showNotification("Minimal Jumlah Transfer Rp.20.000", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else if (test > Constants.maxamount) {
				Clients.showNotification("Maximal Jumlah Transfer Rp " + func.formatNumber(Constants.maxamount, "###,###"), Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else {
				sesi.setSessionAttribute(Constants.noRekeningTujuan, id);
				sesi.setSessionAttribute(Constants.namaAccountTujuan, nama);
				sesi.setSessionAttribute(Constants.nominal, nominal);
				sesi.setSessionAttribute(Constants.issue, ket);
				sesi.setSessionAttribute(Constants.ktp, ktp);
				RedirectMenu menu = new RedirectMenu();
				menu.setMenuLink("truekonfirm",header);
			}
			
			} catch (Exception e) {
				Clients.showNotification("Nominal Tidak Valid", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
		}
		
		} catch (Exception e){
			log.info("Error di Transfer True." + e.getMessage());
		}
	}

}
