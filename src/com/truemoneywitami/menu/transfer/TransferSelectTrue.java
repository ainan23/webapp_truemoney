package com.truemoneywitami.menu.transfer;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.transferpos.TruePos;
import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;

public class TransferSelectTrue extends Window implements RequestListener {
	
	private static Logger log = Logger.getLogger(TransferSelectTrue.class);
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	CheckParameter check = new CheckParameter();
	JSONObject obj;
	String pesan,ack="";
	String namaTujuan = "";
	String namaStatus = "";
	JSONArray norek;
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	//String username = "0852235455";
	String idt="";
	private Window wndHeader;
	
	@Override
	public void requestSucceeded(JSONObject obj) {	 
		
		ack = obj.get("ACK").toString();
		log.info("ack -> " + ack);
		
		String id = "";
		try { id = obj.get("ID").toString(); } catch (Exception e) { id = ""; }
		
		if (obj.get("ACK").toString().equals("OK") && id.equalsIgnoreCase("Rek")){
//			System.out.println("masuk sini 1");
//			ack = obj.get("ACK").toString();
			norek = (JSONArray) obj.get("norek");
		} else if (obj.get("ACK").toString().equals("OK")) {
			System.out.println("masuk sini 2");
//			ack = obj.get("ACK").toString();
//			norek = (JSONArray) obj.get("norek");
			namaTujuan = obj.get("namaTujuan").toString();
			namaStatus = obj.get("namaStatus").toString();
			
			if (ack.equals("OK")) {
				System.out.println("masuk sini 3");
				sesi.setSessionAttribute(Constants.noRekeningTujuan, idt);
				sesi.setSessionAttribute(Constants.namaAccountTujuan, namaTujuan);
				RedirectMenu menu = new RedirectMenu();
				menu.setMenuLink("truerek",wndHeader);
			}
		}
		else {
			System.out.println("masuk sini 4");
			pesan = obj.get("pesan").toString();
			requestFailed(pesan);
		}
	}
	
	@Override
	public void requestFailed(String message){
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
	public void setData(Window wnd) {
		try {
			String idTrx = func.getIDTRX();
			sesi.setSessionAttribute("id_trx", idTrx);
			String tipeAcc = sesi.getSessionAttribute("tipe").toString();
			System.out.println("tipe acc : " + tipeAcc);
			if (tipeAcc.equalsIgnoreCase("agent")) {
				func.setRadioLabel(wnd, "radio2", "ID Member / ID Agent Account");
			}
		JSONObject obj = new JSONObject();
		obj.put("Tipe","RekDataTrue");
		obj.put("username", username);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		setListboxValueRek(wnd,"rek_sel", norek, 0, true, true, true);
		} catch (Exception e){
			//log.info("Error di Transfer True." + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void setRekening(Window wnd)  {
		try {
			idt = func.getListboxValue(wnd, "rek_sel");
			} catch (Exception ex){
				log.info("Error di Transfer True." + ex.getMessage());
			}
		
	}
	
	public void setListboxValueRek(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID)  {
		try {
		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int a = data.size();
		
		//int lIndex = 0;
		
		for (int i =0;i<a;i++){
			Listitem li = new Listitem();
			JSONObject child  = (JSONObject) data.get(i);
			li.setValue(child.get("norek").toString());
			String nama = child.get("norek").toString();
			li.appendChild(new Listcell(nama));
			list.appendChild(li);
		}
		} catch (Exception e) {
			log.info("Error di Transfer True." + e.getMessage());
		}
	}
	
	public void confirm(Window wnd,Window header) throws Exception {
		try {
			wndHeader = header;
		String checked="";
		Checkbox ck = (Checkbox) wnd.getFellow("l1");
//		Checkbox ck2 = (Checkbox) wnd.getFellow("l2");
		Radio rd = (Radio) wnd.getFellow("radio1");
		Radio rd2 = (Radio) wnd.getFellow("radio2");
//		if (ck.isChecked() || ck2.isChecked()){
//			checked = "OK";
//		}
		
		if (ck.isChecked()){
			checked = "OK";
		}
		
		if (rd.isSelected()){
			setRekening(wnd);
		}
		else {
			if (rd2.isSelected()){
				idt = func.getTextboxValue(wnd,"id");
			}
			else {
//				idt = func.getTextboxValue(wnd, "HP");
			}
		}
		 
		if(idt.equals("")){
			//Messagebox.show("ID Tujuan Tidak Boleh Kosong","Error", Messagebox.OK, Messagebox.ERROR);
//			Clients.showNotification("ID Member / No HP Tujuan Tidak Boleh Kosong");
			Clients.showNotification("No Rekening Tujuan Tidak Boleh Kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else {
			if (!check.isNumber(idt)) {
//				Clients.showNotification("ID Member / No HP Tujuan tidak valid. Silahkan coba kembali");
				Clients.showNotification("No Rekening Tujuan tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else if (idt.equalsIgnoreCase(username)){
				//Messagebox.show("Dilarang transfer ke rekening Pribadi","Error", Messagebox.OK, Messagebox.ERROR);
				Clients.showNotification("Dilarang transfer ke rekening Pribadi", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else if (idt.length() > 14 || idt.length() < 9){
//				Clients.showNotification("ID Member / No HP Tujuan  tidak valid. Silahkan coba kembali");
				Clients.showNotification("No Rekening Tujuan tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else {
				JSONObject obj = new JSONObject();
				obj.put("Tipe", "RequestTrueWM");
				obj.put("idAccountTujuan", idt);
				obj.put("username", username);
				obj.put("simpan", checked);
				ProcessingListener processing = new ProcessingListener(this, obj.toString());
				processing.processData();
			}
			
		}
		}catch (Exception e){
			log.info("Error di Transfer True." + e.getMessage());
		}
		}
}
