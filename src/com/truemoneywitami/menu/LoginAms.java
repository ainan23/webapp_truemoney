package com.truemoneywitami.menu;


import java.util.ResourceBundle;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.Function;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.session.mob_Session;

public class LoginAms extends Window implements RequestListener  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(LoginAms.class);
	private static ResourceBundle rb = ResourceBundle.getBundle("config.config");
	
	Function func = new Function();
	
	JSONObject obj;
	mob_Session sesi = new mob_Session();
	public void loginTo(String user, String pass){
		try {
//			String tipeUser = "";
//			JSONObject obj = new JSONObject();
//			obj.put("Tipe", "SignIn");
//			obj.put("username", user);
//			obj.put("password", pass);
//			if (user.startsWith("0")) {
//				tipeUser = "member";
//			} else {
//				tipeUser = "agent";
//			}
//			obj.put("tipeUser", tipeUser);
			String passSha1 = func.sha1(pass).toUpperCase();
			String request = "{\"Tipe\":\"AMS\", \"NIK\":\""+user+"\", \"KEY\":\""+passSha1+"\"}";
			ProcessingListener processing = new ProcessingListener(this, request);
			processing.processData();
			
//			String userServer = ;
//			String passServer = ;
//			
//			if (user.equals(userServer) && pass.equals(passServer)) {
//				(Sessions.getCurrent()).setAttribute("username", user);
//				
//				Executions.getCurrent().sendRedirect("../ams/index.zul");
//			} else {
//				Clients.showBusy("Username / Password Salah.");
//			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			requestFailed(Constants.generalErrorMessage);
		}
	}

	@Override
	public void requestSucceeded(JSONObject data){
		// TODO Auto-generated method stub
		
		String username = data.get("AMS").toString();
		(Sessions.getCurrent()).setAttribute("username",username);
		
		Executions.getCurrent().sendRedirect("../ams/index.zul");
	}

	@Override
	public void requestFailed(String message) {
		// TODO Auto-generated method stub
		if(message.equals("01")) {
			Executions.getCurrent().sendRedirect("../ams/login.zul");
		} else if (message.equals("LOGIN01")) {
			Clients.showNotification("Login gagal. Username / Password salah.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else if (message.equals("LOGIN02")) {
			Clients.showNotification("Login gagal. Username tidak terdaftar.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else if (message.equals("LOGIN03")) {
			Clients.showNotification("Login gagal. Username tidak valid.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
	public String setUsernameFromCookies(){
		String username = "";
		Execution exec = Executions.getCurrent();
		Cookie[] cookies = ((HttpServletRequest)exec.getNativeRequest()).getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				username = cookie.getValue();
			}
		}
		log.info("username : " + username);
		log.error("error username : " + username);
		return username;
	}
	
	public void onCreate() {
		log.warn("revisi " + rb.getString("revisi"));
	}
}
