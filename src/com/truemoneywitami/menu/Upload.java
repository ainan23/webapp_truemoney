package com.truemoneywitami.menu;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.ResourceBundle;

import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.util.media.Media;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ImageFunction;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.ProcessingListenerRegistrasi;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.session.mob_Session;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadBase.FileSizeLimitExceededException;
import org.apache.log4j.Logger;

public class Upload extends Window implements RequestListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger log = Logger.getLogger(Upload.class);
	
	FileUpload fu = new FileUpload();
	
	mob_Session sesi = new mob_Session();
	String fotowajah;
	String fotoktp;
	String fotottd;
	String HP="";
	String ack="";
	
	private Media mediaWajah;
	private Media mediaKtp;
	private Media mediaTtd;
	
	String nama="",
			ktp="",
			jk="",
			telp="",
			Hp="",
			tgl="",
			tempat="",
			alamat="", RT="", RW="", kel1="", kec="", kota="", prov="", pos="", work="", agama="", status="", edu="",
			bank="-", norek="", nopass="", tgl2="", npwp="", ibu="", waris="", warisadd="", waristelp="", warisHP="", email="", pass="", repass = "",tipe="";
	String noKartu="",Ibank="",Ipassport="",Inpwp="";
	@Override
	public void requestSucceeded(JSONObject obj) {
		// TODO Auto-generated method stub
		ack = obj.get("ACK").toString();
		
	}

	@Override
	public void requestFailed(String message) {
		// TODO Auto-generated method stub
		
		
	}
	
	public void setData(Window wnd) {
		try {
		HP = sesi.getSessionAttribute(Constants.handPhone);
		if (sesi.getSessionAttribute("json_register") == null){
			Executions.sendRedirect("signup.zul");	
		}
		/**
		 * edit by adet, karena lebih simple langsung get session data json nya
		 */
//		tipe = sesi.getSessionAttribute("Tipe");
//		pass = sesi.getSessionAttribute("password");
//		nama = sesi.getSessionAttribute("namaLengkap");
//		ktp = sesi.getSessionAttribute("noKTP");
//		email = sesi.getSessionAttribute("email");
//		jk = sesi.getSessionAttribute("jenisKelamin");
//		tempat = sesi.getSessionAttribute("tempatLahir");
//		tgl = sesi.getSessionAttribute("tglLahir");
//		telp = sesi.getSessionAttribute("noTelepon");
//		alamat = sesi.getSessionAttribute("alamat");
//		RT = sesi.getSessionAttribute("rt");
//		RW = sesi.getSessionAttribute("rw");
//		kel1= sesi.getSessionAttribute("kelurahan");
//		kec = sesi.getSessionAttribute("kecamatan");
//		kota = sesi.getSessionAttribute("kota");
//		prov = sesi.getSessionAttribute("provinsi");
//		pos = sesi.getSessionAttribute("kodePos");
//		agama = sesi.getSessionAttribute("agama");
//		work = sesi.getSessionAttribute("pekerjaan");
//		status = sesi.getSessionAttribute("statusPernikahan");
//		edu = sesi.getSessionAttribute("pendidikanTerakhir");
//		ibu = sesi.getSessionAttribute("ibuKandung");
//		waris = sesi.getSessionAttribute("ahliWarisNama");
//		waristelp = sesi.getSessionAttribute("ahliWarisNoTelp");
//		warisHP = sesi.getSessionAttribute("ahliWarisNoHP");
//		warisadd = sesi.getSessionAttribute("ahliWarisAlamat");
//		bank = sesi.getSessionAttribute("namaBank");
//		nopass = sesi.getSessionAttribute("noPaspor");
//		tgl2 = sesi.getSessionAttribute("tanggalBerlakuPaspor");
//		Ibank = sesi.getSessionAttribute("IsiBank");
//		norek = sesi.getSessionAttribute("noRekening");
//		Ipassport = sesi.getSessionAttribute("IsiPaspor");
//		Inpwp = sesi.getSessionAttribute("IsiNPWP");
//		npwp = sesi.getSessionAttribute("noNPWP");
//		noKartu = sesi.getSessionAttribute("noKartu");
		
//		System.out.println("batu--------->>>"  +HP);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void uploadFoto(Media media, String hp, String tipeFoto) {
		BufferedInputStream in = null;
		BufferedOutputStream out = null;
		
		String pathTemp = "";
		String pathDestination = "";
		String fileName = "";
		try {
			ResourceBundle rb = ResourceBundle.getBundle("config.config");
			
			pathTemp = rb.getString("url_report_path");
			pathDestination = rb.getString("url_destination_path");
			fileName = "MEMBER_" + hp +"_"+ tipeFoto +".jpeg";
			
			int size = media.getByteData().length / 1024;
			
			InputStream inputStream = media.getStreamData();
			
			if (size > 1024 && size <= 5120) {
				// compress
				ImageFunction imageFunction = new ImageFunction();
				imageFunction.compressImage(pathTemp + fileName, inputStream);
			} else {
				in = new BufferedInputStream(inputStream);
				
				File file = new File(pathTemp + fileName);//sesi.getUsername() + "_" + idTipePembayaran + "_" + sdf.format(new Date()) + "_" + func.generateUuid() + ".xlsx");
				OutputStream fout = new FileOutputStream(file);
				out = new BufferedOutputStream(fout);
				byte buffer[] = new byte[1024];
				int ch = in.read(buffer);
				while (ch != -1) {
					out.write(buffer, 0, ch);
					ch = in.read(buffer);
				}
			}
			
			
			
			
		} catch (Exception e) {
			log.error("exception saat upload foto ke server", e);
		} finally {
			try {
				out.close();
			} catch (Exception e) {
				
			}
			
			try {
				in.close();
			} catch (Exception e) {
				
			}
			
			try {
				copy(pathTemp, pathDestination, fileName);
			} catch (Exception e) {
				
			}
		}
	}
	
//	public static void main(String [] args) {
//		try {
//			copy();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	public void copy(String pathSource, String pathDestination, String fileName) {
		JSch jsch = new JSch();
		Session session = null;
		try {
			ResourceBundle rb = ResourceBundle.getBundle("config.config");
			
			String username = rb.getString("sftp_user");
			String password = rb.getString("sftp_password");
			String url = rb.getString("sftp_url");
			int port = Integer.parseInt(rb.getString("sftp_port"));
			
			session = jsch.getSession(username, url, port);
            session.setPassword(password);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            log.info("session connected.");
            
            ChannelSftp channelSftp = null;
            channelSftp = (ChannelSftp) session.openChannel("sftp");
            log.info("channel open");
            channelSftp.connect();
            log.info("channel connected");
            channelSftp.put(pathSource+fileName, pathDestination+fileName);

            log.info("Upload Success");
            
            
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception saat copy file to server", e);
		} finally {
			try {
				session.disconnect();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public void setMedia(byte[] media, int param,String type) {
		if (param == 1){
			fotowajah = Base64.encodeBase64URLSafeString(media) +"kaizen" +type;
		} else if (param==2) {
			fotoktp = Base64.encodeBase64URLSafeString(media) +"kaizen" +type;
		}
		else if (param ==3){
			fotottd =  Base64.encodeBase64URLSafeString(media) +"kaizen" +type;
		}
	}
	
	public void setMedia(byte[] media, int param,String type,Media data) {
		if (param == 1){
			fotowajah = Base64.encodeBase64URLSafeString(media) +"kaizen" +type;
			this.mediaWajah = data;
			uploadFoto(data, HP, "WAJAH");
		} else if (param==2) {
			fotoktp = Base64.encodeBase64URLSafeString(media) +"kaizen" +type;
			this.mediaKtp = data;
			uploadFoto(data, HP, "KTP");
		}
		else if (param ==3){
			fotottd =  Base64.encodeBase64URLSafeString(media) +"kaizen" +type;
			this.mediaTtd = data;
			uploadFoto(data, HP, "TTD");
		}
	}
	
	public void doUpload(Window wnd){
		try {
////			HP = "081906241066";
//			System.out.println("foto wajah ---->>>>" +fotowajah);
//			System.out.println("foto ktp--->>>" +fotoktp);
//			System.out.println("foto ttd--->>>" +fotottd);
			if (fotowajah == null || fotoktp == null || fotottd == null){
				Clients.showNotification("Semua Foto Harus di Isi", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else {
//				System.out.println("INI TIPE NYA LOOH>>>" +tipe);
				JSONObject obj = new JSONObject();
				
				/**
				 * edit by adet, karena lebih simple langsung get session data json nya
				 */
//				obj.put("Tipe", tipe);
//				obj.put("noHp", HP);
//				obj.put("password", pass);
//				obj.put("namaLengkap", nama);
//				obj.put("noKTP", ktp);
//				obj.put("email", email);
//				obj.put("jenisKelamin", jk);
//				obj.put("tempatLahir", tempat);
//				obj.put("tglLahir", tgl);
//				obj.put("noTelepon", telp);
//				obj.put("alamat", alamat);
//				obj.put("rt", RT);
//				obj.put("rw", RW);
//				obj.put("kelurahan", kel1);
//				obj.put("kecamatan", kec);
//				obj.put("kota", kota);
//				obj.put("provinsi", prov);
//				obj.put("kodePos", pos);
//				obj.put("agama", agama);
//				obj.put("pekerjaan", work);
//				obj.put("statusPernikahan", status);
//				obj.put("pendidikanTerakhir", edu);
//				obj.put("ibuKandung", ibu);
//				obj.put("ahliWarisNama", waris);
//				obj.put("ahliWarisNoTelp", waristelp);
//				obj.put("ahliWarisNoHP", warisHP);
//				obj.put("ahliWarisAlamat", warisadd);
//				obj.put("namaBank", bank);
//				obj.put("noPaspor", nopass);
//				obj.put("tanggalBerlakuPaspor", tgl2);
//				obj.put("IsiBank", Ibank);
//				obj.put("noRekening", norek);
//				obj.put("IsiPaspor", Ipassport);
//				obj.put("IsiNPWP", Inpwp);
//				obj.put("noNPWP", npwp);
//				obj.put("noKartu", noKartu);
				ProcessingListenerRegistrasi processing = new ProcessingListenerRegistrasi(this, sesi.getSessionAttribute("json_register"));
				processing.processData();
				
				if (ack.equals("OK")){
					Sessions.getCurrent().removeAttribute("json_register");
					obj = new JSONObject();
					obj.put("Tipe", "uploadFoto");
					obj.put("noHp", HP);
					obj.put("fotowajah","MEMBER_"+HP+"_WAJAH.jpeg");
					obj.put("fotoktp", "MEMBER_"+HP+"_KTP.jpeg");
					obj.put("fotottd", "MEMBER_"+HP+"_TTD.jpeg");
//					System.out.println("objek yg dikirim " +obj.toString());
					processing = new ProcessingListenerRegistrasi(this, obj.toString());
					processing.processData();
					
					if (ack.equals("OK")){
//						Cookies.setCookie(Constants.pesan, "Proses Unggah Berhasil");
//						Clients.showNotification("Proses Unggah Berhasil");
//						Thread.sleep(2000);
						Messagebox.show("Proses registrasi Anda berhasil", "Selamat", Messagebox.OK, Messagebox.INFORMATION);
						Executions.sendRedirect("signup_successMember.zul");
					} else {
						Clients.showNotification("Proses Unggah Gagal. Silahkan Coba Kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
					}

				}
				else {
					Clients.showNotification("Gagal. Silahkan Coba Kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				}
				
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}
}
