package com.truemoneywitami.menu.transferbank;

//import java.sql.Date;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.transferpos.TruePos;
import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.ProcessingListenerPrabayar;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;

public class TransferBankconfirm extends Window implements RequestListener {
	
	Logger log = Logger.getLogger(TransferBankconfirm.class);
	ZKFunction func = new ZKFunction();
	CheckParameter check = new CheckParameter();
	JSONObject obj;
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	String ack="",inq="";
	//String username = "0852235455";
	String nama="",
	hp="",
	norek="",
	issue="",
	nominal="",
	ben_name="",
	biaya="",
	bank="";
	String pesan="",
	id_trx="";
	
	String block="";
	String hargaCetak="",
	biayaTransferBank="",
	nominalTransfer="",
	namaTujuan="",
	nomorRekeningTujuan="",
	namaBank="",
	Status="",
	TrxId="",
	Berita="",
	ResponseMsgKey="",
	namaPengirim="",
	hpPengirim="",
	Signature="",
	checked="";
	Window win;
	Window winheader;
	String send_trx = "";
	long transfersend,transferget,transfertime;
	long inqsend,inqget,inqtime;
	int count =0;
	String status = "";
	
	@Override
	public void requestSucceeded(JSONObject obj) {

		if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("inquiry")) {
			inqget = new Date().getTime();
			inqtime = inqget - inqsend;
			String m1 = "Waktu saat Penerimaan Inq Transfer Bank " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk Inq Transfer Bank adalah" +inqtime +" ms";
			func.cetakLog(m1, m2);
			inq = obj.get("ACK").toString();
			hargaCetak = obj.get("hargaCetak").toString();
			biayaTransferBank = obj.get("biayaTransferBank").toString();
			nominalTransfer = obj.get("nominalTransfer").toString();
			namaTujuan = obj.get("namaTujuan").toString();
			nomorRekeningTujuan = obj.get("nomorRekeningTujuan").toString();
			namaBank = obj.get("namaBank").toString();
			bank = obj.get("kodeBank").toString();//countryObj.put("kodeBank",kodeBank);
			Status = obj.get("Status").toString();
			TrxId = obj.get("TrxId").toString();
			Berita = obj.get("Berita").toString();
			ResponseMsgKey = obj.get("ResponseMsgKey").toString();
			namaPengirim = obj.get("namaPengirim").toString();
			hpPengirim = obj.get("hpPengirim").toString();
			Signature = obj.get("Signature").toString();

		} else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("transfer")) {
			transferget = new Date().getTime();
			transfertime = transferget - transfersend;
			String m1 = "Waktu saat Penerimaan  Transfer Bank " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk Transfer Bank adalah" +transfertime +" ms";
			func.cetakLog(m1, m2);
			id_trx = obj.get("id_Trx").toString();
			pesan = obj.get("pesan").toString();
			ack = obj.get("ACK").toString(); 
			status = obj.get("status").toString();
		}
		else if (obj.get("ACK").equals("OK") && obj.get("ID").equals("Block")){
			block = obj.get("ACK").toString();
		}
		else {
			pesan = obj.get("pesan").toString();
			requestFailed(pesan);
		}
	}

	public void setWindow (Window wnd) {
		this.win = wnd;
	}
	
	public void setMenuWindow(Window wnd) {
		this.winheader = wnd;
	}
	
	
	public Window getWindow() {
		return this.win;
	}
	
	@Override
	public void requestFailed(String message) {
		inq = "NOK";
		status = "NOK";
		
		try {
		Window wnd = getWindow();
		if (message.contains("PIN")) {
			Clients.showNotification("PIN Anda Salah. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			count++;
			if (count >3){
				JSONObject obj = new JSONObject();
				obj.put("Tipe", "BlockAccount");
				obj.put("username", username);
				ProcessingListener processing = new ProcessingListener(this, obj.toString());
				processing.processData();
				if (block.equals("OK")){
					Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
					func.setTextboxDisabled(wnd, "pin", true);
				}
				else {
					System.out.println("Akun Gagal di Blokir");
				}
			}
		}
		else if (message.contains("blokir") && count ==3){
			count++;
			Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxDisabled(wnd, "pin", true);
		}
		else if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		}
		else if(message.equals("TIMEOUT")) {
			ack = "NOK";
//			Clients.showNotification(Constants.MSG_TIMEOUT_ERROR, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			sesi.setSessionAttribute(Constants.tipePesan,"TransferBank");
			sesi.setSessionAttribute(Constants.pesan,Constants.MSG_TIMEOUT_ERROR);
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("transaksiGagal",this.winheader);
		}
		else {
//			Constants.tipePesan ="TransferBank";
//			Constants.pesan = message;
			sesi.setSessionAttribute(Constants.tipePesan,"TransferBank");
			sesi.setSessionAttribute(Constants.pesan,message);
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("transaksiGagal",this.winheader);
		}
		} catch (Exception e) {
			
		}
	}

	public void setData(Window wnd,Window header)  {
		try {
		setWindow(wnd);
		setMenuWindow(header);
		norek = sesi.getSessionAttribute(Constants.noRekeningTujuan);
		ben_name = sesi.getSessionAttribute(Constants.namaAccountTujuan);
		bank = sesi.getSessionAttribute(Constants.bank);
		nama = sesi.getSessionAttribute(Constants.sender_name);
		hp = sesi.getSessionAttribute(Constants.sender_cell);
		issue = sesi.getSessionAttribute(Constants.issue);
		nominal = sesi.getSessionAttribute(Constants.nominal);
		biaya = sesi.getSessionAttribute(Constants.biaya);
		String ktp = sesi.getSessionAttribute(Constants.ktp);
		checked = sesi.getSessionAttribute("check");
//		send_trx = func.getIDTRX();
		send_trx = sesi.getSessionAttribute("id_trx");
		func.setLabelValue(wnd, "trx", send_trx);
		String date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
		func.setLabelValue(wnd, "waktu", date);
		JSONObject obj = new JSONObject();
		obj.put("Tipe", "RequestBank");
		obj.put("username", username);
		obj.put("BeneficiaryAccount", norek);
		obj.put("BeneficiaryName", ben_name);
		obj.put("kodeBank", bank);
		obj.put("senderAccount", username);
		obj.put("senderName", nama);
		obj.put("senderCellular", hp);
		obj.put("transferIssue", issue);
		obj.put("nominal", nominal);
		obj.put("biayaTransferBank", biaya);
		obj.put("simpan",checked);
		obj.put("id_trx", send_trx);
		obj.put("senderKtp", ktp);
		obj.put("benef_city", sesi.getSessionAttribute("benef_city"));
		
		log.info("request t2b inquiry -> " + obj.toString());
		inqsend = new Date().getTime();
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		
		if (inq.equals("OK")){
			try {
				sesi.setSessionAttribute("namapengirim", namaPengirim);
				
				func.setLabelValue(wnd, "sen_name", nama);
				func.setLabelValue(wnd, "sen_cell", hp);
				func.setLabelValue(wnd, "ben_name", ben_name);
				func.setLabelValue(wnd, "nama", namaTujuan);
				func.setLabelValue(wnd, "bank_name", namaBank);
				func.setLabelValue(wnd, "ben_account", nomorRekeningTujuan);
				func.setLabelValue(wnd, "amount", func.formatRp(nominalTransfer));
				func.setLabelValue(wnd, "issue", Berita);
				func.setLabelValue(wnd, "sum_amount", func.formatRp(nominalTransfer));
				func.setLabelValue(wnd, "charge_fee", func.formatRp(biayaTransferBank));
				func.setLabelValue(wnd, "total", func.formatRp(hargaCetak));
				func.setLabelValue(wnd, "inq_status", Status);
				func.setLabelValue(wnd, "ktp", ktp);
				} catch (Exception e) {
					//Messagebox.show("Terjadi Kesalahan","Error", Messagebox.OK, Messagebox.ERROR);
					func.setLabelValue(wnd, "nama", "-");
				}
		}
		else {
//			Clients.showNotification("Koneksi ke Server Gagal");
			func.setLabelValue(wnd, "sen_name", "-");
			func.setLabelValue(wnd, "sen_cell", "-");
			func.setLabelValue(wnd, "ben_name", "-");
			func.setLabelValue(wnd, "nama", "-");
			func.setLabelValue(wnd, "bank_name", "-");
			func.setLabelValue(wnd, "ben_account", "-");
			func.setLabelValue(wnd, "amount","-");
			func.setLabelValue(wnd, "issue", "-");
			func.setLabelValue(wnd, "sum_amount", "-");
			func.setLabelValue(wnd, "charge_fee", "-");
			func.setLabelValue(wnd, "total", "-");
			func.setLabelValue(wnd, "inq_status", "-");
			func.setLabelValue(wnd, "ktp", "-");
		}
		
		} catch (Exception e){
			log.info("Error di Transfer Bank." + e.getMessage());
		}
		
	}

	public void doTransfer(Window wnd,Window header) throws Exception {
		try {
		String pin = func.getTextboxValue(wnd, "pin");
		
		log.info("username yg dilempar : " + username);
		

		String ktp = sesi.getSessionAttribute(Constants.ktp);
		
		if(check.isKosong(pin)) {
			Clients.showNotification("PIN tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else if (!(check.pinIsValid(pin))) {
			Clients.showNotification("PIN tidak valid, Silahkan coba kembali.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else {
			JSONObject obj = new JSONObject();
			if (username.startsWith("0")) {
//				obj.put("Tipe", "transferTB");
				obj.put("Tipe", "TransferBank");
			} else {
//				obj.put("Tipe", "transferTBA");
				obj.put("Tipe", "TransferBankA");
			}
			obj.put("username", username);
//			obj.put("BeneficiaryAccount", nomorRekeningTujuan);
			obj.put("nomorRekening", nomorRekeningTujuan);
			obj.put("BeneficiaryName", namaTujuan);
			obj.put("transferIssue", issue);
			obj.put("kodeBank", bank);
//			obj.put("senderAccount", );
			obj.put("senderName", namaPengirim);
			obj.put("SenderCellular", hpPengirim);
			obj.put("nominal", nominalTransfer);
			obj.put("biayaTransferBank", biayaTransferBank);
			obj.put("PIN", pin);
			obj.put("hargaCetak", hargaCetak);
			obj.put("messageKey", ResponseMsgKey);
//			obj.put("id_trx", send_trx);
			obj.put("TrxId", send_trx);
			obj.put("ktp", ktp);
			log.info("request t2b transfer -> " + obj.toString());
			String m1 = "Waktu saat Pengiriman  Transfer Bank " + new Date().toString();
			String m2 = "";
			func.cetakLog(m1, m2);
			transfersend = new Date().getTime();
			ProcessingListenerPrabayar processing = new ProcessingListenerPrabayar(this, obj.toString());
			processing.processData();
			if (ack.equalsIgnoreCase("OK")){
//				Constants.tipePesan = "TransferBank";
//				Constants.pesan = "Transfer True ke Bank Sukses";
				sesi.setSessionAttribute(Constants.tipePesan,"TransferBank");
				
				if (status.equals("OK")) {
					sesi.setSessionAttribute(Constants.pesan,"Transfer True ke Bank Sukses");
				} else {
					sesi.setSessionAttribute(Constants.pesan,"Transfer True ke Bank Pending");
				}
				
				sesi.setSessionAttribute(Constants.header, "Transfer True Ke Bank");
				sesi.setSessionAttribute(Constants.id_trx,send_trx);
				sesi.setSessionAttribute(Constants.status, status);
				sesi.setSessionAttribute(Constants.noRekeningTujuan, nomorRekeningTujuan);
				sesi.setSessionAttribute(Constants.namaAccountTujuan, namaTujuan);
				sesi.setSessionAttribute(Constants.nominal, nominalTransfer);
				sesi.setSessionAttribute(Constants.biaya, biayaTransferBank);
				sesi.setSessionAttribute(Constants.issue, issue);
				sesi.setSessionAttribute(Constants.bank, namaBank);
				sesi.setSessionAttribute(Constants.inquiry, Status);
				sesi.setSessionAttribute(Constants.total, hargaCetak);
				RedirectMenu menu = new RedirectMenu();
				menu.setMenuLink("banksukses",header);
			}
		}
		
		
		} catch (Exception e){
			log.info("Error di Transfer Bank." + e.getMessage());
		}
	}

}
