package com.truemoneywitami.menu.transferbank;

import java.util.ArrayList;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.transferpos.TruePos;
import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;

public class TransferSelectAgent extends Window implements RequestListener {

	Logger log = Logger.getLogger(TransferSelectAgent.class);
	private CheckParameter check = new CheckParameter();
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	JSONObject obj;
	String pesan="", ack="", nomor = "";
	JSONArray listbank, norek, arr;
	mob_Session sesi = new mob_Session();
	String id="", bank = "";
	String username = sesi.getUsername();
	String biaya = "0";
	ArrayList kodeBank = new ArrayList<>();
	ArrayList namaBank = new ArrayList<>();
	ArrayList nomorRekening = new ArrayList<>();
	Pattern pattern;
	Matcher matcher;
	String isValid = "NOK";
	String checked = "NOK";
	String v="",
	nama="",
	hp="",
	issue="",
	nominal="",
	ben_name="";
	String ktp = "";
//	long max = -1;
	
	String min = "";
	String max = "";
	
	// String username = "0852235455";

	@Override
	public void requestSucceeded(JSONObject obj) {

		if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("Bank")) {
			ack = obj.get("ACK").toString();
			listbank = (JSONArray) obj.get("ListBank");
		} else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("MIN_MAX_TRF")) {
			ack = obj.get("ACK").toString();
			min = obj.get("min").toString();
			max = obj.get("max").toString();
		} else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("Rek")) {
			// System.out.println("test2");
			ack = obj.get("ACK").toString();
			norek = (JSONArray) obj.get("norek");
		} else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("biaya")) {
			arr = (JSONArray) obj.get("biayaAdmin");
//			max = Long.parseLong(obj.get("maxday").toString());
		} else {
			System.out.println("test3");
			pesan = obj.get("ACK").toString();
		}
	}

	@Override
	public void requestFailed(String message) {
		
		min = "";
		max = "";
		
		if (message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}

	public void setData(Window wnd) {
		try {
			String idTrx = "TMN"+func.getIDTRX();
			sesi.setSessionAttribute("id_trx", idTrx);
			// id = sesi.getSessionAttribute("noRekeningTujuan");
			JSONObject obj = new JSONObject();
//			obj.put("Tipe", "BankData");
//			ProcessingListener processing = new ProcessingListener(this, obj.toString());
//			processing.processData();
//			obj = new JSONObject();
//			obj.put("Tipe", "RekData");
//			obj.put("ID_Account", username);
//			processing = new ProcessingListener(this, obj.toString());
//			processing.processData();
			
//			obj.put("Tipe", "requestBiayaAdmin");
//			obj.put("nominal", "1000");
//			obj.put("id_TipeTransaksi", 6);
//			obj.put("id_TipeAplikasi", 2);
//			obj.put("username", username);
//			ProcessingListener processing = new ProcessingListener(this, obj.toString());
//			processing.processData();
//			biaya = String.valueOf(arr.get(0));
			
			obj.put("Tipe", "MIN_MAX_TRF");
			obj.put("id_tipe_transaksi", "30");
			obj.put("is_member", "0");
			obj.put("username", username);
			
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			
			func.setLabelValue(wnd, "biaya", func.formatRp(biaya));
//			setListboxValue(wnd, "bank_sel", listbank, 0, true, true, true);
//			setListboxValueRek(wnd, "rek_sel", norek, 0, true, true, true);
		} catch (Exception e) {
			log.info("Error di Transfer Bank." + e.getMessage());
		}
	}

	public void setListboxValue(Window wnd, String idList, JSONArray data, int noUrut, boolean isSelect,
			boolean isClear, boolean firstRowIsID) {
		try {
			Listbox list = (Listbox) wnd.getFellow(idList);
			list.getItems().clear();
			int a = data.size();

			// int lIndex = 0;

			for (int i = 0; i < a; i++) {
				Listitem li = new Listitem();
				JSONObject child = (JSONObject) data.get(i);
				li.setValue(child.get("kode").toString());
				String nama = child.get("nama").toString();
				li.appendChild(new Listcell(nama));
				list.appendChild(li);
				
				if(i==0) {
					li.setSelected(true);
				}
			}
		} catch (Exception e) {
			log.info("Error di Transfer Bank." + e.getMessage());
		}
	}

	public void setListboxValueRek(Window wnd, String idList, JSONArray data, int noUrut, boolean isSelect,
			boolean isClear, boolean firstRowIsID) {

		try {
			Listbox list = (Listbox) wnd.getFellow(idList);
			list.getItems().clear();
			int a = data.size();
			System.out.println(a);
			// int lIndex = 0;

			for (int i = 0; i < a; i++) {
				Listitem li = new Listitem();
				JSONObject child = (JSONObject) data.get(i);

				kodeBank.add(child.get("kodebank").toString());
				namaBank.add(child.get("namabank").toString());
				nomorRekening.add(child.get("norek").toString());

				li.setValue(kodeBank.get(i));
				String nama = (String) namaBank.get(i);
				String noRekening = (String) nomorRekening.get(i);
				String Label = noRekening + "-" + nama;
				li.appendChild(new Listcell(Label));
				list.appendChild(li);
			}
		} catch (Exception e) {
			log.info("Error di Transfer Bank." + e.getMessage());
		}
	}

	public void nextStep(Window wnd, Window header) {
		try {
//			Checkbox ck = (Checkbox) wnd.getFellow("l2");
//			Radio rd = (Radio) wnd.getFellow("radio3");
//			if (ck.isChecked()) {
//				checked = "OK";
//			}
//			if (rd.isSelected()) {
//				setRekening(wnd);
//			} else {
//				try {
//					nomor = func.getTextboxValue(wnd, "norek");
//					bank = func.getListboxValue(wnd, "bank_sel");
//				} catch (Exception e) {
//					nomor = "";
//					bank = "";
//				}
//			}
			try {
//				v = func.getTextboxValue(wnd, "daf");
//				nama = func.getTextboxValue(wnd, "nama");
//				hp = func.getTextboxValue(wnd, "hp");
//				issue = func.getTextboxValue(wnd, "issue");
				nominal = func.getTextboxValue(wnd, "nominal");
//				ben_name = func.getTextboxValue(wnd, "ben_name");
//				ktp = func.getTextboxValue(wnd, "ktp").trim();
			} catch (Exception e) {
//				v = "";
//				nama = "";
//				hp = "";
//				issue = "";
				nominal = "";
//				ben_name = "";
			}
			String isValid = CheckValidasi(wnd);
			System.out.println("TransferSelect.java:192|isValid>>>"+isValid);
			if (isValid == "OK") {
				try {
					System.out.println("TransferSelect.java:195|isValid>>>"+isValid);
//					sesi.setSessionAttribute(Constants.sender_name, nama);
//					sesi.setSessionAttribute(Constants.sender_cell, hp);
//					sesi.setSessionAttribute(Constants.noRekeningTujuan, nomor);
//					sesi.setSessionAttribute(Constants.issue, issue);
					sesi.setSessionAttribute(Constants.nominal, nominal);
//					sesi.setSessionAttribute(Constants.namaAccountTujuan, ben_name);
//					sesi.setSessionAttribute(Constants.bank, bank);
					sesi.setSessionAttribute(Constants.biaya, biaya);
//					sesi.setSessionAttribute(Constants.ktp, ktp);
//					sesi.setSessionAttribute("check", checked);
					RedirectMenu menu = new RedirectMenu();
					menu.setMenuLink("truebankkonfirmagent", header);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				CheckValidasi(wnd);
			}
		} catch (Exception e) {
			log.info("Error di Transfer Bank." + e.getMessage());
		}
	}

	public void setRekening(Window wnd) {
		try {
			Listbox list = (Listbox) wnd.getFellow("rek_sel");
			int a = list.getSelectedIndex();
			nomor = (String) nomorRekening.get(a);
			bank = func.getListboxValue(wnd, "rek_sel");

		} catch (Exception ex) {
			log.info("Error di Transfer Bank." + ex.getMessage());
		}

	}

	public void setBank(Window wnd) {
		try {
			func.getListboxValue(wnd, "bank_sel");
		} catch (Exception e) {
			log.info("Error di Transfer Bank." + e.getMessage());
		}
	}

	public boolean cekValidasiAngka(String value) {
		final String EMAIL_PATTERN = "^[0-9]+$";
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(value);
		return matcher.matches();
	}

	public String CheckValidasi(Window wnd) {
//		if (nama.trim().isEmpty()) {
//			Clients.showNotification("Nama Informasi Pengirim tidak boleh kosong!");
//			func.setTextboxFocus(wnd, "nama");
//			isValid = "NOK";
//		} else if (hp.trim().isEmpty()) {
//			Clients.showNotification("Nomor Handphone Informasi Pengirim tidak boleh kosong!");
//			func.setTextboxFocus(wnd, "hp");
//			isValid = "NOK";
//		} else if (!(hp.trim().isEmpty()) && !cekValidasiAngka(hp)) {
//			Clients.showNotification("Nomor Handphone tidak valid!");
//			func.setTextboxFocus(wnd, "hp");
//			isValid = "NOK";
//		} else if (ktp.length() == 0) {
//			Clients.showNotification("No. KTP tidak boleh kosong");
//			func.setTextboxFocus(wnd, "ktp");
//			isValid = "NOK";
//		} else if (!check.numberValidator(ktp)) {
//			Clients.showNotification("No. KTP tidak valid");
//			func.setTextboxFocus(wnd, "ktp");
//			isValid = "NOK";
//		} else if (!check.checkLength(ktp, 16)) {
//			Clients.showNotification("No. KTP harus 16 digit");
//			func.setTextboxFocus(wnd, "ktp");
//			isValid = "NOK";
//		} else if (!cekValidasiAngka(nomor)) {
//			Clients.showNotification("Nomor Rekening tidak valid!");
//			func.setTextboxFocus(wnd, "norek");
//			isValid = "NOK";
//		} else if (ben_name.trim().isEmpty()) {
//			Clients.showNotification("Nama Informasi Penerima tidak boleh kosong");
//			func.setTextboxFocus(wnd, "ben_name");
//			isValid = "NOK";
//		} else if (issue.trim().isEmpty()) {
//			Clients.showNotification("Keterangan tidak boleh kosong");
//			func.setTextboxFocus(wnd, "issue");
//			isValid = "NOK";
//		} else 
		
//		long maxamount = max;
		if (nominal.trim().isEmpty()) {
			Clients.showNotification("Nominal Transfer tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "nominal");
			isValid = "NOK";
		} else if (!(nominal.trim().isEmpty()) && !(cekValidasiAngka(nominal))) {
			Clients.showNotification("Nominal Transfer Harus diisi dengan angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "nominal");
			isValid = "NOK";
		} else if (min.trim().length() == 0 || max.trim().length() == 0) {
			Clients.showNotification(Constants.failedRequest, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "nominal");
			isValid = "NOK";
		} else if (!(nominal.trim().isEmpty()) && (cekValidasiAngka(nominal))
				&& Integer.parseInt(nominal) < Integer.parseInt(min)) {
			Clients.showNotification("Nominal Transfer Minimal Rp. " + func.formatNumber(Integer.parseInt(min), ",###"), Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "nominal");
			isValid = "NOK";
		} else if (!(nominal.trim().isEmpty()) && (cekValidasiAngka(nominal))
				&& Integer.parseInt(nominal) > Integer.parseInt(max)) {
			Clients.showNotification("Nominal Transfer Maksimal Rp. " + func.formatNumber(Integer.parseInt(max), ",###"), Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "nominal");
			isValid = "NOK";
		} else {
			isValid = "OK";
		}
		return isValid;
	}

}
