package com.truemoneywitami.menu.transferbank;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

public class BankSukses extends Window{
	
	ZKFunction func = new ZKFunction();
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	HashMap map;
	String header="",
	pesan="",
	id_transaksi="",
	id_agent="",
	id_member="",
	nama="",
	namaPengirim="",
	nominal="",
	biaya="",
	ket="",
	total="",
	status="",
	waktu="",
	bank="",
	inq="",
	tgl="",
	jasper="";
	
	
	public void setData(Window wnd){
		try {
			
			String statustrx = "";
			
			statustrx       = sesi.getSessionAttribute(Constants.status);
			header 			= sesi.getSessionAttribute(Constants.header);
			pesan 			= sesi.getSessionAttribute(Constants.pesan);
			id_member 		= sesi.getSessionAttribute(Constants.noRekeningTujuan);
			id_transaksi 	= sesi.getSessionAttribute(Constants.id_trx);
			nama 			= sesi.getSessionAttribute(Constants.namaAccountTujuan);
			nominal 		= sesi.getSessionAttribute(Constants.nominal);
			biaya			= sesi.getSessionAttribute(Constants.biaya);
			status 			= sesi.getSessionAttribute(Constants.status);
			total 			= sesi.getSessionAttribute(Constants.total);
			ket 			= sesi.getSessionAttribute(Constants.issue);
			bank 			= sesi.getSessionAttribute(Constants.bank);
			inq             = sesi.getSessionAttribute(Constants.inquiry);
			namaPengirim = sesi.getSessionAttribute("namapengirim");
			String ktp = sesi.getSessionAttribute(Constants.ktp);
			// fill label with value
			
			Div statusDiv = (Div) wnd.getFellow("statusheader");
			if (statustrx.equalsIgnoreCase("OK")){
				statusDiv.setSclass("alert alert-success");
				status = "Transfer Berhasil";
			}
			else {
				statusDiv.setSclass("alert alert-warning");
				status = "Transfer Pending";
			}
			
			func.setLabelValue(wnd, "header", header);
			func.setLabelValue(wnd, "pesan", pesan);
			func.setLabelValue(wnd, "id_trx", id_transaksi);
			func.setLabelValue(wnd, "sender_name", namaPengirim);
			func.setLabelValue(wnd, "ben_name", nama);
			func.setLabelValue(wnd, "ben_account", id_member);
			func.setLabelValue(wnd, "amount", func.formatRp(nominal));
			func.setLabelValue(wnd, "biaya", func.formatRp(biaya));
			func.setLabelValue(wnd, "total", func.formatRp(total));
			func.setLabelValue(wnd, "status", status);
			func.setLabelValue(wnd, "issue",ket);
			func.setLabelValue(wnd, "bank",bank);
			func.setLabelValue(wnd, "status_inq",inq);
			func.setLabelValue(wnd, "ktp",ktp);
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
			String tgl = sdf.format(date);
			this.tgl = tgl;
			func.setLabelValue(wnd, "waktu", tgl);
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void setBack(Window wnd)throws Exception {
		mob_Session sesi = new mob_Session();
		String username = sesi.getUsername();
		if (username.startsWith("0")){
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("inforekeningmember",wnd);
		} else {
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("inforekeningagen",wnd);
		}
	}
	
	public void cetakStruk(Window Wnd) throws Exception {
		try {
			map = new HashMap();
			map.put(Constants.header, header);
			map.put(Constants.idTransaksi, id_transaksi);			//ID Transaksi
			map.put(Constants.namaPengirim, namaPengirim);				//Nama pengirim
			map.put(Constants.namaAccountTujuan, nama);				//Nama penerima
			map.put(Constants.bank, bank);							//Bank Tujuan
			map.put(Constants.noRekeningTujuan, id_member);			//No Rekening Tujuan
			map.put(Constants.nominal, func.formatRp(nominal));		//Nominal
			map.put(Constants.biaya, func.formatRp(biaya));			//Biaya Admin
			map.put(Constants.total, func.formatRp(total));			//Total
			map.put(Constants.issue,ket);							//Keterangan
			map.put(Constants.inquiry, inq);						//Status Inquiry
			map.put(Constants.status, status);						//Status
			map.put("timeStamp", tgl);								//Waktu
			System.out.println("pesan -> " + pesan);

			Iframe ireport = (Iframe) Wnd.getFellow("ireport");

			System.out.println("pesan -> " + pesan);
			
			if (pesan.toLowerCase().contains("cairkan saldo")) {
				jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/transfer/transferToBankAgent.jasper");
			} else {
				jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/transfer/transferToBank.jasper");
			}
			
			System.out.println("jasper path -> " + jasper);
			File inFileName = new File(jasper);
			JasperPrint print = JasperFillManager.fillReport(inFileName.getPath(), map, new JREmptyDataSource());
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, outStream);
			AMedia amedia = new AMedia("Invoice_" + id_transaksi + "_" + id_member, "pdf", null, outStream.toByteArray());
			ireport.setContent(amedia);
			Wnd.setVisible(true);
		} catch (Exception e){
			e.printStackTrace();
		}
	}
}
