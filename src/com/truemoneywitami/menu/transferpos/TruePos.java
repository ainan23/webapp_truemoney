package com.truemoneywitami.menu.transferpos;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class TruePos extends Window implements RequestListener {
	
	Logger log = Logger.getLogger(TruePos.class);
	ZKFunction func = new ZKFunction();
	mob_Session sesi = new mob_Session();
	JSONArray arr;
	String biaya;
	String name="",
	hp="",
	ADD="",
	ben_name="",
	ben_HP="",
	ben_add="",
	sum="",
	ket="",
	admin="",
	isValid = "NOK";
	Pattern pattern;
	Matcher matcher;
	
	@Override
	public void requestSucceeded(JSONObject obj) {	
		if (obj.get("ACK").toString().equals("OK") ) {
			String ack = obj.get("ACK").toString();
			arr = (JSONArray) obj.get("biayaAdmin");
		}
	}

	@Override
	public void requestFailed(String message) {
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
	public void setData(Window wnd) {
		try {
			String idTrx = func.getIDTRX();
			sesi.setSessionAttribute("id_trx", idTrx);
		String username = sesi.getUsername();
		JSONObject obj = new JSONObject();
		obj.put("Tipe", "requestBiayaAdmin");
		obj.put("nominal", "1000");
		obj.put("id_TipeTransaksi", 31);
		obj.put("id_TipeAplikasi", 2);
		obj.put("username", username);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		biaya = String.valueOf(arr.get(0));
		func.setLabelValue(wnd, "charge",func.formatRp(biaya) );
		} catch(Exception e){
			log.info("Error di Transfer POS." + e.getMessage());
		}
	}
	
    public void confirm(Window wnd,Window header) {
    	try {
    	 name = func.getTextboxValue(wnd,"nama");
    	 hp = func.getTextboxValue(wnd,"HP");
    	 ADD= func.getTextboxValue(wnd,"add");
    	 ben_name = func.getTextboxValue(wnd,"rev_name");
    	 ben_HP = func.getTextboxValue(wnd,"rev_HP");
    	 ben_add = func.getTextboxValue(wnd,"rev_add");
    	 sum = func.getTextboxValue(wnd,"amount");
		 ket = func.getTextboxValue(wnd,"issue");
		 admin = biaya;
    	} catch (Exception e){
    		name="";hp="";ADD="";ben_name="";ben_HP="";ben_add="";sum="";ket="";
    	}
    	
    	isValid = CheckValidasi(wnd);
    	if (isValid == "OK") {
    		 sesi.setSessionAttribute(Constants.sender_name, name);
			 sesi.setSessionAttribute(Constants.sender_cell,hp);
			 sesi.setSessionAttribute(Constants.sender_add, ADD);
			 sesi.setSessionAttribute(Constants.namaAccountTujuan, ben_name);
			 sesi.setSessionAttribute(Constants.ben_cell, ben_HP);
			 sesi.setSessionAttribute(Constants.ben_add,ben_add);
			 sesi.setSessionAttribute(Constants.nominal, sum);
			 sesi.setSessionAttribute(Constants.issue, ket);
			 sesi.setSessionAttribute(Constants.biaya, admin);
			 RedirectMenu menu = new RedirectMenu();
			 menu.setMenuLink("transferposkonfirm",header);
    	} else {
    		CheckValidasi(wnd);
    	}
	}
    
    public String CheckValidasi(Window wnd) {
		if (name.trim().isEmpty()) {
			Clients.showNotification("Nama Informasi Pengirim tidak boleh kosong!", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "nama");
			isValid = "NOK";
		} else if (hp.trim().isEmpty()) {
			Clients.showNotification("Nomor Handphone Pengirim tidak boleh kosong!", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "HP");
			isValid = "NOK";
		} else if (!(hp.trim().isEmpty()) && !cekValidasiAngka(hp)) {
			Clients.showNotification("Nomor Handphone Pengirim tidak valid!", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "HP");
			isValid = "NOK";
		} else if (ADD.trim().isEmpty()) {
			Clients.showNotification("Alamat Pengirim tidak boleh kosong!", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "add");
			isValid = "NOK";
		} else if (ben_name.trim().isEmpty()) {
			Clients.showNotification("Nama Penerima tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "rev_name");
			isValid = "NOK";
		} else if (ben_HP.trim().isEmpty()) {
			Clients.showNotification("Nomor Handphone Penerima tidak boleh kosong!", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "rev_HP");
			isValid = "NOK";
		} else if (!(ben_HP.trim().isEmpty()) && !cekValidasiAngka(ben_HP)) {
			Clients.showNotification("Nomor Handphone Penerima tidak valid!", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "rev_HP");
			isValid = "NOK";
		} else if (ben_add.trim().isEmpty()) {
			Clients.showNotification("Alamat Penerima tidak boleh kosong!", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "rev_add");
			isValid = "NOK";
		} else if (ket.trim().isEmpty()) {
			Clients.showNotification("Keterangan tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "issue");
			isValid = "NOK";
		} else if (sum.isEmpty()) {
			Clients.showNotification("Nominal Transfer tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "amount");
			isValid = "NOK";
		} else if (!(sum.isEmpty()) && !(cekValidasiAngka(sum))) {
			Clients.showNotification("Nominal Transfer Harus diisi dengan angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "amount");
			isValid = "NOK";
		} else if (!(sum.isEmpty()) && (cekValidasiAngka(sum))
				&& Integer.parseInt(sum) < Constants.minamount) {
			Clients.showNotification("Nominal Transfer Minimal Rp. 20.000,-", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "amount");
			isValid = "NOK";
		} else if (!(sum.isEmpty()) && (cekValidasiAngka(sum))
				&& Integer.parseInt(sum) > Constants.maxamount) {
			Clients.showNotification("Nominal Transfer Maximal Rp" + func.formatNumber(Constants.maxamount, "###,###"), Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "amount");
			isValid = "NOK";
		}
		else if (ben_HP.length() < 9 || ben_HP.length() > 13){
			Clients.showNotification("Nomor Handphone Penerima tidak valid!", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxFocus(wnd, "rev_HP");
			isValid ="NOK";
		}
		else {
			isValid = "OK";
		}
		return isValid;
	}
    
    public boolean cekValidasiAngka(String value) {
		final String EMAIL_PATTERN = "^[0-9]+$";
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(value);
		return matcher.matches();
	}
}
