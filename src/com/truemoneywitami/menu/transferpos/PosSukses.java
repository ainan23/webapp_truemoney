package com.truemoneywitami.menu.transferpos;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

public class PosSukses extends Window {

	ZKFunction func = new ZKFunction();
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	String header="",
	pesan="",
	id_transaksi="",
	id_agent="",
	id_member="",
	nama="",
	nominal="",
	biaya="",
	ket="",
	total="",
	status="",
	waktu="",
	bank="",
	inq="",
	sen_name="",
	sen_cell="",
	ben_cell="",
	sen_add="",
	ben_add="",
	tgl="",
	jasper="";
	
	private HashMap map;
	
	
	public void setData(Window wnd){
		try {
			header 			= sesi.getSessionAttribute(Constants.header);
			pesan 			= sesi.getSessionAttribute(Constants.pesan);
			id_transaksi 	= sesi.getSessionAttribute(Constants.id_trx);
			sen_name 		= sesi.getSessionAttribute(Constants.sender_name);
			sen_add  		= sesi.getSessionAttribute(Constants.sender_add);
			nama 			= sesi.getSessionAttribute(Constants.namaAccountTujuan);
			ben_add			= sesi.getSessionAttribute(Constants.ben_add);
			ben_cell        = sesi.getSessionAttribute(Constants.ben_cell);
			nominal 		= sesi.getSessionAttribute(Constants.nominal);
			biaya			= sesi.getSessionAttribute(Constants.biaya);
			status 			= sesi.getSessionAttribute(Constants.status);
			total 			= sesi.getSessionAttribute(Constants.total);
			ket 			= sesi.getSessionAttribute(Constants.issue);
			// fill label with value
			func.setLabelValue(wnd, "header", header);
			func.setLabelValue(wnd, "pesan", pesan);
			func.setLabelValue(wnd, "id_trx", id_transaksi);
			func.setLabelValue(wnd, "sender_name", sen_name);
			func.setLabelValue(wnd, "sender_add", sen_add);
			func.setLabelValue(wnd, "ben_name", nama);
			func.setLabelValue(wnd, "ben_add", ben_add);
			func.setLabelValue(wnd, "ben_cell", ben_cell);
			func.setLabelValue(wnd, "amount", func.formatRp(nominal));
			func.setLabelValue(wnd, "biaya", func.formatRp(biaya));
			func.setLabelValue(wnd, "total", func.formatRp(total));
			func.setLabelValue(wnd, "status", status);
			func.setLabelValue(wnd, "issue",ket);
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY hh:mm:ss");
			tgl = sdf.format(date);
			func.setLabelValue(wnd, "waktu", tgl);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void cetakStruk(Window Wnd) throws Exception {
		map = new HashMap();
		map.put(Constants.header			, header);
		map.put(Constants.id_trx			, id_transaksi);
		map.put(Constants.namaPengirim		, sen_name);
		map.put(Constants.alamatPengirim	, sen_add);
		map.put("namaPenerima"				, nama);
		map.put(Constants.alamatPenerima	, ben_add);
		map.put(Constants.noHpPenerima		, ben_cell);
		map.put(Constants.nominal			, func.formatRp(nominal));
		map.put(Constants.biaya				, func.formatRp(biaya));
		map.put(Constants.status			, status);
		map.put(Constants.total				, func.formatRp(total));
		map.put(Constants.issue				, ket);
		map.put("timeStamp", tgl);
		System.out.println("pesan -> " + pesan);

		Iframe ireport = (Iframe) Wnd.getFellow("ireport");

		System.out.println("pesan -> " + pesan);
		jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/transfer/transferToPos.jasper");
		System.out.println("jasper path -> " + jasper);
		File inFileName = new File(jasper);
		JasperPrint print = JasperFillManager.fillReport(inFileName.getPath(), map, new JREmptyDataSource());
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		JasperExportManager.exportReportToPdfStream(print, outStream);
		AMedia amedia = new AMedia("Invoice_" + id_transaksi + "_" + id_member, "pdf", null, outStream.toByteArray());
		ireport.setContent(amedia);
		Wnd.setVisible(true);
	}
	
	public void setBack(Window wnd)throws Exception {
		mob_Session sesi = new mob_Session();
		String username = sesi.getUsername();
		if (username.startsWith("0")){
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("inforekeningmember",wnd);
		} else {
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("inforekeningagen",wnd);
		}
	}
}
