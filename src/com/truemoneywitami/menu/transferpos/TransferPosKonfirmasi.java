package com.truemoneywitami.menu.transferpos;

import java.util.Date;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.sun.corba.se.impl.orbutil.closure.Constant;
import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.ProcessingListenerPrabayar;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.function.Constants;

public class TransferPosKonfirmasi extends Window implements RequestListener {
	
	Logger log = Logger.getLogger(TransferPosKonfirmasi.class);
	ZKFunction func = new ZKFunction();
	CheckParameter check = new CheckParameter();
	JSONObject obj;
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	String sendername="",
	senderhp="",
	senderadd="",
	benname="",
	benhp="",
	benadd="",
	nominal="",
	issue="",
	biaya="",
	ack="",
	id_trx="";
	String block="";
	String statusAccount="",
	PINDATA="",
	lastBalance="",
	pesan="";
	String send_trx = ""; 
	//
	int total;
	int count =0;
	//
	Window win;
	Window winheader;
	
	long inqsend,inqget,inqtime;
	long transfersend,transferget,transfertime;
	
	@Override
	public void requestSucceeded(JSONObject obj) {	
		Window wnd = getWindow();
		
		String id = "";
		try {
			id = obj.get("ID").toString();
		} catch (Exception e) {
			id = "";
		}
		
		if (obj.get("ACK").toString().equals("OK") && id.equals("status")  ) {
			inqget = new Date().getTime();
			inqtime = inqget  - inqsend;
			String m1 = "Waktu saat Penerimaan inq Transfer POS " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk Inq Transfer POS adalah " +inqtime + " ms";
			func.cetakLog(m1, m2);
			statusAccount = obj.get("statusAccount").toString();
			lastBalance = obj.get("lastBalance").toString();
		}
		else if (obj.get("ACK").toString().equals("OK") && id.equals("transfer")) {
			transferget = new Date().getTime();
			transfertime = transferget - transfersend;
			ack = obj.get("ACK").toString();
			id_trx = obj.get("id_Trx").toString();
			String m1 = "Waktu saat Penerimaan  Transfer POS " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk  Transfer POS adalah " +transfertime +" ms";
			func.cetakLog(m1, m2);
//			sesi.setSessionAttribute(Constants.header, "Transfer POS");
//			sesi.setSessionAttribute(Constants.pesan,"Transfer True ke Pos Sukses");
//			RedirectMenu menu = new RedirectMenu();
//			menu.setMenuLink("transfersukses",this.winheader);
			
		}
		else if (obj.get("ACK").equals("OK") && obj.get("ID").equals("Block")){
			block = obj.get("ACK").toString();
		}
		else if (obj.get("ACK").toString().equals("NOK")) {
			pesan = obj.get("pesan").toString();
			//requestFailed(Constants.generalErrorMessage);
//			sesi.setSessionAttribute(Constants.pesan,pesan);
//			RedirectMenu menu = new RedirectMenu();
//			menu.setMenuLink("transfergagal",this.winheader);
			
		}
	}

	@Override
	public void requestFailed(String message) {
		try {
		Window wnd = getWindow();
		if (message.contains("PIN")) {
			pesan = message;
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			count++;
			if (count >3){
				JSONObject obj = new JSONObject();
				obj.put("Tipe", "BlockAccount");
				obj.put("username", username);
				ProcessingListener processing = new ProcessingListener(this, obj.toString());
				processing.processData();
				if (block.equals("OK")){
					Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
					func.setTextboxDisabled(wnd, "pin", true);
				}
				else {
					System.out.println("Akun Gagal di Blokir");
				}
			}
		} 
		else if (message.contains("blokir") && count ==3){
			count++;
			Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxDisabled(wnd, "pin", true);
		}
		else if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		}
		else if(message.equals("TIMEOUT")) {
			ack = "NOK";
			Clients.showNotification(Constants.MSG_TIMEOUT_ERROR, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else {
			sesi.setSessionAttribute(Constants.tipePesan,"TransferPos");
			sesi.setSessionAttribute(Constants.pesan,message);
			sesi.setSessionAttribute(Constants.status,"Transfer Gagal");
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("transaksiGagal",this.winheader);
		}	
		} catch (Exception e) {
			
		}	
	}
	
	public void setWindow (Window wnd) {
		this.win = wnd;
	}
	
	public Window getWindow() {
		return this.win;
	}
	
	public void setData(Window wnd,Window header) throws Exception {
		try {
		setWindow(wnd);
		sendername = sesi.getSessionAttribute(Constants.sender_name);
		senderhp =sesi.getSessionAttribute(Constants.sender_cell);
		senderadd = sesi.getSessionAttribute(Constants.sender_add);
		benname = sesi.getSessionAttribute(Constants.namaAccountTujuan);
		benhp = sesi.getSessionAttribute(Constants.ben_cell);
		benadd = sesi.getSessionAttribute(Constants.ben_add);
		nominal = sesi.getSessionAttribute(Constants.nominal);
		issue = sesi.getSessionAttribute(Constants.issue);
		biaya = sesi.getSessionAttribute(Constants.biaya);
		total = Integer.parseInt(nominal) + Integer.parseInt(biaya);
		sesi.setSessionAttribute(Constants.total, String.valueOf(total));
		func.setLabelValue(wnd, "sen_name", sendername);
		func.setLabelValue(wnd, "sen_cell", senderhp);
		func.setLabelValue(wnd, "sen_add", senderadd);
		func.setLabelValue(wnd, "ben_name", benname);
		func.setLabelValue(wnd, "ben_cell", benhp);
		func.setLabelValue(wnd, "ben_add", benadd);
		func.setLabelValue(wnd, "amount", nominal);
		func.setLabelValue(wnd, "issue", issue);
		func.setLabelValue(wnd, "sum_amount", func.formatRp(nominal));
		func.setLabelValue(wnd, "charge_fee", func.formatRp(biaya));
		func.setLabelValue(wnd, "total", func.formatRp(total));
		func.setLabelValue(wnd, "inq_status", "Approved");
		JSONObject obj = new JSONObject();
		obj.put("Tipe", "getStatusAccountTtoP");
		obj.put("username", username);
		inqsend = new Date().getTime();
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		} catch (Exception e){
			log.info("Error di Transfer POS." + e.getMessage());
		}
	} 

	public void setMenuWindow(Window wnd) {
		this.winheader = wnd;
	}
	
	public void doTransfer(Window wnd,Window header){
		setMenuWindow(header);
		try {
		String PIN = func.getTextboxValue(wnd, "pin");
		
		if(check.isKosong(PIN)) {
			Clients.showNotification("PIN tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else if (!(check.pinIsValid(PIN))) {
			Clients.showNotification("PIN tidak valid, Silahkan coba kembali.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else {
//			System.out.println("Masuk Active Member or Agent ---------.." + statusAccount);

			if (statusAccount.equalsIgnoreCase("active") || statusAccount.equalsIgnoreCase("inactive") || statusAccount.equalsIgnoreCase("dormant")){
//				System.out.println("Masuk Active Member or Agent ---------..");
//				System.out.println("ifpertama");
				if (total < Integer.parseInt(lastBalance)){
					System.out.println("MAsuk Vlidasi total trasfr");
					System.out.println("ifkedua");
					JSONObject obj = new JSONObject();
//					send_trx = func.getIDTRX();
					send_trx = sesi.getSessionAttribute("id_trx");
					obj.put("Tipe", "transferPos");
					obj.put("id_tipeaplikasi", 2);
					obj.put("username", username);
					obj.put("nama", benname);
					obj.put("no_HP", benhp);
					obj.put("alamat", benadd);
					obj.put("nominal", nominal);
					obj.put("biayaTransferPOS", biaya);
					obj.put("namaPengirim", sendername);
					obj.put("no_HPPengirim", senderhp);
					obj.put("alamatPengirim", senderadd);
					obj.put("berita", issue);
					obj.put("PIN", PIN);
					obj.put("id_trx", send_trx);
					transfersend = new Date().getTime();
					obj.put("total", Integer.parseInt(biaya) + Integer.parseInt(nominal));
					String m1 = "Waktu saat Pengiriman  Transfer POS " + new Date().toString();
					String m2 = "";
					func.cetakLog(m1, m2);
					ProcessingListenerPrabayar processing = new ProcessingListenerPrabayar(this, obj.toString());
					processing.processData();
					log.info("ack -> " + ack);
					if (ack.equals("OK")){
						sesi.setSessionAttribute(Constants.header, "Transfer True Ke Pos");
						sesi.setSessionAttribute(Constants.pesan,"Transfer True ke Pos Sukses");
						sesi.setSessionAttribute(Constants.id_trx,id_trx);
						sesi.setSessionAttribute(Constants.sender_name, sendername);
						sesi.setSessionAttribute(Constants.sender_add, senderadd);
						sesi.setSessionAttribute(Constants.nama, benname);
						sesi.setSessionAttribute(Constants.ben_add, benadd);
						sesi.setSessionAttribute(Constants.ben_cell, benhp);
						sesi.setSessionAttribute(Constants.nominal,nominal);
						sesi.setSessionAttribute(Constants.biaya, biaya);
						sesi.setSessionAttribute(Constants.status, "Berhasil");
						sesi.setSessionAttribute(Constants.total, String.valueOf(total));
						sesi.setSessionAttribute(Constants.issue, issue);
						RedirectMenu menu = new RedirectMenu();
						menu.setMenuLink("possukses",this.winheader);
					}
					else {
						log.info("pesan -> " + pesan);
						if (!pesan.contains("PIN") && !pesan.contains("TIMEOUT")) {
							log.info("masuk apabila tidak mengandung kata pin");
							System.out.println(pesan);
							sesi.setSessionAttribute(Constants.tipePesan,"TransferPos");
							sesi.setSessionAttribute(Constants.pesan, pesan);
							sesi.setSessionAttribute(Constants.status,"Transfer Gagal");
							RedirectMenu menu = new RedirectMenu();
							menu.setMenuLink("transaksiGagal",this.winheader);
						}
					}
				}
				else {
					requestFailed(Constants.saldoTidakCukup);
				}
			}
			else {
				requestFailed(Constants.memberAccountBlocked);
				
			}
		}
	
		} catch (Exception e){
			log.info("Error di Transfer POS." + e.getMessage());
		}
		}
	
}
