package com.truemoneywitami.menu;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

public class CashSukses extends Window {

	ZKFunction func = new ZKFunction();
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	String header,pesan,id_transaksi,id_agent,id_member,nama,nominal,biaya,status,waktu,jasper,tgl;
	HashMap map;
	int total;
	public void setData(Window wnd){
		try {	
			header 			= sesi.getSessionAttribute(Constants.header);
			pesan 			= sesi.getSessionAttribute(Constants.pesan);
			id_member 		= sesi.getSessionAttribute(Constants.noRekeningTujuan);
			id_transaksi 	= sesi.getSessionAttribute(Constants.id_trx);
			nama 			= sesi.getSessionAttribute(Constants.namaAccountTujuan);
			nominal 		= sesi.getSessionAttribute(Constants.nominal);
			biaya			= sesi.getSessionAttribute(Constants.biaya);
			status 			= sesi.getSessionAttribute(Constants.status);
			// fill label with value
			func.setLabelValue(wnd, "header", header);
			func.setLabelValue(wnd, "pesan", pesan);
			func.setLabelValue(wnd, "id_trx", id_transaksi);
			func.setLabelValue(wnd, "id_agent", username);
			func.setLabelValue(wnd, "id_member", id_member);
			func.setLabelValue(wnd, "nama", nama);
			func.setLabelValue(wnd, "amount", func.formatRp(nominal));
			func.setLabelValue(wnd, "biaya", func.formatRp(biaya));
			total = Integer.parseInt(nominal) + Integer.parseInt(biaya);
			func.setLabelValue(wnd, "total", func.formatRp(total));
			func.setLabelValue(wnd, "status", status);
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
			tgl = sdf.format(date);
			func.setLabelValue(wnd, "waktu", tgl);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setBack(Window wnd)throws Exception {
		mob_Session sesi = new mob_Session();
		String username = sesi.getUsername();
		if (username.startsWith("0")){
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("inforekeningmember",wnd);
		} else {
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("inforekeningagen",wnd);
		}
	}
	
public void cetakStruk(Window Wnd) throws Exception {
		
		map = new HashMap();
		map.put(Constants.header, header);
		map.put(Constants.id_trx, id_transaksi);
		map.put(Constants.id_agent, username);
		map.put(Constants.noRekeningTujuan, id_member);
		map.put(Constants.namaAccountTujuan, nama);
		map.put(Constants.nominal, func.formatRp(nominal));
		map.put(Constants.biaya, func.formatRp(biaya));
		map.put(Constants.total, func.formatRp(total));
		map.put(Constants.status, status);
		map.put("timeStamp", tgl);
		System.out.println("header -> " + header);
		
		Iframe ireport = (Iframe)Wnd.getFellow("ireport");
		if (header.contains("Setor Tunai")) {
			jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/transfer/cashIn.jasper");
		} else if (header.contains("Tarik Tunai")) {
			jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/transfer/cashOut.jasper");
		} else {
			System.out.println("Tidak dapat mengambil file jasper");
		}
		
		System.out.println("jasper path -> "+jasper);
		File inFileName = new File(jasper);
		JasperPrint print = JasperFillManager.fillReport(inFileName.getPath(), map, new JREmptyDataSource());
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		JasperExportManager.exportReportToPdfStream(print, outStream); 
		AMedia amedia = new AMedia("Invoice_"+id_transaksi+"_"+id_member, "pdf", null, outStream.toByteArray()); 
		ireport.setContent(amedia);
		Wnd.setVisible(true);
		
	}
}
