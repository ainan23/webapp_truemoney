package com.truemoneywitami.menu;

import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class TransaksiSukses extends Window{
	
	ZKFunction func = new ZKFunction();
	mob_Session sesi = new mob_Session();
	public void setViewData(Window wnd){
		try {
			if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("setorTunai")) {
				func.setLabelValue(wnd, "headerTransaksiSukses", "Transaksi Setor Tunai");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			} else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("tarikTunai")) {
				func.setLabelValue(wnd, "headerTransaksiSukses", "Transaksi Tarik Tunai");
				func.setLabelValue(wnd, "pesan", sesi.getSessionAttribute(Constants.pesan));
			} else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("TransferTrue")) {
				func.setLabelValue(wnd, "headerTransaksiSukses", "Transfer True ke True");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("TransferBank")) {
				func.setLabelValue(wnd, "headerTransaksiSukses", "Transfer True ke Bank");
				func.setLabelValue(wnd, "pesan", sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("TransferPos")) {
				func.setLabelValue(wnd, "headerTransaksiSukses", "Transfer True ke POS");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembayaranPLN")) {
				func.setLabelValue(wnd, "headerTransaksiSukses", "Transaksi Pembayaran PLN");
				func.setLabelValue(wnd, "pesan", sesi.getSessionAttribute(Constants.pesan));
			} 
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembayaranPDAM")) {
				func.setLabelValue(wnd, "headerTransaksiSukses", "Transaksi Pembayaran PDAM");
				func.setLabelValue(wnd, "pesan", sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembayaranTelepon")) {
				func.setLabelValue(wnd, "headerTransaksiSukses", "Pembayaran Telepon");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembayaranInternet")) {
				func.setLabelValue(wnd, "headerTransaksiSukses", "Pembayaran Internet");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembayaranTV")) {
				func.setLabelValue(wnd, "headerTransaksiSukses", "Pembayaran TV");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("deposit")){
				func.setLabelValue(wnd, "headerTransaksiSukses", "Tambah Deposit");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembelianPulsa")){
				func.setLabelValue(wnd, "headerTransaksiSukses", "Pembelian Pulsa");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembelianPLN")){
				func.setLabelValue(wnd, "headerTransaksiSukses", "Pembelian Token PLN");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}
	
	public void setBack(Window wnd) throws Exception {
			
			mob_Session sesi = new mob_Session();
			String username = sesi.getUsername();
			if (username.startsWith("0")){
				RedirectMenu menu = new RedirectMenu();
				menu.setMenuLink("inforekeningmember",wnd);
			} else {
				RedirectMenu menu = new RedirectMenu();
				menu.setMenuLink("inforekeningagen",wnd);
			}
			
		}
	
}
