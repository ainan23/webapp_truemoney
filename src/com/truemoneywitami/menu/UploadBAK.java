package com.truemoneywitami.menu;

import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.session.mob_Session;
import org.apache.commons.codec.binary.Base64;

public class UploadBAK extends Window implements RequestListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	mob_Session sesi = new mob_Session();
	String fotowajah;
	String fotoktp;
	String fotottd;
	String HP="";
	String ack="";
	@Override
	public void requestSucceeded(JSONObject obj) {
		// TODO Auto-generated method stub
		ack = obj.get("ACK").toString();
		
	}

	@Override
	public void requestFailed(String message) {
		// TODO Auto-generated method stub
		
		
	}
	
	public void setData(Window wnd) {
		try {
		HP = sesi.getSessionAttribute(Constants.handPhone);
		System.out.println("batu--------->>>"  +HP);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setMedia(byte[] media, int param,String type) {
		if (param == 1){
			fotowajah = Base64.encodeBase64URLSafeString(media) +"kaizen" +type;
		} else if (param==2) {
			fotoktp = Base64.encodeBase64URLSafeString(media) +"kaizen" +type;
		}
		else if (param ==3){
			fotottd =  Base64.encodeBase64URLSafeString(media) +"kaizen" +type;
		}
	}
	
	public void doUpload(Window wnd){
		try {
//			HP = "081906241066";
			System.out.println("foto wajah ---->>>>" +fotowajah);
			System.out.println("foto ktp--->>>" +fotoktp);
			System.out.println("foto ttd--->>>" +fotottd);
			if (fotowajah == null || fotoktp == null || fotottd == null){
				Clients.showNotification("Semua Foto Harus di Isi", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else {
				
				JSONObject obj = new JSONObject();
				obj.put("Tipe", "uploadFoto");
				obj.put("noHp", HP);
				obj.put("fotowajah",fotowajah);
				obj.put("fotoktp", fotoktp);
				obj.put("fotottd", fotottd);
				System.out.println("objek yg dikirim " +obj.toString());
				ProcessingListener processing = new ProcessingListener(this, obj.toString());
				processing.processData();
				
				if (ack.equals("OK")){
//					Cookies.setCookie(Constants.pesan, "Proses Unggah Berhasil");
//					Clients.showNotification("Proses Unggah Berhasil");
//					Thread.sleep(2000);
					Messagebox.show("Proses registrasi Anda berhasil", "Selamat", Messagebox.OK, Messagebox.INFORMATION);
					Executions.sendRedirect("login.zul");
				} else {
					Clients.showNotification("Proses Unggah Gagal. Silahkan Coba Kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				}
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}
}
