package com.truemoneywitami.menu;

import java.util.regex.Pattern;

import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class changepin extends Window implements RequestListener {

	mob_Session session = new mob_Session();
	ZKFunction func = new ZKFunction();
	CheckParameter chk = new CheckParameter();
	String typenya = (String) (Sessions.getCurrent()).getAttribute("tipe");
	Window wnd;
	Window winheader;
	
	boolean status = false;
	String hp = "";
	String email = "";
	
	public void changePin(Window wnd,Window header) {
		setWindow(wnd);
		setMenuWindow(header);
		try {
			String OTP = func.getTextboxValue(wnd, "tbOtp");
			String pinBaru = func.getTextboxValue(wnd, "tbPinBaru");
			String pinReBaru = func.getTextboxValue(wnd, "tbRePinBaru");
			Pattern pat = Pattern.compile(".*[0-9].*");
			System.out.println("reg");
			if (OTP.equals("")){
				Clients.showNotification("OTP Kosong. Silahkan masukan OTP Anda", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else if (pinBaru.equals("")) {
				Clients.showNotification("PIN Kosong. Silahkan masukan PIN Anda", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else if (pinReBaru.equals("")){
				Clients.showNotification("Silahkan masukan PIN baru anda", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else if (!pat.matcher(OTP).matches() || !(OTP.length() == 6) ){
				Clients.showNotification("OTP yang anda Masukan tidak Valid", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
//			else if (!pat.matcher(pinBaru).matches() || !pat.matcher(pinReBaru).matches()){
//				Clients.showNotification("PIN yang anda Masukan Tidak Valid");
//			}
			else if (!chk.isNumber(pinReBaru)){
				Clients.showNotification("PIN yang anda Masukan Tidak Valid", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			
			else {
				if (!pinBaru.equals(pinReBaru)) {
					requestFailed("PIN baru anda tidak sama. Silahkan coba kembali");
				} else {
					JSONObject obj = new JSONObject();
					String accountType = session.getSessionAttribute(Constants.accountType);
					obj.put("Tipe", "ChangePin");
					obj.put("OTP", OTP);
					obj.put("pinBaru", pinBaru);
					obj.put("tipeUser", typenya);
					obj.put("username", session.getSessionAttribute("username"));
					ProcessingListener process = new ProcessingListener(this, obj.toString());
					process.processData();
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception 
			System.out.println(e.getMessage());
			e.printStackTrace();
			
		}
	}
	
	public void setWindow(Window wnd){
		this.wnd = wnd;
	}
	
	public Window getWindow() {
		return this.wnd;
	}

	@Override
	public void requestSucceeded(JSONObject data) {
		
		if (data.get("ACK").equals("OK") && data.get("ID").equals("requestOTP")) {
//			String pesan = data.get("pesan").toString();
			hp = data.get("hp").toString();
			email = data.get("email").toString();
			status = true;
			
			
//			Clients.showNotification(pesan);
		}
		else if (data.get("ACK").equals("OK") && data.get("ID").equals("changeOK")){
			String pesan = (String) data.get("pesan");
			Sessions.getCurrent().setAttribute("pesan", pesan);
			Clients.showNotification(pesan, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			RedirectMenu menu = new RedirectMenu();
			new RedirectMenu().setMenuLink("inforekeningmember",this.winheader);
		}
		else {
			getWindow();
			String pesan = (String) data.get("pesan");
			Sessions.getCurrent().setAttribute("pesan", pesan);
			Clients.showNotification(pesan, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			
			RedirectMenu menu = new RedirectMenu();
			new RedirectMenu().setMenuLink("inforekeningmember",this.winheader);
		}
		
	}

	@Override
	public void requestFailed(String message) {
		// TODO Auto-generated method stub
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
	public void batal (Window wnd) {
		try {
			func.setTextboxResetValue(wnd, "tbOtp");
			func.setTextboxResetValue(wnd, "tbPinBaru");
			func.setTextboxResetValue(wnd, "tbRePinBaru");
		} catch (Exception e) {
			e.getMessage();
		}
	}
	
	public void setMenuWindow(Window header){
		this.winheader = header;
	}

	public void requestOTP (Window wnd) throws Exception {
		JSONObject obj = new JSONObject();
		String accountType = session.getSessionAttribute(Constants.accountType);
		obj.put("Tipe", "requestOTP");
		obj.put("tipeTransaksi", 21);
		obj.put("username", session.getSessionAttribute("username"));
		obj.put("tipeuser", session.getSessionAttribute("tipe"));
		ProcessingListener process = new ProcessingListener(this, obj.toString());
		process.processData();
		
		if (status) {
			try {
				func.setLabelValue(wnd, "lblHp", hp);
				func.setLabelValue(wnd, "lblEmail", ", email "+email);
				func.setDivVisible(wnd, "divOk", true);
				func.setDivVisible(wnd, "divButtonOtp", false);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	}  
}
