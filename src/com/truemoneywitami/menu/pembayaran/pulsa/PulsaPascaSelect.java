package com.truemoneywitami.menu.pembayaran.pulsa;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.setordantariktunai.SetorTunai;

public class PulsaPascaSelect extends Window implements RequestListener {
	
	Logger log = Logger.getLogger(PulsaPascaSelect.class);
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	JSONObject obj;
	String pesan,ack;
	JSONArray norek;
	JSONArray dataProduk;
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	//String username = "0852235455";
	String idt="";
	String tipeUser="";
	private String namaProduk = "";
	private String idOperator = "";
	
	@Override
public void requestSucceeded(JSONObject obj) {	 
		
		if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("data")){
			ack = obj.get("ACK").toString();
			norek = (JSONArray) obj.get("daftar");
		}
		else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("dataproduk")){
			ack = obj.get("ACK").toString();
			dataProduk = (JSONArray) obj.get("daftar");
		}
		else {
			pesan = obj.get("pesan").toString();
			requestFailed(pesan);
		}
	}
	
	@Override
		public void requestFailed(String message){
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		}
		
		public void setListboxValueRek(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID){
			try {
			Listbox list = (Listbox) wnd.getFellow(idList);
			list.getItems().clear();
			int a = data.size();
			
			//int lIndex = 0;
			Listitem liNone = new Listitem();
			liNone.setValue("NONE");
			liNone.appendChild(new Listcell("--- Pilih No HP ---"));
			list.appendChild(liNone);
			
			for (int i =0;i<a;i++){
				Listitem li = new Listitem();
				JSONObject child  = (JSONObject) data.get(i);
				li.setValue(child.get("data").toString()+"##"+child.get("id_operator").toString()+"##"+child.get("nama_operator").toString());
				String nama = child.get("data").toString() +" - " + child.get("nama_operator").toString();
				li.appendChild(new Listcell(nama));
				list.appendChild(li);
			}
			
			list.setSelectedItem(liNone);
			
			} catch (Exception e){
				log.info("Error di Pembayaran pulsa pasca." + e.getMessage());
			}
		}
		
		public void setListboxValueProduk(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID){
			try {
			Listbox list = (Listbox) wnd.getFellow(idList);
			list.getItems().clear();
			int a = data.size();
			
			//int lIndex = 0;
			Listitem liNone = new Listitem();
			liNone.setValue("NONE");
			liNone.appendChild(new Listcell("--- Pilih Produk ---"));
			list.appendChild(liNone);
			
			for (int i =0;i<a;i++){
				Listitem li = new Listitem();
				JSONObject child  = (JSONObject) data.get(i);
				li.setValue(child.get("id_operator").toString());
				String nama = child.get("data").toString();
				li.appendChild(new Listcell(nama));
				list.appendChild(li);
			}
			
			list.setSelectedItem(liNone);
			
			} catch (Exception e){
				log.info("Error di Pembayaran pulsa pasca." + e.getMessage());
			}
		}
		
		public void setPelanggan(Window wnd) {
			try {
				/**
				 * format value idpel##namapelanggan##idoperator##namaop
				 */
				String temp[] = func.getListboxValue(wnd, "pel_sel").split("##");
				idt = temp[0];
				idOperator = temp[1];
				namaProduk = temp[2];
				} catch (Exception ex){
					idt = "";
					idOperator = "";
					namaProduk = "";
					log.info("Error di Pembayaran pulsa pasca." + ex.getMessage());
				}
		}
		
		public void setData(Window wnd) {
			try {
				String idTrx = func.getIDTRX();
				sesi.setSessionAttribute("id_trx", idTrx);
			if (username.startsWith("0")){
				tipeUser = "member";
			}	else {
				tipeUser="Agent";
			}	
			JSONObject obj = new JSONObject();
			obj.put("Tipe","peldatapulsapasca");
			obj.put("ID_Account", username);
			obj.put("id_TipeTransaksi", 14);
			obj.put("tipeUser",tipeUser);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			setListboxValueRek(wnd,"pel_sel", norek, 0, true, true, true);
			
			obj = new JSONObject();
			obj.put("Tipe","getprodukpulsapasca");
			obj.put("id_account", username);
			processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			setListboxValueProduk(wnd,"lstProduk", dataProduk, 0, true, true, true);
			} catch (Exception e) {
				log.error("Error di Pembayaran pulsa pasca.", e);
				e.printStackTrace();
			}
			
		}
		
		public void next(Window wnd,Window header) {
			try {
			String checked="";
			String pesan = "";
			Checkbox ck = (Checkbox) wnd.getFellow("save");
			Radio rd = (Radio) wnd.getFellow("radio2");
			if (ck.isChecked()){
				checked = "OK";
			}
			if (rd.isSelected()){
				idt = func.getTextboxValue(wnd, "idpel");
				namaProduk = func.getListboxLabel(wnd, "lstProduk");
				idOperator = func.getListboxValue(wnd, "lstProduk");
				pesan = "No HP";
			}
			else {
				setPelanggan(wnd);
				pesan = "No HP";
			}
			
			if (idt.equals("NONE") || idt.trim().equals("")){
				//Messagebox.show("ID Pelanggan Tidak Boleh Kosong","Error", Messagebox.OK, Messagebox.ERROR);
				Clients.showNotification(pesan+" tidak boleh kosong Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else if (idt.length() > 15 || idt.length() <8) {
				Clients.showNotification(pesan+" tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else if (idOperator.equals("NONE")){
				//Messagebox.show("ID Pelanggan Tidak Boleh Kosong","Error", Messagebox.OK, Messagebox.ERROR);
				Clients.showNotification("Silahkan Pilih Produk.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else {
				try {
				long test = Long.parseLong(idt);
				sesi.setSessionAttribute("id_Pelanggan", idt);
				sesi.setSessionAttribute("check",checked);
				sesi.setSessionAttribute("id_operator",idOperator);
				sesi.setSessionAttribute("id_denom","0");
				sesi.setSessionAttribute("nama_produk",namaProduk);
				
				RedirectMenu menu = new RedirectMenu();
				new RedirectMenu().setMenuLink("konfirmasipembayaranpulsa",header);
				
//				/**
//				 * cek id operator, termasuk nominal atau tidak
//				 * 111 (topas tv) ---> non nominal
//				 * 107 (orange tv) ---> nominal
//				 */
//				if (idOperator.equals("111")) {
//					/**
//					 * tembak inquiry
//					 */
//					RedirectMenu menu = new RedirectMenu();
//					new RedirectMenu().setMenuLink("konfirmasipembayarantv",header);
//				} else if (idOperator.equals("107")) {
//					/**
//					 * next, get nominal
//					 */
//					sesi.setSessionAttribute("nama_produk",namaProduk);
//					RedirectMenu menu = new RedirectMenu();
//					new RedirectMenu().setMenuLink("nominalpembayarantv",header);
//				}
				
				} catch (Exception e) {
					Clients.showNotification(pesan+" tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				}
			}
			} catch (Exception e) {
				log.info("Error di Pembayaran pulsa pasca." + e.getMessage());
			}
		}
	
}
