package com.truemoneywitami.menu.pembayaran.pulsa;

import java.util.Date;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.ProcessingListenerPrabayar;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.setordantariktunai.SetorTunai;

public class PulsaPascaKonfirm extends Window implements RequestListener {

	Logger log = Logger.getLogger(PulsaPascaKonfirm.class);
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	CheckParameter check = new CheckParameter();
	JSONObject obj;
	String pesan, ack = "", inq = "", simpan = "";
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	// String username = "0852235455";
	String nama = "", id="", biayaBayar="", tagihan="", hargaCetak="", periode="", id_trx="",statustrx="",message="";
	Window win;
	int count = 0;
	String block = "";
	String namaProduk = "";
	String send_trx = "";
	String mon = "";
	String tipe = "";
	Window winheader;
//	int part =0;
//	String temp="";
	private String idOperator;
	private String idDenom;
	private String ref;
	private String terbilang = "";
	private String waktu = "";
	private String sn = "";
	
	private JSONArray periodeArr = new JSONArray();

	long inqsend,inqget,inqtime;
	long paysend,payget,paytime;
	
	@Override
	public void requestSucceeded(JSONObject obj) {

		if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("inquiry")) {
			inqget = new Date().getTime();
			inqtime = inqget - inqsend;
			String m1 = "Waktu saat Penerimaan Inq Pembayaran pulsa pasca " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk Inq Pembayaran pulsa pasca" +inqtime +" ms";
			func.cetakLog(m1, m2);
			biayaBayar = obj.get("biayaBayar").toString();
			nama = obj.get("nama").toString();
			tagihan = obj.get("tagihan").toString();
			hargaCetak = obj.get("hargaCetak").toString();
			periode = obj.get("periode").toString();
			inq = obj.get("ACK").toString();
			
			System.out.println("****************************** >> " + 1);
		} else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("payment")) {
			payget = new Date().getTime();
			paytime = payget - paysend;
			String m1 = "Waktu saat Penerimaan Pembayaran pulsa pasca " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk Pembayaran pulsapasca " +paytime +" ms";
			func.cetakLog(m1, m2);
			ack = obj.get("ACK").toString();
			id_trx = obj.get("id_Transaksi").toString();
			statustrx = obj.get("status").toString();
			message = obj.get("pesan").toString();
			tipe = obj.get("tipeTrx").toString();
			ref = obj.get("ref").toString();
			terbilang = obj.get("terbilang").toString();
			waktu = obj.get("tanggal").toString();
			
			sn = "";
			try {
				sn = obj.get("sn").toString();
			} catch (Exception e) {
				
			}
			
			periodeArr = new JSONArray();
			try {
				periodeArr = (JSONArray) obj.get("periode_list");
			} catch (Exception e) {
				periodeArr = new JSONArray();
			}
			
		} else if (obj.get("ACK").equals("OK") && obj.get("ID").equals("Block")) {
			block = obj.get("ACK").toString();
			
		} else {
			pesan = obj.get("pesan").toString();
			requestFailed(Constants.pesan);
			
		}
	}

	@Override
	public void requestFailed(String message) {
		try {
			
			String m1 = "Waktu saat Penerimaan Pembayaran Internet " + new Date().toString();
			String m2 = "";
			func.cetakLog(m1, m2);
			Window wnd = getWindow();
			if (message.contains("PIN")) {
				Clients.showNotification("PIN Anda Salah. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				count++;
				if (count > 3) {
					JSONObject obj = new JSONObject();
					obj.put("Tipe", "BlockAccount");
					obj.put("username", username);
					ProcessingListener processing = new ProcessingListener(this, obj.toString());
					processing.processData();
					if (block.equals("OK")) {
						Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
						func.setTextboxDisabled(wnd, "pin", true);
					} else {
						System.out.println("Akun Gagal di Blokir");
					}
				}
			} else if (message.contains("blokir") && count == 3) {
				count++;
				Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				func.setTextboxDisabled(wnd, "pin", true);
			} else if (message.equals("01")) {
				Executions.sendRedirect("/timeout.zul");
			} else if(message.equals("TIMEOUT")) {
				ack = "NOK";
				Clients.showNotification(Constants.MSG_TIMEOUT_ERROR, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else {
				sesi.setSessionAttribute(Constants.tipePesan, "PembayaranPulsaPasca");
				sesi.setSessionAttribute(Constants.pesan, message);
				RedirectMenu menu = new RedirectMenu();
				menu.setMenuLink("TransaksiGagal", this.winheader);
			}
		} catch (Exception e) {

		}
	}

	public void setWindow(Window wnd) {
		this.win = wnd;
	}

	public Window getWindow() {
		return this.win;
	}

	public void setMenuWindow(Window header) {
		this.winheader = header;
	}

	public void setData(Window wnd, Window header) {
		try {
			setWindow(wnd);
			setMenuWindow(header);
			id = sesi.getSessionAttribute("id_Pelanggan");
			idOperator = sesi.getSessionAttribute("id_operator");
			idDenom = sesi.getSessionAttribute("id_denom");
			simpan = sesi.getSessionAttribute("check");
			func.setLabelValue(wnd, "id", id);
//			send_trx = func.getIDTRX();
			send_trx = sesi.getSessionAttribute("id_trx");
			
			String path="";
			int idOp = Integer.parseInt(idOperator);
			
			path = "";
			
			switch (idOp) {
			
			// Telkom Flexi
//						case 40 :
//						path = "/assets/img/LOGOFLEXI.png";
//						namaProduk = "TELKOM FLEXI";
//						break;
						// HALO
						case 42 :
							path = "/assets/img/LOGOKARTUHALO.png";
							namaProduk = "HALO";
							break;
						// MATRIX
						case 43 :
							path = "/assets/img/LOGOMATRIX.png";
							namaProduk = "MATRIX";
							break;	
						// XPLOR
						case 44 :
							path = "/assets/img/LOGOXPLOR.png";
							namaProduk = "XPLOR";
							break;	
						// STARONE
						case 38 :
							path = "/assets/img/LOGOTELKOM.png";
							namaProduk = "TELKOM";
							break;
						// ESIA
						case 46 :
							path = "/assets/img/LOGOESIA.png";
							namaProduk = "ESIA";
							break;
						// TRI
						case 196 :
							path = "/assets/img/LOGOTRI.png";
							namaProduk = "TRI";
							break;
						case 197 :
							path = "/assets/img/LOGOFREN.png";
							namaProduk = "FREN";
							break;
						case 198 :
							path = "/assets/img/LOGOMOBI.png";
							namaProduk = "MOBI";
							break;
						case 199 :
							path = "/assets/img/LOGOHEPI.png";
							namaProduk = "HEPI";
							break;
						case 200 :
							path = "/assets/img/LOGOSMARTFREN.png";
							namaProduk = "SMARTFREN";
							break;
						}
			
			
			func.setImage(wnd, "logo", path);
			
			// System.out.println(id);
			// System.out.println(simpan);
			JSONObject obj = new JSONObject();
			obj.put("Tipe", "InqPembayaran");
			obj.put("id_Pelanggan", id);
			obj.put("id_Operator", idOperator);
			obj.put("id_denom", idDenom);
			obj.put("ID_TipeTransaksi", "14");
			obj.put("username", username);
			obj.put("simpan", simpan);
			obj.put("id_trx", send_trx);
			String m1 = "Waktu saat Pengiriman Inq Pembayaran pulsa pasca " + new Date().toString();
			String m2 = "";
			func.cetakLog(m1, m2);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			if (inq.equalsIgnoreCase("OK")) {
				
				func.setLabelValue(wnd, "nama", nama);
				func.setLabelValue(wnd, "periode",periode);
				func.setLabelValue(wnd, "biaya", func.formatRp(biayaBayar));
				func.setLabelValue(wnd, "amount", func.formatRp(tagihan));
				func.setLabelValue(wnd, "total", func.formatRp(hargaCetak));
			}
		} catch (Exception e) {
			log.error("Error di Pembayaran pulsa pasca." , e);
		}
	}

	public void doPayment(Window wnd, Window header) {
		try {
			JSONObject obj = new JSONObject();
			String pin = func.getTextboxValue(wnd, "pin");

			if (check.isKosong(pin)) {
				Clients.showNotification("PIN tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else if (!(check.pinIsValid(pin))) {
				Clients.showNotification("PIN tidak valid, Silahkan coba kembali.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else {
				
				String tipeRequestMember = "";
				String tipeRequestAgent = "";
				if (this.namaProduk.equals("TELKOM")) {
					tipeRequestMember = "PembayaranPascabayarMember";
					tipeRequestAgent = "PembayaranPascabayarAgent";
				} else {
					tipeRequestMember = "PembayaranPulsaPascaMember";
					tipeRequestAgent = "PembayaranPulsaPascaAgent";
				}
				
				if (username.startsWith("0")) {
					obj.put("Tipe", tipeRequestMember);
				} else {
					obj.put("Tipe", tipeRequestAgent);
				}
				obj.put("username", username);
				obj.put("tagihan", tagihan);
				obj.put("id_Pelanggan", id);
				obj.put("verifikasi", "PIN");
				obj.put("id_TipeAplikasi", "2");
				obj.put("PIN", pin);
				obj.put("id_Operator", idOperator);
				obj.put("id_denom", idDenom);
				obj.put("simpan", simpan);
				obj.put("id_trx", send_trx);
				obj.put("biayaBayar", biayaBayar);
				obj.put("hargaCetak", hargaCetak);
				
//				obj.put("tunggak", part);
				String m1 = "Waktu saat Pengiriman Pembayaran pulsa pasca " + new Date().toString();
				String m2 = "";
				func.cetakLog(m1, m2);
				ProcessingListenerPrabayar processing = new ProcessingListenerPrabayar(this, obj.toString());
				processing.processData();
				String head = "Pembayaran Tagihan Telepon/Seluler Pascabayar";
//				if (statustrx.equals("SUKSES")){
////					message = "Pembayaran PLN Berhasil";
//					head = "Pembayaran Berhasil";
//				} else if (statustrx.equals("PENDING")){
////					message = "Pembayaran PLN PENDING";
//					head = "Transaksi Sedang Diproses";
//				}else {
//					message = "";
//					head = "";
//				}
				
				System.out.println("-------------------------------------------> ack 1 " + this.ack);
				
				if (ack.equalsIgnoreCase("OK")) {
					
					sesi.setSessionAttribute(Constants.status, statustrx);
					sesi.setSessionAttribute(Constants.id_pelanggan, id);
					sesi.setSessionAttribute(Constants.namaPelanggan, nama);
					sesi.setSessionAttribute(Constants.tagihan,tagihan);
					sesi.setSessionAttribute(Constants.biaya,biayaBayar);
					sesi.setSessionAttribute(Constants.total,hargaCetak);
					sesi.setSessionAttribute(Constants.id_trx, id_trx);
					sesi.setSessionAttribute(Constants.header,head);
					sesi.setSessionAttribute(Constants.pesan, message);
					sesi.setSessionAttribute(Constants.periode, periode);
					sesi.setSessionAttribute(Constants.tipe, tipe);
					sesi.setSessionAttribute(Constants.ref, ref);
					
					sesi.setSessionAttribute(Constants.waktu, waktu);
					sesi.setSessionAttribute(Constants.TERBILANG, terbilang);
					sesi.setSessionAttribute("sn", sn);
					
					try {
						sesi.setSessionAttribute(Constants.PERIODE_ARR, periodeArr.toString());
					} catch (Exception e) {
						
					}
					
					
					System.out.println("-------------------------------------------> ack 2 " + this.ack);
					
					RedirectMenu menu = new RedirectMenu();
					menu.setMenuLink("paymentSuccess",header);
				}
			}

		} catch (Exception e) {
			log.error("Error di Pembayaran pulsa pasca.", e);
		}
	}

	public void setBack(Window wnd) throws Exception {
		RedirectMenu menu = new RedirectMenu();
		menu.setMenuLink("pembayaranpulsapasca", wnd);
	}
}
