package com.truemoneywitami.menu.pembayaran.internet;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.setordantariktunai.SetorTunai;

public class InternetSelect extends Window implements RequestListener {


	Logger log = Logger.getLogger(InternetSelect.class);
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	JSONObject obj;
	String pesan,ack;
	JSONArray norek;
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	//String username = "0852235455";
	String idt="";
	String tipeUser="";
	
	@Override
	public void requestSucceeded(JSONObject obj) {	 
			
			if (obj.get("ACK").toString().equals("OK") ){
				ack = obj.get("ACK").toString();
				norek = (JSONArray) obj.get("daftar");
			}
			else {
				pesan = obj.get("pesan").toString();
				requestFailed(pesan);
			}
		}
	
	@Override
	public void requestFailed(String message){
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
//			Clients.showNotification(message);
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
public void setListboxValueRek(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID)  {
		try {
		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int a = data.size();
		
		//int lIndex = 0;
		
		for (int i =0;i<a;i++){
			Listitem li = new Listitem();
			JSONObject child  = (JSONObject) data.get(i);
			li.setValue(child.get("data").toString());
			String nama = child.get("data").toString();
			li.appendChild(new Listcell(nama));
			list.appendChild(li);
		}
		} catch (Exception e){
			log.info("Error di Pembayaran Internet." + e.getMessage());
		}
	}
	
	public void setPelanggan(Window wnd) {
		try {
			idt = func.getListboxValue(wnd, "pel_sel");
			} catch (Exception ex){
				//Messagebox.show("ID Pelanggan Tidak Boleh Kosong","Peringatan", Messagebox.OK, Messagebox.ERROR);
				//Clients.showNotification(message);
				idt = "";
				log.info("Error di Pembayaran Internet." + ex.getMessage());
			}
	}
	
	public void setData(Window wnd) {
		try {
			String idTrx = func.getIDTRX();
			sesi.setSessionAttribute("id_trx", idTrx);
		if (username.startsWith("0")){
			tipeUser = "member";
		}	else {
			tipeUser="Agent";
		}
		JSONObject obj = new JSONObject();
		obj.put("Tipe","peldata");
		obj.put("ID_Account", username);
		obj.put("id_TipeTransaksi", 15);
		obj.put("tipeUser",tipeUser);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		setListboxValueRek(wnd,"pel_sel", norek, 0, true, true, true);
		} catch (Exception e){
			log.info("Error di Pembayaran Internet." + e.getMessage());
		}
	}
	
	public void next(Window wnd,Window header) {
		try {
		String checked="";
		Checkbox ck = (Checkbox) wnd.getFellow("save");
		Radio rd = (Radio) wnd.getFellow("radio2");
		if (ck.isChecked()){
			checked = "OK";
		}
		if (rd.isSelected()){
			idt = func.getTextboxValue(wnd, "idpel");
		}
		else {
			setPelanggan(wnd);
		}
		
		if (idt.equals("")){
			//Messagebox.show("ID Pelanggan Tidak Boleh Kosong","Error", Messagebox.OK, Messagebox.ERROR);
//			Clients.showNotification("ID Pelanggan tidak boleh kosong. Silahkan coba lagi");
			Clients.showNotification("ID Pelanggan tidak boleh kosong. Silahkan coba lagi", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (idt.length() > 15 || idt.length() <10) {
//			Clients.showNotification("No ID Pelanggan tidak valid. Silahkan coba kembali");
			Clients.showNotification("No ID Pelanggan tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else {
			try {
			long test = Long.parseLong(idt); 
			sesi.setSessionAttribute("id_Pelanggan", idt);
			sesi.setSessionAttribute("check",checked);
			RedirectMenu menu = new RedirectMenu();
			new RedirectMenu().setMenuLink("konfirmasipembayaraninternet",header);
			} catch (Exception e) {
//				Clients.showNotification("No ID Pelanggan tidak valid. Silahkan coba kembali");
				Clients.showNotification("No ID Pelanggan tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
		}
		} catch (Exception e) {
			log.info("Error di Pembayaran Internet." + e.getMessage());
		}
	}

}
