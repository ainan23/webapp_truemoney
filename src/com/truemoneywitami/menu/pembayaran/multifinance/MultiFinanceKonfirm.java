package com.truemoneywitami.menu.pembayaran.multifinance;

import id.co.truemoney.report.TelkomReport;

import java.util.Date;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.setordantariktunai.SetorTunai;

public class MultiFinanceKonfirm extends Window implements RequestListener {

	Logger log = Logger.getLogger(MultiFinanceKonfirm.class);
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	CheckParameter check = new CheckParameter();
	JSONObject obj;
	String pesan, ack = "", inq = "", simpan = "";
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	// String username = "0852235455";
	String nama = "", id="", biayaBayar="", tagihan="", hargaCetak="", periode="", id_trx="",statustrx="",message="";
	Window win;
	int count = 0;
	String block = "";
	String send_trx = "";
	String mon = "";
	Window winheader;

	long inqsend,inqget,inqtime;
	long paysend,payget,paytime;
	
	@Override
	public void requestSucceeded(JSONObject obj) {

		if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("inquiry")) {
			inqget = new Date().getTime();
			inqtime = inqget - inqsend;
			String m1 = "Waktu saat Penerimaan Inq Pembayaran TV " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk Inq Pembayaran TV" +inqtime +" ms";
			func.cetakLog(m1, m2);
			biayaBayar = obj.get("biayaBayar").toString();
			nama = obj.get("nama").toString();
			tagihan = obj.get("tagihan").toString();
			hargaCetak = obj.get("hargaCetak").toString();
			periode = obj.get("periode").toString();
			inq = obj.get("ACK").toString();
			sesi.setSessionAttribute("blnthn", obj.get("blnthn").toString());
		} else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("payment")) {
			payget = new Date().getTime();
			paytime = payget - paysend;
			String m1 = "Waktu saat Penerimaan Pembayaran TV " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk Pembayaran TV " +paytime +" ms";
			func.cetakLog(m1, m2);
			ack = obj.get("ACK").toString();
			id_trx = obj.get("id_Transaksi").toString();
			statustrx = obj.get("status").toString();
			message = obj.get("pesan").toString();
			
			sesi.setSessionAttribute("pinalty", obj.get("pinalty").toString());
			sesi.setSessionAttribute("nama_operator", obj.get("nama_operator").toString());
			sesi.setSessionAttribute("tagihan_non_pinalty", obj.get("tagihan_non_pinalty").toString());
			sesi.setSessionAttribute("tenor", obj.get("tenor").toString());
			sesi.setSessionAttribute("ref2", obj.get("ref2").toString());
			sesi.setSessionAttribute("tipe_motor", obj.get("tipe_motor").toString());
			sesi.setSessionAttribute("jatuh_tempo", obj.get("jatuh_tempo").toString());
			
			try {
				sesi.setSessionAttribute("ref", obj.get("sn").toString());
				sesi.setSessionAttribute(Constants.tipe, obj.get("tipeTrx").toString());
				sesi.setSessionAttribute(Constants.TERBILANG, obj.get("terbilang").toString());
				sesi.setSessionAttribute(Constants.waktu, obj.get("tanggal").toString());
				sesi.setSessionAttribute("sn", obj.get("ref").toString());
			} catch (Exception e) {
				sesi.setSessionAttribute("ref", "-");
				sesi.setSessionAttribute(Constants.tipe, obj.get("tipeTrx").toString());
				sesi.setSessionAttribute(Constants.TERBILANG, "-");
				sesi.setSessionAttribute(Constants.waktu, func.getDateFormatNow("yyyy-MM-dd HH:mm:ss"));
				sesi.setSessionAttribute("sn", "-");
			}
		} else if (obj.get("ACK").equals("OK") && obj.get("ID").equals("Block")) {
			block = obj.get("ACK").toString();
		} else {
			pesan = obj.get("pesan").toString();
			requestFailed(Constants.pesan);
		}
	}

	@Override
	public void requestFailed(String message) {
		try {
			String m1 = "Waktu saat Penerimaan Pembayaran Internet " + new Date().toString();
			String m2 = "";
			func.cetakLog(m1, m2);
			Window wnd = getWindow();
			if (message.contains("PIN")) {
				Clients.showNotification("PIN Anda Salah. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				count++;
				if (count > 3) {
					JSONObject obj = new JSONObject();
					obj.put("Tipe", "BlockAccount");
					obj.put("username", username);
					ProcessingListener processing = new ProcessingListener(this, obj.toString());
					processing.processData();
					if (block.equals("OK")) {
						Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
						func.setTextboxDisabled(wnd, "pin", true);
					} else {
						System.out.println("Akun Gagal di Blokir");
					}
				}
			} else if (message.contains("blokir") && count == 3) {
				count++;
				Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				func.setTextboxDisabled(wnd, "pin", true);
			} else if (message.equals("01")) {
				Executions.sendRedirect("/timeout.zul");
			} else {
				sesi.setSessionAttribute(Constants.tipePesan, "PembayaranTV");
				sesi.setSessionAttribute(Constants.pesan, message);
				RedirectMenu menu = new RedirectMenu();
				menu.setMenuLink("TransaksiGagal", this.winheader);
			}
		} catch (Exception e) {

		}
	}

	public void setWindow(Window wnd) {
		this.win = wnd;
	}

	public Window getWindow() {
		return this.win;
	}

	public void setMenuWindow(Window header) {
		this.winheader = header;
	}

	public void setData(Window wnd, Window header) {
		try {
			setWindow(wnd);
			setMenuWindow(header);
			
			String namaProduk = sesi.getSessionAttribute("nama_produk");
			String path = "assets/img/";
			if (namaProduk.contains("ADIRA")) {
				path += "LOGOADIRA.png";
			} else if (namaProduk.contains("BUSSAN")) {
				path += "LOGOBUSSAN.png";
			} else if (namaProduk.contains("COLUMBIA")) {
				path += "LOGOCOLUMBIA.png";
			} else if (namaProduk.contains("MEGA AUTO")) {
				path += "LOGOMAF.png";
			} else if (namaProduk.contains("MEGA CENTRAL")) {
				path += "LOGOMCF.png";
			} else if (namaProduk.contains("WOM")) {
				path += "LOGOWOM.png";
			}
			
			func.setImage(wnd, "logo", path);
			
			id = sesi.getSessionAttribute("id_Pelanggan");
			simpan = sesi.getSessionAttribute("check");
			func.setLabelValue(wnd, "id", id);
//			send_trx = func.getIDTRX();
			send_trx = sesi.getSessionAttribute("id_trx");
			// System.out.println(id);
			// System.out.println(simpan);
			JSONObject obj = new JSONObject();
			obj.put("Tipe", "InqPembayaran");
			obj.put("id_Pelanggan", id);
			obj.put("id_Operator", sesi.getSessionAttribute("id_operator"));
			obj.put("ID_TipeTransaksi", "45");
			obj.put("username", username);
			obj.put("simpan", simpan);
			obj.put("id_trx", send_trx);
			String m1 = "Waktu saat Pengiriman Inq Pembayaran TV " + new Date().toString();
			String m2 = "";
			func.cetakLog(m1, m2);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			if (inq.equalsIgnoreCase("OK")) {
//				String jutag[] = periode.split(",");
//				part = jutag.length;
//				 temp = "";
//				for (int i=0;i<part;i++){
//					mon = jutag[i].substring(4,6) + "/" + jutag[i].substring(0,4);
//					temp += mon;
//					if (i != (part -1)){
//						temp +=", ";
//					}
//				}
				
//				mon = periode.substring(4,6) + "/" + periode.substring(0,4);
//				biayaBayar = String.valueOf(());
				func.setLabelValue(wnd, "nama", nama);
				func.setLabelValue(wnd, "periode", periode);
				func.setLabelValue(wnd, "biaya", func.formatRp(biayaBayar));
				func.setLabelValue(wnd, "amount", func.formatRp(tagihan));
//				hargaCetak = String.valueOf(Integer.parseInt(biayaBayar) + Integer.parseInt(tagihan) );
				func.setLabelValue(wnd, "total", func.formatRp(hargaCetak));
			}
		} catch (Exception e) {
			log.info("Error di Pembayaran TV." + e.getMessage());
		}
	}

	public void doPayment(Window wnd, Window header) {
		try {
			JSONObject obj = new JSONObject();
			String pin = func.getTextboxValue(wnd, "pin");

			if (check.isKosong(pin)) {
				Clients.showNotification("PIN tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else if (!(check.pinIsValid(pin))) {
				Clients.showNotification("PIN tidak valid, Silahkan coba kembali.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else {
				if (username.startsWith("0")) {
					obj.put("Tipe", "PembayaranMFMember");
				} else {
					obj.put("Tipe", "PembayaranMFAgent");
				}
				obj.put("username", username);
				obj.put("tagihan", tagihan);
				obj.put("id_Pelanggan", id);
				obj.put("verifikasi", "PIN");
				obj.put("id_TipeAplikasi", "2");
				obj.put("PIN", pin);
				obj.put("id_Operator", sesi.getSessionAttribute("id_operator"));
				obj.put("simpan", simpan);
				obj.put("id_trx", send_trx);
				obj.put("biayaBayar", biayaBayar);
				obj.put("hargaCetak", hargaCetak);
//				obj.put("tunggak", part);
				String m1 = "Waktu saat Pengiriman Pembayaran TV " + new Date().toString();
				String m2 = "";
				func.cetakLog(m1, m2);
				ProcessingListener processing = new ProcessingListener(this, obj.toString());
				processing.processData();
				String head = "Pembayaran Tagihan TV Berbayar";
//				if (statustrx.equals("SUKSES")){
////					message = "Pembayaran PLN Berhasil";
//					head = "Pembayaran Berhasil";
//				} else if (statustrx.equals("PENDING")){
////					message = "Pembayaran PLN PENDING";
//					head = "Transaksi Sedang Diproses";
//				}else {
//					message = "";
//					head = "";
//				}
				if (ack.equalsIgnoreCase("OK")) {
					sesi.setSessionAttribute(Constants.status, statustrx);
					sesi.setSessionAttribute(Constants.id_pelanggan, id);
					sesi.setSessionAttribute(Constants.namaPelanggan, nama);
					sesi.setSessionAttribute(Constants.tagihan, tagihan);
					sesi.setSessionAttribute(Constants.biaya, biayaBayar);
					sesi.setSessionAttribute(Constants.total, hargaCetak);
					sesi.setSessionAttribute(Constants.id_trx, id_trx);
					sesi.setSessionAttribute(Constants.periode, periode);
					sesi.setSessionAttribute(Constants.header, head);
					sesi.setSessionAttribute(Constants.pesan, message);
					
					RedirectMenu menu = new RedirectMenu();
					menu.setMenuLink("paymentSuccess", header);
				}
			}

		} catch (Exception e) {
			log.info("Error di Pembayaran TV." + e.getMessage());
		}
	}

	public void setBack(Window wnd) throws Exception {
		RedirectMenu menu = new RedirectMenu();
		menu.setMenuLink("pembayarantv", wnd);
	}
}
