package com.truemoneywitami.menu.pembayaran.pdam;

import java.util.Date;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.ProcessingListenerPrabayar;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.setordantariktunai.SetorTunai;

public class PdamConfirm extends Window implements RequestListener {


	Logger log = Logger.getLogger(PdamConfirm.class);
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	CheckParameter check = new CheckParameter();
	JSONObject obj;
	String pesan,ack="",inq="",simpan="";
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	//String username = "0852235455";
	String nama="",id="",biayaBayar="",tagihan="",hargaCetak="",periode="",area="",id_trx="",statustrx="",message="";
	Window win;int count =0;String block ="";
	String send_trx = "";String mon="";
	Window winheader;
	int part;String tipe="";
	private String ref = "";
	private String terbilang = "";
	private String jmlAdmin = "";
	private String standMeter = "";
	private String pdamName = "";
	private String noResi = "";
	private String nonTagAir = "";
	private String alamat = "";
	private String pemakaian = "";
	private String jmlBayar = "";
	private String namaPelanggan = "";
	private String beban = "";
	private String denda = "";
	private String tanggal = "";
	private String idPelanggan = "";
	private String jmlTagihan = "";
//	private String blnThn = "";
	private String nominalStruk = "";
	private String materai = "";
	private String nosambungan = "";
	private String kodePdam = "";
	private JSONArray periodeArr = new JSONArray();
	
	long inqsend,inqget,inqtime;
	long paysend,payget,paytime;
	
	@Override
	public void requestSucceeded(JSONObject obj) {	 
		
		if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("inquiry")){
			inqget = new Date().getTime();
			inqtime = inqget - inqsend;
			String m1 = "Waktu saat penerimaan Inq Pembayaran PDAM " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk Inq Pembayaran PDAM " +inqtime + " ms";
			func.cetakLog(m1, m2);
			biayaBayar = obj.get("biayaBayar").toString();
			nama = obj.get("nama").toString();
			tagihan = obj.get("tagihan").toString();
			hargaCetak = obj.get("hargaCetak").toString();
			periode = obj.get("periode").toString();
			inq = obj.get("ACK").toString();
		}
		else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("payment")){
			payget = new Date().getTime();
			paytime = payget - paysend;
			String m1 = "Waktu saat Penerimaan Pembayaran PDAM " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk  Pembayaran PDAM " +paytime + " ms";
			func.cetakLog(m1, m2);
			ack = obj.get("ACK").toString();
			id_trx = obj.get("id_Transaksi").toString();
			message = obj.get("pesan").toString();
			statustrx = obj.get("status").toString();
			tipe = obj.get("tipeTrx").toString();
			ref = obj.get("ref").toString();
			
			if (statustrx.equals("SUKSES")) {
				// response tambahan untuk struk
				periodeArr = (JSONArray) obj.get("periode_list");
				terbilang = obj.get("terbilang").toString();
				System.out.println("**************************************** terbilang 2 : " + terbilang);
				jmlAdmin = obj.get("jumlahadm").toString();
//				standMeter = obj.get("standmeter").toString();
				pdamName = obj.get("pdamname").toString();
				noResi = obj.get("noresi").toString();
				nonTagAir = obj.get("nontagair").toString();
				alamat = obj.get("alamat").toString();
				pemakaian = obj.get("pemakaian").toString();
				jmlBayar = obj.get("jumlahbayar").toString();
				namaPelanggan = obj.get("namapelanggan").toString();
				beban = obj.get("beban").toString();
				denda = obj.get("denda").toString();
				tanggal = obj.get("tanggal").toString();
				id = obj.get("id_pelanggan").toString();
				jmlTagihan = obj.get("jumlahtagihan").toString();
				periode = obj.get("bulan_thn").toString();
				nominalStruk = obj.get("nominal_struk").toString();
				materai = obj.get("materai").toString();
				nosambungan = obj.get("nosambungan").toString();
				kodePdam = obj.get("kode_pdam").toString();
			}
			
		}
		else if (obj.get("ACK").equals("OK") && obj.get("ID").equals("Block")){
			block = obj.get("ACK").toString();
		}
		else {
			pesan = obj.get("pesan").toString();
			requestFailed(Constants.pesan);
		}
	}
	
	@Override
		public void requestFailed(String message){
			try {
				String m1 = "Waktu saat Penerimaan Pembayaran Internet " + new Date().toString();
				String m2 = "";
				func.cetakLog(m1, m2);
			Window wnd = getWindow();
			if (message.contains("PIN")) {
//				Clients.showNotification("PIN Anda Salah. Silahkan coba kembali");
				Clients.showNotification("PIN Anda Salah. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				count++;
				if (count >3){
					JSONObject obj = new JSONObject();
					obj.put("Tipe", "BlockAccount");
					obj.put("username", username);
					ProcessingListener processing = new ProcessingListener(this, obj.toString());
					processing.processData();
					if (block.equals("OK")){
//						Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami");
						Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
						func.setTextboxDisabled(wnd, "pin", true);
					}
					else {
						System.out.println("Akun Gagal di Blokir");
					}
				}
			}
			else if (message.contains("blokir") && count ==3){
				count++;
//				Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami");
				Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				func.setTextboxDisabled(wnd, "pin", true);
			}
			else if(message.equals("01")) {
				Executions.sendRedirect("/timeout.zul");
			}
			else if(message.equals("TIMEOUT")) {
				ack = "NOK";
//				Clients.showNotification(Constants.MSG_TIMEOUT_ERROR);
				Clients.showNotification(Constants.MSG_TIMEOUT_ERROR, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else {
			sesi.setSessionAttribute(Constants.tipePesan,"PembayaranPDAM");
			sesi.setSessionAttribute(Constants.pesan,message);
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("TransaksiGagal",this.winheader);
			}
			} catch (Exception e){
				
			}
			
		}
		public void setWindow (Window wnd) {
			this.win = wnd;
		}
		
		public Window getWindow() {
			return this.win;
		}
		
		public void setMenuWindow(Window header){
			this.winheader = header;
		}
		
		public void setData(Window wnd,Window header)  {
			try {
			setWindow(wnd);
			setMenuWindow(header);
			id = sesi.getSessionAttribute("id_Pelanggan");
			simpan = sesi.getSessionAttribute("check");
			String namaArea = sesi.getSessionAttribute("NamaArea");
			System.out.println(simpan);
			area = sesi.getSessionAttribute("area");
			func.setLabelValue(wnd, "id", id);
			func.setLabelValue(wnd, "area",namaArea);
//			send_trx = func.getIDTRX();
			send_trx = sesi.getSessionAttribute("id_trx");
			JSONObject obj = new JSONObject();
			obj.put("Tipe","InqPembayaran");
			obj.put("id_Pelanggan",id);
			obj.put("id_Operator",area);
			obj.put("ID_TipeTransaksi","11");
			obj.put("username",username);
			obj.put("simpan",simpan);
			obj.put("id_trx", send_trx);
			String m1 = "Waktu saat Pengiriman Inq Pembayaran PDAM " + new Date().toString();
			String m2 = "";
			func.cetakLog(m1, m2);
			inqsend = new Date().getTime();
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			if (inq.equalsIgnoreCase("OK")) {
				
//				String jutag[] = periode.split(",");
//				part = jutag.length;
//				temp = "";
//				for (int i=0;i<part;i++){
//					mon = jutag[i].substring(4,6) + "/" + jutag[i].substring(0,4);
//					temp += mon;
//					if (i != (part -1)){
//						temp +=", ";
//					}
//				}
				
//				biayaBayar = String.valueOf((Integer.parseInt(biayaBayar) * part));
				func.setLabelValue(wnd, "nama", nama);
				func.setLabelValue(wnd, "periode", periode);
				func.setLabelValue(wnd, "biaya", func.formatRp(biayaBayar));
				func.setLabelValue(wnd, "amount", func.formatRp(tagihan));
//				hargaCetak = String.valueOf(Integer.parseInt(biayaBayar) + Integer.parseInt(tagihan) );
				func.setLabelValue(wnd, "total", func.formatRp(hargaCetak));
			}
			} catch (Exception e) {
				log.info("Error di Pembayaran PDAM." + e.getMessage());
			}
		}
		
		public void doPayment(Window wnd,Window header) {
			try {
			JSONObject obj = new JSONObject();
			String pin = func.getTextboxValue(wnd, "pin");
			
			if(check.isKosong(pin)) {
//				Clients.showNotification("PIN tidak boleh kosong");
				Clients.showNotification("PIN tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else if (!(check.pinIsValid(pin))) {
//				Clients.showNotification("PIN tidak valid, Silahkan coba kembali.");
				Clients.showNotification("PIN tidak valid, Silahkan coba kembali.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else {
				if (username.startsWith("0")){
					obj.put("Tipe","PembayaranPDAMMember");
				}
				else {
					obj.put("Tipe","PembayaranPDAMAgent");
				}
				obj.put("username",username);
				obj.put("tagihan",tagihan);
				obj.put("id_Pelanggan",id);
				obj.put("verifikasi","PIN");
				obj.put("id_TipeAplikasi", "2");
				obj.put("biayaBayar", biayaBayar);
				obj.put("hargaCetak", hargaCetak);
				obj.put("PIN",pin);
				obj.put("id_Operator",area);
				obj.put("simpan",simpan);
				obj.put("id_trx", send_trx);
//				obj.put("tunggak", part);
				String m1 = "Waktu saat Pengiriman Pembayaran PDAM " + new Date().toString();
				String m2 = "";
				func.cetakLog(m1, m2);
				paysend = new Date().getTime();
				ProcessingListenerPrabayar processing = new ProcessingListenerPrabayar(this, obj.toString());
				processing.processData();  
				String head = "Pembayaran Tagihan PDAM";
//				if (statustrx.equals("SUKSES")){
////					message = "Pembayaran PLN Berhasil";
//					head = "Pembayaran Berhasil";
//				} else if (statustrx.equals("PENDING")){
////					message = "Pembayaran PLN PENDING";
//					head = "Transaksi Sedang Diproses";
//				}else {
//					message = "";
//					head = "";
//				}
				if(ack.equalsIgnoreCase("OK")){
					sesi.setSessionAttribute(Constants.status, statustrx);
					sesi.setSessionAttribute(Constants.id_pelanggan, id);
					sesi.setSessionAttribute(Constants.namaPelanggan, nama);
					sesi.setSessionAttribute(Constants.tagihan,tagihan);
					sesi.setSessionAttribute(Constants.biaya,biayaBayar);
					sesi.setSessionAttribute(Constants.total,hargaCetak);
					sesi.setSessionAttribute(Constants.id_trx, id_trx);
					sesi.setSessionAttribute(Constants.header,head);
					sesi.setSessionAttribute(Constants.pesan, message);
					sesi.setSessionAttribute(Constants.periode, periode);
					sesi.setSessionAttribute(Constants.tipe, tipe);
					sesi.setSessionAttribute(Constants.ref, ref);
					
					// tambahan untuk struk
					sesi.setSessionAttribute(Constants.PERIODE_ARR, periodeArr.toString());					
					sesi.setSessionAttribute(Constants.TERBILANG, terbilang);					
					sesi.setSessionAttribute(Constants.JUMLAH_ADMIN, jmlAdmin);					
					sesi.setSessionAttribute(Constants.STAND_METER, standMeter);					
					sesi.setSessionAttribute(Constants.PDAM_NAME, pdamName);					
					sesi.setSessionAttribute(Constants.NO_RESI, noResi);					
					sesi.setSessionAttribute(Constants.NON_TAG_AIR, nonTagAir);					
					sesi.setSessionAttribute(Constants.ALAMAT, alamat);					
					sesi.setSessionAttribute(Constants.PEMAKAIAN, pemakaian);					
					sesi.setSessionAttribute(Constants.JUMLAH_BAYAR, jmlBayar);					
					sesi.setSessionAttribute(Constants.NAMA_PELANGGAN, namaPelanggan);					
					sesi.setSessionAttribute(Constants.BEBAN, beban);					
					sesi.setSessionAttribute(Constants.DENDA, denda);					
					sesi.setSessionAttribute(Constants.TANGGAL, tanggal);					
//					sesi.setSessionAttribute(Constants.id_pelanggan, idPelanggan);					
					sesi.setSessionAttribute(Constants.JUMLAH_TAGIHAN, jmlTagihan);					
//					sesi.setSessionAttribute(Constants.periode, blnThn);
					sesi.setSessionAttribute(Constants.NOMINAL_STRUK, nominalStruk);
					sesi.setSessionAttribute("materai", materai);
					sesi.setSessionAttribute("nosambungan", nosambungan);
					sesi.setSessionAttribute("kode_pdam", kodePdam);
					
					System.out.println("**************************************** terbilang 1 : " + terbilang);
					
					RedirectMenu menu = new RedirectMenu();
					menu.setMenuLink("paymentSuccess",header);
				}
			}
			
			
			} catch (Exception e) {
				log.info("Error di Pembayaran PDAM." + e.getMessage(), e);
			}
		}
		
		public void setBack(Window wnd) throws Exception {
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("pembayaranpdam",wnd);
		}


}
