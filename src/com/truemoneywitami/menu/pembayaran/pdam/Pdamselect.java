package com.truemoneywitami.menu.pembayaran.pdam;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.setordantariktunai.SetorTunai;

public class Pdamselect extends Window implements RequestListener {


	Logger log = Logger.getLogger(Pdamselect.class);
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	JSONObject obj;
	String pesan,ack;
	JSONArray norek,listdata;
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	//String username = "0852235455";
	String idt="",area = "";
	String tipeUser="";
	Window winheader;
	String NamaArea = "";
	@Override
	public void requestSucceeded(JSONObject obj) {	 
		
		if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("data") ){
			ack = obj.get("ACK").toString();
			norek = (JSONArray) obj.get("daftar");
		}
		else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("Pdam")) {
			ack = obj.get("ACK").toString();
			listdata = (JSONArray) obj.get("ListData");
			for (int i = 0; i < listdata.size(); i++) {
				System.out.println(listdata.get(i));
			}
		}		
		else {
			pesan = obj.get("pesan").toString();
			requestFailed(pesan);
		}
	}
	
	@Override
	public void requestFailed(String message){
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
//			Clients.showNotification(message);
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
	public void setMenuWindow(Window header){
		this.winheader = header;
	}
	
	public void setListboxValuePel(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID) {
		try {
		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int a = data.size();
		
		//int lIndex = 0;
		
		String idPel = "";
		String idOperator = "";
		String namaOp = "";
		
		for (int i =0;i<a;i++){			
			Listitem li = new Listitem();
			JSONObject child  = (JSONObject) data.get(i);
			
			// pisahkan id pelanggan dengan kode pdam
			String idPelKode = child.get("data").toString();
			log.info("id pel + kode -> " + idPelKode);
			
			idOperator = child.get("id_operator").toString();
			
			namaOp = child.get("nama_operator").toString();
						
			li.setValue(idPelKode+"##"+idOperator+"##"+namaOp);
			li.appendChild(new Listcell(idPelKode));
			list.appendChild(li);
		}
		} catch (Exception e) {
			log.info("Error di Pembayaran PDAM." + e.getMessage());
		}
	}
	
	public void getPelanggan(Window wnd)  {
		try {
			idt = func.getListboxValue(wnd, "pel_sel");
			} catch (Exception ex){
				log.info("Error di Pembayaran PDAM." + ex.getMessage());
			}
	}
	
	public void setListboxValueArea(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID) throws Exception {
		try {
		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int a = data.size();
		
		//int lIndex = 0;
		
		for (int i =0;i<a;i++){
			Listitem li = new Listitem();
			JSONObject child  = (JSONObject) data.get(i);
			li.setValue(child.get("id").toString());
			String nama = child.get("nama").toString();
			li.appendChild(new Listcell(nama));
			list.appendChild(li);
		}
		} catch (Exception e){
			log.info("Error di Pembayaran PDAM." + e.getMessage());
		}
	}
	
	public void getArea(Window wnd)  {
		try {
			area = func.getListboxValue(wnd, "area_sel");
			} catch (Exception ex){
				log.info("Error di Pembayaran PDAM." + ex.getMessage());
			}
	}
	public void setData(Window wnd,Window header) {
		setMenuWindow(header);
		try {
			String idTrx = func.getIDTRX();
			sesi.setSessionAttribute("id_trx", idTrx);
		if (username.startsWith("0")){
			tipeUser = "member";
		}	else {
			tipeUser="Agent";
		}	
		JSONObject obj = new JSONObject();
		obj.put("Tipe","peldata");
		obj.put("ID_Account", username);
		obj.put("id_TipeTransaksi", 11);
		obj.put("tipeUser",tipeUser);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		obj = new JSONObject();
		obj.put("Tipe","ListPdam");
		obj.put("ID_Account", username);
		processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		setListboxValuePel(wnd,"pel_sel", norek, 0, true, true, true);
		setListboxValueArea(wnd,"area_sel",listdata , 0, true, true, true);
		} catch (Exception e){
			log.info("Error di Pembayaran PDAM." + e.getMessage());
		}
	}
	
	public void next(Window wnd,Window header)  {
		
		try {
		String checked="NOK";
		Checkbox ck = (Checkbox) wnd.getFellow("save");
//		Radio rd = (Radio) wnd.getFellow("radio1");
		if (ck.isChecked()){
			checked = "OK";
		}
//		if (rd.isSelected()){
//			getPelanggan(wnd);
//		}
//		else {
//			
//			idt = func.getTextboxValue(wnd, "idpel");
//		}
		
		Radio rd = (Radio) wnd.getFellow("radio1");
		if (rd.isSelected()){
			try {
				idt = func.getListboxValue(wnd, "pel_sel");
				
				String temp[] = idt.split("##");
				
				idt = temp[0];
				area = temp[1];
				NamaArea = temp[2];
			} catch(Exception e) {
				idt = "";
				area = "";
			}
			
		}
		else {
			try {
				idt = func.getTextboxValue(wnd, "idpel");
				area = func.getListboxValue(wnd, "area_sel");
				NamaArea = func.getListboxLabel(wnd, "area_sel");
			} catch(Exception e) {
				idt = "";
				area = "";
			}
			
		}
		
//		try {
//			area = func.getListboxValue(wnd, "area_sel");
//			NamaArea = func.getListboxLabel(wnd, "area_sel");
//			
//		} catch(Exception e) {
//			area = "";
//		}
		
		if (idt.equals("")){
			//Messagebox.show("ID Pelanggan dan Kode Area Tidak Boleh Kosong","Error", Messagebox.OK, Messagebox.ERROR);
//			Clients.showNotification("No ID Pelanggan Tidak Boleh Kosong, Silahkan coba kembali");
			Clients.showNotification("No ID Pelanggan Tidak Boleh Kosong, Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (area.equals("")){
//			Clients.showNotification("Kode Area Tidak Boleh Kosong, Silahkan coba kembali");
			Clients.showNotification("Kode Area Tidak Boleh Kosong, Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (idt.length() > 20 || idt.length() < 5){
//			Clients.showNotification("No ID Pelanggan tidak valid. Silahkan coba kembali");
			Clients.showNotification("No ID Pelanggan tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else {
			try {
//			long test = Long.parseLong(idt);
			
			sesi.setSessionAttribute("id_Pelanggan", idt);
			sesi.setSessionAttribute("check",checked);
			sesi.setSessionAttribute("area",area);
			sesi.setSessionAttribute("NamaArea", NamaArea);
			RedirectMenu menu = new RedirectMenu();
			new RedirectMenu().setMenuLink("konfirmasipembayaranpdam",header);
			} catch (Exception e) {
//				Clients.showNotification("No ID Pelanggan tidak valid. Silahkan coba kembali");
				Clients.showNotification("No ID Pelanggan tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
		}
		} catch (Exception e) {
			log.info("Error di Pembayaran PDAM." + e.getMessage());
		}
	}

}
