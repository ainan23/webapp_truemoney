package com.truemoneywitami.menu.pembayaran.bpjs;

import java.util.Date;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.ProcessingListenerPrabayar;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class BpjsKonfirm extends Window implements RequestListener {
	
	Logger log = Logger.getLogger(BpjsKonfirm.class);
	//ParamaterDto param = new ParamaterDto();
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	CheckParameter check = new CheckParameter();
	JSONObject obj;
	String pesan,ack="",inq="",simpan="";
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	//String username = "0852235455";
	String nama="",id="",biayaBayar="",tagihan="",hargaCetak="",periode="",id_trx="",message="",statustrx="",jmlPeriode="";
	Window win;int count =0;String block="";
	String send_trx = "";String mon="";
	String tipe = "";
	String ref = "";
	String waktu = "";
	String noHp = "";
	String jmlPremi = "";
	Window winheader; 
//	int part;
//	String temp="";
	long inqsend,inqget,inqtime;
	long paysend,payget,paytime;
	
	String statusPromo = "0";
	String nilai = "0";
	
	private JSONArray promo = new JSONArray();
	
	@Override
	public void requestSucceeded(JSONObject obj) {	 
		
		if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("inquiry")){
			inqget = new Date().getTime();
			inqtime= inqget - inqsend;
			String m1 = "Waktu saat Penerimaan Inquiry   Pembayaran BPJS " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk Inq Pembayaran BPJS " +inqtime +" ms";
			func.cetakLog(m1, m2);
			biayaBayar = obj.get("biayaBayar").toString();
			nama = obj.get("nama").toString();
			tagihan = obj.get("tagihan").toString();
			hargaCetak = obj.get("hargaCetak").toString();
			periode = obj.get("periode").toString() + " BULAN";
			inq = obj.get("ACK").toString();
			jmlPeriode = obj.get("jml_periode").toString();
			
			promo = (JSONArray) obj.get("promo");
			statusPromo = promo.get(0).toString();
			nilai = promo.get(1).toString();
		}
		else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("payment")){	
			payget = new Date().getTime();
			paytime = payget - paysend;
			String m1 = "Waktu saat Penerimaan Pembayaran BPJS " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk Pembayaran BPJS " +paytime + " ms";
			func.cetakLog(m1, m2);
			ack = obj.get("ACK").toString();
			id_trx = obj.get("id_Transaksi").toString();
			message = obj.get("pesan").toString();
			statustrx = obj.get("status").toString();
			tipe = obj.get("tipeTrx").toString();
			ref = obj.get("ref").toString();
			waktu = obj.get("tanggal").toString();
			noHp = obj.get("hp").toString();
		}
		else {
			pesan = obj.get("pesan").toString();
			requestFailed(Constants.pesan);
		}
	}
	
	
		public void setWindow (Window wnd) {
			this.win = wnd;
		}
		
		public  void setMenuWindow (Window header){
			this.winheader = header;
		}
		
		public Window getWindow() {
			return this.win;
		}
	
		@Override
		public void requestFailed(String message){
			try {
				String m1 = "Waktu saat Penerimaan Pembayaran BPJS " + new Date().toString();
				String m2 = "";
				func.cetakLog(m1, m2);
			Window wnd = getWindow();
			if (message.contains("PIN")) {
//				Clients.showNotification("PIN Anda Salah. Silahkan coba kembali");
				Clients.showNotification("PIN Anda Salah. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				count++;
				if (count >3){
					JSONObject obj = new JSONObject();
					obj.put("Tipe", "BlockAccount");
					obj.put("username", username);
					ProcessingListener processing = new ProcessingListener(this, obj.toString());
					processing.processData();
					if (block.equals("OK")){
						count++;
//						Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami");
						Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
						func.setTextboxDisabled(wnd, "pin", true);
					}
					else {
						System.out.println("Akun Gagal di Blokir");
					}
				}
			}
			else if (message.contains("blokir") && count ==3){
				count++;
//				Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami");
				Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				func.setTextboxDisabled(wnd, "pin", true);
			} 
			else if(message.equals("01")) {
				Executions.sendRedirect("/timeout.zul");
			}
			else if(message.equals("TIMEOUT")) {
				ack = "NOK";
//				Clients.showNotification(Constants.MSG_TIMEOUT_ERROR);
				Clients.showNotification(Constants.MSG_TIMEOUT_ERROR, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else {
				sesi.setSessionAttribute(Constants.tipePesan,"PembayaranBpjs");
				sesi.setSessionAttribute(Constants.pesan,message);
				RedirectMenu menu = new RedirectMenu();
				menu.setMenuLink("TransaksiGagal",this.winheader);
			}
			} catch (Exception e) {
				
			}
		}
		
		public void setData(Window wnd,Window header) {
			try {
			setWindow(wnd);
			setMenuWindow(header);
			id = sesi.getSessionAttribute("id_Pelanggan");
			simpan = sesi.getSessionAttribute("check");
			jmlPremi = sesi.getSessionAttribute("jmlpremi");
			noHp = sesi.getSessionAttribute("handphone");
			func.setLabelValue(wnd, "id", id);
//			send_trx = func.getIDTRX();
			send_trx = sesi.getSessionAttribute("id_trx");
//			System.out.println(id);
//			System.out.println(simpan);
			JSONObject obj = new JSONObject();
			obj.put("Tipe","InqPembayaran");
			obj.put("id_Pelanggan",id);
			obj.put("id_Operator","100");
			obj.put("ID_TipeTransaksi","39");
			obj.put("username",username);
			obj.put("simpan",simpan);
			obj.put("id_trx", send_trx);
			obj.put("jmlpremi", jmlPremi);
			obj.put("handphone", noHp);
			String m1 = "Waktu saat Pengiriman  Inq Pembayaran BPJS " + new Date().toString();
			String m2 = "";
			func.cetakLog(m1, m2);
			inqsend = new Date().getTime();
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			if (inq.equalsIgnoreCase("OK")) {
				
				if (statusPromo.equals("0")) {
					func.setDivVisible(wnd, "divdiskon", false);
					func.setDivVisible(wnd, "divtextdiskon", false);
				} else {
					func.setDivVisible(wnd, "divdiskon", true);
					func.setDivVisible(wnd, "divtextdiskon", true);
					
					String diskon = func.getLabelValue(wnd, "lbldiskon");
					func.setLabelValue(wnd, "lbldiskon", diskon + " " + func.formatRp(nilai));
					
					int totalAfterDiskon = Integer.parseInt(hargaCetak) - Integer.parseInt(nilai);
					hargaCetak = String.valueOf(totalAfterDiskon);
				}
				
//				String jutag[] = periode.split(",");
//				part = jutag.length;
//				 temp = "";
//				for (int i=0;i<part;i++){
//					mon = jutag[i].substring(4,6) + "/" + jutag[i].substring(0,4);
//					temp += mon;
//					if (i != (part -1)){
//						temp +=", ";
//					}
//				}
//				part = Integer.parseInt(periode);
				
//				mon = periode.substring(4,6) + "/" + periode.substring(0,4);
//				biayaBayar = String.valueOf((Integer.parseInt(biayaBayar) * part));
				func.setLabelValue(wnd, "nama", nama);
				func.setLabelValue(wnd, "periode",periode);
				func.setLabelValue(wnd, "biaya", func.formatRp(biayaBayar));
				func.setLabelValue(wnd, "diskon", func.formatRp(nilai));
				func.setLabelValue(wnd, "amount", func.formatRp(tagihan));
//				hargaCetak = jumlahBayar;//String.valueOf(Integer.parseInt(biayaBayar) + Integer.parseInt(tagihan) );
				func.setLabelValue(wnd, "total", func.formatRp(hargaCetak));
			}
			} catch (Exception e) {
				log.info("Error di Pembayaran BPJS." + e.getMessage());
			}
		}
		
		public void doPayment(Window wnd,Window header) {
			try {
			JSONObject obj = new JSONObject();
			String pin = func.getTextboxValue(wnd, "pin");
			
			if(check.isKosong(pin)) {
//				Clients.showNotification("PIN tidak boleh kosong");
				Clients.showNotification("PIN tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
//				Messagebox.show("PIN tidak boleh kosong", "Truemoney", Messagebox.OK, Messagebox.ERROR);
			} else if (!(check.pinIsValid(pin))) {
//				Clients.showNotification("PIN tidak valid, Silahkan coba kembali.");
				Clients.showNotification("PIN tidak valid, Silahkan coba kembali.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
//				Messagebox.show("PIN harus angka", "Truemoney", Messagebox.OK, Messagebox.ERROR);
			} else {
				if (username.startsWith("0")){
					obj.put("Tipe","PembayaranBpjsMember");
				}
				else {
					obj.put("Tipe","PembayaranBpjsAgent");
				}
				obj.put("username",username);
				obj.put("tagihan",tagihan);
				obj.put("id_Pelanggan",id);
				obj.put("verifikasi","PIN");
				obj.put("biayaBayar",biayaBayar);
				
				hargaCetak = String.valueOf(Integer.parseInt(hargaCetak) + Integer.parseInt(nilai));
				
				obj.put("hargaCetak",hargaCetak);
				obj.put("id_TipeAplikasi", "2");
				obj.put("PIN",pin);
				obj.put("id_Operator","100");
				obj.put("simpan",simpan);
				obj.put("id_trx", send_trx);
				obj.put("jmlpremi", jmlPremi);
				obj.put("handphone", noHp);
				obj.put("jml_periode", jmlPeriode);
//				obj.put("tunggak", part);
				paysend = new Date().getTime();
				String m1 = "Waktu saat Pengiriman Pembayaran BPJS " + new Date().toString();
				String m2 = "";
				func.cetakLog(m1, m2);
				ProcessingListenerPrabayar processing = new ProcessingListenerPrabayar(this, obj.toString());
				processing.processData();  
				String head = "Pembayaran Tagihan BPJS Kesehatan";
				if (statustrx.equals("SUKSES")){
//					message = "Pembayaran PLN Berhasil";
				} else if (statustrx.equals("PENDING")){
//					message = "Pembayaran PLN PENDING";
				}else {
//					message = "";
				}
				if(ack.equalsIgnoreCase("OK")){
					sesi.setSessionAttribute(Constants.status, statustrx);
					sesi.setSessionAttribute(Constants.id_pelanggan, id);
					sesi.setSessionAttribute(Constants.namaPelanggan, nama);
					sesi.setSessionAttribute(Constants.tagihan,tagihan);
					sesi.setSessionAttribute(Constants.biaya,biayaBayar);
					sesi.setSessionAttribute(Constants.total,hargaCetak);
					sesi.setSessionAttribute(Constants.id_trx, id_trx);
					sesi.setSessionAttribute(Constants.header,head);
					sesi.setSessionAttribute(Constants.pesan, message);
					sesi.setSessionAttribute(Constants.periode,periode);
					sesi.setSessionAttribute(Constants.tipe,tipe);
					sesi.setSessionAttribute(Constants.ref,ref);
					sesi.setSessionAttribute(Constants.handPhone,noHp);
					sesi.setSessionAttribute(Constants.waktu,waktu);
					RedirectMenu menu = new RedirectMenu();
					menu.setMenuLink("paymentBpjsSuccess",header);
				}
			}
			
			} catch (Exception e) {
				log.info("Error di Pembayaran BPJS." + e.getMessage());
			}
		}
		
		public void setBack(Window wnd) throws Exception {
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("pembayaranbpjs",wnd);
		}

}
