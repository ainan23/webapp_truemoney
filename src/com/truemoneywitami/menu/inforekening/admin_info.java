package com.truemoneywitami.menu.inforekening;

import org.zkoss.zul.Datebox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Window;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
//import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.zkoss.bind.annotation.BindingParam;

//import javax.servlet.http.HttpServletRequest;

import org.zkoss.exporter.pdf.PdfExporter;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
//import org.zkoss.zk.ui.Execution;
//import org.zkoss.zk.ui.Executions;
//import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;

import com.truemoneywitami.dto.MutasiTransaksiDto;

//import com.lowagie.text.Font;
//import com.lowagie.text.Phrase;
//import com.lowagie.text.pdf.PdfPCell;
//import com.lowagie.text.pdf.PdfPTable;
//import org.zkoss.exporter.pdf.FontFactory;
//import org.zkoss.exporter.pdf.PdfPCellFactory;
//import org.zkoss.exporter.util.GroupsModelArrayAdapter;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.infokomisi.InfoKomisi;

import net.sf.jasperreports.engine.JasperReport;
//import org.json.JSONObject;

public class admin_info extends Window implements RequestListener {
	
	Logger log = Logger.getLogger(admin_info.class);
	List<MutasiTransaksiDto> listmutasi = new ArrayList<MutasiTransaksiDto>();
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	JSONObject obj;
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	//String username = "0852235455";
	String saldo="",
	ack="",
	pesan="";
	JSONArray mutasi;
	long saldosend,saldoget,mutasisend,mutasiget,saldotime,mutasitime;
	@Override
	public void requestSucceeded(JSONObject obj) {
		if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("saldo")) {
			saldoget = new Date().getTime();
			saldotime = saldoget - saldosend;
			System.out.println("Ini Rumus nya " +saldotime +"==" + saldoget +"-" +saldosend);
			saldo = obj.get("lastBalance").toString();
			String m2 = "Waktu yang dibutuhkan untuk cetak saldo adalah " + saldotime +" ms";
			String m1 = "Waktu penerimaaan  Response check saldo" + new Date().toString();
			func.cetakLog(m1, m2);
			
		} else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("mutasi")) {
			ack = obj.get("ACK").toString();
			mutasi = (JSONArray) obj.get("mutasi");
			mutasiget = new Date().getTime();
			System.out.println("mutasi Send-->yang kedua:: " +mutasisend );
			mutasitime = mutasiget - (mutasisend);
			System.out.println("Ini Rumus nya " +mutasitime +"==" + mutasiget +"-" +mutasisend);
			String m2 = "Waktu yang dibutuhkan untuk Mutasi adalah " + mutasitime +" ms";
			String m1 = "Waktu penerimaaan  Response mutasi" + new Date().toString();
			func.cetakLog(m1, m2);
		} else {
			requestFailed(Constants.generalErrorMessage);
		}
	}

	@Override
	public void requestFailed(String message) {
		
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
//			Clients.showNotification(message);
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}

	public void setData(Window wnd) throws Exception {
		JSONObject obj = new JSONObject();
		
		obj.put("Tipe", "InfoSaldoAgent");
		obj.put("username", username);
		saldosend = new Date().getTime();
		String m2 = "";
		String m1 = "Waktu pengiriman Request check saldo" + new Date().toString();
		func.cetakLog(m1, m2);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		func.setLabelValue(wnd, "norek",":" + username);
		func.setLabelValue(wnd, "saldo",":" + func.formatRp(saldo));
	}

	public void cekMutasi(Window wnd) throws Exception {
		// System.out.println("Test");
		String tgl1="",tgl2="";
		try {
		Date temptgl1;
		Date temptgl2;
		temptgl1 = func.getDateboxValue(wnd, "datetimepicker7");
		temptgl2 = func.getDateboxValue(wnd, "datetimepicker8");
		tgl1 = func.getDateboxText(wnd, "datetimepicker7");
		tgl2 = func.getDateboxText(wnd, "datetimepicker8");
		if (tgl1.equalsIgnoreCase("") || tgl2.equalsIgnoreCase("")){
			String message="Tanggal Awal dan Akhir Harus di Isi";
			requestFailed(message);
		} 
		else if (temptgl1.after(temptgl2)){
			String message="Tanggal Akhir tidak boleh  lebih kecil dari tanggal awal";
			requestFailed(message);
		}
		else {
			JSONObject obj = new JSONObject();
			obj.put("Tipe", "RekapMutasiTanggal");
			obj.put("username", username);
			obj.put("tipeUser", "agent");
			obj.put("tanggalawal", tgl1);
			obj.put("tanggalakhir", tgl2);
			obj.put("id_TipeAplikasi", 2);
			mutasisend = new Date().getTime();
			System.out.println("mutasi Send-->11111 " +mutasisend );
			String m2 = "";
			String m1 = "Waktu pengiriman Request Mutasi" + new Date().toString();
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			func.cetakLog(m1, m2);
			setListboxValue(wnd, "mutasi", mutasi, 0, true, true, true);
		}
		
		} catch (Exception e){
			String message="Tanggal Harus di Isi";
			requestFailed(message);
		}
	}

	public void setListboxValue(Window wnd, String idList, JSONArray data, int noUrut, boolean isSelect,
			boolean isClear, boolean firstRowIsID) {
		try {
		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int iRow = data.size();
		//int lColumn = 6;
		System.out.println(iRow);
		if (iRow > 0){
			for (int j = 0; j < iRow; j++) {
				JSONObject child = (JSONObject) data.get(j);
				Listitem li = new Listitem();
				noUrut++;
				String id = String.valueOf(noUrut);
				li.setValue(String.valueOf(id));
				//for (int i = 0; i < lColumn; i++) {
					try {
					li.appendChild(new Listcell(child.get("timestamp").toString()));
					String id_tr = new String(child.get("idtrx").toString());
					li.appendChild(new Listcell(id_tr));
					li.appendChild(new Listcell(child.get("typetrx").toString()));
					String nominal = child.get("total").toString();
					if (child.get("total").toString().startsWith("-")) {
						nominal = nominal.replace("-", "");
					}
					li.appendChild(new Listcell(nominal));
					li.appendChild(new Listcell(child.get("statustrx").toString()));
					li.appendChild(new Listcell(child.get("keterangan").toString()));
					MutasiTransaksiDto rekap = new MutasiTransaksiDto();
					rekap.setTrx_id(child.get("idtrx").toString());
					rekap.setKeterangan(child.get("keterangan").toString());
					rekap.setNominal(child.get("total").toString());
					rekap.setTanggal(child.get("timestamp").toString());
					rekap.setTrx_code(child.get("statustrx").toString());
					rekap.setType(child.get("typetrx").toString());
					listmutasi.add(rekap);
					} catch(Exception ex){
						li.appendChild(new Listcell("-"));
						//ex.printStackTrace();
					}
				
				//}
				list.appendChild(li);
			}
		}
		else {
			//Messagebox.show("Tidak ada Transaksi pada interval tersebut","Peringatan", Messagebox.OK, Messagebox.INFORMATION);
//			Clients.showNotification("Tidak ada Transaksi pada interval tersebut");
			Clients.showNotification("Tidak ada Transaksi pada interval tersebut", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		} catch (Exception e) {
			log.info("Error di Info Rekening " + e.getMessage());
		}
	}
	
	public void setConstraintTglAkhir(Window wnd) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//			String dateAwal = func.getDateboxText(wnd, "datetimepicker7");
//			func.dateFormatter("yyyy-MM-dd", "yyyyMMdd", dateAwal);
//			
//			Datebox dtAkhir = (Datebox) wnd.getFellow("datetimepicker8");
//			System.out.println(""+dtAkhir);
//			dtAkhir.setConstraint("after " + dtAkhir);
			
			String dateAwal = func.getDateboxText(wnd, "datetimepicker7");
			Date dtAwal = sdf.parse(dateAwal);
			
			sdf = new SimpleDateFormat("yyyyMMdd");
			
			String strDateAwal = sdf.format(dtAwal);
			
			System.out.println("date awal : " + strDateAwal);
			
			Datebox dateBoxAkhir = (Datebox) wnd.getFellow("datetimepicker8");
			dateBoxAkhir.setConstraint("after " + strDateAwal);
		} catch(Exception e) {
			log.info("Error di Info Rekening " + e.getMessage());
		}
	}
	
//	public void cetakStruk(Window wnd) throws Exception {
//		
//		
//		for(MutasiTransaksiDto dto:listmutasi) {
//			//System.out.println(dto.getTrx_id());
//		}
//	} 
	
	public void cetakStruk(Window wnd) {
		try {
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    Listbox list = (Listbox) wnd.getFellow("mutasi"); 
	    PdfExporter exporter = new PdfExporter();
	    exporter.export(list,out);
	     
	    AMedia amedia = new AMedia("Report Mutasi.pdf", "pdf", "application/pdf", out.toByteArray());
	    Filedownload.save(amedia);   
	    out.close();
		} catch(Exception ex){
			log.info("Error di Info Rekening " + ex.getMessage());
		}
	}
	
//	public void cetakLog (String mess1,String mess2){
//		try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("D://test.log", true)))) {
//		    out.print(mess1);
////		    out.print("Waktu Yang dibutuhkan untuk cek Saldo=");
//		    //more code
//		    out.println(mess2);
//		    //more code
//		}catch (Exception e) {
//		    //exception handling left as an exercise for the reader
//			e.printStackTrace();
//		}
//	}
}
