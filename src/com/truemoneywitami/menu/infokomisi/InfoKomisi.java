package com.truemoneywitami.menu.infokomisi;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.zkoss.exporter.pdf.PdfExporter;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.deposit.DepositTicket;

public class InfoKomisi extends Window implements RequestListener{
	
	Logger log = Logger.getLogger(InfoKomisi.class);
	ZKFunction func = new ZKFunction();
	JSONObject obj;
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	String saldo="",
	ack="",
	pesan="";
	JSONArray komisi;
	long komisisend,komisiget,komisitime;

	@Override
	public void requestSucceeded(JSONObject obj) {
		komisiget  = new Date().getTime();
		ack = obj.get("ACK").toString();
		komisi = (JSONArray) obj.get("komisi");
		komisitime = komisiget- komisisend;
		String m1 = "Waktu saat Penerimaan info komisi " + new Date().toString();
		String m2 = "Waktu yang dibutuhkan untuk Cek Komisi adalah " +komisitime +" ms";
		func.cetakLog(m1, m2);
	}

	@Override
	public void requestFailed(String message) {
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
//			Clients.showNotification(message);
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
	public void setData(Window wnd) throws Exception {
		
	}
	
	public void setConstraintTglAkhir(Window wnd) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//			String dateAwal = func.getDateboxText(wnd, "datetimepicker7");
//			func.dateFormatter("yyyy-MM-dd", "yyyyMMdd", dateAwal);
//			
//			Datebox dtAkhir = (Datebox) wnd.getFellow("datetimepicker8");
//			System.out.println(""+dtAkhir);
//			dtAkhir.setConstraint("after " + dtAkhir);
			
			String dateAwal = func.getDateboxText(wnd, "datetimepicker7");
			Date dtAwal = sdf.parse(dateAwal);
			
			sdf = new SimpleDateFormat("yyyyMMdd");
			
			String strDateAwal = sdf.format(dtAwal);
			
			System.out.println("date awal : " + strDateAwal);
			
			Datebox dateBoxAkhir = (Datebox) wnd.getFellow("datetimepicker8");
			dateBoxAkhir.setConstraint("after " + strDateAwal);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void cekMutasi(Window wnd)  {
		try {
		String tgl1="",tgl2="";
		Date temptgl1;
		Date temptgl2;
		temptgl1 = func.getDateboxValue(wnd, "datetimepicker7");
		temptgl2 = func.getDateboxValue(wnd, "datetimepicker8");
		tgl1 = func.getDateboxText(wnd, "datetimepicker7");
		tgl2 = func.getDateboxText(wnd, "datetimepicker8");
		if (tgl1.equalsIgnoreCase("") || tgl2.equalsIgnoreCase("")){
			String message="Tanggal Awal dan Akhir Harus di Isi";
			requestFailed(message);
		} 
		else if (temptgl1.after(temptgl2)){
			String message="Tanggal Awal Harus Lebih Kecil Dari Tanggal Akhir";
			requestFailed(message);
		}
		else {
			JSONObject obj = new JSONObject();
			tgl1 = func.getDateboxText(wnd, "datetimepicker7");
			tgl2 = func.getDateboxText(wnd, "datetimepicker8");
			obj.put("Tipe", "RekapKomisi");
			obj.put("username", username);
			obj.put("tanggalawal", tgl1);
			obj.put("tanggalakhir", tgl2);
			obj.put("id_TipeAplikasi", 2);
			komisisend = new Date().getTime();
			String m1 = "Waktu saat Pengiriman info komisi " + new Date().toString();
			String m2 = "";
			func.cetakLog(m1, m2);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			setListboxValue(wnd, "komisi", komisi, 0, true, true, true);
		}
		
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public void setListboxValue(Window wnd, String idList, JSONArray data, int noUrut, boolean isSelect,
			boolean isClear, boolean firstRowIsID) {
		try {
		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int iRow = data.size();
		int lColumn = 5;
		if (iRow > 0){
			for (int j = 0; j < iRow; j++) {
				JSONObject child = (JSONObject) data.get(j);
				Listitem li = new Listitem();
				noUrut++;
				String id = String.valueOf(noUrut);
				li.setValue(String.valueOf(id));
				//for (int i = 0; i < lColumn; i++) {
					try {
					li.appendChild(new Listcell(child.get("timestamp").toString()));
					String id_tr = new String(child.get("id_trx").toString());
					li.appendChild(new Listcell(id_tr));
					li.appendChild(new Listcell(child.get("TipeStock").toString()));
					String nominal = child.get("nominal").toString();
					if (child.get("nominal").toString().startsWith("-")) {
						nominal = nominal.replace("-", "");
					}
					li.appendChild(new Listcell(nominal));
					li.appendChild(new Listcell(child.get("status").toString()));
					li.appendChild(new Listcell(child.get("tipetransaksi").toString()));
					} catch(Exception ex){
						li.appendChild(new Listcell("-"));
						li.appendChild(new Listcell("-"));
						
					}
				//}
				list.appendChild(li);
			}
		}
		else {
			//Messagebox.show("Tidak ada Transaksi pada interval tersebut","Peringatan", Messagebox.OK, Messagebox.INFORMATION);
//			Clients.showNotification("Tidak ada Transaksi pada interval tersebut");
			Clients.showNotification("Tidak ada Transaksi pada interval tersebut", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		} catch (Exception e) {
			log.info("Error di Info Komisi " + e.getMessage());
		}
	}
	
	public void cetakStruk(Window wnd) throws Exception {
		try {
	    ByteArrayOutputStream out = new ByteArrayOutputStream();
	    Listbox list = (Listbox) wnd.getFellow("komisi"); 
	    PdfExporter exporter = new PdfExporter();
	    exporter.export(list,out);
	     
	    AMedia amedia = new AMedia("Report Bagi Hasil.pdf", "pdf", "application/pdf", out.toByteArray());
	    Filedownload.save(amedia);   
	    out.close();
		} catch(ClassNotFoundException ex){
			log.info("Error di Info Komisi " + ex.getMessage());
		}
	}
	

}
