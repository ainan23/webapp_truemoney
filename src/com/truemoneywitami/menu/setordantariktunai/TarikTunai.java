package com.truemoneywitami.menu.setordantariktunai;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class TarikTunai extends Window implements RequestListener {
	
	private static Logger log = Logger.getLogger(TarikTunai.class);
	mob_Session sesi = new mob_Session();
	ZKFunction func = new ZKFunction();
	CheckParameter check = new CheckParameter();
	
	String noRekTujuan = "";
	String nominal = "";
	String noRekAgent = sesi.getUsername();
	Window wnd;
	Window winheader;
	public void setWindow (Window wnd) {
		this.wnd = wnd;
	}
	
	public Window getWindow () {
		return this.wnd;
	}
	
	public void setMenuWindow(Window header){
		this.winheader = header;
	}
	
	public Window getMenuWindow(){
		return this.winheader;
	}
	public void tarikTunai(Window wnd,Window header){
		setWindow(wnd);
		setMenuWindow(header);
		noRekTujuan = func.getTextboxValue(wnd, "idmember");
		nominal = func.getDecimalboxText(wnd, "nominal");
		JSONObject obj = new JSONObject();
		this.nominal = nominal;
		this.noRekTujuan = noRekTujuan;
		
		String idTrx = func.getIDTRX();
		sesi.setSessionAttribute("id_trx", idTrx);
		
		if (nominal.equals("")){
			Clients.showNotification("Silahkan isi Nominal", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (noRekTujuan.equals("")){
			Clients.showNotification("Silahkan isi ID Member", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (!check.isKosong(noRekTujuan) && !check.checkNumeric(noRekTujuan)) {
			Clients.showNotification("ID Member / No Handphone tidak valid", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} 
//		else if (Integer.parseInt(nominal) < Constants.minAmountCashout){
//			Clients.showNotification("Minimal Jumlah Tarik Tunai Rp" + func.formatNumber(Constants.minAmountCashout, "###,###"), Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
//		}
//		else if (Integer.parseInt(nominal) > Constants.maxAmountCashout){
//			Clients.showNotification("Maksimal Jumlah Tarik Tunai Rp" + func.formatNumber(Constants.maxAmountCashout, "###,###"), Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
//		}
		else {
			if (!noRekAgent.equalsIgnoreCase(noRekTujuan)) {
				try {
					long test1 = Long.parseLong(nominal);
					long test2 = Long.parseLong(noRekTujuan);
					obj.put("Tipe", "inquiryCashOutAgent");
					obj.put("nomorRekening",noRekTujuan );
					obj.put("username",noRekAgent );
					obj.put("nominal",nominal );
					ProcessingListener processing = new ProcessingListener(this, obj.toString());
					processing.processData();
				} catch (Exception e) {
					//requestFailed(Constants.generalErrorMessage);
					Clients.showNotification("ID Member atau Nominal Tidak Valid", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				}
			} else {
				requestFailed("Nomer rekening tujuan tidak boleh sama dengan nomer rekening agent");
			}
		}
	}
	
	@Override
	public void requestSucceeded(JSONObject data) {
		// TODO Auto-generated method stub
		wnd = getWindow();
		try {
			String statusAccountTujuan = data.get("statusAccountTujuan").toString();
			String statusAgentAccount = data.get("statusAgentAccount").toString();
			String namaAccountTujuan = data.get("namaAccountTujuan").toString();
			if (statusAgentAccount.equals("ACTIVE") || statusAgentAccount.equals("INACTIVE")
					|| statusAgentAccount.equals("DORMANT")) {
				if (statusAccountTujuan.equals("ACTIVE") || statusAccountTujuan.equals("INACTIVE")
						|| statusAccountTujuan.equals("DORMANT")) {
					
						mob_Session session = new mob_Session();
						session.setSessionAttribute(Constants.nominal, nominal);
						session.setSessionAttribute(Constants.namaAccountTujuan, namaAccountTujuan);
						session.setSessionAttribute(Constants.noRekeningTujuan, noRekTujuan);
						new RedirectMenu().setMenuLink("konfirmasiTarikTunai",this.winheader);
					
					
				} else if (statusAccountTujuan.equals("UNVERIFIED")) {
					requestFailed(Constants.beneficiariNewFailed);
				} else {
					requestFailed(Constants.memberAccountBlocked);
				}
			} else if (statusAgentAccount.equals("UNVERIFIED")) {
				requestFailed(Constants.senderNewFailed);
			} else {
				requestFailed(Constants.agentAccountBlocked);
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Error di Tarik Tunai." + e.getMessage());
		}
	}

	@Override
	public void requestFailed(String message) {
		// TODO Auto-generated method stub
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
	public void clearData(Window wnd) {
		try {
			func.setTextboxResetValue(wnd, "idmember");
			func.setDecimalResetValue(wnd, "nominal");
			
			func.setTextboxFocus(wnd, "idmember");
		} catch (Exception e) {
			log.info(e.getMessage());
			log.error(e);
		}
	}

}
