package com.truemoneywitami.menu.setordantariktunai;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.NullValidator;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.profile.Agen_profile;

public class SetorTunai extends Window implements RequestListener {
	
	private static Logger log = Logger.getLogger(SetorTunai.class);
	mob_Session session = new mob_Session();
	CheckParameter check = new CheckParameter();
	String noRekAgent = session.getUsername();
	String noRekTujuan = "";
	String nominal = "";
	Window wnd;
	Window winheader;
	JSONArray denom;
	ZKFunction func = new ZKFunction();
	NullValidator nullValidator = new NullValidator();
	
	public void setData(Window wnd,Window header){
		try {
		
		JSONObject obj = new JSONObject();
		obj.put("Tipe","cashinnominal");
		obj.put("ID_Account", noRekAgent);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		setListboxValueNominal(wnd,"no_sel", denom, 0, true, true, true);
		} catch (Exception e){
			log.info("Error di PLN." + e.getMessage());
		}
	}
	
	public void setListboxValueNominal(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID) throws Exception {
		
		try {
		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int a = data.size();
		
		//int lIndex = 0;
		
		for (int i =0;i<a;i++){
			Listitem li = new Listitem();
			JSONObject child  = (JSONObject) data.get(i);
			li.setValue(child.get("id_denom").toString());
			String nama = child.get("nominal").toString();
			li.appendChild(new Listcell(nama));
			list.appendChild(li);
		}
		} catch (Exception e){
			log.info("Error di PLN ." + e.getMessage());
		}
	}
	
	public void setorTunai(Window wnd,Window header) {
		setWindow(wnd);
		setMenuWindow(header);
		try {
			//noRekAgent = session.getSessionAttribute("noRekAgent");
			//System.out.println(no);
			noRekTujuan =func.getTextboxValue(wnd, "idmember");
			
//			nominal = func.getDecimalboxText(wnd, "nominal");
			try { nominal = func.getListboxLabel(wnd, "no_sel"); } catch (Exception e) { nominal = ""; }
			
			String idTrx = func.getIDTRX();
			session.setSessionAttribute("id_trx", idTrx);
			
			JSONObject obj = new JSONObject();
			if (this.noRekTujuan.equals("")) {
				Clients.showNotification("Silahkan Masukan ID Tujuan", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} 
			else if (this.nominal.equals("")) {
				Clients.showNotification("Silahkan Pilih Nominal", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else if (!check.isKosong(noRekTujuan) && !check.checkNumeric(noRekTujuan)) {
				Clients.showNotification("ID Member / No Handphone tidak valid", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} 
			
//			else if (Integer.parseInt(nominal) < Constants.minamount){
//				Clients.showNotification("Minimal Setor Tunai Rp "+Constants.minamount);
//			} 
//			else if (Integer.parseInt(nominal) > Constants.maxamount){
//				Clients.showNotification("Maksimal Setor Tunai Rp "+Constants.maxamount);
//			} 
			
			else {
				long test = Long.parseLong(nominal);
				long test2 = Long.parseLong(noRekTujuan);
				if (!noRekAgent.equalsIgnoreCase(noRekTujuan)) {
						obj.put("Tipe", "inquiryCashInAgent");
						obj.put("nomorRekening", noRekTujuan);
						obj.put("username", noRekAgent);
						ProcessingListener processing = new ProcessingListener(this, obj.toString());
						processing.processData();

				} else {
					requestFailed("Nomer rekening salah. Silakan masukkan nomor rekening Member.");
				}
			}
			
		} catch (Exception e) {
			log.info("Error di Setor Tunai." + e.getMessage(), e);
			Clients.showNotification("ID Tujuan atau Nominal tidak Valid", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			//requestFailed(Constants.generalErrorMessage);
		}
	}

	@Override
	public void requestSucceeded(JSONObject data) {
		// TODO Auto-generated method stub
		wnd = getWindow();
		winheader  = getMenuWindow();
		try {
			if (data.get("ACK").toString().equals("OK") && data.get("ID").toString().equals("cashinnominal")){
				denom = (JSONArray) data.get("denom");
			} else {
				String statusAccountTujuan = data.get("statusAccountTujuan").toString();
				String statusAgentAccount = data.get("statusAgentAccount").toString();
				String namaAccountTujuan = data.get("namaAccountTujuan").toString();
				if (statusAgentAccount.equals("ACTIVE") || statusAgentAccount.equals("INACTIVE")
						|| statusAgentAccount.equals("DORMANT")) {
					if (statusAccountTujuan.equals("ACTIVE") || statusAccountTujuan.equals("INACTIVE")
							|| statusAccountTujuan.equals("DORMANT")) {
						session.setSessionAttribute(Constants.nominal, nominal);
						session.setSessionAttribute(Constants.namaAccountTujuan, namaAccountTujuan);
						session.setSessionAttribute(Constants.noRekeningTujuan, noRekTujuan);
						//session.setSessionAttribute(Constants.id_member, sessionValue);
						new RedirectMenu().setMenuLink("konfirmasiSetorTunai",winheader);
						
					} else if (statusAccountTujuan.equals("UNVERIFIED")) {
						requestFailed(Constants.beneficiariNewFailed);
					} else {
						requestFailed(Constants.memberAccountBlocked);
					}
				} else if (statusAgentAccount.equals("UNVERIFIED")) {
					requestFailed(Constants.senderNewFailed);
				} else {
					requestFailed(Constants.agentAccountBlocked);
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Error di Setor Tunai." + e.getMessage(), e);
			//requestFailed(Constants.generalErrorMessage);
		}
	}

	@Override
	public void requestFailed(String message) {
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
	public void clearData(Window wnd) {
		try {
			func.setTextboxResetValue(wnd, "idmember");
			func.setDecimalResetValue(wnd, "nominal");
			
			func.setTextboxFocus(wnd, "idmember");
		} catch (Exception e) {
			log.info(e.getMessage());
			log.error(e);
		}
	}
	
	public void setWindow(Window wnd){
		this.wnd = wnd;
	}
	
	public Window getWindow(){
		return this.wnd;
	}

	public void setMenuWindow(Window header){
		this.winheader = header;
	}
	
	public Window getMenuWindow(){
		return this.winheader;
	}
}
