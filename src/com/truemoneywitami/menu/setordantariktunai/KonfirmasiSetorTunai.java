package com.truemoneywitami.menu.setordantariktunai;
	import java.util.Date;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.ProcessingListenerPrabayar;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class KonfirmasiSetorTunai extends Window implements RequestListener {

	Logger log = Logger.getLogger(KonfirmasiSetorTunai.class);
	public String nmAccount = "";
	//public String noRek = "";
	public String amount = "";
	mob_Session session = new mob_Session();
	ZKFunction func = new ZKFunction();
	String username = session.getUsername();
	String norek="",
	nama="",
	nominal="";
	String ack="",
	pesan="";
	String block="";
	String send_trx="";
	Window wnd;
	Window winheader;
	String id_trx="";
	int count=0;
	
	long cashsent,cashget,cashtime;
	
	public void setWindow(Window wnd) {
		this.wnd = wnd;
	}

	public Window getWindow(){
		return this.wnd;
	}
	
	public Window setMenuWindow(Window header){
		return this.winheader = header;
	}
	
	public void setViewData(Window wnd){
		setWindow(wnd);
		
		try {
			norek = session.getSessionAttribute(Constants.noRekeningTujuan);
			nama = session.getSessionAttribute(Constants.namaAccountTujuan);
			nominal = session.getSessionAttribute(Constants.nominal);
			func.setLabelValue(wnd, "idmember", norek );
			func.setLabelValue(wnd, "namamember", nama);
			func.setLabelValue(wnd, "nominal", func.formatRp(nominal));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.info("Error di Setor Tunai" + e.getMessage());
		}
	}

	public void cashIn(String pin,Window header){
		wnd = getWindow();
		setMenuWindow(header);
		try {
			//String pin = func.getTextboxValue(wnd, "pin");
//			send_trx = func.getIDTRX();
			send_trx = session.getSessionAttribute("id_trx");
			JSONObject obj = new JSONObject();
			obj.put("Tipe", "CashInAgent");
			obj.put("username", username);
			obj.put("PIN",pin );
			obj.put("nomorRekening", norek);
			obj.put("nominal", nominal);
			obj.put("id_TipeAplikasi", 2);
			obj.put("id_trx", send_trx);
			cashsent = new Date().getTime();
			String m1 = "Waktu saat Penerimaan  Cashin " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk cashin " +cashtime +" ms";
			func.cetakLog(m1, m2);
			ProcessingListenerPrabayar processing = new ProcessingListenerPrabayar(this, obj.toString());
			processing.processData();
			//System.out.println(ack);
			if (ack.equalsIgnoreCase("OK")) {
				session.setSessionAttribute(Constants.tipePesan,"setorTunai");
				session.setSessionAttribute(Constants.header, "Setor Tunai");
				session.setSessionAttribute(Constants.pesan,"Transaksi Setor Tunai Berhasil");
				session.setSessionAttribute(Constants.id_trx,id_trx);
				session.setSessionAttribute(Constants.biaya, "0");
				session.setSessionAttribute(Constants.status, "Berhasil");
				//session.setSessionAttribute(Constants.id_member, sessionValue);
				new RedirectMenu().setMenuLink("cashsukses",header);
			}
			else {
//				String message = Constants.generalErrorMessage;
//				Clients.showNotification(message);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			log.info("Error di Setor Tunai." + e.getMessage());
		}
	}
	
	@Override
	public void requestFailed(String message)  {
		// TODO Auto-generated method stub
		wnd = getWindow();
		try {
			cashget = new Date().getTime();
			cashtime = cashget - cashsent;
			String m1 = "Waktu saat Penerimaan  Cashout " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk Cashout " +cashtime +" ms";
			func.cetakLog(m1, m2);
			if(message.equals("01")) {
				Executions.sendRedirect("/timeout.zul");
			} else if(message.equals("TIMEOUT")) {
				ack = "NOK";
				Clients.showNotification(Constants.MSG_TIMEOUT_ERROR, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else {
				if (message.contains("PIN")) {
					Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
					count++;
					if (count >3){
						JSONObject obj = new JSONObject();
						obj.put("Tipe", "BlockAccount");
						obj.put("username", username);
						ProcessingListener processing = new ProcessingListener(this, obj.toString());
						processing.processData();
						if (block.equals("OK")){
							Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
							//func.setTextboxDisabled(wnd, "pin", true);
						}
						else {
							System.out.println("Akun Gagal di Blokir");
						}
					}
				}
				else if (message.contains("blokir") && count ==3){
					count++;
					Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
					func.setTextboxDisabled(wnd, "pin", true);
				}
					else {
					session.setSessionAttribute(Constants.tipePesan,"setorTunai");
					session.setSessionAttribute(Constants.pesan,message);
					new RedirectMenu().setMenuLink("transfergagal",this.winheader);
				}
			}
		
		} catch (Exception e) {
			
		}
	}

	@Override
	public void requestSucceeded(JSONObject data) {
		//JSONObject obj = new JSONObject();
		// TODO Auto-generated method stub
		if (data.get("ACK").equals("OK") && data.get("ID").equals("Block")){
			block = data.get("ACK").toString();
		}else {
			cashget = new Date().getTime();
			cashtime = cashget - cashsent;
			String m1 = "Waktu saat Penerimaan  pembelian Pulsa " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk Cash IN " +cashtime +" ms";
			func.cetakLog(m1, m2);
			ack = data.get("ACK").toString();
			pesan = data.get("pesan").toString();
			id_trx = data.get("id_Trx").toString();
		}
		
		
	}

}


