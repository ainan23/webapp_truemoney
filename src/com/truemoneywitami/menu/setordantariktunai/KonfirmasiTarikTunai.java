package com.truemoneywitami.menu.setordantariktunai;

import java.util.Date;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.ProcessingListenerPrabayar;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class KonfirmasiTarikTunai extends Window implements RequestListener{

	Logger log = Logger.getLogger(KonfirmasiTarikTunai.class);
	public String nmAccount = "";
	//public String noRek = "";
	public String amount = "";
	mob_Session session = new mob_Session();
	ZKFunction func = new ZKFunction(); 
	String username = session.getUsername();
	String block ="";
	String norek,namaAccount,nominal,ack="",pesan;
	String otpStatus;
	String id_Trx="";
	int biaya;int count=0;
	JSONArray arr;
	String send_trx = "";
	Window wnd;
	Window winheader;
	
	private String ackBiaya;
	
	long cashsent,cashget,cashtime;
//	public KonfirmasiTarikTunai() {
//		
//	}

	
	public void setWindow(Window wnd) {
		this.wnd = wnd;
	}
	
	public Window getWindow (Window wnd) {
		return wnd;
	}
	
	public void setMenuWindow(Window header){
		this.winheader = header;
	}
	
	public Window getMenuWindow(){
		return this.winheader;
	}
	
	public void setViewData(Window wnd) throws Exception{
		setWindow(wnd);
		
		try {
			norek = session.getSessionAttribute(Constants.noRekeningTujuan);
			namaAccount = session.getSessionAttribute(Constants.namaAccountTujuan);
			nominal = session.getSessionAttribute(Constants.nominal);
			func.setLabelValue(wnd, "idmember",  norek);
			func.setLabelValue(wnd, "namamember",namaAccount );
			func.setLabelValue(wnd, "nominal",func.formatRp(nominal) );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.info("Error di Tarik Tunai." + e.getMessage());
		}
		JSONObject obj = new JSONObject();
		obj.put("Tipe","requestBiayaAdmin");
		obj.put("username", username);
		obj.put("nominal",nominal);
		obj.put("id_TipeTransaksi", 3);
		obj.put("id_TipeAplikasi", 2);
		obj.put("username", username);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		if (ackBiaya.equals("OK")) {
			biaya = (int) arr.get(0);
			func.setLabelValue(wnd, "admin", func.formatRp(biaya));
		}
		
	}
	
	public void doTarikTunai(String otp,Window header){
		wnd = getWindow(wnd);
		setMenuWindow(header);
		try {
			JSONObject obj = new JSONObject();
//			send_trx = func.getIDTRX();
			send_trx = session.getSessionAttribute("id_trx");
			obj.put("Tipe","checkToken");
			obj.put("username", username);
			obj.put("RequestOTPMember",otp);
			obj.put("id_TipeTransaksi",3);
			obj.put("nomorRekening", norek);
			cashsent = new Date().getTime();
			String m1 = "Waktu saat Pengiriman  Cashout " + new Date().toString();
			String m2 = "";
			func.cetakLog(m1, m2);
			ProcessingListener  processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			
			if (otpStatus.equalsIgnoreCase("OK")){ // Check OTP Status
				obj = new JSONObject();
				obj.put("Tipe", "CashOutAgent");
				obj.put("username", username);
				obj.put("nomorRekening", norek);
				obj.put("biayaCashOut", biaya);
				obj.put("verifikasi", "OTP");
				obj.put("nominal", nominal);
				obj.put("id_trx", send_trx);
				ProcessingListenerPrabayar processsing = new ProcessingListenerPrabayar(this, obj.toString());
				processsing.processData();
				System.out.println(ack);
				if (ack.equalsIgnoreCase("OK")){ // Check Cashout Status
					//session.setSessionAttribute(Constants.tipePesan,"tarikTunai");
					//session.setSessionAttribute(Constants.pesan,"Dana senilai Rp. "+ String.valueOf(nominal) +"berhasil ditarik.");
					//session.setSessionAttribute(Constants.tipePesan,"cashsukses");
					session.setSessionAttribute(Constants.header, "Tarik Tunai");
					session.setSessionAttribute(Constants.pesan,"Transaksi Tarik Tunai Berhasil");
					session.setSessionAttribute(Constants.id_trx,id_Trx);
					session.setSessionAttribute(Constants.biaya, String.valueOf(biaya));
					session.setSessionAttribute(Constants.status, "Berhasil");
					RedirectMenu menu = new RedirectMenu();
					menu.setMenuLink("cashsukses",header);
				}
				else {
					if (pesan.equals("TIMEOUT")) {
						
					} else {
						session.setSessionAttribute(Constants.tipePesan,"transfergagal");
						session.setSessionAttribute(Constants.pesan,Constants.generalErrorMessage);
						RedirectMenu menu = new RedirectMenu();
						menu.setMenuLink("transaksiGagal",header);
					}
					
				}
			}
			else {
				RedirectMenu menu = new RedirectMenu();
				menu.setMenuLink("transaksiGagal",header);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Error di Tarik Tunai." + e.getMessage());
			e.printStackTrace();
		}
	}
	
	@Override
	public void requestSucceeded(JSONObject obj) {
		// TODO Auto-generated method stub
		if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("biaya") ){
			arr = (JSONArray) obj.get("biayaAdmin");
			ackBiaya = "OK";
		}
		else if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("otp")){
			otpStatus = obj.get("ACK").toString();
		}
		else if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("cashout")) {
			cashget = new Date().getTime();
			cashtime = cashget - cashsent;
			String m1 = "Waktu saat Penerimaan  Cashout " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk Cashout " +cashtime +" ms";
			func.cetakLog(m1, m2);
			ack = obj.get("ACK").toString();
			id_Trx = obj.get("id_Trx").toString();
		}
		else if (obj.get("ACK").equals("OK") && obj.get("ID").equals("Block")){
			block = obj.get("ACK").toString();
		}
	}

	@Override
	public void requestFailed(String message) {
		ackBiaya = "NOK";
		cashget = new Date().getTime();
		cashtime = cashget - cashsent;
		String m1 = "Waktu saat Penerimaan  Cashout " + new Date().toString();
		String m2 = "Waktu yang dibutuhkan untuk Cashout " +cashtime +" ms";
		func.cetakLog(m1, m2);
		// TODO Auto-generated method stub
		wnd = getWindow(wnd);
		try {
			if(message.equals("01")) {
				Executions.sendRedirect("/timeout.zul");
			} else if(message.equals("TIMEOUT")) {
				ack = "NOK";
				pesan = message;
				Clients.showNotification(Constants.MSG_TIMEOUT_ERROR, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else {
				if (message.startsWith("PIN")) {
					Clients.showNotification("PIN Anda Salah. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
					count++;
					if (count >3){
						JSONObject obj = new JSONObject();
						obj.put("Tipe", "BlockAccount");
						obj.put("username", username);
						ProcessingListener processing = new ProcessingListener(this, obj.toString());
						processing.processData();
						if (block.equals("OK")){
							Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
							//func.setTextboxDisabled(wnd, "pin", true);
						}
						else {
							System.out.println("Akun Gagal di Blokir");
						}
						
					}
					else {
						System.out.println(count);
					}
					} else {
						otpStatus = "NOK";
						session.setSessionAttribute(Constants.tipePesan,"tarikTunai");
						session.setSessionAttribute(Constants.pesan,message);
						System.out.println("<<<KonfirmasiTarikTunai.java>>message>asdf>>"+message);
						RedirectMenu menu = new RedirectMenu();
						menu.setMenuLink("transaksiGagal",this.winheader);
					}
				pesan = message;
			}
		
		} catch (Exception e){
			
		}
		}
	


}
