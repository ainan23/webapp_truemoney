package com.truemoneywitami.menu;

import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Font;
import org.zkoss.zhtml.H4;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class changepassword extends Window implements RequestListener {
	ZKFunction func = new ZKFunction();
	CheckParameter check = new CheckParameter();
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	String typenya = (String) (Sessions.getCurrent()).getAttribute("tipe");
	String getData;
	Window wnd;
	Window winheader;
	public void simpen(Window changepasswordzul,Window header) {
		setWindow(changepasswordzul);
		setMenuWindow(header);
		try {
			String passLama = func.getTextboxValue(changepasswordzul, "txtPassword"); 
			String passBaru = func.getTextboxValue(changepasswordzul, "txtNewPassword");
			String passReBaru = func.getTextboxValue(changepasswordzul, "txtReNewPassword");

			if (passLama.equals("")){
				Clients.showNotification("Silahkan masukan password lama anda", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else if (passBaru.equals("") || passReBaru.equals("")) {
				Clients.showNotification("Silahkan masukan password baru anda", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else if (!check.isKosong(passBaru) && passBaru.length() < 6) {
				Clients.showNotification("Panjang Password Minimal adalah 6 karakter", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else if (!check.isKosong(passBaru) && passBaru.length() > 16) {
				Clients.showNotification("Panjang Password Maximal adalah 16 karakter", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else if (!check.isKosong(passBaru) && (!check.checkNumeric(passBaru) || !check.checkAlpha(passBaru))) {
				Clients.showNotification("Password harus terdiri dari huruf dan angka", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else {
				if (!passBaru.equals(passReBaru)) {
				Clients.showNotification("Password baru anda tidak sama. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				} else {
//					if (!passLama.equals(passBaru)) {
						//System.out.println(passLama + "ini pass lama");

						JSONObject obj = new JSONObject();
						obj.put("Tipe", "ChangePassword");
						if (typenya.equalsIgnoreCase("Agent")) {
							obj.put("tipeUser", "Agent");
							obj.put("username", username);
						} else {
							obj.put("tipeUser", "Member");
							obj.put("username", username);
						}
						obj.put("passwordLama", passLama);
						obj.put("passwordBaru", passBaru);

						System.out.println(String.valueOf(obj));

						ProcessingListener processing = new ProcessingListener(this, obj.toString());
						processing.processData();
//					} else {
//						requestFailed("Password lama dan baru yang anda masukkan sama");
//					}

				}
			}
			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
			e.printStackTrace();
		}

	}

	public void setWindow(Window wnd) {
		this.wnd = wnd;
	}
	
	public Window getWindow() {
		return this.wnd;
	}
	
	public void batal(Window wnd) {
		System.out.println("Batal");
		try {
			func.setTextboxResetValue(wnd, "txtPassword");
			func.setTextboxResetValue(wnd, "txtNewPassword");
			func.setTextboxResetValue(wnd, "txtReNewPassword");
		} catch (Exception e) {
			e.getMessage();
		}
	}
	
	public void setViewData(Window wnd){
		Label labelUsername = (Label) wnd.getFellow("labelUsername");
		labelUsername.setValue(username);
	}
	

	@Override
	public void requestSucceeded(JSONObject data) {
		getWindow();
		String pesan = (String) data.get("pesan");
		Sessions.getCurrent().setAttribute(Constants.pesan, pesan);
		Clients.showNotification(pesan, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		
		RedirectMenu menu = new RedirectMenu();
		new RedirectMenu().setMenuLink("inforekeningmember",this.winheader);
	}

	@Override
	public void requestFailed(String message) {
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		
	}
	
	public void setMenuWindow(Window header){
		this.winheader = header;
	}
}

