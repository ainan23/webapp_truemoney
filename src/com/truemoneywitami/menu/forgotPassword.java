package com.truemoneywitami.menu;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Font;
import org.zkoss.zhtml.H4;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class forgotPassword extends Window implements RequestListener {
	ZKFunction func = new ZKFunction();
	private Pattern pattern;
	private Matcher matcher;
	Pattern pat = Pattern.compile(".*[0-9].*");
	public void getPw(Window wnd) {
		try {
			//String i_email = func.getTextboxValue(wnd, "inputEmail");
			String i_nomorHP = func.getTextboxValue(wnd, "nomorhp");
			String i_tglLahir = func.getDateboxText(wnd, "datetimepicker1");
			String i_tipeUser = func.getTextboxValue(wnd, "tipeUser");
			i_tipeUser = i_tipeUser.toLowerCase();			
			//boolean cekEmail = cekValidasiEmail(i_email);
			
//			if (i_email.equals("")) {
//				requestFailed("Silahkan masukan email anda");
//			} 
			 if ( i_nomorHP.equals("") ){
				requestFailed("Silahkan Masukan no. HP anda");
			}
			else if ( i_tglLahir.equals("")){
				requestFailed("Silahkan Masukan Tanggal Lahir Anda");
			}
			else if (i_tipeUser.equals("")){
				requestFailed("Silahkan pilih jenis user anda");
			}
//			else if (!cekEmail) {
//				requestFailed("Format Email tidak Valid");
//			}
			else if (!pat.matcher(i_nomorHP).matches()) {
				requestFailed("Format No HP tidak Valid");
			}
			else {
				JSONObject obj = new JSONObject();
				obj.put("Tipe", "ForgotPassword");
				obj.put("tipeUser", i_tipeUser);
				//obj.put("email", i_email);
				obj.put("noHp", i_nomorHP);
				obj.put("tglLahir", i_tglLahir);
				ProcessingListener processing = new ProcessingListener(this, obj.toString());
				processing.processData();
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
			e.printStackTrace();
		}
	}

	
	public boolean cekValidasiEmail(String i_email) {
		// TODO Auto-generated method stub
		final String EMAIL_PATTERN = 
				"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(i_email);
		return matcher.matches();
	}


	@Override
	public void requestSucceeded(JSONObject data) {
		String pesan = (String) data.get("pesan");
		Sessions.getCurrent().setAttribute("pesan", pesan);
		
//		Messagebox.show(pesan, "Reset Password Berhasil", Messagebox.OK, Messagebox.INFORMATION, new org.zkoss.zk.ui.event.EventListener() {
//		    public void onEvent(Event evt) throws InterruptedException {
//		        if (evt.getName().equals("onOK")) {
//		        	Executions.sendRedirect("/login.zul");
//		        } 
//		    }
//		});
		Executions.sendRedirect("/login.zul");
	}

	@Override
	public void requestFailed(String message) {
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
}
