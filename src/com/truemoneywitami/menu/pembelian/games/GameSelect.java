package com.truemoneywitami.menu.pembelian.games;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.pembelian.pulsa.PulsaSelect;

public class GameSelect extends Window implements RequestListener {
	
	private static Logger log = Logger.getLogger(PulsaSelect.class);
	
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	JSONObject obj;
	String pesan,ack,id_Operator,namaOperator,amount,hargaCetak,biayaAdmin,produk,namaProduk;
	JSONArray norek,denom;
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	String tipeUser="";
	//String username = "0852235455";
	String idt="";
	String checked="NOK";
	Window winheader;
	
	@Override
	public void requestSucceeded(JSONObject obj) {
		
		String id = "";
		try {
			id = obj.get("ID").toString();
		} catch (Exception e) {
			id = "";
		}
		
		if (obj.get("ACK").toString().equals("OK") && id.equals("data") ){
			ack = obj.get("ACK").toString();
			norek = (JSONArray) obj.get("daftar");
		}
		else if (obj.get("ACK").toString().equals("OK") && id.equals("biaya")){
			denom = (JSONArray) obj.get("nominal");
			id_Operator= obj.get("idOperator").toString();
			namaOperator = obj.get("namaOperator").toString();
//			amount = obj.get("").toString();
//			hargaCetak = obj.get("").toString();
//			biayaAdmin = obj.get("").toString();
		}
		else if (obj.get("ACK").toString().equals("OK") && id.equals("produkgames")){
			ack = obj.get("ACK").toString();
			norek = (JSONArray) obj.get("daftar");
		}
		else{
			pesan = obj.get("pesan").toString();
			requestFailed(pesan);
		}
	}
	
	@Override
	public void requestFailed(String message){
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
	
	public void setMenuWindow(Window header){
		this.winheader = header;
	}
	
	public void setData(Window wnd,Window header) throws Exception {
		setMenuWindow(header);
		try {
		if (username.startsWith("0")){
			tipeUser = "member";
		}	else {
			tipeUser="Agent";
		}
		JSONObject obj = new JSONObject();
		obj.put("Tipe","peldata");
		obj.put("ID_Account", username);
		obj.put("id_TipeTransaksi", 19);
		obj.put("tipeUser",tipeUser);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		setListboxValueRek(wnd,"pel_sel", norek, 0, true, true, true);
		} catch (Exception e) {
			log.info("Error di Pulsa." + e.getMessage());
		}
		
		/**
		 * get produk games
		 */
		try {
			JSONObject obj = new JSONObject();
			obj.put("Tipe","getprodukgames");
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			setListboxValueProduk(wnd,"lstProdukGames", norek, 0, true, true, true);
		} catch (Exception e) {
			log.error("Error di Pulsa.", e);
		}
	}
	
	
	public void getNominal(Window wnd) throws Exception {
		try {
//		Checkbox ck = (Checkbox) wnd.getFellow("save");
//		if (ck.isChecked()){
//			checked = "OK";
//		}
//		else {
//			checked = "NOK";
//		}
		
		Radio rd = (Radio) wnd.getFellow("radio2");
		if (rd.isSelected()){
			idt = func.getTextboxValue(wnd, "idpel");
		}
		else {
			setPelanggan(wnd);
		}
		
		if (idt.equals("")) {
			Listbox list = (Listbox) wnd.getFellow("no_sel");
			try { list.getItems().clear(); } catch(Exception e){}
		} else {
			obj = new JSONObject();
			if(username.startsWith("0")){
				obj.put("Tipe","requestPulsaMember");
			}
			else {
				obj.put("Tipe","requestPulsaAgent");
			}
			obj.put("username", username);
			obj.put("no_HP", idt);
			System.out.println("Nilai Check sebelum dikirim -->" +checked);
			obj.put("save", checked);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			setListboxValueNominal(wnd,"no_sel", denom, 0, true, true, true);
		}

		
		} catch (Exception e) {
			Listbox list = (Listbox) wnd.getFellow("no_sel");
			list.getItems().clear();
			log.info("Error di Pulsa." + e.getMessage());
			Clients.showNotification("No HP tidak terdaftar. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			e.printStackTrace();
		}
	}
	
	public void setPelanggan(Window wnd) throws Exception {
		try {
			idt = func.getListboxValue(wnd, "pel_sel");
			} catch (Exception ex){
				//Messagebox.show("No HP tidak boleh kosong Silahkan coba kembali","Peringatan", Messagebox.OK, Messagebox.ERROR);
				idt = "";
				log.info("Error di Pulsa." + ex.getMessage());	
			}
	}
	
	public void setListboxValueRek(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID) throws Exception {
		try {
		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int a = data.size();
		//int lIndex = 0;
		
		Listitem liNone = new Listitem();
		liNone.setValue("NONE");
		liNone.appendChild(new Listcell("--- Pilih No HP Tujuan ---"));
		list.appendChild(liNone);
		
		for (int i =0;i<a;i++){
			Listitem li = new Listitem();
			JSONObject child  = (JSONObject) data.get(i);
			li.setValue(child.get("data").toString());
			String nama = child.get("data").toString();
			li.appendChild(new Listcell(nama));
			list.appendChild(li);
		}
		
		list.setSelectedItem(liNone);
		} catch (Exception e){
			log.info("Error di Pulsa." + e.getMessage());
		}
	}
	
	public void setListboxValueProduk(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID) throws Exception {
		try {
		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int a = data.size();
		//int lIndex = 0;
		
		Listitem liNone = new Listitem();
		liNone.setValue("NONE");
		liNone.appendChild(new Listcell("--- Pilih Produk Games ---"));
		list.appendChild(liNone);
		
		for (int i =0;i<a;i++){
			Listitem li = new Listitem();
			JSONObject child  = (JSONObject) data.get(i);
			li.setValue(child.get("id_operator").toString());
			String nama = child.get("nama_operator").toString();
			li.appendChild(new Listcell(nama));
			list.appendChild(li);
		}
		
		list.setSelectedItem(liNone);
		} catch (Exception e){
			log.info("Error di Pulsa." + e.getMessage());
		}
	}
	
//	public void setNominal(Window wnd) {
//		try {
//		nominal = func.getListboxValue(wnd, "no_sel");
//		System.out.println("nominal nya --->" +nominal);
////		String  index =  nominal.charAt(nominal.length() -1);
//		String split[] = nominal.split("##");
//		
//		//biayaAdmin = sesi.getSessionAttribute("biaya" +index);
//		hargaCetak = sesi.getSessionAttribute("harga" +split[1]);
//		System.out.println("Harganya---->>>" +hargaCetak);
//		nominal = split[0];
//		} catch (Exception e) {
//			log.info("Error di Pulsa." + e.getMessage());
//		}
//	}
	
	public void setListboxValueNominal(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID) throws Exception {
		
		try {
		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int a = data.size();
		
		//int lIndex = 0;
		
		for (int i =0;i<a;i++){
			Listitem li = new Listitem();
			JSONObject child  = (JSONObject) data.get(i);
			String index = String.valueOf(i);
			String value= child.get("nominal" +i ).toString();
			li.setValue(value +"##"+i);
			//sesi.setSessionAttribute("biaya" + i, child.get("biayaAdmin" +i ).toString());
			sesi.setSessionAttribute("harga" + i, child.get("hargaCetak" +i ).toString());
			String nama = child.get("nominal" +i).toString();
			li.appendChild(new Listcell(nama));
			list.appendChild(li);	
		}
		} catch (Exception e){
			log.info("Error di Pulsa." + e.getMessage());
		}
	}
	
	public void next(Window wnd,Window header)  {
		try {
			
			Checkbox ck = (Checkbox) wnd.getFellow("save");
			if (ck.isChecked()){
				checked = "OK";
			}
			else {
				checked = "NOK";
			}
			
			Radio rd = (Radio) wnd.getFellow("radio2");
			if (rd.isSelected()){
				try {
					idt = func.getTextboxValue(wnd, "idpel");
					produk = func.getListboxValue(wnd, "lstProdukGames");
					namaProduk = func.getListboxLabel(wnd, "lstProdukGames");
				} catch(Exception e) {
					idt = "";
					produk = "NONE";
					namaProduk = "";
				}
				
			}
			else {
				try {
					idt = func.getListboxValue(wnd, "pel_sel");
				} catch(Exception e) {
					idt = "";
				}
				
			}
			
//			try {
////				nominal = func.getListboxValue(wnd, "no_sel");
//				produk = func.getListboxValue(wnd, "lstProdukGames");
//				System.out.println("produk sebelum di kirim ke session -->" +produk);
////				char index =  nominal.charAt(nominal.length() -1);
////				String split[] = nominal.split("##");
//				//biayaAdmin = sesi.getSessionAttribute("biaya" +index);
////				hargaCetak = sesi.getSessionAttribute("harga" +split[1]);
////				nominal = split[0];
//			} catch(Exception e) {
//				produk = "";
//			}
			
		if (idt.equals("") ){
//			Messagebox.show("NO HP atau Nominal Tidak Boleh Kosong","Error", Messagebox.OK, Messagebox.ERROR);
			Clients.showNotification("No HP tidak boleh kosong. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (idt.length() < 9 || idt.length() > 14){
			Clients.showNotification("No HP tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (produk.equals("NONE")){
			Clients.showNotification("Silahkan Pilih Produk Games.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else {
			try {
			long test = Long.parseLong(idt);
			sesi.setSessionAttribute("produk", produk);
			sesi.setSessionAttribute("nama_produk", produk);
			sesi.setSessionAttribute("no_hp", idt);
			sesi.setSessionAttribute("save", checked);
			RedirectMenu menu = new RedirectMenu();
			new RedirectMenu().setMenuLink("konfirmasipembelianpulsa",header);
			} catch (Exception e){
				Clients.showNotification("No HP tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
		}
		} catch (Exception e) {
			log.info("Error di Pulsa." + e.getMessage());
		}
	}
	
}
