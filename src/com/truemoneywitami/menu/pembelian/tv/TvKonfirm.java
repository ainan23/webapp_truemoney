package com.truemoneywitami.menu.pembelian.tv;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.ProcessingListenerPrabayar;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.transfer.TrueKon;

public class TvKonfirm extends Window  implements RequestListener{

	Logger log = Logger.getLogger(TvKonfirm.class);
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	CheckParameter check = new CheckParameter();
	JSONObject obj;
	String pesan,ack="";
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	//String username = "0852235455";
	String idt="",save="",idOperator="",denom="",HP="",status="",id_trx="",token="",nomorHandphone="";
	String biayacetak="",hargaCetak="",amount="";
	Window win;int count =0;
	String block="";
	String send_trx = "";
	Window winheader;
	String namaPelanggan = "";
	String biayaAdmin = "";
	String jumlahBayar = "";
	String daya = "";
	String lwbp = "";
	String maxKwh = "";
	String idTrx = "";
	String idPelanggan = "";
	String idDenom = "";
	String ref = "";
	String noMeter = "";
	String nominal = "";
	String ppn = "";
	String ppj = "";
	String admin = "";
	String materai = "";
	String angsuran = "";
	String stroom = "";
	String jumlahKwh = "";
	String timeStamp = "";
	String ackInq = "NOK";
	String strBeliToken = "";
	String statusPromo = "0";
	String nilai = "0";
	
	private JSONArray promo = new JSONArray();
	
	long paidsend,paidget,paidtime;
	
	@Override
	public void requestSucceeded(JSONObject obj) {	 
		if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("biaya") ){
			biayacetak = obj.get("biayaAdmin").toString();
			hargaCetak = obj.get("hargacetak").toString();
			amount = obj.get("nominal").toString();
		}
		else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("inquiry") ){
			namaPelanggan = obj.get("nama").toString();
			biayaAdmin = obj.get("biayaAdmin").toString();
			String nominal = obj.get("nominal").toString();
			String tipe = "";
			try {
				tipe = sesi.getSessionAttribute("tipe").toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(tipe.toLowerCase().equalsIgnoreCase("member")) {
				jumlahBayar = obj.get("harga_cetak_member").toString();
			} else {
				jumlahBayar = obj.get("harga_cetak").toString();
			}
			
			daya = obj.get("daya").toString();
			lwbp = obj.get("lwbp").toString();
			maxKwh = obj.get("maxKwh").toString();
			idTrx = obj.get("id_trx").toString();
			idPelanggan = obj.get("id_pelanggan").toString();
			amount = obj.get("nominal").toString();
			biayaAdmin = obj.get("biayaAdmin").toString();
//			jumlahBayar = obj.get("jumlahBayar").toString();
			noMeter = obj.get("no_meter").toString();
			ackInq = obj.get("ACK").toString();
			
			promo = (JSONArray) obj.get("promo");
			statusPromo = promo.get(0).toString();
			nilai = promo.get(1).toString();
			
			int admin = Integer.parseInt(jumlahBayar) - Integer.parseInt(nominal);
			biayaAdmin = String.valueOf(admin);
			
		}
		else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("payment")){
			paidget = new Date().getTime();
			paidtime = paidget - paidsend;
			String m1 = "Waktu saat Penerimaan  pembelian PLN " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk pembelian PLN " +paidtime +" ms";
			func.cetakLog(m1, m2);
			pesan = obj.get("pesan").toString();
//			idOperator = obj.get("id_Operator").toString();
//			denom = obj.get("denom").toString();
			status = obj.get("status").toString();			
			HP = obj.get("id_pelanggan").toString();
			token = obj.get("token").toString();
			ack = obj.get("ACK").toString();
			id_trx = obj.get("id_Transaksi").toString();			
			ref = obj.get("ref").toString();
			noMeter = obj.get("no_meter").toString();
			nominal = obj.get("nominal").toString();
			admin = obj.get("admin").toString();
			materai = obj.get("materai").toString();
			ppn = obj.get("ppn").toString();
			ppj = obj.get("ppj").toString();
			angsuran = obj.get("angsuran").toString();
			stroom = obj.get("stroom").toString();
			jumlahKwh = obj.get("kwh").toString();
			hargaCetak = obj.get("total").toString();
			idPelanggan = obj.get("id_pelanggan").toString();
			timeStamp = obj.get("timestamp").toString();
			
		}
		else if (obj.get("ACK").equals("OK") && obj.get("ID").equals("Block")){
			block = obj.get("ACK").toString();
		}
	}
		
	public void setMenuWindow(Window header){
		this.winheader = header;
	}
	
	public Window getMenuWindow(){
		return this.winheader;
	}
	
	@Override
	public void requestFailed(String message){
		ackInq = "NOK";
			try {
				Window wnd = getWindow();
				if (message.contains("PIN")) {
					Clients.showNotification("PIN Anda Salah. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
					count++;
					if (count >3){
						JSONObject obj = new JSONObject();
						obj.put("Tipe", "BlockAccount");
						obj.put("username", username);
						ProcessingListener processing = new ProcessingListener(this, obj.toString());
						processing.processData();
						if (block.equals("OK")){
							Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
							func.setTextboxDisabled(wnd, "pin", true);
						}
						else {
							System.out.println("Akun Gagal di Blokir");
						}
					}
				}
				else if (message.contains("blokir") && count ==3){
					count++;
					Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
					func.setTextboxDisabled(wnd, "pin", true);
				}
				else if(message.equals("01")) {
					Executions.sendRedirect("/timeout.zul");
				}
				else if(message.equals("TIMEOUT")) {
					ack = "NOK";
					Clients.showNotification(Constants.MSG_TIMEOUT_ERROR, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				}
			else {
				sesi.setSessionAttribute(Constants.tipePesan,"PembelianPLN");
				sesi.setSessionAttribute(Constants.pesan,message);
				RedirectMenu menu = new RedirectMenu();
				menu.setMenuLink("TransaksiGagal",this.winheader);
			}
			} catch (Exception e) {
				
			}
		}
	
	public void setWindow (Window wnd) {
		this.win = wnd;
	}
	
	public Window getWindow() {
		return this.win;
	}
	public void setData(Window wnd,Window header) {
		setMenuWindow(header);
		try {
		setWindow(wnd);	
		
//		send_trx = func.getIDTRX();
		send_trx = sesi.getSessionAttribute("id_trx");
		
		idt = sesi.getSessionAttribute("id_Pelanggan");
		save = sesi.getSessionAttribute("check");
		idDenom = sesi.getSessionAttribute("denom");
		nomorHandphone = sesi.getSessionAttribute(Constants.handPhone);
		
		JSONObject obj = new JSONObject();
		obj.put("Tipe","inqPembelian");
		obj.put("id_trx",send_trx);
		obj.put("username", username);
		obj.put("id_Pelanggan", idt);
		obj.put("id_Operator", 13);
		obj.put("denom", idDenom);
		obj.put("msisdn", nomorHandphone);
		obj.put("id_tipe_transaksi", 18);
		obj.put("save", save);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		
		if (ackInq.equals("OK")) {
			
			if (statusPromo.equals("0")) {
				func.setDivVisible(wnd, "divdiskon", false);
				func.setDivVisible(wnd, "divtextdiskon", false);
			} else {
				func.setDivVisible(wnd, "divdiskon", true);
				func.setDivVisible(wnd, "divtextdiskon", true);
				
				String diskon = func.getLabelValue(wnd, "lbldiskon");
				func.setLabelValue(wnd, "lbldiskon", diskon + " " + func.formatRp(nilai));
				
				
				int totalAfterDiskon = Integer.parseInt(jumlahBayar) - Integer.parseInt(nilai);
				jumlahBayar = String.valueOf(totalAfterDiskon);
			}
			
			idt = idPelanggan;//sesi.getSessionAttribute("id_Pelanggan");
			
			func.setLabelValue(wnd, "id_tuju", idt); 
			
			func.setLabelValue(wnd, "nama", namaPelanggan);
			func.setLabelValue(wnd, "noMeter", noMeter);
			func.setLabelValue(wnd, "daya", daya);
			func.setLabelValue(wnd, "maxKwh", maxKwh);
			
			func.setLabelValue(wnd, "amount", func.formatRp(amount));
			func.setLabelValue(wnd, "biayaAdmin", func.formatRp(biayaAdmin));
			func.setLabelValue(wnd, "diskon", func.formatRp(nilai));
			func.setLabelValue(wnd, "total", func.formatRp(jumlahBayar));
			
			strBeliToken = func.getLabelValue(wnd, "lblBeliToken");
			func.setLabelValue(wnd, "lblBeliToken", strBeliToken + func.formatRp(amount));
			
			func.setLabelValue(wnd, "noHp", nomorHandphone);
		}
		
		
		} catch (Exception e) {
			log.info("Error di PLN." + e.getMessage());
		}
	}
	
	public void doPayment(Window wnd,Window header) {
		try {
		String pin = func.getTextboxValue(wnd,"pin");
		
		if(check.isKosong(pin)) {
			Clients.showNotification("PIN tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else if (!(check.pinIsValid(pin))) {
			Clients.showNotification("PIN tidak valid, Silahkan coba kembali.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else {
			JSONObject obj = new JSONObject();
			
			if (username.startsWith("0")){
				obj.put("Tipe", "PembelianPLNMember");
			}
			else {
				obj.put("Tipe", "PembelianPLNAgent");
			}
			obj.put("username",username);
			obj.put("pin",pin);
			obj.put("id_Pelanggan",idt);
			obj.put("denom",idDenom);
			obj.put("hargaCetak",hargaCetak);
			obj.put("id_Operator",13);
			obj.put("id_TipeAplikasi",2);
			obj.put("id_Trx", idTrx);
			obj.put("simpan", save);
			obj.put("Handphone", nomorHandphone);
			paidsend = new Date().getTime();
			String m1 = "Waktu saat Pengiriman pembelian PLN " + new Date().toString();
			String m2 = "";
			func.cetakLog(m1, m2);
			ProcessingListenerPrabayar processing = new ProcessingListenerPrabayar(this, obj.toString());
			processing.processData();
			String head="";
			String message="";
			if (status.equals("SUKSES")){
				message = "Pembelian Token PLN Berhasil";
				head = "Pembelian Token PLN";
			} else if (status.equals("PENDING")){
				message = "Pembelian Token PLN Sedang Diproses";
				head = "Pembelian Token PLN";
			}else {
				message = "Pembelian Token PLN Sedang Diproses";
				head = "Pembelian Token PLN";
			}
			if (ack.equalsIgnoreCase("OK")){
				sesi.setSessionAttribute(Constants.header, head);
				sesi.setSessionAttribute(Constants.pesan, message);
				sesi.setSessionAttribute(Constants.id_trx, id_trx);
				sesi.setSessionAttribute(Constants.id_pelanggan, idPelanggan);
				sesi.setSessionAttribute(Constants.namaPelanggan, "Token PLN");
				sesi.setSessionAttribute(Constants.tagihan,amount);
				sesi.setSessionAttribute(Constants.biaya, biayacetak);
				System.out.println("harga cetak -> " + hargaCetak);
				sesi.setSessionAttribute(Constants.total, hargaCetak);
				sesi.setSessionAttribute(Constants.status, status);
				sesi.setSessionAttribute(Constants.handPhone,nomorHandphone);
				sesi.setSessionAttribute("token", token);
				sesi.setSessionAttribute("ref", ref);
				sesi.setSessionAttribute("no_meter", noMeter);
				sesi.setSessionAttribute("nama_pelanggan", namaPelanggan);
				sesi.setSessionAttribute("daya", daya);
				sesi.setSessionAttribute("nominal", nominal);
				sesi.setSessionAttribute("admin", admin);
				sesi.setSessionAttribute("materai", materai);
				sesi.setSessionAttribute("ppn", ppn);
				sesi.setSessionAttribute("ppj", ppj);
				sesi.setSessionAttribute("angsuran", angsuran);
				sesi.setSessionAttribute("stroom", stroom);
				sesi.setSessionAttribute("jumlah_kwh", jumlahKwh);
				sesi.setSessionAttribute("timeStamp", timeStamp);
				RedirectMenu menu = new RedirectMenu();
				menu.setMenuLink("buySuccess",header);
			}
		}
		
		} catch (Exception e){
			e.printStackTrace();
			log.info("Error di PLN." + e.getMessage());
			log.error(e);
		}
	}
	
	public void setBack(Window wnd,Window header) throws Exception {
		RedirectMenu menu = new RedirectMenu();
		menu.setMenuLink("pembelianpln",header);
	}
	
}
