package com.truemoneywitami.menu.pembelian.tv;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.sun.corba.se.impl.orbutil.closure.Constant;
import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.transfer.TrueKon;

public class TvSelect extends Window implements RequestListener {

	Logger log = Logger.getLogger(TvSelect.class);
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	JSONObject obj;
	String pesan,ack="";
	JSONArray norek,denom,arrProdukTvPra,arrNominalTvPra;
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	//String username = "0852235455";
	String idt="";
	String nominal="";
	String produk="";
	String Handphone ="";
	String tipeUser="";
	Window winheader;
	boolean agentflag= false;
	
	CheckParameter chk = new CheckParameter();
	
	
	@Override
	public void requestSucceeded(JSONObject obj) {	 
			
			if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("data") ){
				ack = obj.get("ACK").toString();
				norek = (JSONArray) obj.get("daftar");
			}
			else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("plnnominal")){
				denom = (JSONArray) obj.get("denom");
			}
			else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("dataprodukpra")){
				arrProdukTvPra = (JSONArray) obj.get("daftar");
			}
			else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("tvnominal")){
				arrNominalTvPra = (JSONArray) obj.get("denom");
			}
			else{
				pesan = obj.get("pesan").toString();
				requestFailed(pesan);
			}
		}
	
	@Override
	public void requestFailed(String message){
		
		ack = "NOK";
		
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
	public void setListboxValueRek(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID) {
			try {
			Listbox list = (Listbox) wnd.getFellow(idList);
			list.getItems().clear();
			int a = data.size();
			//int lIndex = 0;
			
			for (int i =0;i<a;i++){
				Listitem li = new Listitem();
				JSONObject child  = (JSONObject) data.get(i);
				li.setValue(child.get("id_operator").toString());
				String nama = child.get("data").toString();
				li.appendChild(new Listcell(nama));
				list.appendChild(li);
			}
			
			if (a > 0) {
				list.setSelectedIndex(0);
			}
			} catch (Exception e){
				log.info("Error di PLN ." + e.getMessage());
			}
		}
	
	public void setListboxValueProduk(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID) {
		try {
		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int a = data.size();
		//int lIndex = 0;
		
		for (int i =0;i<a;i++){
			Listitem li = new Listitem();
			JSONObject child  = (JSONObject) data.get(i);
			li.setValue(child.get("id_operator").toString());
			String nama = child.get("nama_operator").toString();
			li.appendChild(new Listcell(nama));
			list.appendChild(li);
		}
		
		if (a > 0) {
			list.setSelectedIndex(0);
			
			// set nominal
			setNominalTv(wnd);
		}
		} catch (Exception e){
			log.info("Error di PLN ." + e.getMessage());
		}
	}
	
	public void setListboxValueNominalTv(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID) {
		try {
		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int a = data.size();
		//int lIndex = 0;
		
		for (int i =0;i<a;i++){
			Listitem li = new Listitem();
			JSONObject child  = (JSONObject) data.get(i);
			li.setValue(child.get("id_denom").toString());
			String nama = child.get("nominal").toString();
			li.appendChild(new Listcell(nama));
			list.appendChild(li);
		}
		
		if (a > 0) {
			list.setSelectedIndex(0);
		}
		
		} catch (Exception e){
			log.info("Error di PLN ." + e.getMessage());
		}
	}
	
	public void setNominalTv(Window wnd) {
		try {
			
			String idOperator = "";
			
			Radio rd = (Radio) wnd.getFellow("radio2");
			if (rd.isSelected()){
				idOperator = func.getListboxValue(wnd, "lstproduk");
			} else {
				idOperator = func.getListboxValue(wnd, "pel_sel");
			}
			
			
			JSONObject obj = new JSONObject();
			obj.put("Tipe","tvnominal");
			obj.put("id_operator", idOperator);
			obj.put("ID_Account", username);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			
			setListboxValueNominalTv(wnd,"no_sel", arrNominalTvPra, 0, true, true, true);
			
		} catch (Exception e) {
			log.error("error saat set nominal untuk tv prabayar", e);
		}
	}
		
		public void setNominal(Window wnd)  {
			try {
			nominal = func.getListboxValue(wnd, "no_sel");
			} catch (Exception e) {
				log.info("Error di PLN ." + e.getMessage());
			}
		}
		
		public void setListboxValueNominal(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID) throws Exception {
			
			try {
			Listbox list = (Listbox) wnd.getFellow(idList);
			list.getItems().clear();
			int a = data.size();
			
			//int lIndex = 0;
			
			for (int i =0;i<a;i++){
				Listitem li = new Listitem();
				JSONObject child  = (JSONObject) data.get(i);
				li.setValue(child.get("id_denom").toString());
				String nama = child.get("nominal").toString();
				li.appendChild(new Listcell(nama));
				list.appendChild(li);
			}
			} catch (Exception e){
				log.info("Error di PLN ." + e.getMessage());
			}
		}
		
		public void setPelanggan(Window wnd) throws Exception {
			try {
				idt = func.getListboxValue(wnd, "pel_sel");
				} catch (Exception ex){
//					Messagebox.show("ID Pelanggan atau Nominal Tidak Boleh Kosong","Peringatan", Messagebox.OK, Messagebox.ERROR);
					log.info("Error di PLN ." + ex.getMessage());
				}
		}
	
	public void setData(Window wnd,Window header){
		String idTrx = func.getIDTRX();
		sesi.setSessionAttribute("id_trx", idTrx);
		Div handphone = (Div) wnd.getFellow("Handphone");
		try {
		if (username.startsWith("0")){
			tipeUser = "member";
		}	else {
			tipeUser="Agent";
			func.setDivVisible(wnd,"Handphone");
			agentflag = false;
		}
		JSONObject obj = new JSONObject();
		obj.put("Tipe","peldatatv");
		obj.put("ID_Account", username);
		obj.put("id_TipeTransaksi", 13);
		obj.put("tipe_operator", "TVKABEL-PRA");
		obj.put("tipeUser",tipeUser);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		setListboxValueRek(wnd,"pel_sel", norek, 0, true, true, true);
		
		/**
		 * get product list
		 */
		try {
			obj = new JSONObject();
			obj.put("Tipe","getproduktvpra");
			obj.put("id_account", username);
			processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			setListboxValueProduk(wnd,"lstproduk", arrProdukTvPra, 0, true, true, true);
		} catch (Exception e) {
			log.error("Error di Pembayaran TV. get produk tv", e);
		}
		
		
		} catch (Exception e){
			log.info("Error di PLN." + e.getMessage());
		}
	}
	
	public void next(Window wnd,Window header)  {
		setMenuWindow(header);
		try {
		String checked="";
		Checkbox ck = (Checkbox) wnd.getFellow("save");
//		Radio rd = (Radio) wnd.getFellow("radio2");
//		if (rd.isSelected()){
//			idt = func.getTextboxValue(wnd, "idpel");
//		}
//		else {
//			setPelanggan(wnd);
//		}
		if (ck.isChecked()){
			checked = "OK";
		}
		else {
			checked = "NOK";
		}
		
		Radio rd = (Radio) wnd.getFellow("radio2");
		if (rd.isSelected()){
			try {
				idt = func.getTextboxValue(wnd, "idpel");
			} catch(Exception e) {
				idt = "";
			}
			
			try {
				produk = func.getListboxValue(wnd, "lstproduk");
				sesi.setSessionAttribute("nama_produk", func.getListboxLabel(wnd, "lstproduk"));
			} catch(Exception e) {
				produk = "";
			}
			
		}
		else {
			try {
				idt = func.getListboxLabel(wnd, "pel_sel");
				String temp [] = idt.split("-");
				idt = temp[0].trim();
				sesi.setSessionAttribute("nama_produk", temp[1].trim());
			} catch(Exception e) {
				idt = "";
			}
			
			try {
				produk = func.getListboxValue(wnd, "pel_sel");
			} catch(Exception e) {
				produk = "";
			}
			
		}
		
		try {
			nominal = func.getListboxValue(wnd, "no_sel");
			sesi.setSessionAttribute("nominal_label", func.getListboxLabel(wnd, "no_sel"));
		} catch(Exception e) {
			nominal = "";
		}
		
		try {
			Handphone = func.getTextboxValue(wnd, "no_HP");
		} catch(Exception e) {
			Handphone = "";
		}
		
		if (idt.equals("")){
			Clients.showNotification("ID Pelanggan tidak boleh kosong. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} 
		else if (produk.equals("")){
			Clients.showNotification("Nominal tidak boleh kosong. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (nominal.equals("")){
			Clients.showNotification("Nominal tidak boleh kosong. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (Handphone.equals("") && agentflag) {
			Clients.showNotification("Handphone tidak boleh kosong. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (idt.length() <7 || idt.length() > 13){
			Clients.showNotification("No ID Pelanggan tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if ((!chk.isNumber(Handphone)) && agentflag){
			Clients.showNotification("No Handphone tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if ((Handphone.length() <9 || Handphone.length() > 13) && agentflag){
			Clients.showNotification("No Handphone tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else {  
			try {
			Long.parseLong(idt);
			if (agentflag){
				Long.parseLong(Handphone);
			}
			sesi.setSessionAttribute("id_Pelanggan", idt);
			sesi.setSessionAttribute("check",checked);
			sesi.setSessionAttribute("id_operator", produk);
			sesi.setSessionAttribute("denom", nominal);
			new RedirectMenu().setMenuLink("konfirmasipembayarantv",this.winheader);
			} catch (Exception e){
				Clients.showNotification("ID Pelanggan atau Nomor Handphone tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				e.printStackTrace();
			}		
			
		}
		} catch (Exception e){
			log.info("Error di PLN." + e.getMessage());
		}
	}
	
	public void setMenuWindow(Window header){
		this.winheader = header;
	}
}
