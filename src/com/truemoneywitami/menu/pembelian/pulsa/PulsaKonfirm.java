package com.truemoneywitami.menu.pembelian.pulsa;

import java.util.Date;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.ProcessingListenerPrabayar;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.pembelian.pln.PlnKonfirm;

public class PulsaKonfirm extends Window  implements RequestListener {

	Logger log = Logger.getLogger(PulsaKonfirm.class);
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	CheckParameter check = new CheckParameter();
	JSONObject obj;
	String pesan,ack="";
	String denom="",HP="",status="";
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	//String username = "0852235455";
	String idt="";
	Window win;int count =0;String block="";
	String idOperator="",namaOperator="",biayaAdmin="",hargaCetak="",nominal="",id_trx="";
	String send_trx = "";
	String checked ="";
	String Product="";
	String keterangan = "";
	Window winheader;
	
	long paidsend,paidget,paidtime;
	
	@Override
	public void requestSucceeded(JSONObject obj) {	 
		
		if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("payment") ){
			paidget = new Date().getTime();
			paidtime = paidget - paidsend;
			String m1 = "Waktu saat Penerimaan  pembelian Pulsa " + new Date().toString();
			String m2 = "Waktu yang dibutuhkan untuk pembelian Pulsa " +paidtime +" ms";
			func.cetakLog(m1, m2);
			pesan = obj.get("pesan").toString();
			idOperator = obj.get("id_Operator").toString();
			denom = obj.get("denom").toString();
			HP = obj.get("no_HP").toString();
			status = obj.get("statusTopUP").toString();
			ack = obj.get("ACK").toString();
			id_trx = obj.get("id_Transaksi").toString();
			keterangan = obj.get("keterangan").toString();
		}	
		else if (obj.get("ACK").equals("OK") && obj.get("ID").equals("Block")){
			block = obj.get("ACK").toString();
		}
		else{
			pesan = obj.get("pesan").toString();
			requestFailed(pesan);
		}
	}
	
	public void setWindow (Window wnd) {
		this.win = wnd;
	}
	
	public Window getWindow() {
		return this.win;
	}
	
	@Override
	public void requestFailed(String message){
		//
		try {
		Window wnd = getWindow();
		if (message.contains("PIN")) {
			Clients.showNotification("PIN Anda Salah. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			count++;
			if (count >3){
				JSONObject obj = new JSONObject();
				obj.put("Tipe", "BlockAccount");
				obj.put("username", username);
				ProcessingListener processing = new ProcessingListener(this, obj.toString());
				processing.processData();
				if (block.equals("OK")){
					Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
					func.setTextboxDisabled(wnd, "pin", true);
				}
				else {
					System.out.println("Akun Gagal di Blokir");
				}
			}
		}
		else if (message.contains("blokir") && count ==3){
			count++;
			Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxDisabled(wnd, "pin", true);
		}
		else if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		}
		else if(message.equals("TIMEOUT")) {
			ack = "NOK";
			Clients.showNotification(Constants.MSG_TIMEOUT_ERROR, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else {
			sesi.setSessionAttribute(Constants.tipePesan,"PembelianPulsa");
			sesi.setSessionAttribute(Constants.pesan,message);
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("TransaksiGagal",this.winheader);
		}
		} catch (Exception e) {
			
		}
	}
	
	public void setMenuWindow(Window header){
		this.winheader = header;
	}
	
	public Window getMenuWindow(){
		return this.winheader;
	}
	
	public void setData(Window wnd,Window header) {
		setMenuWindow(header);
		try {
		setWindow(wnd);	
		idt= sesi.getSessionAttribute("noHP");
		if (idt.equals("")){
			Clients.showNotification("NO HP atau Nominal Tidak Boleh Kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else {
			idOperator = sesi.getSessionAttribute("id_Operator");
			biayaAdmin = sesi.getSessionAttribute("biayaAdmin");
			namaOperator = sesi.getSessionAttribute("namaOperator");
			hargaCetak = sesi.getSessionAttribute("hargaCetak");
			nominal = sesi.getSessionAttribute("nominal");
			checked = sesi.getSessionAttribute("save");
			String path="";
			int id = Integer.parseInt(idOperator);
			switch (id) {
			
			case 1 :
			path = "/assets/img/LOGOSIMPATI.png";
			Product = "SIMPATI";
			break;
			case 2 :
				path = "/assets/img/LOGOXL.png";
				Product = "XL";
				break;
			case 3 :
				path = "/assets/img/LOGOIM3.png";
				Product = "IM3";
				break;	
			case 4 :
				path = "/assets/img/LOGOKARTUAS.png";
				Product = "AS";
				break;	
			case 5 :
				path = "/assets/img/LOGOMENTARI.png";
				Product = "MENTARI";
				break;
			case 6 :
				path = "/assets/img/LOGOSMARTFREN.png";
				Product = "SMARTFREN";
				break;
			case 7 :
				path = "/assets/img/LOGOBOLT.png";
				Product = "BOLT";
				break;
			case 8 :
				path = "/assets/img/LOGOESIA.png";
				Product = "ESIA";
				break;
			case 9 :
				path = "/assets/img/LOGOFLEXI.png";	
				Product = "FLEXI";
				break;
			case 10 :
				path = "/assets/img/LOGOCERIA.png";	
				Product = "CERIA";
				break;
			case 11 :
				path = "/assets/img/LOGOAXIS.png";
				Product = "AXIS";
				break;
			case 12 :
				path = "/assets/img/LOGOTRI.png";
				Product = "TRI";
				break;	
			}
			
			if (check.isNumber(nominal)){
				func.setLabelValue(wnd, "amount", func.formatRp(nominal));
			}
			else {
				func.setLabelValue(wnd, "amount", nominal);
			}
			
			//func.setLabelValue(wnd, "biaya", func.formatRp(biayaAdmin));
			func.setLabelValue(wnd, "total", func.formatRp(hargaCetak));
			func.setLabelValue(wnd, "hp", idt);
			func.setImage(wnd, "logo", path);
			
		}
		
		} catch (Exception e) {
			log.info("Error di Pulsa." + e.getMessage());
		}
	}
	
	public void doPayment(Window wnd,Window header)  {
		try {
		String pin = func.getTextboxValue(wnd,"pin");
		
		if(check.isKosong(pin)) {
			Clients.showNotification("PIN tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else if (!(check.pinIsValid(pin))) {
			Clients.showNotification("PIN tidak valid, Silahkan coba kembali.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else {
			JSONObject obj = new JSONObject();
			send_trx = sesi.getSessionAttribute("id_trx");
			System.out.println("username -> " + username);
			
			if (username.startsWith("0")){
				obj.put("Tipe", "PembelianPulsaMember");
				System.out.println("tipe member.");
			}
			else {
				obj.put("Tipe", "PembelianPulsaAgent");
				System.out.println("tipe agent.");
			}
			obj.put("username",username);
			obj.put("pin",pin);
			obj.put("Handphone",idt);
			obj.put("denom",nominal);
			obj.put("save",checked);
			obj.put("hargaCetak",hargaCetak);
			obj.put("id_Operator",idOperator);
			obj.put("id_TipeAplikasi",2);
			obj.put("id_trx", send_trx);
			paidsend = new Date().getTime();
			String m1 = "Waktu saat Pengiriman pembelian Pulsa " + new Date().toString();
			String m2 = "";
			func.cetakLog(m1, m2);
			ProcessingListenerPrabayar processing = new ProcessingListenerPrabayar(this, obj.toString());
			processing.processData();
			String head="Pembelian Pulsa Prabayar";
			String message = "";
			System.out.println("status nya adalah--->" +status);
			if (status.equals("SUKSES")){
				message = "Pembelian Pulsa Berhasil";
			} else if (status.equals("PENDING")){
				message = "Pembelian Pulsa sedang Diproses";
			}else {
				message = "Pembelian Pulsa Diproses";
			}
			if (ack.equalsIgnoreCase("OK")){
				sesi.setSessionAttribute(Constants.header, head);
				sesi.setSessionAttribute(Constants.pesan, message);
				sesi.setSessionAttribute(Constants.id_trx, id_trx);
				sesi.setSessionAttribute(Constants.id_pelanggan, idt);
				sesi.setSessionAttribute(Constants.namaPelanggan, Product);
				sesi.setSessionAttribute(Constants.tagihan,nominal );
				//sesi.setSessionAttribute(Constants.biaya, biayaAdmin);
				sesi.setSessionAttribute(Constants.total, hargaCetak);
				sesi.setSessionAttribute(Constants.status, status);
				sesi.setSessionAttribute("keterangan", keterangan);
				RedirectMenu menu = new RedirectMenu();
				menu.setMenuLink("buySuccess",header);
			}
		}
		
		
		} catch (Exception e) {
			//log.info("Error di Pulsa." + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void setBack(Window header) throws Exception {
		RedirectMenu menu = new RedirectMenu();
		menu.setMenuLink("pembelianpulsa",header);
	}	
}
