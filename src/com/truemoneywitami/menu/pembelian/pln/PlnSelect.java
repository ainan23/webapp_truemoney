package com.truemoneywitami.menu.pembelian.pln;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.sun.corba.se.impl.orbutil.closure.Constant;
import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.transfer.TrueKon;

public class PlnSelect extends Window implements RequestListener {

	Logger log = Logger.getLogger(PlnSelect.class);
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	JSONObject obj;
	String pesan,ack="";
	JSONArray norek,denom;
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	//String username = "0852235455";
	String idt="";
	String nominal="";
	String Handphone ="";
	String tipeUser="";
	Window winheader;
	boolean agentflag= true;
	
	CheckParameter chk = new CheckParameter();
	
	
	@Override
	public void requestSucceeded(JSONObject obj) {	 
			
			if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("data") ){
				ack = obj.get("ACK").toString();
				norek = (JSONArray) obj.get("daftar");
			}
			else if (obj.get("ACK").toString().equals("OK") && obj.get("ID").toString().equals("plnnominal")){
				denom = (JSONArray) obj.get("denom");
			}
			else{
				pesan = obj.get("pesan").toString();
				requestFailed(pesan);
			}
		}
	
	@Override
	public void requestFailed(String message){
		
		ack = "NOK";
		
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
	public void setListboxValueRek(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID) {
			try {
			Listbox list = (Listbox) wnd.getFellow(idList);
			list.getItems().clear();
			int a = data.size();
			//int lIndex = 0;
			
			for (int i =0;i<a;i++){
				Listitem li = new Listitem();
				JSONObject child  = (JSONObject) data.get(i);
				li.setValue(child.get("data").toString());
				String nama = child.get("data").toString();
				li.appendChild(new Listcell(nama));
				list.appendChild(li);
			}
			} catch (Exception e){
				log.info("Error di PLN ." + e.getMessage());
			}
		}
		
		public void setNominal(Window wnd)  {
			try {
			nominal = func.getListboxValue(wnd, "no_sel");
			} catch (Exception e) {
				log.info("Error di PLN ." + e.getMessage());
			}
		}
		
		public void setListboxValueNominal(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID) throws Exception {
			
			try {
			Listbox list = (Listbox) wnd.getFellow(idList);
			list.getItems().clear();
			int a = data.size();
			
			//int lIndex = 0;
			
			for (int i =0;i<a;i++){
				Listitem li = new Listitem();
				JSONObject child  = (JSONObject) data.get(i);
				li.setValue(child.get("id_denom").toString());
				String nama = child.get("nominal").toString();
				li.appendChild(new Listcell(nama));
				list.appendChild(li);
			}
			} catch (Exception e){
				log.info("Error di PLN ." + e.getMessage());
			}
		}
		
		public void setPelanggan(Window wnd) throws Exception {
			try {
				idt = func.getListboxValue(wnd, "pel_sel");
				} catch (Exception ex){
//					Messagebox.show("ID Pelanggan atau Nominal Tidak Boleh Kosong","Peringatan", Messagebox.OK, Messagebox.ERROR);
					log.info("Error di PLN ." + ex.getMessage());
				}
		}
	
	public void setData(Window wnd,Window header){
		String idTrx = func.getIDTRX();
		sesi.setSessionAttribute("id_trx", idTrx);
		Div handphone = (Div) wnd.getFellow("Handphone");
		try {
		if (username.startsWith("0")){
			tipeUser = "member";
		}	else {
			tipeUser="Agent";
			func.setDivVisible(wnd,"Handphone");
			agentflag = true;
		}
		JSONObject obj = new JSONObject();
		obj.put("Tipe","peldata");
		obj.put("ID_Account", username);
		obj.put("id_TipeTransaksi", 18);
		obj.put("tipeUser",tipeUser);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		obj = new JSONObject();
		obj.put("Tipe","plnnominal");
		obj.put("ID_Account", username);
		processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		setListboxValueRek(wnd,"pel_sel", norek, 0, true, true, true);
		setListboxValueNominal(wnd,"no_sel", denom, 0, true, true, true);
		} catch (Exception e){
			log.info("Error di PLN." + e.getMessage());
		}
	}
	
	public void next(Window wnd,Window header)  {
		setMenuWindow(header);
		try {
		String checked="";
		Checkbox ck = (Checkbox) wnd.getFellow("save");
//		Radio rd = (Radio) wnd.getFellow("radio2");
//		if (rd.isSelected()){
//			idt = func.getTextboxValue(wnd, "idpel");
//		}
//		else {
//			setPelanggan(wnd);
//		}
		if (ck.isChecked()){
			checked = "OK";
		}
		else {
			checked = "NOK";
		}
		
		Radio rd = (Radio) wnd.getFellow("radio2");
		if (rd.isSelected()){
			try {
				idt = func.getTextboxValue(wnd, "idpel");
			} catch(Exception e) {
				idt = "";
			}
			
		}
		else {
			try {
				idt = func.getListboxValue(wnd, "pel_sel");
			} catch(Exception e) {
				idt = "";
			}
			
		}
		
		try {
			nominal = func.getListboxValue(wnd, "no_sel");
		} catch(Exception e) {
			nominal = "";
		}
		
		try {
			Handphone = func.getTextboxValue(wnd, "no_HP");
		} catch(Exception e) {
			Handphone = "";
		}
		
		if (idt.equals("")){
			Clients.showNotification("ID Pelanggan tidak boleh kosong. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} 
		else if (nominal.equals("")){
			Clients.showNotification("Nominal tidak boleh kosong. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (Handphone.equals("") && agentflag) {
			Clients.showNotification("Handphone tidak boleh kosong. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if (idt.length() <10 || idt.length() > 13){
			Clients.showNotification("No ID Pelanggan tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if ((!chk.isNumber(Handphone)) && agentflag){
			Clients.showNotification("No Handphone tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else if ((Handphone.length() <9 || Handphone.length() > 13) && agentflag){
			Clients.showNotification("No Handphone tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else {  
			try {
			long test = Long.parseLong(idt);
			if (agentflag){
				long test2 = Long.parseLong(Handphone);
			}
			sesi.setSessionAttribute("id_Pelanggan", idt);
			sesi.setSessionAttribute("check",checked);
			sesi.setSessionAttribute("denom", nominal);
			sesi.setSessionAttribute(Constants.handPhone, Handphone);
			new RedirectMenu().setMenuLink("konfirmasipembelianpln",this.winheader);
			} catch (Exception e){
				Clients.showNotification("ID Pelanggan atau Nomor Handphone tidak valid. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
				e.printStackTrace();
			}		
			
		}
		} catch (Exception e){
			log.info("Error di PLN." + e.getMessage());
		}
	}
	
	public void setMenuWindow(Window header){
		this.winheader = header;
	}
}
