package com.truemoneywitami.menu;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

import java.awt.print.PrinterException;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.ResourceBundle;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;
import org.zkoss.json.JSONObject;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;

public class PurchaseSukses extends Window {
	
	private static Logger log = Logger.getLogger(PurchaseSukses.class);
	
	private ResourceBundle rb = ResourceBundle.getBundle("config.config");

	ZKFunction func = new ZKFunction();
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	String header, id, nama = "-", tagihan, biayaBayar, hargaCetak, pesan, id_trx, jasper, timeStamp,periode,statustrx,token="";
	private HashMap map;
	String noMeter = "";
	String daya = "";
	String namaPelanggan = "";
	String ref = "";
	String nominal = "";
	String admin = "";
	String materai = "";
	String ppn = "";
	String ppj = "";
	String angsuran = "";
	String stroom = "";
	String jumlahKwh = "";

	public void setData(Window wnd)  {
		try {
		header 		= sesi.getSessionAttribute(Constants.header);
		id 			= sesi.getSessionAttribute(Constants.id_pelanggan);
		id_trx 		= sesi.getSessionAttribute(Constants.id_trx);
		nama 		= sesi.getSessionAttribute(Constants.namaPelanggan);
		tagihan 	= sesi.getSessionAttribute(Constants.tagihan);
		biayaBayar 	= sesi.getSessionAttribute(Constants.biaya);
		hargaCetak 	= sesi.getSessionAttribute(Constants.total);
		System.out.println("harga cetak struk -> " + hargaCetak);
		pesan 		= sesi.getSessionAttribute(Constants.pesan);
		
		//periode 	= sesi.getSessionAttribute(Constants.periode);
		statustrx 		= sesi.getSessionAttribute(Constants.status);
		System.out.println(statustrx);
		timeStamp 	= new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
		func.setLabelValue(wnd, "header", header);
		func.setLabelValue(wnd, "pesan", pesan);
		if (header.contains("Pulsa")){
			func.setLabelValue(wnd, "identify", "Username");
		}
		else {
			func.setLabelValue(wnd, "identify", "ID Pelanggan");
		}
		Div status = (Div) wnd.getFellow("status");
		Div tokendiv = (Div) wnd.getFellow("vitoken");
		Div plnNama = (Div) wnd.getFellow("plnnama");
		Div plnMeter = (Div) wnd.getFellow("plnmeter");
		Div plnDaya = (Div) wnd.getFellow("plndaya");
		Div keterangan = (Div) wnd.getFellow("divKeterangan");
		if (pesan.contains("PLN")){
			
			token = sesi.getSessionAttribute("token");
			ref = sesi.getSessionAttribute("ref");
			noMeter = sesi.getSessionAttribute("no_meter");
			daya = sesi.getSessionAttribute("daya");
			namaPelanggan = sesi.getSessionAttribute("nama_pelanggan");
			nominal = sesi.getSessionAttribute("nominal");
			admin = sesi.getSessionAttribute("admin");
			materai = sesi.getSessionAttribute("materai");
			ppn = sesi.getSessionAttribute("ppn");
			ppj = sesi.getSessionAttribute("ppj");
			angsuran = sesi.getSessionAttribute("angsuran");
			stroom = sesi.getSessionAttribute("stroom");
			jumlahKwh = sesi.getSessionAttribute("jumlah_kwh");
			timeStamp = sesi.getSessionAttribute("timeStamp");
			
			tokendiv.setVisible(true);
			plnNama.setVisible(true);
			plnMeter.setVisible(true);
			plnDaya.setVisible(true);
			keterangan.setVisible(false);
			if (token == null || token.equals("")){
				token = "Token Akan dikirim Via SMS";			
			}
			func.setLabelValue(wnd, "token", token);
			// nama
			namaPelanggan = sesi.getSessionAttribute("nama_pelanggan");
			func.setLabelValue(wnd, "nama_pelanggan", namaPelanggan);
			
			// no meter
			noMeter = sesi.getSessionAttribute("no_meter");
			func.setLabelValue(wnd, "no_meter", noMeter);
			
			// daya
			daya = sesi.getSessionAttribute("daya");
			func.setLabelValue(wnd, "daya", daya);
			
			// ganti label ke pln
			func.setLabelValue(wnd, "lblTujuan", "ID Pelanggan");
		} else {
			String strKeterangan = sesi.getSessionAttribute("keterangan");
			keterangan.setVisible(true);
			
			// ganti label ke pulsa
			func.setLabelValue(wnd, "lblTujuan", "No. HP");
			func.setLabelValue(wnd, "keterangan", strKeterangan);
		}
		if (statustrx.equalsIgnoreCase("SUKSES")){
			status.setSclass("alert alert-success");
		}
		else {
			status.setSclass("alert alert-warning");
		}
		
		func.setLabelValue(wnd, "idpelanggan", id);
		func.setLabelValue(wnd, "id", id);
		func.setLabelValue(wnd, "id_trx", id_trx);
		func.setLabelValue(wnd, "nama", nama);
		func.setLabelValue(wnd, "amount", func.formatRp(tagihan));
		//func.setLabelValue(wnd, "periode", periode);
		func.setLabelValue(wnd, "total", func.formatRp(hargaCetak));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void cetakStruk(Window Wnd) throws Exception {
		
		map = new HashMap();
		map.put("header", statustrx);
		map.put("idPelanggan", id);
		map.put("namaPelanggan", nama);
		map.put("tagihan", tagihan);
		//map.put("biayaAdmin", func.formatRp(biayaBayar));
		map.put("totalBayar", func.formatRp(hargaCetak));
		map.put("idTransaksi", id_trx);
		map.put("timeStamp", timeStamp);
		System.out.println("pesan -> " + pesan);
		
		Iframe ireport = (Iframe)Wnd.getFellow("ireport");
		
		System.out.println("pesan -> " + pesan);
		if (pesan.toUpperCase().contains("PLN")){
			map.put("noMeter", noMeter);
			map.put("daya", daya);
			map.put("ref", ref);
			map.put("token", token);
			
			int newNominal = (int) Double.parseDouble(nominal);
			map.put("rpBayar", func.formatRp(String.valueOf(newNominal)));
			
			NumberFormat formatter = new DecimalFormat("#0.00");
			
			System.out.println("-------------------------------- " + func.formatRp(Double.parseDouble(materai)));
			
			map.put("adminBank", func.formatRp(admin));
			map.put("materai", func.formatRp(Double.parseDouble(materai)));
			map.put("ppn", func.formatRp(Double.parseDouble(ppn)));
			map.put("ppj", func.formatRp(Double.parseDouble(ppj)));
			map.put("angsuran", func.formatRp(Double.parseDouble(angsuran)));
			map.put("stroom", func.formatRp(Double.parseDouble(stroom)));
			map.put("jmlKwh", func.formatDecimal(Double.parseDouble(jumlahKwh)));
			map.put("namaPelanggan", namaPelanggan);
			map.put("idPelanggan", id);
			
//			jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/pembelian/pembelianPlnNew.jasper");
//			jasper = "/home/admin/data_web/report/pembelian/pembelianVPLN.jasper";
			jasper = rb.getString("url_report_pra_pln");
		} else if (pesan.toUpperCase().contains("GAME")){
//			jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/pembelian/pembelianGames.jasper");
//			jasper = "/home/admin/data_web/report/pembelian/pembelianVPLN.jasper";
			jasper = rb.getString("url_report_pra_game");
		} else {
			String keterangan = sesi.getSessionAttribute("keterangan");
			map.put("keterangan", keterangan);
			
//			jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/pembelian/pembelianPULSA.jasper");
//			jasper = "/home/admin/data_web/report/pembelian/pembelianPULSA.jasper";
			jasper = rb.getString("url_report_pasca_other");
		}
		System.out.println("jasper path -> "+jasper);
		File inFileName = new File(jasper);
		JasperPrint print = JasperFillManager.fillReport(inFileName.getPath(), map, new JREmptyDataSource());
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		JasperExportManager.exportReportToPdfStream(print, outStream); 
		AMedia amedia = new AMedia("Invoice_"+id_trx+"_"+id, "pdf", null, outStream.toByteArray()); 
		ireport.setContent(amedia);
		Wnd.setVisible(true);
		
	}
		
	
	public void Downloaded(String pth) {
		String pesan = "File Downloaded on " + pth;
		Clients.showNotification(pesan, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		System.out.println("\n" + pesan);
	}

	public void setBack(Window wnd,Window header) throws Exception {
		
		mob_Session sesi = new mob_Session();
		String username = sesi.getUsername();
		if (username.startsWith("0")) {
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("inforekeningmember",header);
		} else {
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("inforekeningagen",header);
		}

	}

}
