package com.truemoneywitami.menu;


import java.util.ResourceBundle;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.HtmlNativeComponent;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Include;
import org.zkoss.zul.Window;
import org.zkoss.html.HTMLs;

import com.sun.org.glassfish.gmbal.ParameterNames;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.ProcessingListenerPrabayar;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.session.mob_Session;

public class Login extends Window implements RequestListener  {
	
	private static Logger log = Logger.getLogger(Login.class);
	private static ResourceBundle rb = ResourceBundle.getBundle("config.config");
	
	JSONObject obj;
	mob_Session sesi = new mob_Session();
	public void loginTo(String user, String pass){
		System.out.println(user);
		System.out.println(pass);
		try {
			String tipeUser = "";
			JSONObject obj = new JSONObject();
			obj.put("Tipe", "SignIn");
			obj.put("username", user);
			obj.put("password", pass);
			if (user.startsWith("0")) {
				tipeUser = "member";
			} else {
				tipeUser = "agent";
			}
			obj.put("tipeUser", tipeUser);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.print(e);
			requestFailed(Constants.generalErrorMessage);
		}
	}

	@Override
	public void requestSucceeded(JSONObject data){
		// TODO Auto-generated method stub
		
		String accountType = data.get("accountType").toString();
		String username = data.get("username").toString();
		Cookie cookies = new Cookie("username", username);
		(Sessions.getCurrent()).setAttribute("username",username );
		(Sessions.getCurrent()).setAttribute("tipe", accountType);
		
		//sesi.setSession(username);
//		Include inclMenu = (Include) getFellow("inclMenu");
//		if (accountType.equalsIgnoreCase("agent")) {
//			inclMenu.setSrc(Constants.adminHeaderPath);
//		} else {
//			inclMenu.setSrc(Constants.memberHeaderPath);
//		}
		Executions.getCurrent().sendRedirect("/index.zul");
	}

	@Override
	public void requestFailed(String message) {
		// TODO Auto-generated method stub
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			log.info("kesini ya gan...");
			log.info("user agent -- " + Executions.getCurrent().getHeader("user-agent"));
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
//			Clients.showNotification(message);
		}
	}
	
	public String setUsernameFromCookies(){
		String username = "";
		Execution exec = Executions.getCurrent();
		Cookie[] cookies = ((HttpServletRequest)exec.getNativeRequest()).getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				username = cookie.getValue();
			}
		}
		System.out.println("sdhskhdfkdhkfsdkfskfskjdfkshdkfhskdj");
		log.info("username : " + username);
		log.error("error username : " + username);
		return username;
	}
	
	public void onCreate() {
		log.warn("revisi " + rb.getString("revisi"));
	}
}
