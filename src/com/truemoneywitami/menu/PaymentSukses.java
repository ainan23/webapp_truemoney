package com.truemoneywitami.menu;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.A;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Window;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.corba.se.impl.orbutil.closure.Constant;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.Function;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

import datasource.DataSourceConnectionPostgres;
import id.co.truemoney.report.MultiFinanceReport;
import id.co.truemoney.report.PdamReport;
import id.co.truemoney.report.PulsaPascaReport;
import id.co.truemoney.report.TelkomReport;
import id.co.truemoney.report.TvPascaReport;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import javax.sql.DataSource;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

public class PaymentSukses extends Window{
	
	private static Logger log = Logger.getLogger(PaymentSukses.class);
	
	private ResourceBundle rb = ResourceBundle.getBundle("config.config");
	
	ZKFunction func = new ZKFunction();
	Function fung = new Function();
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	String header,id_pelanggan,nama,tagihan,biayaBayar,hargaCetak,id_trx,jasper, pesan, daya, lwbp, periode, timeStamp,statustrx,ref,tipe,namaProduk,kodePdam;
	private HashMap map;
	
	String waktu = "";
	String noHp = "";
	
	private JsonArray periodeArr = new JsonArray();
	
		public void setData(Window wnd) throws Exception {
			statustrx 		= sesi.getSessionAttribute(Constants.status);
			header 			= sesi.getSessionAttribute(Constants.header);
			pesan 			= sesi.getSessionAttribute(Constants.pesan);
			id_trx 			= sesi.getSessionAttribute(Constants.id_trx);
			id_pelanggan 	= sesi.getSessionAttribute(Constants.id_pelanggan);
			nama 			= sesi.getSessionAttribute(Constants.namaPelanggan);
			tagihan 		= sesi.getSessionAttribute(Constants.tagihan);
			biayaBayar 		= sesi.getSessionAttribute(Constants.biaya);
			hargaCetak 		= sesi.getSessionAttribute(Constants.total);
			daya 			= sesi.getSessionAttribute(Constants.daya);
			lwbp 			= sesi.getSessionAttribute(Constants.lwbp); //stand meter
			periode 		= sesi.getSessionAttribute(Constants.periode);
			tipe 		= sesi.getSessionAttribute(Constants.tipe);
			timeStamp 		= new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
			
			Div status = (Div) wnd.getFellow("statusheader");
			if (statustrx.equalsIgnoreCase("SUKSES")){
				status.setSclass("alert alert-success");
			}
			else {
				status.setSclass("alert alert-warning");
//				
//				if (tipe.equals("TELKOM") || tipe.equals("PDAM")) {
					A a = (A) wnd.getFellow("btnStruk");
					a.setVisible(false);
//				}
			}
			
			func.setLabelValue(wnd, "header"	, header					);
			func.setLabelValue(wnd, "pesan"		, pesan						);
			func.setLabelValue(wnd, "id_trx"	, id_trx					);
			
			if (tipe.equals("PDAM")) {
				String idPel = "";
				
				if (id_pelanggan.trim().length() == 0 || id_pelanggan.trim().equals("0")) {
					idPel = sesi.getSessionAttribute("nosambungan");
					log.info("id pelanggan kosong");
				} else {
					idPel = id_pelanggan;
					log.info("id_pelanggan : " + id_pelanggan);
					log.info("id pelanggan tidak kosong");
				}
				log.info("INI PDAM");
				log.info("pake idPel " + idPel);
				func.setLabelValue(wnd, "id"		, idPel				);
			} else {
				log.info("INI BUKAN PDAM");
				log.info("pake id_pelanggan " + id_pelanggan);
				func.setLabelValue(wnd, "id"		, id_pelanggan				);
			}
			
			func.setLabelValue(wnd, "nama"		, nama						);
			func.setLabelValue(wnd, "periode"	, periode					);
			func.setLabelValue(wnd, "amount"	, func.formatRp(tagihan)	);
			func.setLabelValue(wnd, "biaya"		, func.formatRp(biayaBayar)	);
			func.setLabelValue(wnd, "total"		, func.formatRp(hargaCetak)	);
			
			if (nama.trim().isEmpty()) {
				func.setDivVisible(wnd, "divnama", false);
			}
		}
		
		public void cetakStruk(Window Wnd) throws Exception {
			
			map = new HashMap();
			map.put("header", statustrx);
			map.put("idPelanggan", id_pelanggan);
			map.put("namaPelanggan", nama);
			map.put("tagihan", fung.getFormatRp(tagihan));
			map.put("biayaAdmin", fung.getFormatRp(biayaBayar));
			map.put("totalBayar", fung.getFormatRp(hargaCetak));
			map.put("idTransaksi", id_trx); //id_trx
			map.put("daya", daya);
			map.put("lwbp", lwbp);
			map.put("periode", periode);
			map.put("timeStamp", timeStamp);
			
//			System.out.println("pesan -> " + pesan);
			
			Iframe ireport = (Iframe)Wnd.getFellow("ireport");
			
			System.out.println("---------------------------------------------------------- -> " + tipe);
			if (tipe.equals("PLN")){
				ref = sesi.getSessionAttribute(Constants.ref);
				map.put("ref", ref);
				
//				jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/pembayaran/pembayaranPlnNew.jasper");
//				jasper = "/home/admin/data_web/report/pembayaran/pembayaranPLN.jasper";
				jasper = rb.getString("url_report_pasca_pln");
			} else if (tipe.equals("TELKOM")){
				ref = sesi.getSessionAttribute(Constants.ref);
				namaProduk = sesi.getSessionAttribute("nama_produk");
				String tanggal = sesi.getSessionAttribute(Constants.waktu);
				String sn = sesi.getSessionAttribute("sn");
				String terbilang = sesi.getSessionAttribute(Constants.TERBILANG);
				
				map.put("ref", ref);
				map.put("sn", sn);
				map.put("tanggal", tanggal);
				map.put("namaProduk", namaProduk.toUpperCase());
				map.put("terbilang", terbilang);
				
				// struk tambahan
				String strPeriodeArr = sesi.getSessionAttribute(Constants.PERIODE_ARR);
				JsonParser jsonParser = new JsonParser();
				Object object = jsonParser.parse(strPeriodeArr);
				periodeArr = (JsonArray) object;
				
//				jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/pembayaran/pembayaranTelkomTelp.jasper");
//				jasper = "/home/admin/data_web/report/pembayaran/pembayaranTelepon.jasper";
				jasper = rb.getString("url_report_telkom_telepon");
			} else if (tipe.equals("ESIA") || tipe.equals("INDOSAT") || tipe.equals("TELKOMSEL") || tipe.equals("XL")){
				ref = sesi.getSessionAttribute(Constants.ref);
//				namaProduk = sesi.getSessionAttribute("nama_produk");
				String tanggal = sesi.getSessionAttribute(Constants.waktu);
				String terbilang = sesi.getSessionAttribute(Constants.TERBILANG);
				String sn = sesi.getSessionAttribute("sn");
				
				map.put("biayaAdmin", biayaBayar);
				map.put("ref", ref);
				map.put("sn", sn);
				map.put("namaProduk", tipe);
				map.put("timeStamp", tanggal);
				map.put("terbilang", terbilang);
				
//				jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/pembayaran/pembayaranEsiaIndosatTelkomselXl.jasper");
//				jasper = rb.getString("url_report_pasca_hp_eitx");
				
				jasper = rb.getString("url_report_path");
				
				PulsaPascaReport pulsaPascaReport = new PulsaPascaReport();
				String fileReport = pulsaPascaReport.generateReport(jasper, map);
				
				File inFileName = new File(jasper+fileReport);
				
				FileInputStream fis = new FileInputStream(inFileName);
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				byte[] buf = new byte[1024];
				try {
		            for (int readNum; (readNum = fis.read(buf)) != -1;) {
		                bos.write(buf, 0, readNum); //no doubt here is 0
		            }
		        } catch (Exception ex) {
		        	ex.printStackTrace();
		        	log.error("", ex);
		        }
				
				AMedia amedia = new AMedia("Invoice_"+id_trx+"_"+id_pelanggan, "pdf", null, bos.toByteArray()); 
				ireport.setContent(amedia);
//				ireport.setSrc(inFileName.getPath());
				Wnd.setVisible(true);
				
				return;
			} else if (tipe.equals("FREN") || tipe.equals("SMART") || tipe.equals("THREE")){
				ref = sesi.getSessionAttribute(Constants.ref);
//				namaProduk = sesi.getSessionAttribute("nama_produk");
				String tanggal = sesi.getSessionAttribute(Constants.waktu);
				String terbilang = sesi.getSessionAttribute(Constants.TERBILANG);
				String sn = sesi.getSessionAttribute("sn");
				
//				if (sn.equals("")) {
//					sn = "no_reff";
//				}
				
				map.put("biayaAdmin", biayaBayar);
				map.put("ref", ref);
				map.put("sn", sn);
				map.put("namaProduk", tipe);
				map.put("timeStamp", tanggal);
				map.put("terbilang", terbilang);
				
//				jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/pembayaran/pembayaranFrenSmartThree.jasper");
//				jasper = rb.getString("url_report_pasca_hp_fst");
				
				jasper = rb.getString("url_report_path");
				
				PulsaPascaReport pulsaPascaReport = new PulsaPascaReport();
				String fileReport = pulsaPascaReport.generateReport(jasper, map);
				
				File inFileName = new File(jasper+fileReport);
				
				FileInputStream fis = new FileInputStream(inFileName);
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				byte[] buf = new byte[1024];
				try {
		            for (int readNum; (readNum = fis.read(buf)) != -1;) {
		                bos.write(buf, 0, readNum); //no doubt here is 0
		            }
		        } catch (Exception ex) {
		        	ex.printStackTrace();
		        	log.error("", ex);
		        }
				
				AMedia amedia = new AMedia("Invoice_"+id_trx+"_"+id_pelanggan, "pdf", null, bos.toByteArray()); 
				ireport.setContent(amedia);
//				ireport.setSrc(inFileName.getPath());
				Wnd.setVisible(true);
				
				return;
			} else if (tipe.equals("PDAM")){

				ref = sesi.getSessionAttribute(Constants.ref);
				namaProduk = sesi.getSessionAttribute("NamaArea");
				kodePdam = sesi.getSessionAttribute("kode_pdam");
				map.put("ref", ref);
				map.put("namaProduk", namaProduk);
				
				// struk tambahan
				String strPeriodeArr = sesi.getSessionAttribute(Constants.PERIODE_ARR);
				JsonParser jsonParser = new JsonParser();
				Object object = jsonParser.parse(strPeriodeArr);
				periodeArr = (JsonArray) object;
				
				String terbilang = sesi.getSessionAttribute(Constants.TERBILANG);					
				String jmlAdmin = sesi.getSessionAttribute(Constants.JUMLAH_ADMIN);				
				String standMeter = sesi.getSessionAttribute(Constants.STAND_METER);				
				String pdamName = sesi.getSessionAttribute(Constants.PDAM_NAME);				
				String noResi = sesi.getSessionAttribute(Constants.NO_RESI);					
				String nonTagAir = sesi.getSessionAttribute(Constants.NON_TAG_AIR);				
				String alamat = sesi.getSessionAttribute(Constants.ALAMAT);				
				String pemakaian = sesi.getSessionAttribute(Constants.PEMAKAIAN);				
				String jmlBayar = sesi.getSessionAttribute(Constants.JUMLAH_BAYAR);				
				String namaPelanggan = sesi.getSessionAttribute(Constants.NAMA_PELANGGAN);				
				String beban = sesi.getSessionAttribute(Constants.BEBAN);				
				String denda = sesi.getSessionAttribute(Constants.DENDA);				
				String tanggal = sesi.getSessionAttribute(Constants.TANGGAL);				
				String idPelanggan = sesi.getSessionAttribute(Constants.id_pelanggan);		
				String noSambungan = sesi.getSessionAttribute("nosambungan");		
				String jmlTagihan = sesi.getSessionAttribute(Constants.JUMLAH_TAGIHAN);				
				String blnThn = sesi.getSessionAttribute(Constants.periode);
				String nominalStruk = sesi.getSessionAttribute(Constants.NOMINAL_STRUK);
				String materai = sesi.getSessionAttribute("materai");
				
				int totalTagihan = Integer.parseInt(jmlAdmin) + Integer.parseInt(nominalStruk);
				
				map.put("tanggal", tanggal);
				map.put("noResi", noResi);
				map.put("namaProduk", namaProduk);
				map.put("idPelanggan", idPelanggan);
				map.put("noSambungan", noSambungan);
				map.put("namaPelanggan", namaPelanggan);
				map.put("alamat", alamat);
				map.put("pemakaian", pemakaian);
				map.put("denda", denda);
				map.put("beban", beban);
				map.put("tagNonAir", nonTagAir);
				map.put("biayaAdmin", jmlAdmin);
				map.put("totalTagihan", fung.getFormatRp(""+totalTagihan));
				map.put("terbilang", terbilang);
				map.put("materai", materai);
				map.put("detaillist", strPeriodeArr);
				
				System.out.println("**************************************** terbilang 3 : " + terbilang);
				
//				jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/pembayaran/pembayaranPdamNew.jasper");
//				jasper = "/home/admin/data_web/report/pembayaran/pembayaranPDAM.jasper";
				jasper = rb.getString("url_report_path");
				
				PdamReport pdamReport = new PdamReport();
				
				String fileReport = "";
				
//				if (namaProduk.toUpperCase().contains("AETRA") || namaProduk.toUpperCase().contains("JEMBER")) {
//					fileReport = pdamReport.generateReportAetra(jasper, map);
//				} else if (namaProduk.toUpperCase().contains("BANGKALAN") || namaProduk.toUpperCase().contains("BOGOR") || namaProduk.toUpperCase().contains("CILACAP") || namaProduk.toUpperCase().contains("SEMARANG") || namaProduk.toUpperCase().contains("SIDOARJO") || namaProduk.toUpperCase().contains("SITUBONDO") || namaProduk.toUpperCase().contains("TAPIN")  || namaProduk.toUpperCase().contains("PASURUAN")) {
//					fileReport = pdamReport.generateReportBangkalan(jasper, map);
//				} else if (namaProduk.toUpperCase().contains("BOJONEGORO")) {
//					fileReport = pdamReport.generateReportBojonegoro(jasper, map);
//				} else if (namaProduk.toUpperCase().contains("BONDOWOSO")) {
//					fileReport = pdamReport.generateReportBondowoso(jasper, map);
//				} else if (namaProduk.toUpperCase().contains("MOJOKERTO")) {
//					fileReport = pdamReport.generateReportMojokerto(jasper, map);
//				} else if (namaProduk.toUpperCase().contains("SURABAYA")) {
//					fileReport = pdamReport.generateReportSurabaya(jasper, map);
//				}
				
				log.info("kode pdam : " + kodePdam);
				
				if (kodePdam.equalsIgnoreCase("WAAETRA") || kodePdam.equalsIgnoreCase("WAJMBR") || 
						kodePdam.equalsIgnoreCase("WABEKASI") || kodePdam.equalsIgnoreCase("WAGIRIMM") || 
						kodePdam.equalsIgnoreCase("WAPLYJ") || kodePdam.equalsIgnoreCase("WAPLMBNG") || 
						kodePdam.equalsIgnoreCase("WABDG") || kodePdam.equalsIgnoreCase("WADEPOK") || 
						kodePdam.equalsIgnoreCase("WAJAMBI") || kodePdam.equalsIgnoreCase("WALMPNG") || 
						kodePdam.equalsIgnoreCase("WABOGOR")) {
					fileReport = pdamReport.generateReportAetra(jasper, map);
				} else if (kodePdam.equalsIgnoreCase("WABGK") || kodePdam.equalsIgnoreCase("WAKOBGR") || 
						kodePdam.equalsIgnoreCase("WACLCP") || kodePdam.equalsIgnoreCase("WASMG") || 
						kodePdam.equalsIgnoreCase("WASDA") || kodePdam.equalsIgnoreCase("WASITU") || 
						kodePdam.equalsIgnoreCase("WATAPIN") || kodePdam.equalsIgnoreCase("WAKOPASU") || 
						kodePdam.equalsIgnoreCase("WAKUBURAYA") || kodePdam.equalsIgnoreCase("WAKRWNG") || 
						kodePdam.equalsIgnoreCase("WABANJAR") || kodePdam.equalsIgnoreCase("WAGROGOT") || 
						kodePdam.equalsIgnoreCase("WAGROBGAN") || kodePdam.equalsIgnoreCase("WABALIKPPN") || 
						kodePdam.equalsIgnoreCase("WAMANADO") || kodePdam.equalsIgnoreCase("WAMAKASAR") || 
						kodePdam.equalsIgnoreCase("WAMEDAN") || kodePdam.equalsIgnoreCase("WAPONTI") || 
						kodePdam.equalsIgnoreCase("WADPSR") || kodePdam.equalsIgnoreCase("WAKLATEN") || 
						kodePdam.equalsIgnoreCase("WASLTIGA") || kodePdam.equalsIgnoreCase("WAWONOSB") || 
						kodePdam.equalsIgnoreCase("WAPURWORE") || kodePdam.equalsIgnoreCase("WAPROLING") || 
						kodePdam.equalsIgnoreCase("WAKABMLG") || kodePdam.equalsIgnoreCase("WAKABBDG") || 
						kodePdam.equalsIgnoreCase("WAMADIUN") || kodePdam.equalsIgnoreCase("WALOMBOKT") || 
						kodePdam.equalsIgnoreCase("WASLMN") || kodePdam.equalsIgnoreCase("WABYMS") || 
						kodePdam.equalsIgnoreCase("WABYL") || kodePdam.equalsIgnoreCase("WAREMBANG") || 
						kodePdam.equalsIgnoreCase("WAKNDL") || kodePdam.equalsIgnoreCase("WABULELENG") || 
						kodePdam.equalsIgnoreCase("WAPBLINGGA") || kodePdam.equalsIgnoreCase("WAKBMN") || 
						kodePdam.equalsIgnoreCase("WABREBES") || kodePdam.equalsIgnoreCase("WAKABSMG") || 
						kodePdam.equalsIgnoreCase("WASRAGEN") || kodePdam.equalsIgnoreCase("WAKARANGA") || 
						kodePdam.equalsIgnoreCase("WAKPKLNGAN") || kodePdam.equalsIgnoreCase("WAWONOGIRI") || 
						kodePdam.equalsIgnoreCase("WAIBANJAR") || kodePdam.equalsIgnoreCase("WAKOSOLO")) {
					fileReport = pdamReport.generateReportBangkalan(jasper, map);
				} else if (kodePdam.equalsIgnoreCase("WABJN")) {
					fileReport = pdamReport.generateReportBojonegoro(jasper, map);
				} else if (kodePdam.equalsIgnoreCase("WABONDO")) {
					fileReport = pdamReport.generateReportBondowoso(jasper, map);
				} else if (kodePdam.equalsIgnoreCase("WAMJK")) {
					fileReport = pdamReport.generateReportMojokerto(jasper, map);
				} else if (kodePdam.equalsIgnoreCase("WASBY")) {
					fileReport = pdamReport.generateReportSurabaya(jasper, map);
				}
				
				File inFileName = new File(jasper+fileReport);
				
				FileInputStream fis = new FileInputStream(inFileName);
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				byte[] buf = new byte[1024];
				try {
		            for (int readNum; (readNum = fis.read(buf)) != -1;) {
		                bos.write(buf, 0, readNum); //no doubt here is 0
		            }
		        } catch (Exception ex) {
		        	ex.printStackTrace();
		        	log.error("", ex);
		        }
				
				AMedia amedia = new AMedia("Invoice_"+id_trx+"_"+id_pelanggan, "pdf", null, bos.toByteArray()); 
				ireport.setContent(amedia);
//				ireport.setSrc(inFileName.getPath());
				Wnd.setVisible(true);
				
				return;
			
			} else if (tipe.equals("INTERNET") || tipe.equals("TELKOMVISION")){

				
				ref = sesi.getSessionAttribute(Constants.ref);
				String terbilang = sesi.getSessionAttribute(Constants.TERBILANG);
				String tanggal = sesi.getSessionAttribute(Constants.TANGGAL);
				String sn = sesi.getSessionAttribute("sn");
				String npwp = sesi.getSessionAttribute("npwp");
				String blnThn = sesi.getSessionAttribute("blnthn");
				
				// struk tambahan
				String strPeriodeArr = sesi.getSessionAttribute(Constants.PERIODE_ARR);
				JsonParser jsonParser = new JsonParser();
				Object object = jsonParser.parse(strPeriodeArr);
				periodeArr = (JsonArray) object;
				
				map.put("ref", ref);
				map.put("sn", sn);
				map.put("terbilang", terbilang);
				map.put("tanggal", tanggal);
				map.put("npwp", npwp);
				map.put("blnthn", blnThn);
				map.put("detaillist", strPeriodeArr);
				
//				jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/pembayaran/pembayaranInternet.jasper");
//				jasper = "/home/admin/data_web/report/pembayaran/pembayaranInternet.jasper";
				jasper = rb.getString("url_report_path");
				
				TelkomReport telkomReport = new TelkomReport();
				String fileReport = telkomReport.generateReport(jasper, map);
				
				File inFileName = new File(jasper+fileReport);
				
				FileInputStream fis = new FileInputStream(inFileName);
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				byte[] buf = new byte[1024];
				try {
		            for (int readNum; (readNum = fis.read(buf)) != -1;) {
		                bos.write(buf, 0, readNum); //no doubt here is 0
		            }
		        } catch (Exception ex) {
		        	ex.printStackTrace();
		        	log.error("", ex);
		        }
				
				AMedia amedia = new AMedia("Invoice_"+id_trx+"_"+id_pelanggan, "pdf", null, bos.toByteArray()); 
				ireport.setContent(amedia);
//				ireport.setSrc(inFileName.getPath());
				Wnd.setVisible(true);
				
				return;
			
			} else if (tipe.equals("TV")){

				ref = sesi.getSessionAttribute(Constants.ref);
				namaProduk = sesi.getSessionAttribute("nama_produk");
				String tanggal = sesi.getSessionAttribute(Constants.waktu);
				String terbilang = sesi.getSessionAttribute(Constants.TERBILANG);
				String productCategory = sesi.getSessionAttribute("product_category");
				String sn = sesi.getSessionAttribute("sn");
				map.put("biayaAdmin", biayaBayar);
//				if (sn.equals("")) {
//					sn = "no_reff";
//				}
				
				map.put("ref", ref);
				map.put("namaProduk", namaProduk);
				map.put("timeStamp", tanggal);
				map.put("terbilang", terbilang);
				
//				jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/pembayaran/pembayaranFrenSmartThree.jasper");
//				jasper = rb.getString("url_report_pasca_hp_fst");
				
				jasper = rb.getString("url_report_path");
				
				String fileReport = "";
				
				TvPascaReport tvPascaReport = new TvPascaReport();
				if (namaProduk.contains("BIG")) {
					map.put("timeStamp", func.dateFormatter("yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss", tanggal));
					fileReport = tvPascaReport.generateReport(jasper, map);
				} else if (namaProduk.contains("INDOVISION") || namaProduk.contains("OKEVISION") || namaProduk.equalsIgnoreCase("TOP TV")) {
					fileReport = tvPascaReport.generateReportIndovision(jasper, map);
				} else if (namaProduk.contains("INNOVATE")) {
					map.put("timeStamp", func.dateFormatter("yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss", tanggal));
					map.put("sn", sn);
					fileReport = tvPascaReport.generateReportInnovate(jasper, map);
				} else if (namaProduk.contains("NEX")) {
					map.put("productCategory", productCategory);
					map.put("sn", sn);
					fileReport = tvPascaReport.generateReportNex(jasper, map);
				} else if (namaProduk.contains("TOPAS")) {
					map.put("sn", sn);
					fileReport = tvPascaReport.generateReportTopas(jasper, map);
				} else if (namaProduk.contains("ORANGE")) {
					map.put("sn", sn);
					fileReport = tvPascaReport.generateReportOrange(jasper, map);
				} else if (namaProduk.contains("K VISION")) {
					map.put("sn", sn);
					fileReport = tvPascaReport.generateReportKvision(jasper, map);
				} else if (namaProduk.contains("SKYNINDO")) {
					map.put("sn", sn);
					map.put("paket", sesi.getSessionAttribute("nominal_label"));
					map.put("jatuh_tempo", sesi.getSessionAttribute("jatuh_tempo"));
					map.put("denom", hargaCetak);
					fileReport = tvPascaReport.generateReportSkynindo(jasper, map);
				} else if (namaProduk.contains("TELKOM")) {
					map.put("sn", sn);
					map.put("tanggal", tanggal);
					
					String npwp = sesi.getSessionAttribute("npwp");
					String blnThn = sesi.getSessionAttribute("blnthn");
					
					// struk tambahan
					String strPeriodeArr = sesi.getSessionAttribute(Constants.PERIODE_ARR);
					JsonParser jsonParser = new JsonParser();
					Object object = jsonParser.parse(strPeriodeArr);
					periodeArr = (JsonArray) object;
					
					map.put("npwp", npwp);
					map.put("blnthn", blnThn);
					map.put("detaillist", strPeriodeArr);
					
					TelkomReport telkomReport = new TelkomReport();
					fileReport = telkomReport.generateReport(jasper, map);
				} 
				
				
				File inFileName = new File(jasper+fileReport);
				
				FileInputStream fis = new FileInputStream(inFileName);
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				byte[] buf = new byte[1024];
				try {
		            for (int readNum; (readNum = fis.read(buf)) != -1;) {
		                bos.write(buf, 0, readNum); //no doubt here is 0
		            }
		        } catch (Exception ex) {
		        	ex.printStackTrace();
		        	log.error("", ex);
		        }
				
				AMedia amedia = new AMedia("Invoice_"+id_trx+"_"+id_pelanggan, "pdf", null, bos.toByteArray()); 
				ireport.setContent(amedia);
//				ireport.setSrc(inFileName.getPath());
				Wnd.setVisible(true);
				
				return;
			
			} else if (tipe.equals("MULTIFINANCE")){

				ref = sesi.getSessionAttribute(Constants.ref);
				namaProduk = sesi.getSessionAttribute("nama_produk");
				String tanggal = sesi.getSessionAttribute(Constants.waktu);
				String terbilang = sesi.getSessionAttribute(Constants.TERBILANG);
				String namaOperator = sesi.getSessionAttribute("nama_operator");
				map.put("biayaAdmin", biayaBayar);
//				if (sn.equals("")) {
//					sn = "no_reff";
//				}
				
				map.put("ref", ref);
				map.put("namaProduk", namaOperator);
				map.put("timeStamp", tanggal);
				map.put("terbilang", terbilang);
				
				map.put("ref2", sesi.getSessionAttribute("ref2"));
				
//				jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/pembayaran/pembayaranFrenSmartThree.jasper");
//				jasper = rb.getString("url_report_pasca_hp_fst");
				
				jasper = rb.getString("url_report_path");
				
				String fileReport = "";
				
				MultiFinanceReport report = new MultiFinanceReport();
				if (namaOperator.trim().equalsIgnoreCase("Adira Finance") || 
						namaOperator.trim().equalsIgnoreCase("PT MEGA AUTO FINANCE") || 
						namaOperator.trim().equalsIgnoreCase("PT MEGA CENTRAL FINANCE") || 
						namaOperator.trim().equalsIgnoreCase("WOM Finance")) {
					map.put("pinalty", sesi.getSessionAttribute("pinalty"));
					map.put("tagihan_non_pinalty", sesi.getSessionAttribute("tagihan_non_pinalty"));
					fileReport = report.generateReport(jasper, map);
				} else if (namaOperator.trim().equalsIgnoreCase("COLUMBIA")) {
					map.put("pinalty", sesi.getSessionAttribute("pinalty"));
					map.put("tagihan_non_pinalty", sesi.getSessionAttribute("tagihan_non_pinalty"));
					map.put("tenor", sesi.getSessionAttribute("tenor"));
					map.put("sn", sesi.getSessionAttribute("sn"));
					map.put("timeStamp", func.dateFormatter("yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm:ss", tanggal));
					fileReport = report.generateReportColumbia(jasper, map);
				} else if (namaOperator.trim().equalsIgnoreCase("Bussan Auto Finance")) {
					map.put("tagihan_non_pinalty", sesi.getSessionAttribute("tagihan_non_pinalty"));
					map.put("tenor", sesi.getSessionAttribute("tenor"));
					map.put("sn", sesi.getSessionAttribute("sn")); // no pol
					map.put("tipe_motor", sesi.getSessionAttribute("tipe_motor"));
					map.put("jatuh_tempo", sesi.getSessionAttribute("jatuh_tempo"));
					map.put("total", hargaCetak);
					fileReport = report.generateReportBaf(jasper, map);
				}
				
				File inFileName = new File(jasper+fileReport);
				
				FileInputStream fis = new FileInputStream(inFileName);
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				byte[] buf = new byte[1024];
				try {
		            for (int readNum; (readNum = fis.read(buf)) != -1;) {
		                bos.write(buf, 0, readNum); //no doubt here is 0
		            }
		        } catch (Exception ex) {
		        	ex.printStackTrace();
		        	log.error("", ex);
		        }
				
				AMedia amedia = new AMedia("Invoice_"+id_trx+"_"+id_pelanggan, "pdf", null, bos.toByteArray()); 
				ireport.setContent(amedia);
//				ireport.setSrc(inFileName.getPath());
				Wnd.setVisible(true);
				
				return;
			
			} else if (tipe.equals("BPJSKESEHATAN")){

				ref = sesi.getSessionAttribute(Constants.ref);
				waktu = sesi.getSessionAttribute(Constants.waktu);
				noHp = sesi.getSessionAttribute(Constants.handPhone);
				map.put("ref", ref);
				map.put("timeStamp", waktu);
				map.put("noHp", noHp);
				
//				jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/pembayaran/pembayaranBpjsNew.jasper");
//				jasper = "/home/admin/data_web/report/pembayaran/pembayaranTVKABEL.jasper";
				jasper = rb.getString("url_report_pasca_bpjskes");
			
			} else {
//				jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/pembelian/pembelianBpjs.jasper");
//				jasper = "/home/admin/data_web/report/pembelian/pembelianPULSA.jasper";
				jasper = rb.getString("url_report_pasca_other");
			}
			System.out.println("jasper path -> "+jasper);
			File inFileName = new File(jasper);
			
			// testing untuk pdam
						JasperPrint print = new JasperPrint();
						
						if (tipe.equals("TELKOM")) {
							// open cn
							Connection conn = null;
							try {
								DataSourceConnectionPostgres.getInstance();
					            DataSource ds = DataSourceConnectionPostgres.getDataSource();
					            conn = ds.getConnection();
								
								// query get periode -- amount
					            String sql = "";
					            int size = periodeArr.size();
					            for (int i=0; i<size; i++) {
//					            	JSONObject child  = (JSONObject) data.get(i);
					            	JsonObject child = (JsonObject) periodeArr.get(i);
					            	
					            	sql += "SELECT '"+child.get("period").getAsString()+"' AS bl_thn, '"+fung.getFormatRp(child.get("total").getAsString())+"' AS amount ";
					            	if (i < (size-1)) {
					            		sql += "UNION ALL ";
					            	}
					            	
					            }
					            System.out.println("****************************************** sql : " + sql);
					            PreparedStatement ps = conn.prepareStatement(sql);
					            ResultSet rs = ps.executeQuery();
					            
								print = JasperFillManager.fillReport(inFileName.getPath(), map, new JRResultSetDataSource(rs));
								
								rs.close();
								ps.close();
							} catch (Exception e) {
								e.printStackTrace();
							} finally {
								if (!conn.isClosed()) conn.close();
							}
						} else {
							print = JasperFillManager.fillReport(inFileName.getPath(), map, new JREmptyDataSource());
						}
						
//			JasperPrint print = JasperFillManager.fillReport(inFileName.getPath(), map, new JREmptyDataSource());
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			JasperExportManager.exportReportToPdfStream(print, outStream); 
			AMedia amedia = new AMedia("Invoice_"+id_trx+"_"+id_pelanggan, "pdf", null, outStream.toByteArray()); 
			ireport.setContent(amedia);
			Wnd.setVisible(true);
		}
		
		public void setBack(Window wnd) throws Exception {
			
			mob_Session sesi = new mob_Session();
			String username = sesi.getUsername();
			if (username.startsWith("0")){
				RedirectMenu menu = new RedirectMenu();
				menu.setMenuLink("inforekeningmember",wnd);
			} else {
				RedirectMenu menu = new RedirectMenu();
				menu.setMenuLink("inforekeningagen",wnd);
			}
			
		}
}
