package com.truemoneywitami.menu;

import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class TransaksiGagal extends Window{

	ZKFunction func = new ZKFunction();
	mob_Session sesi = new mob_Session();
	public void setViewData(Window wnd){
		try {
			if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("setorTunai")) {
				func.setLabelValue(wnd, "headerTransaksiGagal", "Transaksi Setor Tunai");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			} else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("tarikTunai")) {
				func.setLabelValue(wnd, "headerTransaksiGagal", "Transaksi Tarik Tunai");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			} else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("TransferTrue")) {
				func.setLabelValue(wnd, "headerTransaksiGagal", "Transfer True ke True");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("TransferBank")) {
				func.setLabelValue(wnd, "headerTransaksiGagal", "Transfer True ke Bank");
				func.setLabelValue(wnd, "pesan", sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("CairkanSaldo")) {
				func.setLabelValue(wnd, "headerTransaksiGagal", "Cairkan Saldo");
				func.setLabelValue(wnd, "pesan", sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("TransferPos")) {
				func.setLabelValue(wnd, "headerTransaksiGagal", "Transfer True ke POS");
				func.setLabelValue(wnd, "pesan", sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembayaranPLN")) {
				func.setLabelValue(wnd, "headerTransaksiGagal", "Pembayaran PLN");
				func.setLabelValue(wnd, "pesan", sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembayaranPDAM")) {
				func.setLabelValue(wnd, "headerTransaksiGagal", "Pembayaran PDAM");
				func.setLabelValue(wnd, "pesan", sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembayaranTelepon")) {
				func.setLabelValue(wnd, "headerTransaksiGagal", "Pembayaran Telepon");
				func.setLabelValue(wnd, "pesan", sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembayaranInternet")) {
				func.setLabelValue(wnd, "headerTransaksiGagal", "Pembayaran Internet");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembayaranBpjs")) {
				func.setLabelValue(wnd, "headerTransaksiGagal", "Pembayaran BPJS");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembayaranPulsaPasca")) {
				func.setLabelValue(wnd, "headerTransaksiGagal", "Pembayaran Pulsa Pascabayar");
				func.setLabelValue(wnd, "pesan", sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembayaranTV")) {
				func.setLabelValue(wnd, "headerTransaksiGagal", "Pembayaran TV");
				func.setLabelValue(wnd, "pesan", sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("deposit")){
				func.setLabelValue(wnd, "headerTransaksiGagal", "Tambah Deposit");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembelianPulsa")){
				func.setLabelValue(wnd, "headerTransaksiGagal", "Pembelian Pulsa");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			}
			else if (sesi.getSessionAttribute(Constants.tipePesan).equalsIgnoreCase("PembelianPLN")){
				func.setLabelValue(wnd, "headerTransaksiGagal", "Pembelian Token PLN");
				func.setLabelValue(wnd, "pesan",sesi.getSessionAttribute(Constants.pesan));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
}
	
	public void setBack(Window wnd) throws Exception {
		
		mob_Session sesi = new mob_Session();
		String username = sesi.getUsername();
		if (username.startsWith("0")){
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("inforekeningmember",wnd);
		} else {
			RedirectMenu menu = new RedirectMenu();
			menu.setMenuLink("inforekeningagen",wnd);
		}
		
	}
}