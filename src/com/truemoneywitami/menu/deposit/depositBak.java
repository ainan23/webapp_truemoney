package com.truemoneywitami.menu.deposit;

import java.util.regex.Pattern;

import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class depositBak extends Window {

	ZKFunction func = new ZKFunction();
	mob_Session sesi = new mob_Session();
//	Pattern pat = Pattern.compile(".*[0-9].*");
	
	public void setData(Window wnd,Window header) throws Exception {
		try {
		String amount = func.getTextboxValue(wnd,"nominal");
		
		if (amount.equals("")){
			String pesan ="Silahkan Masukan Jumlah Nominal";
			Clients.showNotification(pesan, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
		else {
			if (Integer.parseInt(amount) > Constants.maxamount){
				String pesan = "Maximal Jumlah Transfer Rp" + func.formatNumber(Constants.maxamount, "###,###");
				Clients.showNotification(pesan, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else if (Integer.parseInt(amount) < Constants.minamount){
				String pesan ="Minimal Jumlah Transfer Rp" + func.formatNumber(Constants.minamount, "###,###");
				Clients.showNotification(pesan, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
			else {
				sesi.setSessionAttribute("nominal",amount);
				RedirectMenu menu = new RedirectMenu();
				//sesi.setWindowAttribute(Constants.body, wnd);
				menu.setMenuLink("konfirmasideposit",header);
			}
		}	
		} catch (Exception e){
			String pesan ="Format Nominal Tidak Valid";
			Clients.showNotification(pesan, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			//e.printStackTrace();
		}
	}
		
}
