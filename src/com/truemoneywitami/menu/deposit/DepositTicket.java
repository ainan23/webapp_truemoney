package com.truemoneywitami.menu.deposit;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.json.parser.JSONParser;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Window;

import com.sun.media.jfxmedia.events.NewFrameEvent;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

public class DepositTicket extends Window {

	Logger log = Logger.getLogger(DepositTicket.class);
	ZKFunction func = new ZKFunction();
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	// String username = "0852235455";
	int total;
	JSONArray arr = new JSONArray();
	JSONParser parser = new JSONParser();
	
	String nomi="",
	ex="",
	jasper="";
	
	HashMap map;
	public void setData(Window wnd) {
		try {
		String x = sesi.getSessionAttribute("array");
		nomi = sesi.getSessionAttribute("amount");
		ex = sesi.getSessionAttribute("expired");
		String op = sesi.getSessionAttribute("statusOpen");
		arr = (JSONArray) parser.parse(x);
		func.setLabelValue(wnd, "nominal","Rp." + nomi);
		SimpleDateFormat df = new SimpleDateFormat("dd MMMM YYYY HH:mm");
		Calendar cal = Calendar.getInstance();
		cal.add(cal.HOUR, 3);
		String jam = df.format(cal.getTime());;
		func.setLabelValue(wnd, "limit", ex);
//		int a = arr.size();
//		for (int i=0;i<a;i++){
//			String y = String.valueOf(i+1);
//			JSONObject child  = (JSONObject) arr.get(i);
//			func.setLabelValue(wnd, "bank"+y, child.get("namaBank").toString());
//			func.setLabelValue(wnd, "norek"+y, child.get("nomorRekening").toString());
//			
//		}
		//func.setLabelValue(wnd, "nama1", "PT.Witami Tunai Mandiri");
		//func.setLabelValue(wnd, "nama2", "PT.Witami Tunai Mandiri");
		} catch(Exception e) {
			log.info("Error di Deposit " + e.getMessage());
		}
	}

	public void cetakStruk(Window Wnd) throws Exception {

		map = new HashMap();
		map.put("nominal", func.formatRp(nomi));
		map.put("expired", ex);
		map.put("id_trx", sesi.getSessionAttribute(Constants.id_trx));

		Iframe ireport = (Iframe) Wnd.getFellow("ireport");

		jasper = Sessions.getCurrent().getWebApp().getRealPath("/report/transfer/tambahDeposit.jasper");
		System.out.println("jasper path -> " + jasper);
		File inFileName = new File(jasper);
		JasperPrint print = JasperFillManager.fillReport(inFileName.getPath(), map, new JREmptyDataSource());
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		JasperExportManager.exportReportToPdfStream(print, outStream);
		AMedia amedia = new AMedia("Invoice_" + nomi + "_" + ex, "pdf", null, outStream.toByteArray());
		ireport.setContent(amedia);
		Wnd.setVisible(true);

	}

}
