package com.truemoneywitami.menu.deposit;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.transferpos.TruePos;

public class depositKonfirm extends Window implements RequestListener{
	
	Logger log = Logger.getLogger(depositKonfirm.class);
	ZKFunction func = new ZKFunction();
	CheckParameter check = new CheckParameter();
	mob_Session sesi = new mob_Session();
	String nominal; 
	String username = sesi.getUsername();
	//String username="0852235455";
	JSONObject obj;
	JSONArray arr;
	Window win;
	Window winheader;
	int count =0;
	String block="";
	String statusAccount="",
	total="",
	list="",
	expired="",
	statusOpen="",
	ack="";
	String send_trx = "";
	String id_trx="";
	
	@Override
	public void requestSucceeded(JSONObject obj) {
		
		if (obj.get("ACK").equals("OK") && obj.get("ID").equals("status")) {
			//PINMatch = obj.get("PIN").toString();
			statusAccount = obj.get("statusAccount").toString();
		}
		else if (obj.get("ACK").equals("OK") && obj.get("ID").equals("deposit")) {
			total = obj.get("nominal").toString();
			list =  obj.get("listBank").toString();
			expired = obj.get("expiredHour").toString();
			statusOpen = obj.get("StatusTicket").toString();
			ack = obj.get("ACK").toString();
			id_trx = obj.get("id_trx").toString();
		}
		else if (obj.get("ACK").equals("OK") && obj.get("ID").equals("Block")){
			block = obj.get("ACK").toString();
		}
	}

	@Override
	public void requestFailed(String message) {
		try {
		Window wnd = getWindow();
		if (message.contains("PIN")) {
//			Clients.showNotification("PIN Anda Salah. Silahkan coba kembali");
			Clients.showNotification("PIN Anda Salah. Silahkan coba kembali", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			count++;
			if (count >3){
				JSONObject obj = new JSONObject();
				obj.put("Tipe", "BlockAccount");
				obj.put("username", username);
				ProcessingListener processing = new ProcessingListener(this, obj.toString());
				processing.processData();
				if (block.equals("OK")){
//					Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami");
					Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
					func.setTextboxDisabled(wnd, "pin", true);
				}
				else {
					System.out.println("Akun Gagal di Blokir");
				}
				
			}
			else {
				System.out.println(count);
			}
		}
		else if (message.contains("blokir") && count ==3){
			count++;
//			Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami");
			Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			func.setTextboxDisabled(wnd, "pin", true);
		}
		else if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
//			Constants.pesan = message;
//			Constants.tipePesan = "deposit";
			sesi.setSessionAttribute(Constants.tipePesan,"deposit");
			sesi.setSessionAttribute(Constants.pesan,message);
			new RedirectMenu().setMenuLink("transfergagal",this.winheader);
		}
		} catch (Exception e){
			System.out.println(e.getMessage());
		}
	}
	
	
	
	public void setData(Window wnd, Window header) {
		try {
		//Window wnd = sesi.getWindowAttribute(Constants.body);
		nominal = sesi.getSessionAttribute("nominal");
		func.setLabelValue(wnd, "amount",func.formatRp(nominal));
		setWindow(wnd);
		setMenuWindow(header);
		} catch (Exception e) {
			//
		}
	}
	
	public void setWindow (Window wnd) {
		this.win = wnd;
	}
	
	
	public Window getWindow() {
		return this.win;
	}
	
	public void setMenuWindow(Window header){
		this.winheader = header;
	}
	
	public Window getMenuWindow(){
		return this.winheader;
	}
	
	public void doPayment(Window wnd)  {
		try {
		String pin = func.getTextboxValue(wnd, "pin");
		
		if(check.isKosong(pin)) {
//			Clients.showNotification("PIN tidak boleh kosong");
			Clients.showNotification("PIN tidak boleh kosong", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else if (!(check.pinIsValid(pin))) {
//			Clients.showNotification("PIN tidak valid, Silahkan coba kembali.");
			Clients.showNotification("PIN tidak valid, Silahkan coba kembali.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else {
			JSONObject obj = new JSONObject();
			send_trx = func.getIDTRX();
			if (username.startsWith("0")){
				obj.put("Tipe", "getStatusAccountIndividualCashIn");
			}else {
				obj.put("Tipe", "getStatusTambahDeposit");
			}
			
			obj.put("username", username);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
				
				if (statusAccount.equalsIgnoreCase("active") || statusAccount.equalsIgnoreCase("inactive") || statusAccount.equalsIgnoreCase("dormant")){
					obj = new JSONObject();
					if (username.startsWith("0")){
						obj.put("Tipe", "generateTicketIndividualCashIn");
					}
					else {
						obj.put("Tipe", "generateTicketTambahDeposit");
					}
					
					obj.put("username", username);
					obj.put("nominal", nominal);
					obj.put("PIN", pin);
					obj.put("id_TipeAplikasi", 2);
					obj.put("id_trx", send_trx);
					processing = new ProcessingListener(this, obj.toString());
					processing.processData();
					if (ack.equals("OK")){
						sesi.setSessionAttribute("username",username);
						sesi.setSessionAttribute("array",list);
						sesi.setSessionAttribute("amount",total);
						sesi.setSessionAttribute("expired", expired);
						sesi.setSessionAttribute("statusOpen", statusOpen);
						sesi.setSessionAttribute(Constants.id_trx, id_trx);
						sesi.setWindowAttribute(Constants.body, wnd);
						RedirectMenu menu = new RedirectMenu();
						new RedirectMenu().setMenuLink("deposittiket",this.winheader);
					}
				} else if (statusAccount.equalsIgnoreCase("UNVERIFIED")) {
					String pesan = "Account Anda tidak bisa melakukan layanan ini.";
					sesi.setSessionAttribute(Constants.tipePesan,"deposit");
					sesi.setSessionAttribute(Constants.pesan,pesan);
					new RedirectMenu().setMenuLink("transfergagal",this.winheader);
		        }
				else {
//					func.setTextboxDisabled(wnd, "pin", true);
//					Clients.showNotification("Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami");
					String pesan = "Maaf, Akun Anda Terblokir. Silahkan hubungi customer care kami";
					sesi.setSessionAttribute(Constants.tipePesan,"deposit");
					sesi.setSessionAttribute(Constants.pesan,pesan);
					new RedirectMenu().setMenuLink("transfergagal",this.winheader);
				}
		}
		
		}
		catch (Exception e) {
			log.info("Error di Deposit" +e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void setBack(Window wnd) throws Exception {
		
		RedirectMenu menu = new RedirectMenu();
		menu.setMenuLink("deposit",this.winheader);
	}
	}

