package com.truemoneywitami.menu.deposit;


import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class depositVa extends Window implements RequestListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	mob_Session sesi = new mob_Session();
//	Pattern pat = Pattern.compile(".*[0-9].*");
	
	private String ack = "NOK";
	private String va = "NULL";
	private String nama = "NULL";
	
	public void setData(Window wnd) {
//		try {
//		String amount = func.getTextboxValue(wnd,"nominal");
//		
//		if (amount.equals("")){
//			String pesan ="Silahkan Masukan Jumlah Nominal";
//			Clients.showNotification(pesan, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
//		}
//		else {
//			if (Integer.parseInt(amount) > Constants.maxamount){
//				String pesan = "Maksimal Jumlah Nominal Rp" + func.formatNumber(Constants.maxamount, "###,###");
//				Clients.showNotification(pesan, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
//			} else if (Integer.parseInt(amount) < Constants.minamount){
//				String pesan ="Minimal Jumlah Nominal Rp" + func.formatNumber(Constants.minamount, "###,###");
//				Clients.showNotification(pesan, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
//			}
//			else {
//				sesi.setSessionAttribute("nominal",amount);
//				RedirectMenu menu = new RedirectMenu();
//				//sesi.setWindowAttribute(Constants.body, wnd);
//				menu.setMenuLink("konfirmasideposit",header);
//			}
//		}	
//		} catch (Exception e){
//			String pesan ="Format Nominal Tidak Valid";
//			Clients.showNotification(pesan, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
//			//e.printStackTrace();
//		}
		
		try {
			String username = sesi.getUsername();
			
			JSONObject obj = new JSONObject();
			obj.put("Tipe", "getVa");
			obj.put("username", username);
			
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			
			func.setLabelValue(wnd, "va", va);
			func.setLabelValue(wnd, "nama", nama);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void requestSucceeded(JSONObject data) {
		// TODO Auto-generated method stub
		ack = data.get("ACK").toString();
		try {
			nama = data.get("nama").toString();
		} catch (Exception e) {
			nama = "NULL";
		}
		
		try {
			va = data.get("va").toString();
		} catch (Exception e) {
			va = "NULL";
		}
		
		
	}

	@Override
	public void requestFailed(String message) {
		// TODO Auto-generated method stub
		
	}
		
}
