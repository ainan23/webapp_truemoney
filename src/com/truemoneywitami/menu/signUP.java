package com.truemoneywitami.menu;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.Cookies;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;

public class signUP extends Window implements RequestListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static Logger log = Logger.getLogger(signUP.class);
	
	ZKFunction func = new ZKFunction();
	CheckParameter check = new CheckParameter();
	JSONObject obj = new JSONObject();
	Cookies cookie = new Cookies();
	JSONArray arrkel, arrkec, arrkot;
	boolean isValid = false;
	Window win;
	JSONArray arrbank;
	String kelSelected, kecSelected;
	String ack = "", tipeUser = "", givemail = "", givname = "", givaccount = "";
	String nama, ktp, jk, telp, Hp, tgl, tempat, alamat, RT, RW, kel1, kec, kota, prov, pos, work, agama, status, edu,
			bank="-", norek, nopass, tgl2, npwp, ibu, waris, warisadd, waristelp, warisHP, email, pass, repass = "";
	Pattern pattern;
	Matcher matcher;

	public void daftar(Window wnd) throws Exception {
		try {
			setWindow(wnd);
			nama = func.getTextboxValue(wnd, "nama");
			ktp = func.getTextboxValue(wnd, "ktp");
			jk = func.getTextboxValue(wnd, "jk");
			tempat = func.getTextboxValue(wnd, "tempat");
			tgl = func.getDateboxText(wnd, "tgl");
			telp = func.getTextboxValue(wnd, "telp");
			Hp = func.getTextboxValue(wnd, "HP");
			alamat = func.getTextboxValue(wnd, "alamat");
			RT = func.getTextboxValue(wnd, "RT");
			RW = func.getTextboxValue(wnd, "RW");
			kota = func.getTextboxValue(wnd, "kota");
			prov = func.getTextboxValue(wnd, "prov");
			pos = func.getTextboxValue(wnd, "pos");
			work = func.getTextboxValue(wnd, "work");
			agama = func.getTextboxValue(wnd, "agama");
			status = func.getTextboxValue(wnd, "status");
			edu = func.getTextboxValue(wnd, "edu");
			norek = func.getTextboxValue(wnd, "norek");
			nopass = func.getTextboxValue(wnd, "nopass");
			tgl2 = func.getDateboxText(wnd, "tgl2");
			npwp = func.getTextboxValue(wnd, "npwp");
			ibu = func.getTextboxValue(wnd, "ibu");
			waris = func.getTextboxValue(wnd, "waris");
			warisadd = func.getTextboxValue(wnd, "warisadd");
			waristelp = func.getTextboxValue(wnd, "waristel");
			warisHP = func.getTextboxValue(wnd, "warishp");
			email = func.getTextboxValue(wnd, "email");
			pass = func.getTextboxValue(wnd, "pass");
			repass = func.getTextboxValue(wnd, "repass");
			
			if (check.checkAlpha(pass) && check.checkNumber(pass)) {
				
			} else {
				Messagebox.show("Password harus memiliki kombinasi huruf dan angka", "TMN", Messagebox.OK, Messagebox.ERROR);
				return;
			}
			
			String bank = "";
			try {
				bank = func.getListboxLabel(wnd, "bank");
			} catch(Exception e) {
				log.info("error saat get nama bank");
				log.info("nilai nama bank -> " + bank);
			}
			
			if(func.getRadioValue(wnd, "rw4")) {
				if(check.isKosong(work)) {
					func.setTextboxFocus(wnd, "work");
					Messagebox.show("Pekerjan Lainnya tidak boleh kosong", "Truemoney", Messagebox.OK, Messagebox.ERROR);
					return;
				}
			}
			
//			if(func.getRadioValue(wnd, "re8")) {
//				if(check.isKosong(edu)) {
//					func.setTextboxFocus(wnd, "edu");
//					Messagebox.show("Pendidikan Terakhir Lainnya tidak boleh kosong", "Truemoney", Messagebox.OK, Messagebox.ERROR);
//					return;
//				}
//			}

			Cookies.setCookie("Tipe", "RegisterMember");
			Cookies.setCookie("noHp", Hp);
			Cookies.setCookie("password", pass);
			Cookies.setCookie("namaLengkap", nama);
			Cookies.setCookie("noKTP", ktp);
			Cookies.setCookie("email", email);
			Cookies.setCookie("jenisKelamin", jk);
			Cookies.setCookie("tempatLahir", tempat);
			Cookies.setCookie("tglLahir", tgl);
			Cookies.setCookie("noTelepon", telp);
			Cookies.setCookie("alamat", alamat);
			Cookies.setCookie("rt", RT);
			Cookies.setCookie("rw", RW);
			Cookies.setCookie("kelurahan", kel1);
			Cookies.setCookie("kecamatan", kec);
			Cookies.setCookie("kota", kota);
			Cookies.setCookie("provinsi", prov);
			Cookies.setCookie("kodePos", pos);
			Cookies.setCookie("agama", agama);
			Cookies.setCookie("pekerjaan", work);
			Cookies.setCookie("statusPernikahan", status);
			Cookies.setCookie("pendidikanTerakhir", edu);
			Cookies.setCookie("ibuKandung", ibu);
			Cookies.setCookie("ahliWarisNama", waris);
			Cookies.setCookie("ahliWarisNoTelp", waristelp);
			Cookies.setCookie("ahliWarisNoHP", warisHP);
			Cookies.setCookie("ahliWarisAlamat", warisadd);
			Cookies.setCookie("namaBank", bank);
			Cookies.setCookie("noPaspor", nopass);
			Cookies.setCookie("tanggalBerlakuPaspor", tgl2);
			Cookies.setCookie("noRekening", norek);
			Cookies.setCookie("noNPWP", npwp);
			
			System.out.println("!!!!masuk Validasi Pass OK!!!!!!!");
			String Ibank = "False", Inpwp = "False", Ipassport = "False";
			if (func.getRadioValue(wnd, "rb1")) {
				Ibank = "True";
			}
			if (func.getRadioValue(wnd, "rnp1")) {
				Inpwp = "True";
			}
			if (func.getRadioValue(wnd, "rp1")) {
				Ipassport = "True";
			}

			JSONObject obj = new JSONObject();
			obj.put("Tipe", "RegisterMember");
			obj.put("noHp", Hp);
			obj.put("password", pass);
			obj.put("namaLengkap", nama);
			obj.put("noKTP", ktp);
			obj.put("email", email);
			obj.put("jenisKelamin", jk);
			obj.put("tempatLahir", tempat);
			obj.put("tglLahir", tgl);
			obj.put("noTelepon", telp);
			obj.put("alamat", alamat);
			obj.put("rt", RT);
			obj.put("rw", RW);
			obj.put("kelurahan", kel1);
			obj.put("kecamatan", kec);
			obj.put("kota", kota);
			obj.put("provinsi", prov);
			obj.put("kodePos", pos);
			obj.put("agama", agama);
			obj.put("pekerjaan", work);
			obj.put("statusPernikahan", status);
			obj.put("pendidikanTerakhir", edu);
			obj.put("ibuKandung", ibu);
			obj.put("ahliWarisNama", waris);
			obj.put("ahliWarisNoTelp", waristelp);
			obj.put("ahliWarisNoHP", warisHP);
			obj.put("ahliWarisAlamat", warisadd);
			obj.put("namaBank", bank);
			obj.put("noPaspor", nopass);
			obj.put("tanggalBerlakuPaspor", tgl2);
			obj.put("IsiBank", Ibank);
			obj.put("noRekening", norek);
			obj.put("IsiPaspor", Ipassport);
			obj.put("IsiNPWP", Inpwp);
			obj.put("noNPWP", npwp);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			if (ack.equalsIgnoreCase("OK")) {
				Executions.sendRedirect(Constants.signup);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
	}
	
	public void setData(Window wnd) throws Exception {
		try {
				JSONObject obj2 = new JSONObject();
				obj2.put("Tipe","BankData");
				ProcessingListener processing = new ProcessingListener(this, obj2.toString());
				processing.processData();
				setListboxValue(wnd,"bank", arrbank, 0, true, true, true);
				System.out.println("Mulai ngeSet data"); 
		} catch (Exception e) {
			e.getMessage();
		}
	}
	
	public void setListboxValue(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID) throws Exception {
		try{
			Listbox list = (Listbox) wnd.getFellow(idList);
			list.getItems().clear();
			int a = data.size();
			for (int i =0;i<a;i++){
				Listitem li = new Listitem();
				JSONObject child  = (JSONObject) data.get(i);
				li.setValue(child.get("kode").toString());
				String nama = child.get("nama").toString();
				li.appendChild(new Listcell(nama));
				list.appendChild(li);
				
				if (i == 0) {
					li.setSelected(true);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void loadKelurahan(Window wnd) {
		String namaKelurahan = func.getTextboxValue(wnd, "listsearchkel");
		try {
			if (namaKelurahan.trim().length() <4){
//				Clients.showNotification("Minimal 4 Karakter");
			}else {
				obj.put("Tipe", "inquiryKelurahan");
				obj.put("namaKelurahan", namaKelurahan);
				ProcessingListener processing = new ProcessingListener(this, obj.toString());
				processing.processData();
				setListboxValueKel(wnd, "kel", arrkel, 0, true, true, true);
			}
		}catch (Exception e){
		}
	}

	public void loadKecamatan(Window wnd, Bandbox bd) throws Exception {
		try {
			Bandbox bkec = (Bandbox) wnd.getFellow("bandkec");
			bkec.setDisabled(false);
			
			bkec.setReadonly(true);
			bkec.setText("");
			func.setTextboxResetValue(wnd, "kota");
			func.setTextboxResetValue(wnd, "prov");
			
			kelSelected = func.getListboxValue(wnd, "kel");
			kel1 = kelSelected;
			bd.setValue(kelSelected);
			obj.put("Tipe","inquiryKecamatan");
			obj.put("namaKelurahan", kelSelected);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			setListboxValueKec(wnd, "kec", arrkec, 0, true, true, true);
		} catch (Exception e) {
			
		}
	}

	public void setListboxValueKel(Window wnd, String idList, JSONArray data, int noUrut, boolean isSelect,
			boolean isClear, boolean firstRowIsID) {

		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int iRow = data.size();
		for (int j = 0; j < iRow; j++) {
			Listitem li = new Listitem();
			JSONObject child = (JSONObject) data.get(j);
			li.setValue(child.get("namaKelurahan").toString());
			li.appendChild(new Listcell(child.get("namaKelurahan").toString()));
			list.appendChild(li);
		}

	}

	public void setListboxValueKec(Window wnd, String idList, JSONArray data, int noUrut, boolean isSelect,
			boolean isClear, boolean firstRowIsID) {
		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int iRow = data.size();
		for (int j = 0; j < iRow; j++) {
			Listitem li = new Listitem();
			JSONObject child = (JSONObject) data.get(j);
			li.setValue(child.get("namaKecamatan").toString());
			li.appendChild(new Listcell(child.get("namaKecamatan").toString()));
			list.appendChild(li);
		}
	}

	public void setKotaProv(Window wnd, Bandbox bd) throws Exception {
		kecSelected = func.getListboxValue(wnd, "kec");
		kec = kecSelected;
		bd.setValue(kecSelected);
		obj.put("Tipe", "inquiryKotaProvinsi");
		obj.put("namaKelurahan", kelSelected);
		obj.put("namaKecamatan", kecSelected);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
		JSONObject child = (JSONObject) arrkot.get(0);
		kota = child.get("namaKota").toString();
		prov = child.get("namaProvinsi").toString();
		func.setTextboxValue(wnd, "kota", kota);
		func.setTextboxValue(wnd, "prov", prov);
	}

	public boolean cekValidasiEmail(String value) {
		final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(value);
		return matcher.matches();
	}

	public boolean cekValidasiAngka(String value) {
		final String EMAIL_PATTERN = "^[0-9]+$";
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(value);
		return matcher.matches();
	}
	
	public void setWindow (Window wnd) {
		this.win = wnd;
	}
	
	public Window getWindow() {
		return this.win;
	}
	
	public void setBank(String i_bank) throws Exception {
		try{
			System.out.println("Bank Selected>>"+ bank);
			bank = i_bank;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void requestSucceeded(JSONObject obj) {
		// TODO Auto-generated method stub
		if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("kel")) {
			arrkel = (JSONArray) obj.get("listKelurahan");
		} else if (obj.get("ACK").toString().equalsIgnoreCase("OK")
				&& obj.get("ID").toString().equalsIgnoreCase("kec")) {
			arrkec = (JSONArray) obj.get("listKecamatan");
		} else if (obj.get("ACK").toString().equalsIgnoreCase("OK")
				&& obj.get("ID").toString().equalsIgnoreCase("prov")) {
			arrkot = (JSONArray) obj.get("listKotaProvinsi");
		} else if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("Bank") ){
			arrbank = (JSONArray) obj.get("ListBank");
		}
		if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("reg")) {
			// Response : ACK, idAccount, message,Email,nama;
			ack = obj.get("ACK").toString();
			givaccount = obj.get("ID_Account").toString();
			givname = obj.get("nama").toString();
			givemail = obj.get("email").toString();
			tipeUser = obj.get("tipeUser").toString();

			Cookies.setCookie(Constants.tipeUser, "member");
			Cookies.setCookie(Constants.email, givemail);
			Cookies.setCookie(Constants.nama, givname);
			Cookies.setCookie(Constants.id_account, givaccount);
		}
	}

	@Override
	public void requestFailed(String message) {
		// TODO Auto-generated method stub
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}

}
