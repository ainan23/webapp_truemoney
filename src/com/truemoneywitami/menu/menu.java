package com.truemoneywitami.menu;

import org.zkoss.zul.Include;
import org.zkoss.zul.Window;

public class menu extends Window {

	public void setBody(String lLINK, boolean lFull_Screen) throws Exception {
		Include incl = (Include) this.getFellow("inclBody");
		incl.setSrc(lLINK);
	}

	public void setMenu(String lLINK, boolean lFull_Screen) throws Exception {
		Include incl = (Include) this.getFellow("inclMenu");
		incl.setSrc(lLINK);
	}

	
}
