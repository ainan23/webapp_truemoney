package com.truemoneywitami.menu.profile;
import org.zkoss.zul.Window;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import  com.truemoneywitami.function.ZKFunction;
import  com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.pembelian.pulsa.PulsaKonfirm;

public class Agen_profile extends Window implements RequestListener {

	Logger log = Logger.getLogger(Agen_profile.class);
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	JSONObject obj;
	String nama,JK,TL,TGL,KTP,telp,HP,Fax,NPWP,alamat,NamaKantor,AlamatKantor,TelKantor,AlamatDealer,HPKantor;
	mob_Session sesi = new mob_Session();
	String username= sesi.getUsername();
	//String username ="";
	
	@Override
	public void requestSucceeded(JSONObject obj){	 
		//user = obj.get("Username").toString();
		String ack = obj.get("ACK").toString();
		if (ack.equalsIgnoreCase("OK")) {
			nama= obj.get("Nama").toString();
			KTP = obj.get("NoKTP").toString();
			TL = obj.get("TempatLahir").toString();
			JK = obj.get("JK").toString();
			TGL = obj.get("TanggalLahir").toString();
			TL = obj.get("TempatLahir").toString();
			telp = obj.get("NoTelepon").toString();
			HP = obj.get("NoHandphone").toString();
			Fax = obj.get("Fax").toString();
			alamat = obj.get("Alamat").toString();
			NamaKantor = obj.get("NamaKantor").toString();
			AlamatKantor = obj.get("AlamatKantor").toString();
			TelKantor = obj.get("TeleponKantor").toString();
			HPKantor = obj.get("HPKantor").toString();
			AlamatDealer = obj.get("AlamatDealer").toString();
			NPWP = obj.get("NPWP").toString();
		}else {
			String message = obj.get("pesan").toString();
			requestFailed(message);
		}
	}
	
	@Override
	public void requestFailed(String message){
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
public void setData(Window wnd) throws Exception{
		try {			
			JSONObject obj = new JSONObject();
			obj.put("Tipe", "ViewAgentProfile");
			obj.put("username", username);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
//			Clients.showBusy("Please wait....");
			processing.processData();
			func.setLabelValue(wnd, "username", username);
			func.setLabelValue(wnd, "labelnama" , nama);
			func.setLabelValue(wnd, "labelktp" ,KTP);
			func.setLabelValue(wnd, "labeljk" ,JK);
			func.setLabelValue(wnd, "labeltgl" ,TGL);
			func.setLabelValue(wnd, "labeltel" ,telp);
			func.setLabelValue(wnd, "labelhp" ,HP);
			func.setLabelValue(wnd, "labelnamakantor" ,NamaKantor);
			func.setLabelValue(wnd, "labeltelkantor" , TelKantor);
			func.setLabelValue(wnd, "labelhpkantor" , HPKantor);
			func.setLabelValue(wnd, "labelfaxkantor" , Fax);
			func.setLabelValue(wnd, "labelalamatkantor" ,AlamatKantor);
			func.setLabelValue(wnd, "labelalamatdealer" ,AlamatDealer);
			func.setLabelValue(wnd, "labelalamat" , alamat);
			func.setLabelValue(wnd, "labelnpwp" , NPWP);
			//func.setHtmlContent(wnd, h5, nama);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.info("Error di Agen Profile." + e.getMessage());
				//requestFailed("Error at "+this.getClass() + " cause of : "+e.getMessage());
		}   
	    }	

		public void editProfile(Window wnd) throws Exception {
			RedirectMenu menu = new RedirectMenu();
			new RedirectMenu().setMenuLink("profileagenedit",wnd);
		}
}
