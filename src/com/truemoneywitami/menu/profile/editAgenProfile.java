package com.truemoneywitami.menu.profile;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.MessageboxDlg;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RequestListener;
import  com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;
import com.truemoneywitami.menu.pembelian.pulsa.PulsaKonfirm;

public class editAgenProfile extends Window implements RequestListener {
	
	Logger log = Logger.getLogger(editAgenProfile.class);
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	JSONObject obj;
	String nama,JK,TL,TGL,KTP,telp,HP,Fax,NPWP,alamat,NamaKantor,AlamatKantor,TelKantor,AlamatDealer,HPKantor,ack;
	mob_Session sesi = new mob_Session();
	String username = sesi.getUsername();
	//String username ="15121010000";
	
	@Override
	public void requestSucceeded(JSONObject obj){	 
		//user = obj.get("Username").toString();
		if (obj.get("EOF").toString().equals("YES")){
			 ack = obj.get("ACK").toString();
		}
		else {
		nama= obj.get("Nama").toString();
		KTP = obj.get("NoKTP").toString();
		TL = obj.get("TempatLahir").toString();
		JK = obj.get("JK").toString();
		TGL = obj.get("TanggalLahir").toString();
		TL = obj.get("TempatLahir").toString();
		telp = obj.get("NoTelepon").toString();
		HP = obj.get("NoHandphone").toString();
		Fax = obj.get("Fax").toString();
		alamat = obj.get("Alamat").toString();
		NamaKantor = obj.get("NamaKantor").toString();
		AlamatKantor = obj.get("AlamatKantor").toString();
		TelKantor = obj.get("TeleponKantor").toString();
		HPKantor = obj.get("HPKantor").toString();
		AlamatDealer = obj.get("AlamatDealer").toString();
		NPWP = obj.get("NPWP").toString();
		ack = obj.get("ACK").toString();
		}
	}
	
	@Override
	public void requestFailed(String message){
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
	public void setData(Window wnd) throws Exception{
		try {	
			JSONObject obj = new JSONObject();
			obj.put("Tipe", "ViewAgentProfile");
			obj.put("username", username);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			//Clients.showBusy("Please wait....");
	        processing.processData();
			//func.setTValue(wnd, "username","1311151001");
	        func.setTextboxValue(wnd, "labelnama", nama);
			func.setTextboxValue(wnd, "labelktp" ,KTP);
			func.setTextboxValue(wnd, "labelJK" ,JK);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date tanggal = (Date) sdf.parse(TGL);
			sdf = new SimpleDateFormat("MMM d, yyyy");
			func.setDateboxText(wnd, "labeltgl" ,sdf.format(tanggal));
			func.setTextboxValue(wnd, "labeltel" ,telp);
			func.setTextboxValue(wnd, "labelhp" ,HP);
			func.setTextboxValue(wnd, "labelnamakantor" ,NamaKantor);
			func.setTextboxValue(wnd, "labeltelkantor" ,TelKantor);
			func.setTextboxValue(wnd, "labelhpkantor" ,HPKantor);
			func.setTextboxValue(wnd, "labelfaxkantor" ,Fax);
			func.setTextboxValue(wnd, "labelalamatkantor" ,AlamatKantor);
			func.setTextboxValue(wnd, "labelalamatdealer" ,AlamatDealer);
			func.setTextboxValue(wnd, "labelalamat" ,alamat);
			func.setTextboxValue(wnd, "labelnpwp" ,NPWP);
			//obj.get("ACK").toString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.info("Error di Edit Profile Agen ." + e.getMessage());
				requestFailed("Error at "+this.getClass() + " cause of : "+e.getMessage());
		}   
	    }
	
			public void editAgen(Window wnd){
			try {		
				String nama,JK,TL,TGL,KTP,telp,HP,Fax,NPWP,alamat,NamaKantor,AlamatKantor,TelKantor,AlamatDealer,HPKantor;
				nama = func.getTextboxValue(wnd,"labelnama");
				KTP = func.getTextboxValue(wnd,"labelktp");
				TGL = func.getTextboxValue(wnd,"labeltgl");
				telp = func.getTextboxValue(wnd,"labeltel");
				HP = func.getTextboxValue(wnd,"labelhp");
				alamat = func.getTextboxValue(wnd,"labelalamat");
				NPWP = func.getTextboxValue(wnd,"labelnpwp");
				NamaKantor = func.getTextboxValue(wnd,"labelnamakantor");
				AlamatKantor = func.getTextboxValue(wnd,"labelalamatkantor");
				TelKantor = func.getTextboxValue(wnd,"labeltelkantor");
				HPKantor = func.getTextboxValue(wnd,"labelhpkantor");
				Fax = func.getTextboxValue(wnd, "labelfaxkantor");
				AlamatDealer=func.getTextboxValue(wnd, "labelalamatdealer");
				JK = func.getTextboxValue(wnd,"labelJK");
				JSONObject obj2 = new JSONObject();
				obj2.put("Tipe", "EditAgentProfile");
				obj2.put("username", username);
				obj2.put("nama",nama);
				obj2.put("nomorKTP",KTP);
				obj2.put("JK",JK);
	            obj2.put("tglLahir",TGL);
	            obj2.put("telepon",telp);
	            obj2.put("alamat",alamat);
	            obj2.put("Handphone",HP);
	            obj2.put("NPWP",NPWP);
	            obj2.put("namaKantor",NamaKantor);
	            obj2.put("alamatKantor",AlamatKantor);
	            obj2.put("alamatDealer",AlamatDealer);
	            obj2.put("fax", Fax);
	            obj2.put("HandphoneKantor", HPKantor);
	            obj2.put("teleponKantor", TelKantor);
 			    ProcessingListener processing = new ProcessingListener(this, obj2.toString());
		//		Clients.showBusy("Please wait....");
                processing.processData();
                if (ack.equals("OK")){
                	Messagebox.show("Profile Agen Berhasil di Ubah", "Berhasil", Messagebox.OK, Messagebox.EXCLAMATION);
                }
                else {
                	Messagebox.show("Profile Agen  Gagal di Ubah", "Error!!!", Messagebox.OK, Messagebox.ERROR);
                }
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.info("Error di Edit Profile Agen ." + e.getMessage());
					requestFailed("Error at "+this.getClass() + " cause of : "+e.getMessage());
			}   
		    }
			

}

