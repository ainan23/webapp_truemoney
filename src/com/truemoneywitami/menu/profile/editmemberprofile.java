package com.truemoneywitami.menu.profile;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONObject;

import  com.truemoneywitami.function.session.mob_Session;

import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;
import org.zkoss.zul.impl.MessageboxDlg;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RequestListener;
import  com.truemoneywitami.function.ZKFunction;

public class editmemberprofile extends Window implements RequestListener {
	
	Logger log = Logger.getLogger(editmemberprofile.class);
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	JSONObject obj;
	mob_Session sesi = new mob_Session();
	String ack,HP,JK,nama,alamat,tempat,tgl,ktp,bank,norek,nopass,tglpass,npwp,ibu,agama,pend,nikah,kerja,telp,username,waris,warisadd;
	String username2 = sesi.getUsername();
	
	@Override
	public void requestSucceeded(JSONObject obj){	 
		//user = obj.get("Username").toString();
		if (obj.get("EOF").toString().equals("YES")){
			 ack = obj.get("ACK").toString();
		}
		else {
		nama= obj.get("Nama").toString();
		ktp = obj.get("NoKTP").toString();
		JK = obj.get("JK").toString();
		tgl = obj.get("TanggalLahir").toString();
		telp = obj.get("NoTelepon").toString();
		HP = obj.get("NoHandphone").toString();
		alamat = obj.get("Alamat").toString();
		kerja = obj.get("Pekerjaan").toString();
		agama = obj.get("Agama").toString();
		nikah = obj.get("StatusPernikahan").toString();
		pend = obj.get("Pendidikan").toString();
		bank = obj.get("Bank").toString();
		norek = obj.get("NoRekening").toString();
		nopass = obj.get("Passport").toString();
		tglpass = obj.get("TanggalBerlakuPassport").toString();
		npwp = obj.get("NPWP").toString();
		ibu = obj.get("NamaIbu").toString();
		waris = obj.get("NamaAhliWaris").toString();
		warisadd=obj.get("AlamatWaris").toString();
		//ack= obj.get("ACK").toString();
		}
	}
	
	@Override
	public void requestFailed(String message){
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
	public void setData(Window wnd) throws Exception{
		try {			
			JSONObject obj = new JSONObject();
			obj.put("Tipe", "ViewMemberProfile");
			obj.put("username", username2);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
//			Clients.showBusy("Please wait....");
			processing.processData();
			func.setTextboxValue(wnd, "namamember" ,nama);
			func.setTextboxValue(wnd, "ktp" ,ktp);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date tanggal = (Date) sdf.parse(tgl);
			sdf = new SimpleDateFormat("MMM d, yyyy");
			func.setDateboxText(wnd, "datetimepicker7",sdf.format(tanggal));
			func.setTextboxValue(wnd, "telepon" , telp);
			func.setTextboxValue(wnd, "handphone" ,HP);
			func.setTextboxValue(wnd, "alamat" ,alamat);
			func.setTextboxValue(wnd, "kerja" ,kerja);
			func.setTextboxValue(wnd, "edu" ,pend);
			func.setTextboxValue(wnd, "bank" ,bank);
			func.setTextboxValue(wnd, "norek" , ": " + norek);
			func.setTextboxValue(wnd, "labelpassport" ,nopass);
			sdf = new SimpleDateFormat("yyyy-MM-dd");
			tanggal = (Date) sdf.parse(tglpass);
			sdf = new SimpleDateFormat("MMM d, yyyy");
			func.setDateboxText(wnd, "datetimepicker8",sdf.format(tanggal));
			func.setTextboxValue(wnd, "npwp" ,npwp);
			func.setTextboxValue(wnd, "ibu" ,ibu);
			func.setTextboxValue(wnd, "waris" ,waris);
			func.setTextboxValue(wnd, "warisadd" ,warisadd);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.info("Error di Edit Profile Member ." + e.getMessage());
				requestFailed("Error at "+this.getClass() + " cause of : "+e.getMessage());
		}   
	    }
	
			public void editMember(Window wnd) throws Exception{
			try {			
				String nama,ktp,tgl,telp,bank,norek,nopass,tglpass,npwp,ibu,agama,pend,nikah,kerja,username,waris,warisadd,HP,jk;
				nama = func.getTextboxValue(wnd,"namamember");
				ktp = func.getTextboxValue(wnd,"ktp");
				tgl = func.getDateboxText(wnd,"datetimepicker7");
				telp = func.getTextboxValue(wnd,"telepon");
				HP = func.getTextboxValue(wnd,"handphone");
				alamat = func.getTextboxValue(wnd,"alamat");
				bank = func.getTextboxValue(wnd,"bank");
				norek = func.getTextboxValue(wnd,"norek");
				nopass = func.getTextboxValue(wnd,"labelpassport");
				tglpass = func.getDateboxText(wnd,"datetimepicker8");
				npwp = func.getTextboxValue(wnd,"npwp");
				ibu = func.getTextboxValue(wnd,"ibu");
				waris = func.getTextboxValue(wnd,"waris");
				warisadd = func.getTextboxValue(wnd,"warisadd");
				agama = func.getTextboxValue(wnd,"agama");
				nikah=func.getTextboxValue(wnd, "nikah");
				kerja=func.getTextboxValue(wnd, "kerja");
			    pend= func.getTextboxValue(wnd,"edu");
				JSONObject obj = new JSONObject();
				obj.put("Tipe", "EditMemberProfile");
				obj.put("username", username2);
				obj.put("nama",nama);
				obj.put("nomorKTP",ktp);
	            obj.put("tglLahir",tgl);
	            obj.put("JK",JK);
	            obj.put("telepon",telp);
	            obj.put("alamat",alamat);
	            obj.put("pekerjaan",kerja);
	            obj.put("agama",agama);
	            obj.put("pendidikan",pend);
	            obj.put("pernikahan",nikah);
	            obj.put("namaBank",bank);
	            obj.put("noRekening",norek);
	            obj.put("noPassport",nopass);
	            obj.put("tglBerlaku",tglpass);
	            obj.put("NPWP",npwp);
	            obj.put("ibu",ibu);
	            obj.put("Handphone",HP);
	            obj.put("ahliWaris",waris);
	            obj.put("alamatWaris",warisadd);
 			    ProcessingListener processing = new ProcessingListener(this, obj.toString());
		//		Clients.showBusy("Please wait....");
                processing.processData();
                if (ack.equals("OK")){
                	Messagebox.show("Profile Member Berhasil di Ubah", "Berhasil", Messagebox.OK, Messagebox.EXCLAMATION);
                }
                else {
                	Messagebox.show("Profile Member Gagal di Ubah", "Error!!!", Messagebox.OK, Messagebox.ERROR);
                }
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.info("Error di Edit Profile Member ." + e.getMessage());
					requestFailed("Error at "+this.getClass() + " cause of : "+e.getMessage());
			}   
		    }
			

}
