package com.truemoneywitami.menu.profile;

import org.zkoss.zul.Window;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Clients;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import  com.truemoneywitami.function.ZKFunction;
import  com.truemoneywitami.function.session.mob_Session;

public class member_profile extends Window implements RequestListener {

	Logger log = Logger.getLogger(member_profile.class);
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	JSONObject obj;
	String HP,JK,nama,alamat,tempat,tgl,ktp,bank,norek,nopass,tglpass,npwp,ibu,agama,pend,nikah,kerja,telp,username,waris,warisadd;
	mob_Session sesi = new mob_Session();
	
	@Override
	public void requestSucceeded(JSONObject obj){	 
		//user = obj.get("Username").toString();
		nama= obj.get("Nama").toString();
		ktp = obj.get("NoKTP").toString();
		JK = obj.get("JK").toString();
		tgl = obj.get("TanggalLahir").toString();
		telp = obj.get("NoTelepon").toString();
		HP = obj.get("NoHandphone").toString();
		alamat = obj.get("Alamat").toString();
		kerja = obj.get("Pekerjaan").toString();
		agama = obj.get("Agama").toString();
		nikah = obj.get("StatusPernikahan").toString();
		pend = obj.get("Pendidikan").toString();
		bank = obj.get("Bank").toString();
		norek = obj.get("NoRekening").toString();
		nopass = obj.get("Passport").toString();
		tglpass = obj.get("TanggalBerlakuPassport").toString();
		npwp = obj.get("NPWP").toString();
		ibu = obj.get("NamaIbu").toString();
		waris = obj.get("NamaAhliWaris").toString();
		warisadd=obj.get("AlamatWaris").toString();
		obj.get("ACK").toString();
	}
	
	@Override
	public void requestFailed(String message){
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
	
	public void setData(Window wnd) throws Exception{
		
		try {			
			JSONObject obj = new JSONObject();
			obj.put("Tipe", "ViewMemberProfile");
			String user = sesi.getUsername();
			obj.put("username",user);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
//			Clients.showBusy("Please wait....");
			processing.processData();
			func.setLabelValue(wnd, "labeltest",user);
			func.setLabelValue(wnd, "labelnama" ,nama);
			func.setLabelValue(wnd, "labelktp" ,ktp);
			func.setLabelValue(wnd, "labeljk" ,JK);
			func.setLabelValue(wnd, "labeltgl" ,tgl);
			func.setLabelValue(wnd, "labeltelp" ,telp);
			func.setLabelValue(wnd, "labelhp" ,HP);
			func.setLabelValue(wnd, "labelalamat" ,alamat);
			func.setLabelValue(wnd, "labelkerja" ,kerja);
			func.setLabelValue(wnd, "labelagama" ,agama);
			func.setLabelValue(wnd, "labelstatus" ,nikah);
			func.setLabelValue(wnd, "Pend" ,pend);
			func.setLabelValue(wnd, "labelbank" ,bank);
			func.setLabelValue(wnd, "labelnorek" ,norek);
			func.setLabelValue(wnd, "labelpass" ,nopass);
			func.setLabelValue(wnd, "labeltglpass" ,tglpass);
			func.setLabelValue(wnd, "labelnpwp" ,npwp);
			func.setLabelValue(wnd, "labelibu" ,ibu);
			func.setLabelValue(wnd, "labelwaris" ,waris);
			func.setLabelValue(wnd, "labelwarisadd" ,warisadd);
			//func.setHtmlContent(wnd, h5, nama);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.info("Error di Profile Member ." + e.getMessage());
				//requestFailed("Error at "+this.getClass() + " cause of : "+e.getMessage());
			e.printStackTrace();
		}   
	    }	
	
	public void editProfile(Window wnd) throws Exception {
		RedirectMenu menu = new RedirectMenu();
		new RedirectMenu().setMenuLink("profilememberedit",wnd);
	}
}

