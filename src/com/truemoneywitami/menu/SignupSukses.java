package com.truemoneywitami.menu;

import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.Cookies;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class SignupSukses extends Window  implements RequestListener{
	
	/**
	 * Modified By Enggar Ranu Hariawan
	 */
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	Cookies cookie = new Cookies();
	String ack="";
	JSONObject obj = new JSONObject();
	String email="a",tipeUser="",nama="",id_account="a";
	String id_agent="";
	mob_Session sesi = new mob_Session();

	public void setData (Window wnd) {
		try {
//			email = sesi.getSessionAttribute("email");
			email = Cookies.getCookie(Constants.email);
			tipeUser = Cookies.getCookie(Constants.tipeUser);
			nama = Cookies.getCookie(Constants.nama);
			id_account = Cookies.getCookie(Constants.id_account);
			id_agent = Cookies.getCookie(Constants.id_agent);
		try {
			
			func.setLabelValue(wnd, "email",email);
		}catch (Exception e){
			func.setLabelValue(wnd, "email","");
		}
		try {
		} catch (Exception e){
			tipeUser = "";
		}
		try {
			
		} catch (Exception e){
			nama = "";
		}
		try {
			func.setLabelValue(wnd, "account",id_account);
		} catch (Exception e) {
			func.setLabelValue(wnd, "account","");
		}
		
		try {
			func.setLabelValue(wnd, "id_agent",id_agent);
		} catch (Exception e) {
			try {
				func.setLabelValue(wnd, "id_agent","");
			} catch (Exception a) {
				
			}
			
		}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void next() throws Exception {
		Executions.sendRedirect("login.zul");
	}
	
	public void sendEmail(Window wnd) throws Exception{
		obj.put("Tipe", "ResendEmail");
		obj.put("email", email);
		obj.put("tipeUser", tipeUser);
		obj.put("nama", nama);
		obj.put("ID_Account", id_account);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
	}

	@Override
	public void requestSucceeded(JSONObject obj) {
		// TODO Auto-generated method stub
		email = Cookies.getCookie(Constants.email);
		tipeUser = Cookies.getCookie(Constants.tipeUser);
		nama = Cookies.getCookie(Constants.nama);
		id_account = Cookies.getCookie(Constants.id_account);
		if (obj.get("ACK").toString().equalsIgnoreCase("OK")){
			System.out.println(obj.get("ACK").toString());
			Clients.showNotification("Data Registrasi telah dikirim ulang ke email "+email+", Silahkan cek email anda kembali.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		} else {
			Clients.showNotification("Gagal Mengirim ke "+email+", Silahkan hubungi Customer Service.", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}

	@Override
	public void requestFailed(String message) {
		// TODO Auto-generated method stub
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
}
