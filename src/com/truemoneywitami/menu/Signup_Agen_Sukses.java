package com.truemoneywitami.menu;

import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;

public class Signup_Agen_Sukses extends Window  implements RequestListener{
	
	ZKFunction func = new ZKFunction();
	String ack="";
	
	public void setData (Window wnd) throws Exception {
//		func.setLabelValue(wnd, "email", Constants.email);
//		func.setLabelValue(wnd, "account", Constants.id_account);
	}
	
	public void next() throws Exception {
		Executions.sendRedirect("login.zul");
	}
	
	public void sendEmail(Window wnd) throws Exception{
		JSONObject obj = new JSONObject();
		obj.put("Tipe", "ResendEmail");
		obj.put("email", Constants.email);
		obj.put("tipeUser", Constants.tipeUser);
		obj.put("nama", Constants.nama);
		obj.put("ID_Account", Constants.id_account);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
	}

	@Override
	public void requestSucceeded(JSONObject obj) {
		// TODO Auto-generated method stub
		if (obj.get("ACK").toString().equals("OK")){
			ack = obj.get("ACK").toString();
		}
	}

	@Override
	public void requestFailed(String message) {
		// TODO Auto-generated method stub
		if(message.equals("01")) {
			Executions.sendRedirect("/timeout.zul");
		} else {
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
		}
	}
}
