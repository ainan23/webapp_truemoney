package com.truemoneywitami.menu;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ResourceBundle;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.zkoss.json.JSONArray;
import org.zkoss.json.JSONObject;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Window;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.truemoneywitami.function.CheckParameter;
import com.truemoneywitami.function.Constants;
import com.truemoneywitami.function.Cookies;
import com.truemoneywitami.function.ImageFunction;
import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class Signup_Agen extends Window  implements RequestListener {
	/**
	 * Modified by : Enggar Ranu Hariawan 
	 */
	private static final long serialVersionUID = 1L;
	ZKFunction func = new ZKFunction();
	JSONObject obj = new JSONObject();
	mob_Session sesi = new mob_Session();
	CheckParameter check = new CheckParameter();
	JSONArray arrkel,arrkec,arrkot,arrbank;
	Window win;
	//String kel1,kec1,kota1,prov1,kel2,kec2,kota2,prov2,kel3,kec3,kota3,prov3;;
	String kelSelected1,kecSelected1,kelSelected2,kecSelected2,kelSelected3,kecSelected3, ack = "";
	String noHP,password,repass,namaLengkap,noKTP,jenisKelamin,tempatLahir,tglLahir,noTelPribadi,alamatPribadi,npwpPribadi,rtPribadi,rwpribadi,kelPribadi,kecPribadi,kotaPribadi,provPribadi,posPribadi;
	String noHPLembaga;
	String alamatUsaha,kelUsaha,kecUsaha,kotaUsaha,provUsaha,posUsaha,email;
	
//	private String fotowajah;
	private String fotoktp;
	private String fotottd;
	
//	private Media mediaWajah;
	private Media mediaKtp;
	private Media mediaTtd;
	
	
	private static Logger log = Logger.getLogger(Signup_Agen.class);
	
	
	public void loadKelurahan(Window wnd) throws Exception {
		String namaKelurahan = func.getTextboxValue(wnd, "listsearchkel");
		try {
			if (namaKelurahan.trim().length() <4){
//				Clients.showNotification("Maksimal 4 Karakter");
			} else {
				obj.put("Tipe","inquiryKelurahan");
				obj.put("namaKelurahan", namaKelurahan);
				ProcessingListener processing = new ProcessingListener(this, obj.toString());
				processing.processData();
				setListboxValueKel(wnd, "kel", arrkel, 0, true, true, true);
			}
			
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public void loadKecamatan(Window wnd,Bandbox bd) throws Exception {
		try {
			Bandbox bkec = (Bandbox) wnd.getFellow("bandkec");
			bkec.setDisabled(false);
			
			bkec.setReadonly(true);
			bkec.setText("");
//			func.setTextboxResetValue(wnd, "kota1");
			func.listboxHapus(wnd, "lstKota1");
			func.setTextboxResetValue(wnd, "prov1");
			
			kelSelected1 = func.getListboxValue(wnd, "kel");
			bd.setValue(kelSelected1);
			obj.put("Tipe","inquiryKecamatan");
			obj.put("namaKelurahan", kelSelected1);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			setListboxValueKec(wnd, "kec", arrkec, 0, true, true, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setListboxValueKel(Window wnd, String idList, JSONArray data, int noUrut, boolean isSelect,
			boolean isClear, boolean firstRowIsID) {
		try {
			Listbox list = (Listbox) wnd.getFellow(idList);
			list.getItems().clear();
			int iRow = data.size();
			for (int j = 0; j < iRow; j++) {
				Listitem li = new Listitem();
				JSONObject child = (JSONObject) data.get(j);
				li.setValue(child.get("namaKelurahan").toString());
				li.appendChild(new Listcell(child.get("namaKelurahan").toString()));
				list.appendChild(li);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setListboxValueKec(Window wnd, String idList, JSONArray data, int noUrut, boolean isSelect,
			boolean isClear, boolean firstRowIsID) {

		try {
			Listbox list = (Listbox) wnd.getFellow(idList);
			list.getItems().clear();
			int iRow = data.size();
			for (int j = 0; j < iRow; j++) {
				Listitem li = new Listitem();
				JSONObject child = (JSONObject) data.get(j);
				li.setValue(child.get("namaKecamatan").toString());
				li.appendChild(new Listcell(child.get("namaKecamatan").toString()));
				list.appendChild(li);
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setKotaProvFirst(Window wnd, Bandbox bd) throws Exception {
		kecSelected1 = func.getListboxValue(wnd, "kec");
		bd.setValue(kecSelected1);
		obj.put("Tipe","inquiryKotaProvinsi");
		obj.put("namaKelurahan", kelSelected1);
		obj.put("namaKecamatan", kecSelected1);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
//		JSONObject child = (JSONObject) arrkot.get(0);
//		func.setTextboxValue(wnd, "kota1", child.get("namaKota").toString());
//		func.setTextboxValue(wnd, "prov1",child.get("namaProvinsi").toString());
		
		func.setTextboxValue(wnd, "prov1", "");
		
		setListboxValueKota(wnd, "lstKota1", arrkot, "prov1");
	}
	
	public void setKotaProvSecond(Window wnd, Bandbox bd) throws Exception {
//		kecSelected2 = func.getListboxValue(wnd, "kec2");
//		bd.setValue(kecSelected2);
//		obj.put("Tipe","inquiryKotaProvinsi");
//		obj.put("namaKelurahan", kelSelected2);
//		obj.put("namaKecamatan", kecSelected2);
//		ProcessingListener processing = new ProcessingListener(this, obj.toString());
//		processing.processData();
//		JSONObject child = (JSONObject) arrkot.get(0);
//		func.setTextboxValue(wnd, "kota2", child.get("namaKota").toString());
//		func.setTextboxValue(wnd, "prov2",child.get("namaProvinsi").toString());
	}
	
	public void loadKelurahanSecond(Window wnd) throws Exception {
//		String namaKelurahan = func.getTextboxValue(wnd, "listsearchkel2");
//		try {
//			if (namaKelurahan.trim().length() < 4) {
////				Clients.showNotification("Maksimal 4 Karakter");
//			} else {
//				obj.put("Tipe","inquiryKelurahan");
//				obj.put("namaKelurahan", namaKelurahan);
//				ProcessingListener processing = new ProcessingListener(this, obj.toString());
//				processing.processData();
//				setListboxValueKel(wnd, "kel2", arrkel, 0, true, true, true);
//			}
//		} catch (Exception e) {
//			
//		}
		
		
	}
	
	public void loadKecamatanSecond(Window wnd,Bandbox bd) throws Exception {
////		Bandbox bkec2 = (Bandbox) wnd.getFellow("bandkec2");
////		bkec2.setDisabled(false);
////		kelSelected2 = func.getListboxValue(wnd, "kel2");
////		bd.setValue(kelSelected2);
////		obj.put("Tipe","inquiryKecamatan");
////		obj.put("namaKelurahan", kelSelected2);
////		ProcessingListener processing = new ProcessingListener(this, obj.toString());
////		processing.processData();
////		setListboxValueKec(wnd, "kec2", arrkec, 0, true, true, true);
//		
//		try {
//			Bandbox bkec2 = (Bandbox) wnd.getFellow("bandkec2");
//			bkec2.setDisabled(false);
//			
//			bkec2.setReadonly(true);
//			bkec2.setText("");
//			func.setTextboxResetValue(wnd, "kota2");
//			func.setTextboxResetValue(wnd, "prov2");
//			
//			kelSelected2 = func.getListboxValue(wnd, "kel2");
//			bd.setValue(kelSelected2);
//			obj.put("Tipe","inquiryKecamatan");
//			obj.put("namaKelurahan", kelSelected2);
//			ProcessingListener processing = new ProcessingListener(this, obj.toString());
//			processing.processData();
//			setListboxValueKec(wnd, "kec2", arrkec, 0, true, true, true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}
	
	public void loadKelurahanThird(Window wnd) throws Exception {
		String namaKelurahan = func.getTextboxValue(wnd, "listsearchkel3");
		try {
			if (namaKelurahan.trim().length() <4) {
//				Clients.showNotification("Maksimal 4 Karakter");
			}else {
				obj.put("Tipe","inquiryKelurahan");
				obj.put("namaKelurahan", namaKelurahan);
				ProcessingListener processing = new ProcessingListener(this, obj.toString());
				processing.processData();
				setListboxValueKel(wnd, "kel3", arrkel, 0, true, true, true);
			}
		}catch (Exception e) {
			
		}
		
	}
	
	public void loadKecamatanThird(Window wnd,Bandbox bd) throws Exception {
		try {
//			Bandbox bkec3 = (Bandbox) wnd.getFellow("bandkec3");
//			bkec3.setDisabled(false);
//			kelSelected3 = func.getListboxValue(wnd, "kel3");
//			bd.setValue(kelSelected3);
//			obj.put("Tipe","inquiryKecamatan");
//			obj.put("namaKelurahan", kelSelected3);
//			ProcessingListener processing = new ProcessingListener(this, obj.toString());
//			processing.processData();
//			setListboxValueKec(wnd, "kec3", arrkec, 0, true, true, true);
			
			Bandbox bkec3 = (Bandbox) wnd.getFellow("bandkec3");
			bkec3.setDisabled(false);
			
			bkec3.setReadonly(true);
			bkec3.setText("");
			func.listboxHapus(wnd, "lstKota3");
			func.setTextboxResetValue(wnd, "prov3");
			
			kelSelected3 = func.getListboxValue(wnd, "kel3");
			bd.setValue(kelSelected3);
			obj.put("Tipe","inquiryKecamatan");
			obj.put("namaKelurahan", kelSelected3);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
			setListboxValueKec(wnd, "kec3", arrkec, 0, true, true, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setKotaProvThird(Window wnd, Bandbox bd) throws Exception {
		kecSelected3 = func.getListboxValue(wnd, "kec3");
		bd.setValue(kecSelected3);
		obj.put("Tipe","inquiryKotaProvinsi");
		obj.put("namaKelurahan", kelSelected3);
		obj.put("namaKecamatan", kecSelected3);
		ProcessingListener processing = new ProcessingListener(this, obj.toString());
		processing.processData();
//		JSONObject child = (JSONObject) arrkot.get(0);
//		func.setTextboxValue(wnd, "kota3", child.get("namaKota").toString());
//		func.setTextboxValue(wnd, "prov3",child.get("namaProvinsi").toString());
		
		func.setTextboxValue(wnd, "prov3", "");
		
		setListboxValueKota(wnd, "lstKota3", arrkot, "prov3");
	}
	
	public void setData(Window wnd) throws Exception {
//		JSONObject obj2 = new JSONObject();
//		obj2.put("Tipe","BankData");
//		ProcessingListener processing = new ProcessingListener(this, obj2.toString());
//		processing.processData();
//		setListboxValue(wnd,"bank_sel", arrbank, 0, true, true, true);
	}
	
	public void setListboxValue(Window wnd,String idList,JSONArray data,int noUrut,boolean isSelect, boolean isClear, boolean firstRowIsID) throws Exception {
		try{
			Listbox list = (Listbox) wnd.getFellow(idList);
			list.getItems().clear();
			int a = data.size();
			for (int i =0;i<a;i++){
				Listitem li = new Listitem();
				JSONObject child  = (JSONObject) data.get(i);
				li.setValue(child.get("kode").toString());
				String nama = child.get("nama").toString();
				li.appendChild(new Listcell(nama));
				list.appendChild(li);
				
				if (i == 0) {
					li.setSelected(true);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void setBank(String bank) throws Exception {
//		try{
//			System.out.println("Bank Selected>>"+ bank);
//			bankLembaga = bank;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}
	
	public void daftar(Window wnd) throws Exception {
		try {
			if (fotoktp == null || fotottd == null) {
				Clients.showNotification("Semua Foto Harus di Isi", Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			} else {

				setWindow(wnd);
				//Data Pribadi
				namaLengkap 		= func.getTextboxValue(wnd,"nama");
				noKTP 				= func.getTextboxValue(wnd,"ktp");
				jenisKelamin		= func.getTextboxValue(wnd,"jk");
				tempatLahir 		= func.getTextboxValue(wnd, "tempat");
				tglLahir 			= func.getDateboxText(wnd, "tgl");
				noTelPribadi	 	= func.getTextboxValue(wnd, "telp");
				noHP 				= func.getTextboxValue(wnd, "HP");
				alamatPribadi 		= func.getTextboxValue(wnd,"alamat");
				rtPribadi 			= func.getTextboxValue(wnd,"RT");
				rwpribadi 			= func.getTextboxValue(wnd,"RW");
				kelPribadi	 		= kelSelected1;
				kecPribadi 			= kecSelected1;
				
				kotaPribadi			= func.getListboxLabel(wnd, "lstKota1");//func.getTextboxValue(wnd,"kota1");
				
				provPribadi			= func.getTextboxValue(wnd,"prov1");
				posPribadi 			= func.getTextboxValue(wnd,"pos");
				npwpPribadi 		= func.getTextboxValue(wnd,"npwp");
				
				//Data Perusahaan/Lembaga
//				namaLembaga 		= func.getTextboxValue(wnd,"namapt");
//				noTelLembaga 		= func.getTextboxValue(wnd,"telpt");
//				fax 				= func.getTextboxValue(wnd,"fax");
//				alamatLembaga 		= func.getTextboxValue(wnd,"alamatpt");
//				kelLembaga 			= kelSelected2;
//				kecLembaga 			= kecSelected2;
//				kotaLembaga 		= func.getTextboxValue(wnd,"kota2");
//				provLembaga 		= func.getTextboxValue(wnd,"prov2");
//				posLembaga 			= func.getTextboxValue(wnd,"pos2");
//				npwpLembaga 		= func.getTextboxValue(wnd,"npwp2");
				
//				String bankLembaga = func.getListboxLabel(wnd, "bank_sel");
//				if (bankLembaga.toString().isEmpty()) {
//					bankLembaga = "DEFAULT_VALUE";
//				}
//				
//				log.info("nama bank -> " + bankLembaga);
				
//				norek				= func.getTextboxValue(wnd, "norek");
				
				//Lokasi Kantor
				alamatUsaha 		= func.getTextboxValue(wnd,"alamat3");
				kelUsaha 			= kelSelected3;
				kecUsaha 			= kecSelected3;
				
				kotaUsaha 			= func.getListboxLabel(wnd, "lstKota3");//func.getTextboxValue(wnd,"kota3");
				
				provUsaha 			= func.getTextboxValue(wnd,"prov3");
				posUsaha 			= func.getTextboxValue(wnd,"pos3");
				email 				= func.getTextboxValue(wnd,"email");
				password 			= func.getTextboxValue(wnd,"pass");
				repass 				= func.getTextboxValue(wnd, "repass");
				
				if (check.checkAlpha(password) && check.checkNumber(password)) {
					
				} else {
					Messagebox.show("Password harus memiliki kombinasi huruf dan angka", "TMN", Messagebox.OK, Messagebox.ERROR);
					return;
				}
				
				//SetCookie
				Cookies.setCookie("Tipe", "RegisterAgent");
				Cookies.setCookie("noHP",noHP);
				Cookies.setCookie("password", password);
				Cookies.setCookie("namaLengkap",namaLengkap);
				Cookies.setCookie("noKTP", noKTP);
				Cookies.setCookie("email", email);
				Cookies.setCookie("jenisKelamin", jenisKelamin);
				Cookies.setCookie("tempatLahir", tempatLahir);
				Cookies.setCookie("tglLahir", tglLahir);
				Cookies.setCookie("noTelPribadi", noTelPribadi);
				Cookies.setCookie("alamatPribadi", alamatPribadi);
				Cookies.setCookie("rtPribadi", rtPribadi);
				Cookies.setCookie("rwPribadi", rwpribadi);
				Cookies.setCookie("kelPribadi", kelPribadi);
				Cookies.setCookie("kecPribadi", kecPribadi);
				Cookies.setCookie("kotaPribadi", kotaPribadi);
				Cookies.setCookie("provPribadi", provPribadi);
				Cookies.setCookie("posPribadi", posPribadi);
				Cookies.setCookie("npwpPribadi", npwpPribadi);
//				Cookies.setCookie("namaLembaga", namaLembaga);
//				Cookies.setCookie("noTelLembaga", noTelLembaga);
//				Cookies.setCookie("fax", fax);
//				Cookies.setCookie("alamatLembaga", alamatLembaga);
//				Cookies.setCookie("kelLembaga", kelLembaga);
//				Cookies.setCookie("kecLembaga", kecLembaga);
//				Cookies.setCookie("kotaLembaga", kotaLembaga);
//				Cookies.setCookie("provLembaga", provLembaga);
//				Cookies.setCookie("posLembaga", posLembaga);
//				Cookies.setCookie("namaBank",bankLembaga);
//				Cookies.setCookie("nomorRekening", norek);
//				Cookies.setCookie("npwpLembaga", npwpLembaga);
				Cookies.setCookie("alamatUsaha", alamatUsaha);
				Cookies.setCookie("kelUsaha", kelUsaha);
				Cookies.setCookie("kecUsaha", kecUsaha);
				Cookies.setCookie("kotaUsaha",kotaUsaha);
				Cookies.setCookie("provUsaha", provUsaha);
				Cookies.setCookie("posUsaha", posUsaha);
				
				// upload foto
//				uploadFoto(mediaWajah, noHP, "WAJAH");
				uploadFoto(mediaKtp, noHP, "KTP");
				uploadFoto(mediaTtd, noHP, "TTD");
				
				JSONObject obj = new JSONObject();
				obj.put("Tipe", "RegisterAgent");
				obj.put("noHP",noHP);
				obj.put("password", password);
				obj.put("namaLengkap",namaLengkap);
				obj.put("noKTP", noKTP);
				obj.put("email", email);
				obj.put("jenisKelamin", jenisKelamin);
				obj.put("tempatLahir", tempatLahir);
				obj.put("tglLahir", tglLahir);
				obj.put("noTelPribadi", noTelPribadi);
				obj.put("alamatPribadi", alamatPribadi);
				obj.put("rtPribadi", rtPribadi);
				obj.put("rwPribadi", rwpribadi);
				obj.put("kelPribadi", kelPribadi);
				obj.put("kecPribadi", kecPribadi);
				obj.put("kotaPribadi", kotaPribadi);
				obj.put("provPribadi", provPribadi);
				obj.put("posPribadi", posPribadi);
				obj.put("npwpPribadi", npwpPribadi);
//				obj.put("namaLembaga", namaLembaga);
//				obj.put("noTelLembaga", noTelLembaga);
//				obj.put("fax", fax);
//				obj.put("alamatLembaga", alamatLembaga);
//				obj.put("kelLembaga", kelLembaga);
//				obj.put("kecLembaga", kecLembaga);
//				obj.put("kotaLembaga", kotaLembaga);
//				obj.put("provLembaga", provLembaga);
//				obj.put("posLembaga", posLembaga);
//				obj.put("namaBank",bankLembaga);
//				obj.put("nomorRekening", norek);
//				obj.put("npwpLembaga", npwpLembaga);
				obj.put("alamatUsaha", alamatUsaha);
				obj.put("kelUsaha", kelUsaha);
				obj.put("kecUsaha", kecUsaha);
				obj.put("kotaUsaha",kotaUsaha);
				obj.put("provUsaha", provUsaha);
				obj.put("posUsaha", posUsaha);
				ProcessingListener processing = new ProcessingListener(this, obj.toString());
				processing.processData();
				
				if (ack.equalsIgnoreCase("OK")) {
					Executions.sendRedirect(Constants.signupagen);
				}
			
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e);
		}
	}
	
	public void setWindow (Window wnd) {
		this.win = wnd;
	}
	
	public Window getWindow() {
		return this.win;
	}

	@Override
	public void requestSucceeded(JSONObject obj) {
		// TODO Auto-generated method stub
		
		if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("kel") ) {
			arrkel = (JSONArray) obj.get("listKelurahan");
		}
		else if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("kec") ) {
			arrkec = (JSONArray) obj.get("listKecamatan"); 	
		}
		else if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("prov") ) {
			arrkot = (JSONArray) obj.get("listKotaProvinsi");
		}
		else if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("Bank") ){
			arrbank = (JSONArray) obj.get("ListBank");
		}
		if (obj.get("ACK").toString().equalsIgnoreCase("OK") && obj.get("ID").toString().equalsIgnoreCase("reg") ) {
			// Response : ACK, idAccount, message,Email,nama;
						ack = obj.get("ACK").toString();
						String givaccount = obj.get("ID_Account").toString();
						String givname = obj.get("nama").toString();
						String givemail = obj.get("email").toString();
						String tipeUser = obj.get("tipeUser").toString();

						Cookies.setCookie(Constants.tipeUser, tipeUser);
						Cookies.setCookie(Constants.email, givemail);
						Cookies.setCookie(Constants.nama, givname);
						Cookies.setCookie(Constants.id_account, givaccount);
		}
	}

	@Override
	public void requestFailed(String message) {
		// TODO Auto-generated method stub

		try {
			System.out.println("Req. Failed>>"+message);
			Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			Window wnd = getWindow();
			try {
//				setData(wnd);
				log.info("sukses set data");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.info("Set Data Gagal>>"+e.getMessage());
			}
			
			if(message.equals("01")) {
				Executions.sendRedirect("/timeout.zul");
			} else {
				Clients.showNotification(message, Clients.NOTIFICATION_TYPE_INFO, null, Constants.POSITION_NOTIF, Constants.TIMER_NOTIF);
			}
		} catch (Exception e){
			e.printStackTrace();
		}
	
	}
	
	public void setMedia(byte[] media, int param,String type,Media data) {
		if (param == 1){
//			fotowajah = "ADA";
//			this.mediaWajah = data;
		} else if (param==2) {
			fotoktp = "ADA";
			this.mediaKtp = data;
		}
		else if (param ==3){
			fotottd = "ADA";
			this.mediaTtd = data;
		}
	}
	
	public void uploadFoto(Media media, String hp, String tipeFoto) {
		BufferedInputStream in = null;
		BufferedOutputStream out = null;
		
		String pathTemp = "";
		String pathDestination = "";
		String fileName = "";
		try {
			ResourceBundle rb = ResourceBundle.getBundle("config.config");
			
			pathTemp = rb.getString("url_report_path");
			pathDestination = rb.getString("url_destination_path");
			fileName = "AGEN_" + hp +"_"+ tipeFoto +".jpeg";
			
			int size = media.getByteData().length / 1024;
			
			InputStream inputStream = media.getStreamData();
			
			if (size > 1024 && size <= 5120) {
				// compress
				ImageFunction imageFunction = new ImageFunction();
				imageFunction.compressImage(pathTemp + fileName, inputStream);
			} else {
				in = new BufferedInputStream(inputStream);
				
				File file = new File(pathTemp + fileName);//sesi.getUsername() + "_" + idTipePembayaran + "_" + sdf.format(new Date()) + "_" + func.generateUuid() + ".xlsx");
				OutputStream fout = new FileOutputStream(file);
				out = new BufferedOutputStream(fout);
				byte buffer[] = new byte[1024];
				int ch = in.read(buffer);
				while (ch != -1) {
					out.write(buffer, 0, ch);
					ch = in.read(buffer);
				}
			}
			
			
			
			
		} catch (Exception e) {
			log.error("exception saat upload foto ke server", e);
		} finally {
			try {
				out.close();
			} catch (Exception e) {
				
			}
			
			try {
				in.close();
			} catch (Exception e) {
				
			}
			
			try {
				copy(pathTemp, pathDestination, fileName);
			} catch (Exception e) {
				
			}
		}
	}
	
	public void copy(String pathSource, String pathDestination, String fileName) {
		JSch jsch = new JSch();
		Session session = null;
		try {
			ResourceBundle rb = ResourceBundle.getBundle("config.config");
			
			String username = rb.getString("sftp_user");
			String password = rb.getString("sftp_password");
			String url = rb.getString("sftp_url");
			int port = Integer.parseInt(rb.getString("sftp_port"));
			
			session = jsch.getSession(username, url, port);
            session.setPassword(password);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            log.info("session connected.");
            
            ChannelSftp channelSftp = null;
            channelSftp = (ChannelSftp) session.openChannel("sftp");
            log.info("channel open");
            channelSftp.connect();
            log.info("channel connected");
            channelSftp.put(pathSource+fileName, pathDestination+fileName);

            log.info("Upload Success");
            
            
		} catch (Exception e) {
			e.printStackTrace();
			log.error("exception saat copy file to server", e);
		} finally {
			try {
				session.disconnect();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public void setProv1(Window wnd) throws Exception {
		 Listbox lstKota = (Listbox) wnd.getFellow("lstKota1");
		 String value = func.getListboxValue(lstKota);
		 if (value.equalsIgnoreCase("PILIHKOTA")) {
			 func.setTextboxValue(wnd, "prov1", "");
		 } else {
			 func.setTextboxValue(wnd, "prov1", value);
		 }
	 }
	
	public void setProv3(Window wnd) throws Exception {
		 Listbox lstKota = (Listbox) wnd.getFellow("lstKota3");
		 String value = func.getListboxValue(lstKota);
		 if (value.equalsIgnoreCase("PILIHKOTA")) {
			 func.setTextboxValue(wnd, "prov3", "");
		 } else {
			 func.setTextboxValue(wnd, "prov3", value);
		 }
	 }
	
	public void setListboxValueKota(Window wnd,String idList,JSONArray data, String txtprov) throws Exception {
		
		try {
		Listbox list = (Listbox) wnd.getFellow(idList);
		list.getItems().clear();
		int a = data.size();
		
		//int lIndex = 0;
		
		if (a > 1) {
			
			Listitem li = new Listitem();
			li.setValue("PILIHKOTA");
			li.appendChild(new Listcell("--- PILIH ---"));
			li.setSelected(true);
			list.appendChild(li);
			
			for (int i =0;i<a;i++){
				li = new Listitem();
				JSONObject child  = (JSONObject) data.get(i);
				li.setValue(child.get("namaProvinsi").toString());
				String nama = child.get("namaKota").toString();
				li.appendChild(new Listcell(nama));
				list.appendChild(li);
			}
		} else if (a == 1) {
			JSONObject child  = (JSONObject) data.get(0);
			Listitem li = new Listitem();
			li.setValue(child.get("namaProvinsi").toString());
			li.appendChild(new Listcell(child.get("namaKota").toString()));
			li.setSelected(true);
			list.appendChild(li);
			
			func.setTextboxValue(wnd, txtprov, child.get("namaProvinsi").toString());
		}
		
		
		} catch (Exception e){
			log.info("Error di PLN ." + e.getMessage());
		}
	}

}