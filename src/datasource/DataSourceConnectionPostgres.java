package datasource;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.util.ResourceBundle;

/**
 * Created with IntelliJ IDEA.
 * User: adetiamarhadi
 * Date: 2/21/16
 * Time: 6:28 AM
 * To change this template use File | Settings | File Templates.
 */
public class DataSourceConnectionPostgres {

    private static DataSourceConnectionPostgres dataSourceConnection= null;
    private static DataSource dataSource = null;

    public static DataSource getDataSource() {
        return dataSource;
    }

    public static void setDataSource(DataSource dataSource) {
        DataSourceConnectionPostgres.dataSource = dataSource;
    }

    private DataSourceConnectionPostgres() {
        if (dataSourceConnection == null) {

            ResourceBundle rb = ResourceBundle.getBundle("config.db");
            String userName = rb.getString("postgre.db.username");
            String password = rb.getString("postgre.db.password");
            String url = rb.getString("postgre.db.url")
                    + rb.getString("postgre.db.dbname");

            ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
            try {
                comboPooledDataSource.setDriverClass(rb.getString("postgre.db.driver"));
            } catch (PropertyVetoException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            comboPooledDataSource.setJdbcUrl(url);
            comboPooledDataSource.setUser(userName);
            comboPooledDataSource.setPassword(password);
            comboPooledDataSource.setInitialPoolSize(Integer.parseInt(rb.getString("db.init.pool")));
            comboPooledDataSource.setMaxPoolSize(Integer.parseInt(rb.getString("db.max.pool")));
            comboPooledDataSource.setMinPoolSize(Integer.parseInt(rb.getString("db.min.pool")));
            comboPooledDataSource.setIdleConnectionTestPeriod(300);
            comboPooledDataSource.setMaxIdleTimeExcessConnections(240);
            comboPooledDataSource.setPreferredTestQuery("select 1");
            comboPooledDataSource.setTestConnectionOnCheckin(true);
            comboPooledDataSource.getConnectionCustomizerClassName();
            dataSource = comboPooledDataSource;
        }
    }

    public static DataSourceConnectionPostgres getInstance() {
        if(dataSourceConnection == null) {
            try {
                dataSourceConnection =  new DataSourceConnectionPostgres();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return dataSourceConnection;
    }

}
