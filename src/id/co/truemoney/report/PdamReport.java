package id.co.truemoney.report;

import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.truemoneywitami.function.Function;

public class PdamReport {
	
	private static Logger log = Logger.getLogger(PdamReport.class);
	
	private static ResourceBundle bundle = ResourceBundle.getBundle("config.config");
	private String pathImg = bundle.getString("url_path_img_report");
	
	private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	private static Font footerFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
	
	public String generateReportAetra(String path, HashMap<String, Object> map) {
		String namaFile = "";
		try {
			Function func = new Function();
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("tanggal").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("noResi").toString();//"18142680";
			String valNoPelanggan = map.get("idPelanggan").toString();//"0162406900527";
			String valNoSambungan = map.get("noSambungan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
			String valNamaPam = map.get("namaProduk").toString();//"";
			String valAlamat = map.get("alamat").toString();//"1";			
			String valPemakaian = map.get("pemakaian").toString();//"205A";
			String valDenda = map.get("denda").toString();//"MEI12";
			String valTagNonAir = map.get("tagNonAir").toString();//"MEI12";
			String valMaterai = map.get("materai").toString();//"MEI12";
			String valAdminn = map.get("biayaAdmin").toString();//"Rp 2.500";
			String valTotal = map.get("totalTagihan").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valPeriodeArr = map.get("detaillist").toString();//"SUKSES";
			
			String label = "";
			if (valNoPelanggan.trim().length() == 0 || valNoPelanggan.trim().equals("0")) {
				label = "NO. SAMBUNGAN";
				valNoPelanggan = valNoSambungan;
			} else {
				label = "NO. PELANGGAN";
			}
			
			JsonParser jsonParser = new JsonParser();
			Object object = jsonParser.parse(valPeriodeArr);
			JsonArray periodeArr = new JsonArray();
			periodeArr = (JsonArray) object;
			
			namaFile = "PDAM_"+valIdTransaksi+"_"+valNoPelanggan+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN " + valNamaPam.toUpperCase(), titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNamaPam = new PdfPCell(new Paragraph("NAMA PAM"));
	        cellLblNamaPam.setBorder(0);
	        PdfPCell cellLblValNamaPam = new PdfPCell(new Paragraph(valNamaPam));
	        cellLblValNamaPam.setBorder(0);
	        table.addCell(cellLblNamaPam);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaPam);
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph(label));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valNoPelanggan));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        PdfPCell cellLblNama = new PdfPCell(new Paragraph("NAMA"));
	        cellLblNama.setBorder(0);
	        PdfPCell cellLblValNama = new PdfPCell(new Paragraph(valNama));
	        cellLblValNama.setBorder(0);
	        table.addCell(cellLblNama);
	        table.addCell(cell2);
	        table.addCell(cellLblValNama);
	        
	        if (valAlamat.trim().length() != 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("ALAMAT"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valAlamat));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (!valPemakaian.trim().equals("0 M3")) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("PEMAKAIAN"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valPemakaian));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        PdfPCell cellLblRincianTagihan = new PdfPCell(new Paragraph("RINCIAN TAGIHAN"));
	        cellLblRincianTagihan.setBorder(0);
	        PdfPCell cellEmpty= new PdfPCell(new Paragraph(" "));
	        cellEmpty.setBorder(0);
	        table.addCell(cellLblRincianTagihan);
	        table.addCell(cellEmpty);
	        table.addCell(cellEmpty);
	        
	        for (int i=0; i<periodeArr.size(); i++) {
	        	JsonObject child = (JsonObject) periodeArr.get(i);
	        	
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph(child.get("period").getAsString()));
	        	cellLblNpwp.setBorder(0);	        	
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(child.get("total").getAsString())));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
		        
		        if ((i+1) == periodeArr.size()) {
		        	table.addCell(cellEmpty);
			        table.addCell(cellEmpty);
			        table.addCell(cellEmpty);
	        	}
	        }
	        
	        if (Integer.parseInt(valDenda) > 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("DENDA"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(valDenda)));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (Integer.parseInt(valTagNonAir) > 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("TAG.NON AIR"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(valTagNonAir)));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (Integer.parseInt(valMaterai) > 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("MATERAI"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(valMaterai)));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (Integer.parseInt(valAdminn) > 0) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("ADMIN"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(func.getFormatRp(valAdminn)));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL TAGIHAN"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph(valNamaPam.toUpperCase()+" MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	}
	
	public String generateReportBangkalan(String path, HashMap<String, Object> map) {
		String namaFile = "";
		try {
			Function func = new Function();
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("tanggal").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("noResi").toString();//"18142680";
			String valNoPelanggan = map.get("idPelanggan").toString();//"0162406900527";
			String valNoSambungan = map.get("noSambungan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
			String valNamaPam = map.get("namaProduk").toString();//"";
			String valAlamat = map.get("alamat").toString();//"1";			
			String valPemakaian = map.get("pemakaian").toString();//"205A";
			String valDenda = map.get("denda").toString();//"MEI12";
			String valTagNonAir = map.get("tagNonAir").toString();//"MEI12";
//			String valMaterai = map.get("materai").toString();//"MEI12";
			String valAdminn = map.get("biayaAdmin").toString();//"Rp 2.500";
			String valTotal = map.get("totalTagihan").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valPeriodeArr = map.get("detaillist").toString();//"SUKSES";
			
			String label = "";
			if (valNoPelanggan.trim().length() == 0 || valNoPelanggan.trim().equals("0")) {
				label = "NO. SAMBUNGAN";
				valNoPelanggan = valNoSambungan;
			} else {
				label = "NO. PELANGGAN";
			}
			
			JsonParser jsonParser = new JsonParser();
			Object object = jsonParser.parse(valPeriodeArr);
			JsonArray periodeArr = new JsonArray();
			periodeArr = (JsonArray) object;
			
			namaFile = "PDAM_"+valIdTransaksi+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN " + valNamaPam.toUpperCase(), titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNamaPam = new PdfPCell(new Paragraph("NAMA PAM"));
	        cellLblNamaPam.setBorder(0);
	        PdfPCell cellLblValNamaPam = new PdfPCell(new Paragraph(valNamaPam));
	        cellLblValNamaPam.setBorder(0);
	        table.addCell(cellLblNamaPam);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaPam);
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph(label));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valNoPelanggan));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        PdfPCell cellLblNama = new PdfPCell(new Paragraph("NAMA"));
	        cellLblNama.setBorder(0);
	        PdfPCell cellLblValNama = new PdfPCell(new Paragraph(valNama));
	        cellLblValNama.setBorder(0);
	        table.addCell(cellLblNama);
	        table.addCell(cell2);
	        table.addCell(cellLblValNama);
	        
	        if (valAlamat.trim().length() != 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("ALAMAT"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valAlamat));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (!valPemakaian.trim().equals("0 M3")) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("PEMAKAIAN"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valPemakaian));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        PdfPCell cellLblRincianTagihan = new PdfPCell(new Paragraph("RINCIAN TAGIHAN"));
	        cellLblRincianTagihan.setBorder(0);
	        PdfPCell cellEmpty= new PdfPCell(new Paragraph(" "));
	        cellEmpty.setBorder(0);
	        table.addCell(cellLblRincianTagihan);
	        table.addCell(cellEmpty);
	        table.addCell(cellEmpty);
	        
	        for (int i=0; i<periodeArr.size(); i++) {
	        	JsonObject child = (JsonObject) periodeArr.get(i);
	        	
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph(child.get("period").getAsString()));
	        	cellLblNpwp.setBorder(0);	        	
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(child.get("total").getAsString())));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
		        
		        if ((i+1) == periodeArr.size()) {
		        	table.addCell(cellEmpty);
			        table.addCell(cellEmpty);
			        table.addCell(cellEmpty);
	        	}
	        }
	        
	        if (Integer.parseInt(valDenda) > 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("DENDA"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(valDenda)));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (Integer.parseInt(valTagNonAir) > 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("TAG.NON AIR"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(valTagNonAir)));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (Integer.parseInt(valAdminn) > 0) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("ADMIN"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(func.getFormatRp(valAdminn)));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL TAGIHAN"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph(valNamaPam.toUpperCase()+" MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	}
	
	public String generateReportMojokerto(String path, HashMap<String, Object> map) {
		String namaFile = "";
		try {
			Function func = new Function();
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("tanggal").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("noResi").toString();//"18142680";
			String valNoPelanggan = map.get("idPelanggan").toString();//"0162406900527";
			String valNoSambungan = map.get("noSambungan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
			String valNamaPam = map.get("namaProduk").toString();//"";
			String valAlamat = map.get("alamat").toString();//"1";			
			String valPemakaian = map.get("pemakaian").toString();//"205A";
			String valDenda = map.get("denda").toString();//"MEI12";
//			String valTagNonAir = map.get("tagNonAir").toString();//"MEI12";
//			String valMaterai = map.get("materai").toString();//"MEI12";
			String valAdminn = map.get("biayaAdmin").toString();//"Rp 2.500";
			String valTotal = map.get("totalTagihan").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valPeriodeArr = map.get("detaillist").toString();//"SUKSES";
			
			String label = "";
			if (valNoPelanggan.trim().length() == 0 || valNoPelanggan.trim().equals("0")) {
				label = "NO. SAMBUNGAN";
				valNoPelanggan = valNoSambungan;
			} else {
				label = "NO. PELANGGAN";
			}
			
			JsonParser jsonParser = new JsonParser();
			Object object = jsonParser.parse(valPeriodeArr);
			JsonArray periodeArr = new JsonArray();
			periodeArr = (JsonArray) object;
			
			namaFile = "PDAM_"+valIdTransaksi+"_"+valNoPelanggan+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN " + valNamaPam.toUpperCase(), titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNamaPam = new PdfPCell(new Paragraph("NAMA PAM"));
	        cellLblNamaPam.setBorder(0);
	        PdfPCell cellLblValNamaPam = new PdfPCell(new Paragraph(valNamaPam));
	        cellLblValNamaPam.setBorder(0);
	        table.addCell(cellLblNamaPam);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaPam);
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph(label));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valNoPelanggan));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        PdfPCell cellLblNama = new PdfPCell(new Paragraph("NAMA"));
	        cellLblNama.setBorder(0);
	        PdfPCell cellLblValNama = new PdfPCell(new Paragraph(valNama));
	        cellLblValNama.setBorder(0);
	        table.addCell(cellLblNama);
	        table.addCell(cell2);
	        table.addCell(cellLblValNama);
	        
	        if (valAlamat.trim().length() != 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("ALAMAT"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valAlamat));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (!valPemakaian.trim().equals("0 M3")) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("PEMAKAIAN"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valPemakaian));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        PdfPCell cellLblRincianTagihan = new PdfPCell(new Paragraph("RINCIAN TAGIHAN"));
	        cellLblRincianTagihan.setBorder(0);
	        PdfPCell cellEmpty= new PdfPCell(new Paragraph(" "));
	        cellEmpty.setBorder(0);
	        table.addCell(cellLblRincianTagihan);
	        table.addCell(cellEmpty);
	        table.addCell(cellEmpty);
	        
	        for (int i=0; i<periodeArr.size(); i++) {
	        	JsonObject child = (JsonObject) periodeArr.get(i);
	        	
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph(child.get("period").getAsString()));
	        	cellLblNpwp.setBorder(0);	        	
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(child.get("total").getAsString())));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
		        
		        if ((i+1) == periodeArr.size()) {
		        	table.addCell(cellEmpty);
			        table.addCell(cellEmpty);
			        table.addCell(cellEmpty);
	        	}
	        }
	        
	        if (Integer.parseInt(valDenda) > 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("DENDA"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(valDenda)));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (Integer.parseInt(valAdminn) > 0) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("ADMIN"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(func.getFormatRp(valAdminn)));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL TAGIHAN"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph(valNamaPam.toUpperCase()+" MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	}
	
	public String generateReportPasuruan(String path, HashMap<String, Object> map) {
		String namaFile = "";
		try {
			Function func = new Function();
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("tanggal").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("noResi").toString();//"18142680";
			String valNoPelanggan = map.get("idPelanggan").toString();//"0162406900527";
			String valNoSambungan = map.get("noSambungan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
			String valNamaPam = map.get("namaProduk").toString();//"";
			String valAlamat = map.get("alamat").toString();//"1";			
			String valPemakaian = map.get("pemakaian").toString();//"205A";
//			String valDenda = map.get("denda").toString();//"MEI12";
			String valTagNonAir = map.get("tagNonAir").toString();//"MEI12";
//			String valMaterai = map.get("materai").toString();//"MEI12";
			String valAdminn = map.get("biayaAdmin").toString();//"Rp 2.500";
			String valTotal = map.get("totalTagihan").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valPeriodeArr = map.get("detaillist").toString();//"SUKSES";
			
			String label = "";
			if (valNoPelanggan.trim().length() == 0 || valNoPelanggan.trim().equals("0")) {
				label = "NO. SAMBUNGAN";
				valNoPelanggan = valNoSambungan;
			} else {
				label = "NO. PELANGGAN";
			}
			
			JsonParser jsonParser = new JsonParser();
			Object object = jsonParser.parse(valPeriodeArr);
			JsonArray periodeArr = new JsonArray();
			periodeArr = (JsonArray) object;
			
			namaFile = "PDAM_"+valIdTransaksi+"_"+valNoPelanggan+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN " + valNamaPam.toUpperCase(), titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNamaPam = new PdfPCell(new Paragraph("NAMA PAM"));
	        cellLblNamaPam.setBorder(0);
	        PdfPCell cellLblValNamaPam = new PdfPCell(new Paragraph(valNamaPam));
	        cellLblValNamaPam.setBorder(0);
	        table.addCell(cellLblNamaPam);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaPam);
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph(label));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valNoPelanggan));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        PdfPCell cellLblNama = new PdfPCell(new Paragraph("NAMA"));
	        cellLblNama.setBorder(0);
	        PdfPCell cellLblValNama = new PdfPCell(new Paragraph(valNama));
	        cellLblValNama.setBorder(0);
	        table.addCell(cellLblNama);
	        table.addCell(cell2);
	        table.addCell(cellLblValNama);
	        
	        if (valAlamat.trim().length() != 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("ALAMAT"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valAlamat));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (!valPemakaian.trim().equals("0 M3")) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("PEMAKAIAN"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valPemakaian));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        PdfPCell cellLblRincianTagihan = new PdfPCell(new Paragraph("RINCIAN TAGIHAN"));
	        cellLblRincianTagihan.setBorder(0);
	        PdfPCell cellEmpty= new PdfPCell(new Paragraph(" "));
	        cellEmpty.setBorder(0);
	        table.addCell(cellLblRincianTagihan);
	        table.addCell(cellEmpty);
	        table.addCell(cellEmpty);
	        
	        for (int i=0; i<periodeArr.size(); i++) {
	        	JsonObject child = (JsonObject) periodeArr.get(i);
	        	
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph(child.get("period").getAsString()));
	        	cellLblNpwp.setBorder(0);	        	
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(child.get("total").getAsString())));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
		        
		        if ((i+1) == periodeArr.size()) {
		        	table.addCell(cellEmpty);
			        table.addCell(cellEmpty);
			        table.addCell(cellEmpty);
	        	}
	        }
	        
	        if (Integer.parseInt(valTagNonAir) > 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("TAG.NON AIR"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(valTagNonAir)));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (Integer.parseInt(valAdminn) > 0) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("ADMIN"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(func.getFormatRp(valAdminn)));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL TAGIHAN"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph(valNamaPam.toUpperCase()+" MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	}
	
	public String generateReportBondowoso(String path, HashMap<String, Object> map) {
		String namaFile = "";
		try {
			Function func = new Function();
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("tanggal").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("noResi").toString();//"18142680";
			String valNoPelanggan = map.get("idPelanggan").toString();//"0162406900527";
			String valNoSambungan = map.get("noSambungan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
			String valNamaPam = map.get("namaProduk").toString();//"";
			String valAlamat = map.get("alamat").toString();//"1";			
			String valPemakaian = map.get("pemakaian").toString();//"205A";
			String valDenda = map.get("denda").toString();//"MEI12";
			String valTagNonAir = map.get("tagNonAir").toString();//"MEI12";
			String valBeban = map.get("beban").toString();//"MEI12";
			String valAdminn = map.get("biayaAdmin").toString();//"Rp 2.500";
			String valTotal = map.get("totalTagihan").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valPeriodeArr = map.get("detaillist").toString();//"SUKSES";
			
			String label = "";
			if (valNoPelanggan.trim().length() == 0 || valNoPelanggan.trim().equals("0")) {
				label = "NO. SAMBUNGAN";
				valNoPelanggan = valNoSambungan;
			} else {
				label = "NO. PELANGGAN";
			}
			
			JsonParser jsonParser = new JsonParser();
			Object object = jsonParser.parse(valPeriodeArr);
			JsonArray periodeArr = new JsonArray();
			periodeArr = (JsonArray) object;
			
			namaFile = "PDAM_"+valIdTransaksi+"_"+valNoPelanggan+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN " + valNamaPam.toUpperCase(), titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNamaPam = new PdfPCell(new Paragraph("NAMA PAM"));
	        cellLblNamaPam.setBorder(0);
	        PdfPCell cellLblValNamaPam = new PdfPCell(new Paragraph(valNamaPam));
	        cellLblValNamaPam.setBorder(0);
	        table.addCell(cellLblNamaPam);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaPam);
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph(label));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valNoPelanggan));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        PdfPCell cellLblNama = new PdfPCell(new Paragraph("NAMA"));
	        cellLblNama.setBorder(0);
	        PdfPCell cellLblValNama = new PdfPCell(new Paragraph(valNama));
	        cellLblValNama.setBorder(0);
	        table.addCell(cellLblNama);
	        table.addCell(cell2);
	        table.addCell(cellLblValNama);
	        
	        if (valAlamat.trim().length() != 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("ALAMAT"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valAlamat));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (!valPemakaian.trim().equals("0 M3")) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("PEMAKAIAN"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valPemakaian));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        PdfPCell cellLblRincianTagihan = new PdfPCell(new Paragraph("RINCIAN TAGIHAN"));
	        cellLblRincianTagihan.setBorder(0);
	        PdfPCell cellEmpty= new PdfPCell(new Paragraph(" "));
	        cellEmpty.setBorder(0);
	        table.addCell(cellLblRincianTagihan);
	        table.addCell(cellEmpty);
	        table.addCell(cellEmpty);
	        
	        for (int i=0; i<periodeArr.size(); i++) {
	        	JsonObject child = (JsonObject) periodeArr.get(i);
	        	
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph(child.get("period").getAsString()));
	        	cellLblNpwp.setBorder(0);	        	
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(child.get("total").getAsString())));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
		        
		        if ((i+1) == periodeArr.size()) {
		        	table.addCell(cellEmpty);
			        table.addCell(cellEmpty);
			        table.addCell(cellEmpty);
	        	}
	        }
	        
	        if (Integer.parseInt(valDenda) > 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("DENDA"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(valDenda)));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (Integer.parseInt(valTagNonAir) > 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("TAG.NON AIR"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(valTagNonAir)));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (Integer.parseInt(valBeban) > 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("BEBAN"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(valBeban)));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (Integer.parseInt(valAdminn) > 0) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("ADMIN"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(func.getFormatRp(valAdminn)));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL TAGIHAN"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph(valNamaPam.toUpperCase()+" MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	}
	
	public String generateReportSurabaya(String path, HashMap<String, Object> map) {
		String namaFile = "";
		try {
			Function func = new Function();
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("tanggal").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("noResi").toString();//"18142680";
			String valNoPelanggan = map.get("idPelanggan").toString();//"0162406900527";
			String valNoSambungan = map.get("noSambungan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
			String valNamaPam = map.get("namaProduk").toString();//"";
			String valAlamat = map.get("alamat").toString();//"1";			
			String valPemakaian = map.get("pemakaian").toString();//"205A";
			String valDenda = map.get("denda").toString();//"MEI12";
			String valTagNonAir = map.get("tagNonAir").toString();//"MEI12";
			String valBeban = map.get("beban").toString();//"MEI12";
			String valAdminn = map.get("biayaAdmin").toString();//"Rp 2.500";
			String valTotal = map.get("totalTagihan").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valPeriodeArr = map.get("detaillist").toString();//"SUKSES";
			
			String label = "";
			if (valNoPelanggan.trim().length() == 0 || valNoPelanggan.trim().equals("0")) {
				label = "NO. SAMBUNGAN";
				valNoPelanggan = valNoSambungan;
			} else {
				label = "NO. PELANGGAN";
			}
			
			JsonParser jsonParser = new JsonParser();
			Object object = jsonParser.parse(valPeriodeArr);
			JsonArray periodeArr = new JsonArray();
			periodeArr = (JsonArray) object;
			
			namaFile = "PDAM_"+valIdTransaksi+"_"+valNoPelanggan+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN " + valNamaPam.toUpperCase(), titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
//	        PdfPCell cellLblNamaPam = new PdfPCell(new Paragraph("NAMA PAM"));
//	        cellLblNamaPam.setBorder(0);
//	        PdfPCell cellLblValNamaPam = new PdfPCell(new Paragraph(valNamaPam));
//	        cellLblValNamaPam.setBorder(0);
//	        table.addCell(cellLblNamaPam);
//	        table.addCell(cell2);
//	        table.addCell(cellLblValNamaPam);
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph(label));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valNoPelanggan));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        PdfPCell cellLblNama = new PdfPCell(new Paragraph("NAMA"));
	        cellLblNama.setBorder(0);
	        PdfPCell cellLblValNama = new PdfPCell(new Paragraph(valNama));
	        cellLblValNama.setBorder(0);
	        table.addCell(cellLblNama);
	        table.addCell(cell2);
	        table.addCell(cellLblValNama);
	        
	        if (valAlamat.trim().length() != 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("ALAMAT"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valAlamat));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (!valPemakaian.trim().equals("0 M3")) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("STAND METER"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valPemakaian));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        PdfPCell cellLblRincianTagihan = new PdfPCell(new Paragraph("RINCIAN TAGIHAN"));
	        cellLblRincianTagihan.setBorder(0);
	        PdfPCell cellEmpty= new PdfPCell(new Paragraph(" "));
	        cellEmpty.setBorder(0);
	        table.addCell(cellLblRincianTagihan);
	        table.addCell(cellEmpty);
	        table.addCell(cellEmpty);
	        
	        for (int i=0; i<periodeArr.size(); i++) {
	        	JsonObject child = (JsonObject) periodeArr.get(i);
	        	
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph(child.get("period").getAsString()));
	        	cellLblNpwp.setBorder(0);	        	
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(child.get("total").getAsString())));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
		        
		        if ((i+1) == periodeArr.size()) {
		        	table.addCell(cellEmpty);
			        table.addCell(cellEmpty);
			        table.addCell(cellEmpty);
	        	}
	        }
	        
	        if (Integer.parseInt(valDenda) > 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("TAG.LALU/DENDA"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(valDenda)));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (Integer.parseInt(valBeban) > 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("BEBAN"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(valBeban)));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (Integer.parseInt(valTagNonAir) > 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("RESTRIBUSI"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(valTagNonAir)));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (Integer.parseInt(valAdminn) > 0) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("ADMIN"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(func.getFormatRp(valAdminn)));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL TAGIHAN"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph(valNamaPam.toUpperCase()+" MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	}
	
	
	
	public String generateReportBojonegoro(String path, HashMap<String, Object> map) {
		String namaFile = "";
		try {
			Function func = new Function();
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("tanggal").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("noResi").toString();//"18142680";
			String valNoPelanggan = map.get("idPelanggan").toString();//"0162406900527";
			String valNoSambungan = map.get("noSambungan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
			String valNamaPam = map.get("namaProduk").toString();//"";
			String valAlamat = map.get("alamat").toString();//"1";			
			String valPemakaian = map.get("pemakaian").toString();//"205A";
			String valDenda = map.get("denda").toString();//"MEI12";
			String valTagNonAir = map.get("tagNonAir").toString();//"MEI12";
//			String valMaterai = map.get("materai").toString();//"MEI12";
			String valAdminn = map.get("biayaAdmin").toString();//"Rp 2.500";
			String valTotal = map.get("totalTagihan").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valPeriodeArr = map.get("detaillist").toString();//"SUKSES";
			
			String label = "";
			if (valNoPelanggan.trim().length() == 0 || valNoPelanggan.trim().equals("0")) {
				label = "NO. SAMBUNGAN";
				valNoPelanggan = valNoSambungan;
			} else {
				label = "NO. PELANGGAN";
			}
			
			JsonParser jsonParser = new JsonParser();
			Object object = jsonParser.parse(valPeriodeArr);
			JsonArray periodeArr = new JsonArray();
			periodeArr = (JsonArray) object;
			
			namaFile = "PDAM_"+valIdTransaksi+"_"+valNoPelanggan+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN " + valNamaPam.toUpperCase(), titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNamaPam = new PdfPCell(new Paragraph("NAMA PAM"));
	        cellLblNamaPam.setBorder(0);
	        PdfPCell cellLblValNamaPam = new PdfPCell(new Paragraph(valNamaPam));
	        cellLblValNamaPam.setBorder(0);
	        table.addCell(cellLblNamaPam);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaPam);
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph(label));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valNoPelanggan));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        PdfPCell cellLblNama = new PdfPCell(new Paragraph("NAMA"));
	        cellLblNama.setBorder(0);
	        PdfPCell cellLblValNama = new PdfPCell(new Paragraph(valNama));
	        cellLblValNama.setBorder(0);
	        table.addCell(cellLblNama);
	        table.addCell(cell2);
	        table.addCell(cellLblValNama);
	        
	        if (valAlamat.trim().length() != 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("ALAMAT"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valAlamat));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (!valPemakaian.trim().equals("0 M3")) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("STAND METER"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valPemakaian));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        PdfPCell cellLblRincianTagihan = new PdfPCell(new Paragraph("RINCIAN TAGIHAN"));
	        cellLblRincianTagihan.setBorder(0);
	        PdfPCell cellEmpty= new PdfPCell(new Paragraph(" "));
	        cellEmpty.setBorder(0);
	        table.addCell(cellLblRincianTagihan);
	        table.addCell(cellEmpty);
	        table.addCell(cellEmpty);
	        
	        for (int i=0; i<periodeArr.size(); i++) {
	        	JsonObject child = (JsonObject) periodeArr.get(i);
	        	
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph(child.get("period").getAsString()));
	        	cellLblNpwp.setBorder(0);	        	
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(child.get("total").getAsString())));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
		        
		        if ((i+1) == periodeArr.size()) {
		        	table.addCell(cellEmpty);
			        table.addCell(cellEmpty);
			        table.addCell(cellEmpty);
	        	}
	        }
	        
	        if (Integer.parseInt(valDenda) > 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("DENDA"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(valDenda)));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (Integer.parseInt(valTagNonAir) > 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("TAG.NON AIR"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(func.getFormatRp(valTagNonAir)));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        if (Integer.parseInt(valAdminn) > 0) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("ADMIN"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(func.getFormatRp(valAdminn)));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL TAGIHAN"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph(valNamaPam.toUpperCase()+" MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	}
	
//	public static void main(String[] args) {
//		try {
//			new TelkomReport().generateReport();
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.error("", e);
//		}
//		
//	}

}
