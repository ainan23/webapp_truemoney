package id.co.truemoney.report;

import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.truemoneywitami.function.Function;

public class TvPascaReport {
	
	private static Logger log = Logger.getLogger(TvPascaReport.class);
	
	private static ResourceBundle bundle = ResourceBundle.getBundle("config.config");
	private String pathImg = bundle.getString("url_path_img_report");
	
	private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	private static Font footerFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
	
	Function func = new Function();
	
	public String generateReport(String path, HashMap<String, Object> map) {
		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("timeStamp").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("ref").toString();//"18142680";
			String valOperator = "TV BIG"; //map.get("namaProduk").toString().toUpperCase();//"18142680";
			String valNoJastel = map.get("idPelanggan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
			String valNominal = map.get("tagihan").toString();//"Rp 744.750";
			String valTotal = map.get("totalBayar").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			
			namaFile = valOperator+"_"+valIdTransaksi+"_"+valNoJastel+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN TV PASCABAYAR", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNama = new PdfPCell(new Paragraph("NAMA PRODUK"));
	        cellLblNama.setBorder(0);
	        PdfPCell cellLblValNama = new PdfPCell(new Paragraph(valOperator));
	        cellLblValNama.setBorder(0);
	        table.addCell(cellLblNama);
	        table.addCell(cell2);
	        table.addCell(cellLblValNama);
	        
	        PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
	        cellLblNamaPel.setBorder(0);
	        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
	        cellLblValNamaPel.setBorder(0);
	        table.addCell(cellLblNamaPel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaPel);
	        
	        PdfPCell cellLblId = new PdfPCell(new Paragraph("ID PEL"));
	        cellLblId.setBorder(0);
	        PdfPCell cellLblValId = new PdfPCell(new Paragraph(valNoJastel));
	        cellLblValId.setBorder(0);
	        table.addCell(cellLblId);
	        table.addCell(cell2);
	        table.addCell(cellLblValId);
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("TAGIHAN"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valNominal));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wordingThanks = new Paragraph("TERIMA KASIH TELAH MELAKUKAN PEMBAYARAN TV BIG BERLANGGANAN.");
	        wordingThanks.setAlignment(Element.ALIGN_CENTER);
	        Paragraph wording = new Paragraph("TV BIG MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wordingThanks);
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	}

	public String generateReportIndovision(String path, HashMap<String, Object> map) {

		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("timeStamp").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("ref").toString();//"18142680";
			String valOperator = map.get("namaProduk").toString().toUpperCase();//"18142680";
			String valNoJastel = map.get("idPelanggan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
			String valNominal = map.get("tagihan").toString();//"Rp 744.750";
			String valTotal = map.get("totalBayar").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valPeriode = map.get("periode").toString();//"SUKSES";
			String valBiaya = map.get("biayaAdmin").toString();//"SUKSES";
			
			namaFile = valOperator+"_"+valIdTransaksi+"_"+valNoJastel+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN TV PASCABAYAR", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblId = new PdfPCell(new Paragraph("ID PEL"));
	        cellLblId.setBorder(0);
	        PdfPCell cellLblValId = new PdfPCell(new Paragraph(valNoJastel));
	        cellLblValId.setBorder(0);
	        table.addCell(cellLblId);
	        table.addCell(cell2);
	        table.addCell(cellLblValId);
	        
	        if (valNama.trim().length() > 0) {
	        	PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
		        cellLblNamaPel.setBorder(0);
		        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
		        cellLblValNamaPel.setBorder(0);
		        table.addCell(cellLblNamaPel);
		        table.addCell(cell2);
		        table.addCell(cellLblValNamaPel);
	        }	        
	        
	        PdfPCell cellLblPeriode = new PdfPCell(new Paragraph("PERIODE BAYAR"));
	        cellLblPeriode.setBorder(0);
	        PdfPCell cellLblValPeriode = new PdfPCell(new Paragraph(valPeriode));
	        cellLblValPeriode.setBorder(0);
	        table.addCell(cellLblPeriode);
	        table.addCell(cell2);
	        table.addCell(cellLblValPeriode);
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("TAGIHAN"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valNominal));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        if (!valBiaya.trim().equals("0")) {
	        	PdfPCell cellLblBiaya = new PdfPCell(new Paragraph("BIAYA ADMIN"));
		        cellLblBiaya.setBorder(0);
		        PdfPCell cellLblValBiaya = new PdfPCell(new Paragraph(func.getFormatRp(valBiaya)));
		        cellLblValBiaya.setBorder(0);
		        table.addCell(cellLblBiaya);
		        table.addCell(cell2);
		        table.addCell(cellLblValBiaya);
	        }	        
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        if (valOperator.contains("INDOVISION")) {
	        	valOperator = "INDOVISION TV";
	        } else if (valOperator.contains("TOP")) {
	        	valOperator = "TOP TV";
	        } else if (valOperator.contains("OKEVISION")) {
	        	valOperator = "OKEVISION TV";
	        }
	        
	        Paragraph wordingThanks = new Paragraph("TERIMA KASIH TELAH MELAKUKAN PEMBAYARAN "+valOperator+" BERLANGGANAN.");
	        wordingThanks.setAlignment(Element.ALIGN_CENTER);
	        Paragraph wording = new Paragraph(valOperator+" MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wordingThanks);
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	
	}

	public String generateReportInnovate(String path, HashMap<String, Object> map) {

		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("timeStamp").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("ref").toString();//"18142680";
			String valOperator = "TV INNOVATE"; //map.get("namaProduk").toString().toUpperCase();//"18142680";
			String valNoJastel = map.get("idPelanggan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
			String valNominal = map.get("tagihan").toString();//"Rp 744.750";
			String valTotal = map.get("totalBayar").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valSn = map.get("sn").toString();//"SUKSES";
			
			namaFile = valOperator+"_"+valIdTransaksi+"_"+valNoJastel+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN TV PASCABAYAR", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblId = new PdfPCell(new Paragraph("ID PEL"));
	        cellLblId.setBorder(0);
	        PdfPCell cellLblValId = new PdfPCell(new Paragraph(valNoJastel));
	        cellLblValId.setBorder(0);
	        table.addCell(cellLblId);
	        table.addCell(cell2);
	        table.addCell(cellLblValId);
	        
	        PdfPCell cellLblRef = new PdfPCell(new Paragraph("REF"));
	        cellLblRef.setBorder(0);
	        PdfPCell cellLblValRef = new PdfPCell(new Paragraph(valSn));
	        cellLblValRef.setBorder(0);
	        table.addCell(cellLblRef);
	        table.addCell(cell2);
	        table.addCell(cellLblValRef);
	        
	        PdfPCell cellLblNama = new PdfPCell(new Paragraph("NAMA PRODUK"));
	        cellLblNama.setBorder(0);
	        PdfPCell cellLblValNama = new PdfPCell(new Paragraph(valOperator));
	        cellLblValNama.setBorder(0);
	        table.addCell(cellLblNama);
	        table.addCell(cell2);
	        table.addCell(cellLblValNama);
	        
	        PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
	        cellLblNamaPel.setBorder(0);
	        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
	        cellLblValNamaPel.setBorder(0);
	        table.addCell(cellLblNamaPel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaPel);
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("TAGIHAN"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valNominal));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wordingThanks = new Paragraph("TERIMA KASIH TELAH MELAKUKAN PEMBAYARAN "+valOperator+" BERLANGGANAN.");
	        wordingThanks.setAlignment(Element.ALIGN_CENTER);
	        Paragraph wording = new Paragraph(valOperator + " MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wordingThanks);
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	
	}
	
	public String generateReportNex(String path, HashMap<String, Object> map) {

		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("timeStamp").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("sn").toString();//"18142680";
			String valOperator = map.get("namaProduk").toString().toUpperCase();//"18142680";
			String valNoJastel = map.get("idPelanggan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
			String valNominal = map.get("tagihan").toString();//"Rp 744.750";
			String valTotal = map.get("totalBayar").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valPeriode = map.get("periode").toString();//"SUKSES";
			String valBiaya = map.get("biayaAdmin").toString();//"SUKSES";
			String valProductCat = map.get("productCategory").toString();
			String valRef2 = map.get("ref").toString();
			
			namaFile = valOperator+"_"+valIdTransaksi+"_"+valNoJastel+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN TV PASCABAYAR", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valRef2));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblId = new PdfPCell(new Paragraph("ID PEL"));
	        cellLblId.setBorder(0);
	        PdfPCell cellLblValId = new PdfPCell(new Paragraph(valNoJastel));
	        cellLblValId.setBorder(0);
	        table.addCell(cellLblId);
	        table.addCell(cell2);
	        table.addCell(cellLblValId);
	        
	        if (valNama.trim().length() > 0) {
	        	PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
		        cellLblNamaPel.setBorder(0);
		        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
		        cellLblValNamaPel.setBorder(0);
		        table.addCell(cellLblNamaPel);
		        table.addCell(cell2);
		        table.addCell(cellLblValNamaPel);
	        }	    
	        
	        PdfPCell cellLblNoRef = new PdfPCell(new Paragraph("REF"));
	        cellLblNoRef.setBorder(0);
	        PdfPCell cellLblValNoRef = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoRef.setBorder(0);
	        table.addCell(cellLblNoRef);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoRef);
	        
	        PdfPCell cellLblPaket = new PdfPCell(new Paragraph("PAKET"));
	        cellLblPaket.setBorder(0);
	        PdfPCell cellLblValPaket = new PdfPCell(new Paragraph(valProductCat));
	        cellLblValPaket.setBorder(0);
	        table.addCell(cellLblPaket);
	        table.addCell(cell2);
	        table.addCell(cellLblValPaket);
	        
	        PdfPCell cellLblPeriode = new PdfPCell(new Paragraph("JATUH TEMPO"));
	        cellLblPeriode.setBorder(0);
	        PdfPCell cellLblValPeriode = new PdfPCell(new Paragraph(valPeriode));
	        cellLblValPeriode.setBorder(0);
	        table.addCell(cellLblPeriode);
	        table.addCell(cell2);
	        table.addCell(cellLblValPeriode);
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("TAGIHAN"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valNominal));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        if (!valBiaya.trim().equals("0")) {
	        	PdfPCell cellLblBiaya = new PdfPCell(new Paragraph("BIAYA ADMIN"));
		        cellLblBiaya.setBorder(0);
		        PdfPCell cellLblValBiaya = new PdfPCell(new Paragraph(func.getFormatRp(valBiaya)));
		        cellLblValBiaya.setBorder(0);
		        table.addCell(cellLblBiaya);
		        table.addCell(cell2);
		        table.addCell(cellLblValBiaya);
	        }	        
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wordingThanks = new Paragraph("CALL CENTER NEXMEDIA 021-29352600.");
	        wordingThanks.setAlignment(Element.ALIGN_CENTER);
	        Paragraph wording = new Paragraph("NEX MEDIA MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wordingThanks);
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	
	}
	
	public String generateReportTopas(String path, HashMap<String, Object> map) {

		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("timeStamp").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("sn").toString();//"18142680";
			String valOperator = map.get("namaProduk").toString().toUpperCase();//"18142680";
			String valNoJastel = map.get("idPelanggan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
			String valNominal = map.get("tagihan").toString();//"Rp 744.750";
			String valTotal = map.get("totalBayar").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valPeriode = map.get("periode").toString();//"SUKSES";
			String valBiaya = map.get("biayaAdmin").toString();//"SUKSES";
			String valRef2 = map.get("ref").toString();//"SUKSES";
			
			namaFile = valOperator+"_"+valIdTransaksi+"_"+valNoJastel+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN TV PASCABAYAR", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valRef2));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblId = new PdfPCell(new Paragraph("ID PEL"));
	        cellLblId.setBorder(0);
	        PdfPCell cellLblValId = new PdfPCell(new Paragraph(valNoJastel));
	        cellLblValId.setBorder(0);
	        table.addCell(cellLblId);
	        table.addCell(cell2);
	        table.addCell(cellLblValId);
	        
	        if (valNama.trim().length() > 0) {
	        	PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
		        cellLblNamaPel.setBorder(0);
		        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
		        cellLblValNamaPel.setBorder(0);
		        table.addCell(cellLblNamaPel);
		        table.addCell(cell2);
		        table.addCell(cellLblValNamaPel);
	        }	    
	        
	        PdfPCell cellLblNoRef = new PdfPCell(new Paragraph("REF"));
	        cellLblNoRef.setBorder(0);
	        PdfPCell cellLblValNoRef = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoRef.setBorder(0);
	        table.addCell(cellLblNoRef);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoRef);
	        
	        PdfPCell cellLblPeriode = new PdfPCell(new Paragraph("PERIODE BAYAR"));
	        cellLblPeriode.setBorder(0);
	        PdfPCell cellLblValPeriode = new PdfPCell(new Paragraph(valPeriode));
	        cellLblValPeriode.setBorder(0);
	        table.addCell(cellLblPeriode);
	        table.addCell(cell2);
	        table.addCell(cellLblValPeriode);
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("TAGIHAN"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valNominal));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        if (!valBiaya.trim().equals("0")) {
	        	PdfPCell cellLblBiaya = new PdfPCell(new Paragraph("BIAYA ADMIN"));
		        cellLblBiaya.setBorder(0);
		        PdfPCell cellLblValBiaya = new PdfPCell(new Paragraph(func.getFormatRp(valBiaya)));
		        cellLblValBiaya.setBorder(0);
		        table.addCell(cellLblBiaya);
		        table.addCell(cell2);
		        table.addCell(cellLblValBiaya);
	        }	        
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wordingThanks = new Paragraph("CALL CENTER TOPAS TV 021-29396363 / 08041292929.");
	        wordingThanks.setAlignment(Element.ALIGN_CENTER);
	        Paragraph wording = new Paragraph("TOPAS TV MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wordingThanks);
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	
	}
	
	public String generateReportOrange(String path, HashMap<String, Object> map) {

		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("timeStamp").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("sn").toString();//"18142680";
			String valOperator = "ORANGE TV";//map.get("namaProduk").toString().toUpperCase();//"18142680";
			String valNoJastel = map.get("idPelanggan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
			String valNominal = map.get("tagihan").toString();//"Rp 744.750";
			String valTotal = map.get("totalBayar").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valBiaya = map.get("biayaAdmin").toString();//"SUKSES";
			String valRef2 = map.get("ref").toString();//"SUKSES";
			
			namaFile = valOperator+"_"+valIdTransaksi+"_"+valNoJastel+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBELIAN VOUCHER TV", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valRef2));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblProduk = new PdfPCell(new Paragraph("NAMA PRODUK"));
	        cellLblProduk.setBorder(0);
	        PdfPCell cellLblValProduk = new PdfPCell(new Paragraph(valOperator));
	        cellLblValProduk.setBorder(0);
	        table.addCell(cellLblProduk);
	        table.addCell(cell2);
	        table.addCell(cellLblValProduk);
	        
	        PdfPCell cellLblId = new PdfPCell(new Paragraph("ID PELANGGAN"));
	        cellLblId.setBorder(0);
	        PdfPCell cellLblValId = new PdfPCell(new Paragraph(valNoJastel));
	        cellLblValId.setBorder(0);
	        table.addCell(cellLblId);
	        table.addCell(cell2);
	        table.addCell(cellLblValId);
	        
	        if (valNama.trim().length() > 0) {
	        	PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
		        cellLblNamaPel.setBorder(0);
		        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
		        cellLblValNamaPel.setBorder(0);
		        table.addCell(cellLblNamaPel);
		        table.addCell(cell2);
		        table.addCell(cellLblValNamaPel);
	        }	    
	        
	        PdfPCell cellLblNoRef = new PdfPCell(new Paragraph("NO. REF"));
	        cellLblNoRef.setBorder(0);
	        PdfPCell cellLblValNoRef = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoRef.setBorder(0);
	        table.addCell(cellLblNoRef);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoRef);
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("DENOMINASI"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valNominal));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        if (!valBiaya.trim().equals("0")) {
	        	PdfPCell cellLblBiaya = new PdfPCell(new Paragraph("BIAYA ADMIN"));
		        cellLblBiaya.setBorder(0);
		        PdfPCell cellLblValBiaya = new PdfPCell(new Paragraph(func.getFormatRp(valBiaya)));
		        cellLblValBiaya.setBorder(0);
		        table.addCell(cellLblBiaya);
		        table.addCell(cell2);
		        table.addCell(cellLblValBiaya);
	        }	        
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph(valOperator + " MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	
	}
	
	public String generateReportKvision(String path, HashMap<String, Object> map) {

		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("timeStamp").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("sn").toString();//"18142680";
			String valOperator = "K-Vision TV";//map.get("namaProduk").toString().toUpperCase();//"18142680";
			String valNoJastel = map.get("idPelanggan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
			String valNominal = map.get("tagihan").toString();//"Rp 744.750";
			String valTotal = map.get("totalBayar").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valBiaya = map.get("biayaAdmin").toString();//"SUKSES";
			String valRef2 = map.get("ref").toString();//"SUKSES";
			
			namaFile = valOperator+"_"+valIdTransaksi+"_"+valNoJastel+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBELIAN VOUCHER TV", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valRef2));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblProduk = new PdfPCell(new Paragraph("NAMA PRODUK"));
	        cellLblProduk.setBorder(0);
	        PdfPCell cellLblValProduk = new PdfPCell(new Paragraph(valOperator));
	        cellLblValProduk.setBorder(0);
	        table.addCell(cellLblProduk);
	        table.addCell(cell2);
	        table.addCell(cellLblValProduk);
	        
	        PdfPCell cellLblId = new PdfPCell(new Paragraph("ID PELANGGAN"));
	        cellLblId.setBorder(0);
	        PdfPCell cellLblValId = new PdfPCell(new Paragraph(valNoJastel));
	        cellLblValId.setBorder(0);
	        table.addCell(cellLblId);
	        table.addCell(cell2);
	        table.addCell(cellLblValId);
	        
	        if (valNama.trim().length() > 0) {
	        	PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
		        cellLblNamaPel.setBorder(0);
		        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
		        cellLblValNamaPel.setBorder(0);
		        table.addCell(cellLblNamaPel);
		        table.addCell(cell2);
		        table.addCell(cellLblValNamaPel);
	        }	    
	        
	        PdfPCell cellLblNoRef = new PdfPCell(new Paragraph("NO. REF TRX"));
	        cellLblNoRef.setBorder(0);
	        PdfPCell cellLblValNoRef = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoRef.setBorder(0);
	        table.addCell(cellLblNoRef);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoRef);
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("DENOMINASI"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valNominal));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        if (!valBiaya.trim().equals("0")) {
	        	PdfPCell cellLblBiaya = new PdfPCell(new Paragraph("BIAYA ADMIN"));
		        cellLblBiaya.setBorder(0);
		        PdfPCell cellLblValBiaya = new PdfPCell(new Paragraph(func.getFormatRp(valBiaya)));
		        cellLblValBiaya.setBorder(0);
		        table.addCell(cellLblBiaya);
		        table.addCell(cell2);
		        table.addCell(cellLblValBiaya);
	        }	        
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph(valOperator + " MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        Paragraph wordingAktifasiPaket = new Paragraph("AKTIVASI PAKET: KETIK KVN<SPASI>PKT#NAMA_PAKET#NO_PELANGGAN");
	        wordingAktifasiPaket.setAlignment(Element.ALIGN_CENTER);
	        
	        Paragraph wordingKirimKe = new Paragraph("KIRIM KE 99333 (T-SEL, INDOSAT, 3). CALL CENTER K-VISION: 1500 828");
	        wordingKirimKe.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(wordingAktifasiPaket);
	        doc.add(wordingKirimKe);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	
	}
	
	public String generateReportSkynindo(String path, HashMap<String, Object> map) {
		
		Function fung = new Function();

		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("timeStamp").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("sn").toString();//"18142680";
			String valOperator = "TV SKYNINDO";//map.get("namaProduk").toString().toUpperCase();//"18142680";
			String valNoJastel = map.get("idPelanggan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
			String valTotal = map.get("totalBayar").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valBiaya = map.get("biayaAdmin").toString();//"SUKSES";
			String valRef2 = map.get("ref").toString();//"SUKSES";
			String valDenom = map.get("denom").toString();//"SUKSES";
			String valPaket = map.get("paket").toString();//"SUKSES";
			String valTempo = map.get("jatuh_tempo").toString();//"SUKSES";
			
			String temp [] = valPaket.split("-");
			valPaket = "SKYNINDO TV " + temp[0].trim() + " (" + fung.getFormatTitik(valDenom) + ")";
			
			namaFile = valOperator+"_"+valIdTransaksi+"_"+valNoJastel+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBELIAN VOUCHER TV", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valRef2));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNoRef = new PdfPCell(new Paragraph("NO. REF"));
	        cellLblNoRef.setBorder(0);
	        PdfPCell cellLblValNoRef = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoRef.setBorder(0);
	        table.addCell(cellLblNoRef);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoRef);
	        
	        PdfPCell cellLblProduk = new PdfPCell(new Paragraph("NAMA PRODUK"));
	        cellLblProduk.setBorder(0);
	        PdfPCell cellLblValProduk = new PdfPCell(new Paragraph(valOperator));
	        cellLblValProduk.setBorder(0);
	        table.addCell(cellLblProduk);
	        table.addCell(cell2);
	        table.addCell(cellLblValProduk);
	        
	        PdfPCell cellLblId = new PdfPCell(new Paragraph("ID PELANGGAN"));
	        cellLblId.setBorder(0);
	        PdfPCell cellLblValId = new PdfPCell(new Paragraph(valNoJastel));
	        cellLblValId.setBorder(0);
	        table.addCell(cellLblId);
	        table.addCell(cell2);
	        table.addCell(cellLblValId);
	        
	        if (valNama.trim().length() > 0) {
	        	PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
		        cellLblNamaPel.setBorder(0);
		        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
		        cellLblValNamaPel.setBorder(0);
		        table.addCell(cellLblNamaPel);
		        table.addCell(cell2);
		        table.addCell(cellLblValNamaPel);
	        }	
	        
	        PdfPCell cellLblPaket = new PdfPCell(new Paragraph("PAKET"));
	        cellLblPaket.setBorder(0);
	        PdfPCell cellLblValPaket = new PdfPCell(new Paragraph(valPaket));
	        cellLblValPaket.setBorder(0);
	        table.addCell(cellLblPaket);
	        table.addCell(cell2);
	        table.addCell(cellLblValPaket);
	        
	        if (valTempo.trim().length() > 0) {
	        	PdfPCell cellLblNominal = new PdfPCell(new Paragraph("JATUH TEMPO"));
		        cellLblNominal.setBorder(0);
		        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valTempo));
		        cellLblValNominal.setBorder(0);
		        table.addCell(cellLblNominal);
		        table.addCell(cell2);
		        table.addCell(cellLblValNominal);
	        }
	        
	        
	        if (!valBiaya.trim().equals("0")) {
	        	PdfPCell cellLblBiaya = new PdfPCell(new Paragraph("BIAYA ADMIN"));
		        cellLblBiaya.setBorder(0);
		        PdfPCell cellLblValBiaya = new PdfPCell(new Paragraph(func.getFormatRp(valBiaya)));
		        cellLblValBiaya.setBorder(0);
		        table.addCell(cellLblBiaya);
		        table.addCell(cell2);
		        table.addCell(cellLblValBiaya);
	        }	        
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph("SIMPAN STRUK INI SEBAGAI BUKTI TRANSAKSI");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        Paragraph wordingAktifasiPaket = new Paragraph("UNTUK PERUBAHAN PAKET SKYNINDO : 021�29678222");
	        wordingAktifasiPaket.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(wordingAktifasiPaket);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	
	}
	
//	public static void main(String[] args) {
//		try {
//			new TelkomReport().generateReport();
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.error("", e);
//		}
//		
//	}

}
