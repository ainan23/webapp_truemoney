package id.co.truemoney.report;

import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.truemoneywitami.function.Function;

public class MultiFinanceReport {
	
	private static Logger log = Logger.getLogger(MultiFinanceReport.class);
	
	private static ResourceBundle bundle = ResourceBundle.getBundle("config.config");
	private String pathImg = bundle.getString("url_path_img_report");
	
	private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	private static Font footerFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
	
	Function func = new Function();
	
	public String generateReport(String path, HashMap<String, Object> map) {
		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("timeStamp").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("ref2").toString();//"18142680";
			String valOperator = map.get("namaProduk").toString().toUpperCase();//"18142680";
			String valNoJastel = map.get("idPelanggan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
//			String valNominal = map.get("tagihan").toString();//"Rp 744.750";
			String valNominalBersih = map.get("tagihan_non_pinalty").toString();//"Rp 744.750";
			
			String valTotal = map.get("totalBayar").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valRef = map.get("ref").toString();//"SUKSES";
			String valPeriode = map.get("periode").toString();
			String valAdmin = map.get("biayaAdmin").toString();
			String valPinalty = map.get("pinalty").toString();
			
			namaFile = valOperator+"_"+valIdTransaksi+"_"+valNoJastel+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN PINJAMAN/KREDIT MULTI FINANCE", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNama = new PdfPCell(new Paragraph("NAMA KREDIT"));
	        cellLblNama.setBorder(0);
	        PdfPCell cellLblValNama = new PdfPCell(new Paragraph(valOperator.toUpperCase()));
	        cellLblValNama.setBorder(0);
	        table.addCell(cellLblNama);
	        table.addCell(cell2);
	        table.addCell(cellLblValNama);
	        
	        PdfPCell cellLblId = new PdfPCell(new Paragraph("NOMOR KONTRAK"));
	        cellLblId.setBorder(0);
	        PdfPCell cellLblValId = new PdfPCell(new Paragraph(valNoJastel));
	        cellLblValId.setBorder(0);
	        table.addCell(cellLblId);
	        table.addCell(cell2);
	        table.addCell(cellLblValId);
	        
	        PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
	        cellLblNamaPel.setBorder(0);
	        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
	        cellLblValNamaPel.setBorder(0);
	        table.addCell(cellLblNamaPel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaPel);
	        
	        if (!valRef.trim().equals("")) {
	        	PdfPCell cellLblReff = new PdfPCell(new Paragraph("ID REFF"));
		        cellLblReff.setBorder(0);
		        PdfPCell cellLblValReff = new PdfPCell(new Paragraph(valRef));
		        cellLblValReff.setBorder(0);
		        table.addCell(cellLblReff);
		        table.addCell(cell2);
		        table.addCell(cellLblValReff);
	        }
	        
	        PdfPCell cellLblPeriode = new PdfPCell(new Paragraph("NO ANGSURAN"));
	        cellLblPeriode.setBorder(0);
	        PdfPCell cellLblValPeriode = new PdfPCell(new Paragraph(valPeriode));
	        cellLblValPeriode.setBorder(0);
	        table.addCell(cellLblPeriode);
	        table.addCell(cell2);
	        table.addCell(cellLblValPeriode);
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("JUMLAH TAGIHAN"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(func.getFormatRp(valNominalBersih)));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        if (Long.parseLong(valPinalty) > 0) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("PINALTI"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(func.getFormatRp(valPinalty)));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }
	        
	        if (!valAdmin.trim().equals("0")) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("ADMIN"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(func.getFormatRp(valAdmin)));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }
	        
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL TAGIHAN"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph(valOperator.toUpperCase() + " MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	}

	public String generateReportColumbia(String path, HashMap<String, Object> map) {
		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("timeStamp").toString();//"24-05-2012 11:20:49";
			String valOperator = map.get("namaProduk").toString().toUpperCase();//"18142680";
			String valNoJastel = map.get("idPelanggan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
//			String valNominal = map.get("tagihan").toString();//"Rp 744.750";
			String valNominalBersih = map.get("tagihan_non_pinalty").toString();//"Rp 744.750";
			
			String valTotal = map.get("totalBayar").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valRef = map.get("sn").toString();//"SUKSES";
			String valPeriode = map.get("periode").toString();
			String valPinalty = map.get("pinalty").toString();
			String valTenor = map.get("tenor").toString();
			String valRef2 = map.get("ref2").toString();
			
			namaFile = valOperator+"_"+valIdTransaksi+"_"+valNoJastel+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN PINJAMAN/KREDIT MULTI FINANCE", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valRef2));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNama = new PdfPCell(new Paragraph("NAMA KREDIT"));
	        cellLblNama.setBorder(0);
	        PdfPCell cellLblValNama = new PdfPCell(new Paragraph(valOperator.toUpperCase()));
	        cellLblValNama.setBorder(0);
	        table.addCell(cellLblNama);
	        table.addCell(cell2);
	        table.addCell(cellLblValNama);
	        
	        PdfPCell cellLblId = new PdfPCell(new Paragraph("NOMOR KONTRAK"));
	        cellLblId.setBorder(0);
	        PdfPCell cellLblValId = new PdfPCell(new Paragraph(valNoJastel));
	        cellLblValId.setBorder(0);
	        table.addCell(cellLblId);
	        table.addCell(cell2);
	        table.addCell(cellLblValId);
	        
	        PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
	        cellLblNamaPel.setBorder(0);
	        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
	        cellLblValNamaPel.setBorder(0);
	        table.addCell(cellLblNamaPel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaPel);
	        
//	        if (!valRef.trim().equals("")) {
	        	PdfPCell cellLblReff = new PdfPCell(new Paragraph("ID REF"));
		        cellLblReff.setBorder(0);
		        PdfPCell cellLblValReff = new PdfPCell(new Paragraph(valRef));
		        cellLblValReff.setBorder(0);
		        table.addCell(cellLblReff);
		        table.addCell(cell2);
		        table.addCell(cellLblValReff);
//	        }
	        
		    if (!valPeriode.trim().equals("") || !valPeriode.trim().equals("0")) {   
		    	PdfPCell cellLblPeriode = new PdfPCell(new Paragraph("NO ANGSURAN"));
		        cellLblPeriode.setBorder(0);
		        PdfPCell cellLblValPeriode = new PdfPCell(new Paragraph(valPeriode));
		        cellLblValPeriode.setBorder(0);
		        table.addCell(cellLblPeriode);
		        table.addCell(cell2);
		        table.addCell(cellLblValPeriode);
		    }
		    
		    if (!valTenor.trim().equals("") || !valTenor.trim().equals("0")) {   
		    	PdfPCell cellLblPeriode = new PdfPCell(new Paragraph("TENOR"));
		        cellLblPeriode.setBorder(0);
		        PdfPCell cellLblValPeriode = new PdfPCell(new Paragraph(valTenor+" BULAN"));
		        cellLblValPeriode.setBorder(0);
		        table.addCell(cellLblPeriode);
		        table.addCell(cell2);
		        table.addCell(cellLblValPeriode);
		    }
	        
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("JUMLAH TAGIHAN"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(func.getFormatRp(valNominalBersih)));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        if (Long.parseLong(valPinalty) > 0) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("PINALTI"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(func.getFormatRp(valPinalty)));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }	        
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL TAGIHAN"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph(valOperator.toUpperCase() + " MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	}
	
	public String generateReportBaf(String path, HashMap<String, Object> map) {
		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("timeStamp").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("ref2").toString();//"18142680";
			String valNoJastel = map.get("idPelanggan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
//			String valNominal = map.get("tagihan").toString();//"Rp 744.750";
			String valNominalBersih = map.get("tagihan_non_pinalty").toString();//"Rp 744.750";
			
			String valTotal = map.get("totalBayar").toString();//"Rp 747.250";
			String total = map.get("total").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			String valRef = map.get("ref").toString();//"SUKSES";
			String valPeriode = map.get("periode").toString();			
			String valTenor = map.get("tenor").toString();
			String valTipeMotor = map.get("tipe_motor").toString();
			String valJatuhTempo = map.get("jatuh_tempo").toString();
			
			namaFile = "BAF_"+valIdTransaksi+"_"+valNoJastel+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN PINJAMAN/KREDIT BUSSAN AUTO FINANCE", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblId = new PdfPCell(new Paragraph("NOMOR KONTRAK"));
	        cellLblId.setBorder(0);
	        PdfPCell cellLblValId = new PdfPCell(new Paragraph(valNoJastel));
	        cellLblValId.setBorder(0);
	        table.addCell(cellLblId);
	        table.addCell(cell2);
	        table.addCell(cellLblValId);
	        
	        PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
	        cellLblNamaPel.setBorder(0);
	        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
	        cellLblValNamaPel.setBorder(0);
	        table.addCell(cellLblNamaPel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaPel);
	        
	        if (!valRef.trim().equals("")) {
	        	PdfPCell cellLblReff = new PdfPCell(new Paragraph("NO. POLISI"));
		        cellLblReff.setBorder(0);
		        PdfPCell cellLblValReff = new PdfPCell(new Paragraph(valRef.toUpperCase()));
		        cellLblValReff.setBorder(0);
		        table.addCell(cellLblReff);
		        table.addCell(cell2);
		        table.addCell(cellLblValReff);
	        }
	        
	        if (!valTipeMotor.trim().equals("")) {
	        	PdfPCell cellLblTipeMotor = new PdfPCell(new Paragraph("TYPE MOTOR"));
		        cellLblTipeMotor.setBorder(0);
		        PdfPCell cellLblValTipeMotor = new PdfPCell(new Paragraph(valTipeMotor.toUpperCase()));
		        cellLblValTipeMotor.setBorder(0);
		        table.addCell(cellLblTipeMotor);
		        table.addCell(cell2);
		        table.addCell(cellLblValTipeMotor);
	        }
	        
	        PdfPCell cellLblTempo = new PdfPCell(new Paragraph("JATUH TEMPO"));
	        cellLblTempo.setBorder(0);
	        PdfPCell cellLblValTempo = new PdfPCell(new Paragraph(valJatuhTempo));
	        cellLblValTempo.setBorder(0);
	        table.addCell(cellLblTempo);
	        table.addCell(cell2);
	        table.addCell(cellLblValTempo);
	        
	        PdfPCell cellLblAngsuran = new PdfPCell(new Paragraph("ANGSURAN KE"));
	        cellLblAngsuran.setBorder(0);
	        PdfPCell cellLblValAngsuran = new PdfPCell(new Paragraph(""+Long.parseLong(valPeriode)));
	        cellLblValAngsuran.setBorder(0);
	        table.addCell(cellLblAngsuran);
	        table.addCell(cell2);
	        table.addCell(cellLblValAngsuran);
	        
		    
		    if (!valTenor.trim().equals("") || !valTenor.trim().equals("0")) {   
		    	PdfPCell cellLblPeriode = new PdfPCell(new Paragraph("TENOR"));
		        cellLblPeriode.setBorder(0);
		        PdfPCell cellLblValPeriode = new PdfPCell(new Paragraph(valTenor+" BULAN"));
		        cellLblValPeriode.setBorder(0);
		        table.addCell(cellLblPeriode);
		        table.addCell(cell2);
		        table.addCell(cellLblValPeriode);
		    }
	        
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("ANGSURAN POKOK"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(func.getFormatRp(valNominalBersih)));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        long biayaTagihan = Long.parseLong(total) - Long.parseLong(valNominalBersih); // val total dalam keadaan format Rp, ganti ke format biasa
	        PdfPCell cellLblBiya = new PdfPCell(new Paragraph("BIAYA TAGIHAN"));
	        cellLblBiya.setBorder(0);
	        PdfPCell cellLblValBiaya = new PdfPCell(new Paragraph(func.getFormatRp(String.valueOf(biayaTagihan))));
	        cellLblValBiaya.setBorder(0);
	        table.addCell(cellLblBiya);
	        table.addCell(cell2);
	        table.addCell(cellLblValBiaya);        
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL TAGIHAN"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wordingTotalTagihan = new Paragraph("TOTAL TAGIHAN BELUM TERMASUK DENDA (JIKA ADA). ");
	        wordingTotalTagihan.setAlignment(Element.ALIGN_CENTER);
	        Paragraph wordingStruk = new Paragraph("HARAP STRUK INI DISIMPAN SEBAGAI PEMBAYARAN YANG SAH.");
	        wordingStruk.setAlignment(Element.ALIGN_CENTER);
	        Paragraph wording = new Paragraph("UNTUK INFORMASI SILAHKAN MENGHUBUNGI KANTOR CABANG BAF TERDEKAT ATAU BAF CARE 500223");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wordingTotalTagihan);
	        doc.add(wordingStruk);
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	}

//	public static void main(String[] args) {
//		try {
//			new TelkomReport().generateReport();
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.error("", e);
//		}
//		
//	}

}
