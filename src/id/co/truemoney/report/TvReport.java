package id.co.truemoney.report;

import java.io.FileOutputStream;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * class untuk generate struk transaksi tv berbayar
 * @author adetiamarhadi
 *
 */
public class TvReport {
	
	private static Logger log = Logger.getLogger(TvReport.class);
	
	private static ResourceBundle bundle = ResourceBundle.getBundle("config.config");
	private String pathImg = bundle.getString("url_path_img_report");
	
	private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	private static Font footerFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
	
	/**
	 * method ini berfungsi untuk generate struk produk kvision
	 * @param path
	 * @param map
	 * @return
	 */
	public String generateReportKvision(String path, Map<String, Object> map) {
		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();
			String valTanggal = map.get("timeStamp").toString();
			String valNoResi = map.get("ref").toString();
			String valNamaProduk = map.get("namaProduk").toString().toUpperCase();
			String valIdPelanggan = map.get("idPelanggan").toString();
			String valNama = map.get("namaPelanggan").toString();
			String valRef = map.get("sn").toString();
			String valNominal = map.get("tagihan").toString();
			String valTotal = map.get("totalBayar").toString();
			String valTerbilang = map.get("terbilang").toString();
			String valStatus = map.get("header").toString();
			
			namaFile = valNamaProduk+"_"+valIdTransaksi+"_"+valIdPelanggan+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBELIAN VOUCHER TV", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNama = new PdfPCell(new Paragraph("NAMA PRODUK"));
	        cellLblNama.setBorder(0);
	        PdfPCell cellLblValNama = new PdfPCell(new Paragraph(valNamaProduk));
	        cellLblValNama.setBorder(0);
	        table.addCell(cellLblNama);
	        table.addCell(cell2);
	        table.addCell(cellLblValNama);
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph("ID PELANGGAN"));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valIdPelanggan));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
	        cellLblNamaPel.setBorder(0);
	        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
	        cellLblValNamaPel.setBorder(0);
	        table.addCell(cellLblNamaPel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaPel);
	        
	        if (valRef.trim().length() != 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("NO. REF TRX"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valRef));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("DENOMINASI"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valNominal));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph("K-VISION MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        Paragraph wording2 = new Paragraph("AKTIVASI PAKET: KETIK KVN<SPASI>PKT#NAMA_PAKET#NO_PELANGGAN");
	        wording2.setAlignment(Element.ALIGN_CENTER);
	        Paragraph wording3 = new Paragraph("KIRIM KE 99333 (T-SEL, INDOSAT, 3). CALL CENTER K-VISION: 1500 828");
	        wording3.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(wording2);
	        doc.add(wording3);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			log.error("", e);
		}
		return namaFile;
	}
	
	/**
	 * method ini berfungsi untuk generate struk produk orange
	 * @param path
	 * @param map
	 * @return
	 */
	public String generateReportOrange(String path, Map<String, Object> map) {
		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();
			String valTanggal = map.get("timeStamp").toString();
			String valNoResi = map.get("ref").toString();
			String valNamaProduk = map.get("namaProduk").toString().toUpperCase();
			String valIdPelanggan = map.get("idPelanggan").toString();
			String valNama = map.get("namaPelanggan").toString();
			String valRef = map.get("sn").toString();
			String valNominal = map.get("tagihan").toString();
			String valAdminn = map.get("biayaAdmin").toString();
			String valTotal = map.get("totalBayar").toString();
			String valTerbilang = map.get("terbilang").toString();
			String valStatus = map.get("header").toString();
			
			namaFile = "ORANGE_"+valIdTransaksi+"_"+valIdPelanggan+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBELIAN VOUCHER TV", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNama = new PdfPCell(new Paragraph("NAMA PRODUK"));
	        cellLblNama.setBorder(0);
	        PdfPCell cellLblValNama = new PdfPCell(new Paragraph(valNamaProduk));
	        cellLblValNama.setBorder(0);
	        table.addCell(cellLblNama);
	        table.addCell(cell2);
	        table.addCell(cellLblValNama);
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph("ID PELANGGAN"));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valIdPelanggan));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        if (valNama.trim().length() != 0) {
	        	PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
		        cellLblNamaPel.setBorder(0);
		        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
		        cellLblValNamaPel.setBorder(0);
		        table.addCell(cellLblNamaPel);
		        table.addCell(cell2);
		        table.addCell(cellLblValNamaPel);
	        }
	        
	        PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("NO. REF"));
        	cellLblNpwp.setBorder(0);
	        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valRef));
	        cellLblValNpwp.setBorder(0);
	        table.addCell(cellLblNpwp);
	        table.addCell(cell2);
	        table.addCell(cellLblValNpwp);
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("DENOMINASI"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valNominal));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        if (Integer.parseInt(valAdminn) > 0) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("BIAYA ADMIN"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(valAdminn));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph(valNamaProduk + " MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			log.error("", e);
		}
		return namaFile;
	}
	
	/**
	 * method ini berfungsi untuk generate struk produk sky
	 * @param path
	 * @param map
	 * @return
	 */
	public String generateReportSky(String path, Map<String, Object> map) {
		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();
			String valTanggal = map.get("timeStamp").toString();
			String valNoResi = map.get("ref").toString();
			String valNamaProduk = map.get("namaProduk").toString().toUpperCase();
			String valIdPelanggan = map.get("idPelanggan").toString();
			String valRef = map.get("sn").toString();
			String valTotal = map.get("totalBayar").toString();
			String valTerbilang = map.get("terbilang").toString();
			String valStatus = map.get("header").toString();
			
			String valPaket = map.get("paket").toString();
			String valTglTempo = map.get("tgl_tempo").toString();
			
			namaFile = "SKYNINDO_"+valIdTransaksi+"_"+valIdPelanggan+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBELIAN VOUCHER TV", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNoRef = new PdfPCell(new Paragraph("NO. REF"));
	        cellLblNoRef.setBorder(0);
	        PdfPCell cellLblValNoRef = new PdfPCell(new Paragraph(valRef));
	        cellLblValNoRef.setBorder(0);
	        table.addCell(cellLblNoRef);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoRef);
	        
	        PdfPCell cellLblNama = new PdfPCell(new Paragraph("NAMA PRODUK"));
	        cellLblNama.setBorder(0);
	        PdfPCell cellLblValNama = new PdfPCell(new Paragraph(valNamaProduk));
	        cellLblValNama.setBorder(0);
	        table.addCell(cellLblNama);
	        table.addCell(cell2);
	        table.addCell(cellLblValNama);
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph("ID PELANGGAN"));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valIdPelanggan));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("PAKET"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valPaket));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        PdfPCell cellLblTempo = new PdfPCell(new Paragraph("JATUH TEMPO"));
	        cellLblTempo.setBorder(0);
	        PdfPCell cellLblValTempo = new PdfPCell(new Paragraph(valTglTempo));
	        cellLblValTempo.setBorder(0);
	        table.addCell(cellLblTempo);
	        table.addCell(cell2);
	        table.addCell(cellLblValTempo);
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph("SIMPAN STRUK INI SEBAGAI BUKTI TRANSAKSI");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        Paragraph wording2 = new Paragraph("UNTUK PERUBAHAN PAKET SKYNINDO :021�-29678222");
	        wording2.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(wording2);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			log.error("", e);
		}
		return namaFile;
	}
	
	/**
	 * method ini berfungsi untuk generate struk produk aora tv
	 * @param path
	 * @param map
	 * @return
	 */
	public String generateReportAora(String path, Map<String, Object> map) {
		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();
			String valTanggal = map.get("timeStamp").toString();
			String valNoResi = map.get("ref").toString();
			String valIdPelanggan = map.get("idPelanggan").toString();
			String valNama = map.get("namaPelanggan").toString();
			String valRef = map.get("sn").toString();
			String valNominal = map.get("tagihan").toString();
			String valAdminn = map.get("biayaAdmin").toString();
			String valTotal = map.get("totalBayar").toString();
			String valTerbilang = map.get("terbilang").toString();
			String valStatus = map.get("header").toString();
			String valNoSoa = map.get("no_soa").toString();
			String valTglSoa = map.get("tgl_soa").toString();
			String valTglSiklus = map.get("tgl_siklus").toString();
			String valTglTempo = map.get("tgl_tempo").toString();
			
			namaFile = "AORATV_"+valIdTransaksi+"_"+valIdPelanggan+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN AORA TV", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph("ID PELANGGAN"));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valIdPelanggan));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        if (valNama.trim().length() != 0) {
	        	PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
		        cellLblNamaPel.setBorder(0);
		        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
		        cellLblValNamaPel.setBorder(0);
		        table.addCell(cellLblNamaPel);
		        table.addCell(cell2);
		        table.addCell(cellLblValNamaPel);
	        }
	        
	        PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("REF"));
        	cellLblNpwp.setBorder(0);
	        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valRef));
	        cellLblValNpwp.setBorder(0);
	        table.addCell(cellLblNpwp);
	        table.addCell(cell2);
	        table.addCell(cellLblValNpwp);
	        
	        PdfPCell cellLblNoSoa = new PdfPCell(new Paragraph("NOMOR SOA"));
	        cellLblNoSoa.setBorder(0);
	        PdfPCell cellLblValNoSoa = new PdfPCell(new Paragraph(valNoSoa));
	        cellLblValNoSoa.setBorder(0);
	        table.addCell(cellLblNoSoa);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoSoa);
	        
	        PdfPCell cellLblTglSoa = new PdfPCell(new Paragraph("TGL. SOA"));
	        cellLblTglSoa.setBorder(0);
	        PdfPCell cellLblValTglSoa = new PdfPCell(new Paragraph(valTglSoa));
	        cellLblValTglSoa.setBorder(0);
	        table.addCell(cellLblTglSoa);
	        table.addCell(cell2);
	        table.addCell(cellLblValTglSoa);
	        
	        PdfPCell cellLblTglSiklus = new PdfPCell(new Paragraph("TGL. SIKLUS"));
	        cellLblTglSiklus.setBorder(0);
	        PdfPCell cellLblValTglSiklus = new PdfPCell(new Paragraph(valTglSiklus));
	        cellLblValTglSiklus.setBorder(0);
	        table.addCell(cellLblTglSiklus);
	        table.addCell(cell2);
	        table.addCell(cellLblValTglSiklus);
	        
	        PdfPCell cellLblTglTempo = new PdfPCell(new Paragraph("JATUH TEMPO"));
	        cellLblTglTempo.setBorder(0);
	        PdfPCell cellLblValTglTempo = new PdfPCell(new Paragraph(valTglTempo));
	        cellLblValTglTempo.setBorder(0);
	        table.addCell(cellLblTglTempo);
	        table.addCell(cell2);
	        table.addCell(cellLblValTglTempo);
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("TAGIHAN"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valNominal));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        if (Integer.parseInt(valAdminn) > 0) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("ADMIN"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(valAdminn));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph("TERIMA KASIH TELAH MELAKUKAN PEMBAYARAN AORA TV BERLANGGANAN");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        Paragraph wording2 = new Paragraph("AORA TV MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording2.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(wording2);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			log.error("", e);
		}
		return namaFile;
	}
	
	/**
	 * method ini berfungsi untuk generate struk produk nex tv
	 * @param path
	 * @param map
	 * @return
	 */
	public String generateReportNex(String path, Map<String, Object> map) {
		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();
			String valTanggal = map.get("timeStamp").toString();
			String valNoResi = map.get("ref").toString();
			String valIdPelanggan = map.get("idPelanggan").toString();
			String valNama = map.get("namaPelanggan").toString();
			String valRef = map.get("sn").toString();
			String valNominal = map.get("tagihan").toString();
			String valAdminn = map.get("biayaAdmin").toString();
			String valTotal = map.get("totalBayar").toString();
			String valTerbilang = map.get("terbilang").toString();
			String valStatus = map.get("header").toString();
			
			String valPaket = map.get("paket").toString();
			String valTglTempo = map.get("tgl_tempo").toString();
			
			namaFile = "NEXMEDIA_"+valIdTransaksi+"_"+valIdPelanggan+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN NEX MEDIA", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph("ID PELANGGAN"));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valIdPelanggan));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        if (valNama.trim().length() != 0) {
	        	PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
		        cellLblNamaPel.setBorder(0);
		        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
		        cellLblValNamaPel.setBorder(0);
		        table.addCell(cellLblNamaPel);
		        table.addCell(cell2);
		        table.addCell(cellLblValNamaPel);
	        }
	        
	        PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("REF"));
        	cellLblNpwp.setBorder(0);
	        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valRef));
	        cellLblValNpwp.setBorder(0);
	        table.addCell(cellLblNpwp);
	        table.addCell(cell2);
	        table.addCell(cellLblValNpwp);
	        
	        PdfPCell cellLblNoSoa = new PdfPCell(new Paragraph("PAKET"));
	        cellLblNoSoa.setBorder(0);
	        PdfPCell cellLblValNoSoa = new PdfPCell(new Paragraph(valPaket));
	        cellLblValNoSoa.setBorder(0);
	        table.addCell(cellLblNoSoa);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoSoa);
	        
	        PdfPCell cellLblTglTempo = new PdfPCell(new Paragraph("JATUH TEMPO"));
	        cellLblTglTempo.setBorder(0);
	        PdfPCell cellLblValTglTempo = new PdfPCell(new Paragraph(valTglTempo));
	        cellLblValTglTempo.setBorder(0);
	        table.addCell(cellLblTglTempo);
	        table.addCell(cell2);
	        table.addCell(cellLblValTglTempo);
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("TAGIHAN"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valNominal));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        if (Integer.parseInt(valAdminn) > 0) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("ADMIN"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(valAdminn));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph("CALL CENTER NEXMEDIA 021-29352600");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        Paragraph wording2 = new Paragraph("NEX MEDIA MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording2.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(wording2);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			log.error("", e);
		}
		return namaFile;
	}
	
	/**
	 * method ini berfungsi untuk generate struk produk big dan indovision tv
	 * @param path
	 * @param map
	 * @return
	 */
	public String generateReportBigIndovision(String path, Map<String, Object> map) {
		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();
			String valTanggal = map.get("timeStamp").toString();
			String valNoResi = map.get("ref").toString();
			String valIdPelanggan = map.get("idPelanggan").toString();
			String valNamaProduk = map.get("namaProduk").toString().toUpperCase();
			String valNama = map.get("namaPelanggan").toString();
			String valNominal = map.get("tagihan").toString();
			String valAdminn = map.get("biayaAdmin").toString();
			String valTotal = map.get("totalBayar").toString();
			String valTerbilang = map.get("terbilang").toString();
			String valStatus = map.get("header").toString();
			String valPeriode = map.get("blnthn").toString();
			
			namaFile = valNamaProduk+"_"+valIdTransaksi+"_"+valIdPelanggan+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        if (valNamaProduk.contains("BIG")) {
	        	valNamaProduk = "TV BIG";
	        	PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA PRODUK"));
		        cellLblNamaPel.setBorder(0);
		        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNamaProduk));
		        cellLblValNamaPel.setBorder(0);
		        table.addCell(cellLblNamaPel);
		        table.addCell(cell2);
		        table.addCell(cellLblValNamaPel);
	        } else {
	        	valNamaProduk = "INDOVISION TV";
	        }
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph("ID PELANGGAN"));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valIdPelanggan));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        if (valNama.trim().length() != 0) {
	        	PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
		        cellLblNamaPel.setBorder(0);
		        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
		        cellLblValNamaPel.setBorder(0);
		        table.addCell(cellLblNamaPel);
		        table.addCell(cell2);
		        table.addCell(cellLblValNamaPel);
	        }
	        
	        if (valNama.trim().length() != 0) {
	        	PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
		        cellLblNamaPel.setBorder(0);
		        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
		        cellLblValNamaPel.setBorder(0);
		        table.addCell(cellLblNamaPel);
		        table.addCell(cell2);
		        table.addCell(cellLblValNamaPel);
	        }
	        
	        if (valPeriode.trim().length() != 0) {
	        	PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("PERIODE BAYAR"));
		        cellLblNamaPel.setBorder(0);
		        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valPeriode));
		        cellLblValNamaPel.setBorder(0);
		        table.addCell(cellLblNamaPel);
		        table.addCell(cell2);
		        table.addCell(cellLblValNamaPel);
	        }
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("TAGIHAN"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valNominal));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        if (Integer.parseInt(valAdminn) > 0) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("BIAYA ADMIN"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(valAdminn));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph("TERIMA KASIH TELAH MELAKUKAN PEMBAYARAN "+valNamaProduk+" BERLANGGANAN");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        Paragraph wording2 = new Paragraph(valNamaProduk+" MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording2.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(wording2);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			log.error("", e);
		}
		return namaFile;
	}
	
	/**
	 * method ini berfungsi untuk generate struk produk big dan indovision tv
	 * @param path
	 * @param map
	 * @return
	 */
	public String generateReportTopas(String path, Map<String, Object> map) {
		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();
			String valTanggal = map.get("timeStamp").toString();
			String valNoResi = map.get("ref").toString();
			String valRef = map.get("sn").toString();
			String valIdPelanggan = map.get("idPelanggan").toString();
			String valNama = map.get("namaPelanggan").toString();
			String valNominal = map.get("tagihan").toString();
			String valAdminn = map.get("biayaAdmin").toString();
			String valTotal = map.get("totalBayar").toString();
			String valTerbilang = map.get("terbilang").toString();
			String valStatus = map.get("header").toString();
			String valPeriode = map.get("blnthn").toString();
			
			namaFile = "TOPASTV_"+valIdTransaksi+"_"+valIdPelanggan+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN TOPAS TV", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph("ID PELANGGAN"));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valIdPelanggan));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        if (valNama.trim().length() != 0) {
	        	PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
		        cellLblNamaPel.setBorder(0);
		        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
		        cellLblValNamaPel.setBorder(0);
		        table.addCell(cellLblNamaPel);
		        table.addCell(cell2);
		        table.addCell(cellLblValNamaPel);
	        }
	        
	        PdfPCell cellLblRef = new PdfPCell(new Paragraph("REF"));
	        cellLblRef.setBorder(0);
	        PdfPCell cellLblValRef = new PdfPCell(new Paragraph(valRef));
	        cellLblValRef.setBorder(0);
	        table.addCell(cellLblRef);
	        table.addCell(cell2);
	        table.addCell(cellLblValRef);
	        
	        PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("PERIODE BAYAR"));
	        cellLblNamaPel.setBorder(0);
	        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valPeriode));
	        cellLblValNamaPel.setBorder(0);
	        table.addCell(cellLblNamaPel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaPel);
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("TAGIHAN"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valNominal));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        if (Integer.parseInt(valAdminn) > 0) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("BIAYA ADMIN"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(valAdminn));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph("CALL CENTER TOPAS TV 021-29396363 / 08041292929");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        Paragraph wording2 = new Paragraph("TOPAS TV MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording2.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(wording2);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			log.error("", e);
		}
		return namaFile;
	}
	
	/**
	 * method ini berfungsi untuk generate struk produk innovate tv
	 * @param path
	 * @param map
	 * @return
	 */
	public String generateReportInnovate(String path, Map<String, Object> map) {
		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();
			String valTanggal = map.get("timeStamp").toString();
			String valNoResi = map.get("ref").toString();
			String valRef = map.get("sn").toString();
			String valIdPelanggan = map.get("idPelanggan").toString();
			String valNamaProduk = map.get("namaProduk").toString().toUpperCase();
			String valNama = map.get("namaPelanggan").toString();
			String valNominal = map.get("tagihan").toString();
			String valTotal = map.get("totalBayar").toString();
			String valTerbilang = map.get("terbilang").toString();
			String valStatus = map.get("header").toString();
			
			namaFile = "INNOVATE_"+valIdTransaksi+"_"+valIdPelanggan+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph("ID PELANGGAN"));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valIdPelanggan));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        PdfPCell cellLblRef = new PdfPCell(new Paragraph("REF"));
	        cellLblRef.setBorder(0);
	        PdfPCell cellLblValRef = new PdfPCell(new Paragraph(valRef));
	        cellLblValRef.setBorder(0);
	        table.addCell(cellLblRef);
	        table.addCell(cell2);
	        table.addCell(cellLblValRef);
	        
	        PdfPCell cellLblNamaProduk = new PdfPCell(new Paragraph("NAMA PRODUK"));
	        cellLblNamaProduk.setBorder(0);
	        PdfPCell cellLblValNamaProduk = new PdfPCell(new Paragraph(valNamaProduk));
	        cellLblValNamaProduk.setBorder(0);
	        table.addCell(cellLblNamaProduk);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaProduk);
	        
	        PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
	        cellLblNamaPel.setBorder(0);
	        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
	        cellLblValNamaPel.setBorder(0);
	        table.addCell(cellLblNamaPel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaPel);
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("TAGIHAN"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valNominal));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL BAYAR"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph("TERIMA KASIH TELAH MELAKUKAN PEMBAYARAN TV INNOVATE BERLANGGANAN");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        Paragraph wording2 = new Paragraph("TV INNOVATE MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording2.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(wording2);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			log.error("", e);
		}
		return namaFile;
	}

}
