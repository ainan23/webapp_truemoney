package id.co.truemoney.report;

import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PulsaPascaReport {
	
	private static Logger log = Logger.getLogger(PulsaPascaReport.class);
	
	private static ResourceBundle bundle = ResourceBundle.getBundle("config.config");
	private String pathImg = bundle.getString("url_path_img_report");
	
	private static Font titleFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	private static Font footerFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
	
	public String generateReport(String path, HashMap<String, Object> map) {
		String namaFile = "";
		try {
			
			String valIdTransaksi = map.get("idTransaksi").toString();//"2016271291281928123";
			String valTanggal = map.get("timeStamp").toString();//"24-05-2012 11:20:49";
			String valNoResi = map.get("ref").toString();//"18142680";
			String valOperator = map.get("namaProduk").toString().toUpperCase();//"18142680";
			String valNoJastel = map.get("idPelanggan").toString();//"0162406900527";
			String valNama = map.get("namaPelanggan").toString();//"GEREJA GBI NANGA BUL";
//			String valJmlBulan = map.get("periode").toString();//"1";
			String valRef = map.get("sn").toString();//"205A";
//			String valPeriode = map.get("blnthn").toString();//"MEI12";
			String valNominal = map.get("tagihan").toString();//"Rp 744.750";
			String valAdminn = map.get("biayaAdmin").toString();//"Rp 2.500";
			String valTotal = map.get("totalBayar").toString();//"Rp 747.250";
			String valTerbilang = map.get("terbilang").toString();//"TUJUH RATUS EMPAT PULUH TUJUH RIBU DUA RATUS LIMA PULUH RUPIAH";
			String valStatus = map.get("header").toString();//"SUKSES";
			
			namaFile = valOperator+"_"+valIdTransaksi+"_"+valNoJastel+".pdf";
			
			Document doc = new Document(PageSize.A4, 50, 50, 50, 50);
			
			PdfWriter pdfWriter = PdfWriter.getInstance(doc, new FileOutputStream(path+namaFile));
			
			doc.open();
			
			Image image1 = Image.getInstance(pathImg+"Letterhead highres-10.png");
			
			Image image2 = Image.getInstance(pathImg+"Logo-TrueMoney-Master-Medium.png");
			
			Image image3 = Image.getInstance(pathImg+"Letterhead-11.png");
			
			PdfPTable table0 = new PdfPTable(1); // 3 columns.
			table0.setWidthPercentage(100); //Width 100%
	        float[] columnWidths0 = {1f};
	        table0.setWidths(columnWidths0);
	        PdfPCell cellImg0 = new PdfPCell(image1, true);
	        cellImg0.setBorder(0);
	        table0.addCell(cellImg0);
			
			Paragraph title = new Paragraph("STRUK PEMBAYARAN TELEPON PASCABAYAR", titleFont);
			title.setAlignment(Element.ALIGN_CENTER);
			
			Paragraph footer = new Paragraph("Semua Agent Truemoney menerima pembayaran PLN, Telkom, Speedy, dan juga jual Pulsa All Operator, Voucher Game dan Token PLN", footerFont);
			footer.setAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(3); // 3 columns.
	        table.setWidthPercentage(100); //Width 100%
	        table.setSpacingBefore(10f); //Space before table
	        table.setSpacingAfter(10f); //Space after table
	        float[] columnWidths = {0.8f, 0.1f, 2.1f};
	        table.setWidths(columnWidths);
	        
	        PdfPCell cell0 = new PdfPCell(new Paragraph("ID TRANSAKSI"));
	        cell0.setBorder(0);
	        PdfPCell cell2 = new PdfPCell(new Paragraph(":"));
	        cell2.setBorder(0);
	        PdfPCell cell03 = new PdfPCell(new Paragraph(valIdTransaksi));
	        cell03.setBorder(0);
	        table.addCell(cell0);
	        table.addCell(cell2);
	        table.addCell(cell03);
	        
	        PdfPCell cell1 = new PdfPCell(new Paragraph("TANGGAL"));
	        cell1.setBorder(0);
	        PdfPCell cell3 = new PdfPCell(new Paragraph(valTanggal));
	        cell3.setBorder(0);
	        table.addCell(cell1);
	        table.addCell(cell2);
	        table.addCell(cell3);
	        
	        PdfPCell cellLblNoResi = new PdfPCell(new Paragraph("NO. RESI"));
	        cellLblNoResi.setBorder(0);
	        PdfPCell cellLblValNoResi = new PdfPCell(new Paragraph(valNoResi));
	        cellLblValNoResi.setBorder(0);
	        table.addCell(cellLblNoResi);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoResi);
	        
	        PdfPCell cellLblNama = new PdfPCell(new Paragraph("NAMA OPERATOR"));
	        cellLblNama.setBorder(0);
	        PdfPCell cellLblValNama = new PdfPCell(new Paragraph(valOperator));
	        cellLblValNama.setBorder(0);
	        table.addCell(cellLblNama);
	        table.addCell(cell2);
	        table.addCell(cellLblValNama);
	        
	        PdfPCell cellLblNoJastel = new PdfPCell(new Paragraph("NOMOR TELEPON"));
	        cellLblNoJastel.setBorder(0);
	        PdfPCell cellLblValNoJastel = new PdfPCell(new Paragraph(valNoJastel));
	        cellLblValNoJastel.setBorder(0);
	        table.addCell(cellLblNoJastel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNoJastel);
	        
	        PdfPCell cellLblNamaPel = new PdfPCell(new Paragraph("NAMA"));
	        cellLblNamaPel.setBorder(0);
	        PdfPCell cellLblValNamaPel = new PdfPCell(new Paragraph(valNama));
	        cellLblValNamaPel.setBorder(0);
	        table.addCell(cellLblNamaPel);
	        table.addCell(cell2);
	        table.addCell(cellLblValNamaPel);
	        
	        if (valRef.trim().length() != 0) {
	        	PdfPCell cellLblNpwp = new PdfPCell(new Paragraph("NO. REF"));
	        	cellLblNpwp.setBorder(0);
		        PdfPCell cellLblValNpwp = new PdfPCell(new Paragraph(valRef));
		        cellLblValNpwp.setBorder(0);
		        table.addCell(cellLblNpwp);
		        table.addCell(cell2);
		        table.addCell(cellLblValNpwp);
	        }
	        
	        PdfPCell cellLblNominal = new PdfPCell(new Paragraph("JUMLAH TAGIHAN"));
	        cellLblNominal.setBorder(0);
	        PdfPCell cellLblValNominal = new PdfPCell(new Paragraph(valNominal));
	        cellLblValNominal.setBorder(0);
	        table.addCell(cellLblNominal);
	        table.addCell(cell2);
	        table.addCell(cellLblValNominal);
	        
	        if (Integer.parseInt(valAdminn) > 0) {
	        	PdfPCell cellLblAdmin = new PdfPCell(new Paragraph("ADMIN"));
		        cellLblAdmin.setBorder(0);
		        PdfPCell cellLblValAdmin = new PdfPCell(new Paragraph(valAdminn));
		        cellLblValAdmin.setBorder(0);
		        table.addCell(cellLblAdmin);
		        table.addCell(cell2);
		        table.addCell(cellLblValAdmin);
	        }
	        
	        PdfPCell cellLblTotal = new PdfPCell(new Paragraph("TOTAL TAGIHAN"));
	        cellLblTotal.setBorder(0);
	        PdfPCell cellLblValTotal = new PdfPCell(new Paragraph(valTotal));
	        cellLblValTotal.setBorder(0);
	        table.addCell(cellLblTotal);
	        table.addCell(cell2);
	        table.addCell(cellLblValTotal);
	        
	        PdfPCell cellLblTerbilang = new PdfPCell(new Paragraph("TERBILANG"));
	        cellLblTerbilang.setBorder(0);
	        PdfPCell cellLblValTerbilang = new PdfPCell(new Paragraph(valTerbilang));
	        cellLblValTerbilang.setBorder(0);
	        table.addCell(cellLblTerbilang);
	        table.addCell(cell2);
	        table.addCell(cellLblValTerbilang);
	        
	        PdfPCell cellLblStatus = new PdfPCell(new Paragraph("STATUS"));
	        cellLblStatus.setBorder(0);
	        PdfPCell cellLblValStatus = new PdfPCell(new Paragraph(valStatus));
	        cellLblValStatus.setBorder(0);
	        table.addCell(cellLblStatus);
	        table.addCell(cell2);
	        table.addCell(cellLblValStatus);
	        
	        Paragraph wording = new Paragraph(valOperator+" MENYATAKAN STRUK INI SEBAGAI BUKTI PEMBAYARAN YANG SAH");
	        wording.setAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable table2 = new PdfPTable(2); // 3 columns.
	        table2.setWidthPercentage(100); //Width 100%
	        float[] columnWidths2 = {0.8f, 1.2f};
	        table2.setWidths(columnWidths2);
	        PdfPCell cellImg1 = new PdfPCell(image2, true);
	        cellImg1.setBorder(0);
	        PdfPCell cellImg2 = new PdfPCell(image3, true);
	        cellImg2.setBorder(0);
	        table2.addCell(cellImg1);
	        table2.addCell(cellImg2);
	        
	        doc.add(table0);
	        doc.add(title);
	        doc.add(new Paragraph(" "));
	        doc.add(table);
	        doc.add(new Paragraph(" "));
	        doc.add(wording);
	        doc.add(new Paragraph(" "));
	        doc.add(footer);
	        doc.add(new Paragraph(" "));
	        doc.add(new Paragraph(" "));
	        doc.add(table2);
	        
			doc.close();
			
			pdfWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("", e);
		}
		return namaFile;
	}
	
//	public static void main(String[] args) {
//		try {
//			new TelkomReport().generateReport();
//		} catch (Exception e) {
//			e.printStackTrace();
//			log.error("", e);
//		}
//		
//	}

}
