$(document).ready(function(){
    var slide_header=$('.header.slider_full');
    console.log($('.header.slider_full .content').length);
    $('header .i-nav').hide();
 if ($('.header.slider_full .content').length > 2 ) {
     $('header .i-nav').show();
     slide_header.owlCarousel({
                    stagepadding:20,
                    loop:true,
                    nav:false,
                    dots:true,
                    autoplay:true,
                    autoplaySpeed:4500,
                    autoplayTimeout:3500,
                    autoplayHoverPause:true,
                     animateOut: 'slideOutRight',
                    animateIn: 'slideInLeft',
                    items:1

    });
    slide_header.on('translate.owl.carousel',function(){
        var window   =$( document ).width();
        if (window >= 1024){
            $('.header.slider_full .content .brand .button p').hide();
        }
        else{

            $('.header.slider_full .content .brand .button p').css({
                    visibility : 'hidden',
            });
            $('.header.slider_full .content .brand .button p').removeClass('fadeInRight');
        }
        $('.header.slider_full .content .brand h3').hide();


    });
    slide_header.on('translated.owl.carousel',function(property){

        var current = property.item.index,
            window   =$( document ).width();
            if (window >= 1024){
                $('.owl-item').eq(current).find('.brand h3').show();
            }
            else{
                $('.header.slider_full .content .brand .button p').css({
                    visibility : 'visible',
                });
            }
            $('.owl-item').eq(current).find('.brand .button p').show();
            $('.header.slider_full .content .brand .button p').addClass('fadeInRight');

    });
    $('header .nav-right').click(function() {
          slide_header.trigger('next.owl.carousel');
       })
     $('header .nav-left').click(function() {
          slide_header.trigger('prev.owl.carousel');
       })
    }
             $(".overlay-logo").on("click",function(){
                 $(".nav-bar-true-a.navbar-default").toggleClass("active");
                 $(".box-menu").toggleClass("active");
                 $(".navbar-header.mobile").toggleClass("active");
             });
             
             
             $(function(){
                $.w = $(window);
                $.w.on('resize', res);
                res();
            });

            function res() {
                if($.w.width()<1440){
                    $('#box_header').css('height',($.w.width()*6.2/16)+'px');
                }
                if($.w.width()<=768){
                    $('#box_header').css('height',($.w.width()*8.5/16)+'px');
                }
            }
    
});
