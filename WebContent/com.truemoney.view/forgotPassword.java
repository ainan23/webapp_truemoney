package com.truemoneywitami.menu;

import org.zkoss.json.JSONObject;
import org.zkoss.zhtml.Font;
import org.zkoss.zhtml.H4;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.truemoneywitami.function.ProcessingListener;
import com.truemoneywitami.function.RedirectMenu;
import com.truemoneywitami.function.RequestListener;
import com.truemoneywitami.function.ZKFunction;
import com.truemoneywitami.function.session.mob_Session;

public class forgotPassword extends Window implements RequestListener {
	ZKFunction func = new ZKFunction();
	
	public void getPw(Window wnd) {
		try {
			String i_email = func.getTextboxValue(wnd, "inputEmail");
			String i_nomorHP = func.getTextboxValue(wnd, "nomorhp");
			String i_tglLahir = func.getDateboxText(wnd, "datetimepicker1");
			String i_tipeUser = func.getTextboxValue(wnd, "tipeUser");
			i_tipeUser = i_tipeUser.toLowerCase();			
			
			JSONObject obj = new JSONObject();
			obj.put("Tipe", "ForgotPassword");
			obj.put("tipeUser", i_tipeUser);
			obj.put("email", i_email);
			obj.put("noHp", i_nomorHP);
			obj.put("tglLahir", i_tglLahir);
			ProcessingListener processing = new ProcessingListener(this, obj.toString());
			processing.processData();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
			e.printStackTrace();
		}
	}

	
	public void requestSucceeded(JSONObject data) {
		String pesan = (String) data.get("pesan");
		Sessions.getCurrent().setAttribute("pesan", pesan);
		
		Messagebox.show(pesan, "Reset Password Berhasil", Messagebox.OK, Messagebox.INFORMATION, new org.zkoss.zk.ui.event.EventListener() {
		    public void onEvent(Event evt) throws InterruptedException {
		        if (evt.getName().equals("onOK")) {
		        	Executions.sendRedirect("/login.zul");
		        } 
		    }
		});
	}

	@Override
	public void requestFailed(String message) {
		Clients.showNotification(message, "info", null, "middle_center", 4000);
	}
}
